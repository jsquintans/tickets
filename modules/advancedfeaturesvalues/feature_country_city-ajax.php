<?php 
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

if(Tools::getValue('valor_feaute') == 7 || Tools::getValue('valor_feaute') == 8 ){

	if(Tools::getValue('valor_feaute') == 7){
		$sql = 'SELECT fv.`id_feature_value` as `id_feature_value`, fvl.`value` as `name`, fvi.`url_name` as `url_name`, fv.`id_feature` as `id_feature`  FROM '._DB_PREFIX_.'feature_value fv
				LEFT JOIN '._DB_PREFIX_.'feature_value_lang  fvl ON fv.id_feature_value = fvl.id_feature_value AND fvl.id_lang = 1
				LEFT JOIN '._DB_PREFIX_.'layered_indexable_feature_value_lang_value  as fvi ON fvi.id_feature_value = fv.id_feature_value AND fvi.id_lang = 3
				WHERE fv.id_feature = 7';

	}elseif(Tools::getValue('valor_feaute') == 8){
		$sql = 'SELECT fv.`id_feature_value` as `id_feature_value`, fvl.`value` as `name`, fvi.`url_name` as `url_name`, fv.`id_feature` as `id_feature` FROM '._DB_PREFIX_.'feature_value fv
				LEFT JOIN '._DB_PREFIX_.'feature_value_lang fvl ON fv.id_feature_value = fvl.id_feature_value  AND fvl.id_lang = 1
				LEFT JOIN '._DB_PREFIX_.'layered_indexable_feature_value_lang_value  as fvi ON fvi.id_feature_value = fv.id_feature_value AND fvi.id_lang = 3
				WHERE fv.id_feature = 8';
	}


	if(Tools::getValue('valor_feaute') == 8 || Tools::getValue('valor_feaute') == 7 ){
		$results = Db::getInstance()->ExecuteS($sql);
		//print_r($results);
	}


/*    foreach ($results as $row)
        echo $row['id_shop'].' :: '.$row['name'].'<br />';
*/


//print_r($result)


 die( Tools::jsonEncode($results));

}


if(Tools::getValue('sincronizar_ciudades') == 'yes'){

	$id_feature_value = Tools::getValue('id_feature_update');

	//Lo que tenemos que hacer es actualizar las ciudades que corresponden al país seleccionado 
	//
	// Selecionamos todos los productos que tienen el id_feature == al seleccionado 
	//  De esos productos cogemos los valores agrupados de las ciudades //
	//  
	
	$sql = 'SELECT p.`id_product` FROM  '._DB_PREFIX_.'product p 
			LEFT JOIN '._DB_PREFIX_.'feature_product fp ON p.`id_product` = fp.`id_product`
			WHERE fp.`id_feature_value` = '.$id_feature_value.';';

	$ids = Db::getInstance()->ExecuteS($sql);

	if(count($ids) > 0){
	$in =  implode(',', array_column($ids,'id_product'));
	$select = "SELECT *
		FROM "._DB_PREFIX_."feature_product fp
		LEFT JOIN "._DB_PREFIX_."feature_value_lang fl ON (fp.`id_feature_value` = fl.`id_feature_value` AND fl.`id_lang` = 3)
		WHERE fp.`id_product` IN ($in) AND fp.`id_feature`= 8 GROUP BY fl.`value`";
	$results = Db::getInstance()->ExecuteS($select);

	//print_r($results);
	

	die( Tools::jsonEncode($results));
	}
	else{
		die( Tools::jsonEncode(array()));		
	}

}


?>