<div class="ovicnewsletter leka-popup" {if isset($newsletter_setting.background) && $newsletter_setting.background != ''} style="background-image: url({$newsletter_setting.background});" {/if}>
    <div class="block block-subscribe">
    	<div class="block-content">
            <div class="popup-title">
			{if isset($newsletter_setting.content) && $newsletter_setting.content != ''}
				{$newsletter_setting.content}
			{else}
                <h3>{l s='NEWSLETTER' mod='ovicnewsletter'}</h3>
                <p class="notice">{l s='Sign up to our email newsletter to be the first to hear about great offers & more' mod='ovicnewsletter'}</p>
			{/if}
            </div>
	        <div class="input-box form-subscribe"> 
				<input type="text" name="email" id="input-email" title="{l s='Sign up for our newsletter' mod='ovicnewsletter'}" required="required" class="input-text required-entry validate-email" placeholder="{l s='Enter your email...' mod='ovicnewsletter'}">
                <button type="button" onclick="regisNewsletter()" name="submitNewsletter" title="{l s='Sign in' mod='ovicnewsletter'}" class="button button_111 button_second_111_hover"><span><span>{l s='Sign in' mod='ovicnewsletter'}</span></span></button>
	        </div>
    	</div>
    	<div id="regisNewsletterMessage"></div>
        <div class="subscribe-bottom">
	        <input class="not_uniform" data-no-uniform="true" type="checkbox" id="persistent" name="persistent" value="1" />
	        <label class="label-persistent" for="persistent">{l s='Don\'t show this popup again' mod='ovicnewsletter'}</label>
	    </div>
    </div>
</div>
{strip}
{addJsDefL name=regisNewsletterMessage}{l s='You have just subscribled successfully!' js=1}{/addJsDefL}
{addJsDefL name=enterEmail}{l s='Enter your email please!' js=1}{/addJsDefL}
{/strip}
<script type="text/javascript">
var ovicNewsletterUrl = "{$ovicNewsletterUrl}";
</script>