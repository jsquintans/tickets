DROP TABLE IF EXISTS `PREFIX_ovic_register_newsletter`;
CREATE TABLE `PREFIX_ovic_register_newsletter` (
  `id_shop` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `background` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `width` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id_shop`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `PREFIX_ovic_register_newsletter` (`id_shop`, `id_lang`, `content`, `background`, `width`) VALUES
(1,	1,	'<h2>NEWSLETTER</h2>\r\n<p>Subscribe to the Logancee mailing list to receiveupdates on new arrivals, special offers andother discount information</p>',	'background-1-1.jpg',	0),
(1,	2,	'',	'',	0),
(2,	1,	'<h2>NEWSLETTER</h2>\r\n<p>Subscribe to the Logancee mailing list to receiveupdates on new arrivals, special offers andother discount information</p>',	'background-2-1.jpg',	0),
(2,	2,	'',	'',	0),
(3,	1,	'<h2>NEWSLETTER</h2>\r\n<p>Subscribe to the Logancee mailing list to receiveupdates on new arrivals, special offers andother discount information</p>',	'background-3-1.jpg',	0),
(3,	2,	'',	'',	0),
(4,	1,	'<h2>NEWSLETTER</h2>\r\n<p>Subscribe to the Logancee mailing list to receiveupdates on new arrivals, special offers andother discount information</p>',	'background-4-1.jpg',	0),
(4,	2,	'',	'',	0),
(5,	1,	'<h2>NEWSLETTER</h2>\r\n<p>Subscribe to the Logancee mailing list to receiveupdates on new arrivals, special offers andother discount information</p>',	'background-5-1.jpg',	0),
(5,	2,	'',	'',	0),
(6,	1,	'<h2>NEWSLETTER</h2>\r\n<p>Subscribe to the Logancee mailing list to receiveupdates on new arrivals, special offers andother discount information</p>',	'background-6-1.jpg',	0),
(6,	2,	'',	'',	0),
(7,	1,	'<h2>NEWSLETTER</h2>\r\n<p>Subscribe to the Logancee mailing list to receiveupdates on new arrivals, special offers andother discount information</p>',	'background-7-1.jpg',	0),
(7,	2,	'',	'',	0);
