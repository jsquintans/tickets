jQuery(function($) {
	if($(window).width() >= 768){
		var check_cookie = $.cookie('newsletter_popup');
	    if(window.location!=window.parent.location || check_cookie == 'dontshowitagain' || page_name != 'index'){
	        jQuery('.ovicnewsletter').remove();
	    }else{
		    if(check_cookie == null || check_cookie == 'shown') {
				setTimeout(function(){beginNewsletterForm();}, 1000);
		    }
			$('#persistent').on('change', function(){
		        if($(this).length){        
					var check_cookie = $.cookie('newsletter_popup');
		            if(check_cookie == null || check_cookie == 'shown') {
		                $.cookie('newsletter_popup','dontshowitagain');            
		            }else{
		                $.cookie('newsletter_popup','shown');
		                beginNewsletterForm();
		            }
		        } else {
		            $.cookie('newsletter_popup','shown');
		        }
			}); 
		}
    }	
	    
});
function beginNewsletterForm() {
    jQuery.fancybox({
        'padding': '0px',
        'autoScale': true,
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'type': 'inline',
        'href': '.ovicnewsletter',
        'onComplete': function() {
            $.cookie('newsletter_popup', 'shown');
        },
        'tpl': { 
            closeBtn: '<a title="Close" class="fancybox-item fancybox-close fancybox-newsletter-close" href="javascript:void(0);"></a>' 
        },
        'helpers': {
            overlay : {
                locked  : false
            }
        }
    });
    jQuery('.ovicnewsletter').trigger('click');
}
function check_email(email){
	emailRegExp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$/;	
	if(emailRegExp.test(email)){
		return true;
	}else{
		return false;
	}
}
function regisNewsletter(){
    var data={'task':'regisNewsletter', 'action':0};
    var email = $("#input-email").val();
    if(check_email(email) == true){
        data.email = email;
        $("#regisNewsletterMessage").html("");
    }else{
        $("#regisNewsletterMessage").show().html('<p class="alert-danger">'+enterEmail+'</p>');
        setTimeout(function(){ $("#regisNewsletterMessage").html("").hide();}, 3000);
        return false;
    }
    
    if ($('#persistent').is(':checked')){
        data.persistent = '1';
    }else{
        data.persistent = '0';
    }
    $.ajax({
		type: "POST",
		cache: false,
		url: ovicNewsletterUrl + '/front-end-ajax.php',
		dataType : "json",
		data: data,
        complete: function(){},
		success: function (response) {
			$("#regisNewsletterMessage").show().html(response);
            setTimeout(function(){ $("#regisNewsletterMessage").html("").hide();}, 3000);
		}
	});
}