DROP TABLE IF EXISTS `PREFIX_ovic_page_banner`;
CREATE TABLE `PREFIX_ovic_page_banner` (
  `id_page` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(20) NOT NULL,
  `page_name` VARCHAR(255) NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `setting` text,
  `active` tinyint(1) unsigned DEFAULT '1',
  `usedefault` tinyint(1) unsigned DEFAULT '1',
  INDEX(`id_page`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `PREFIX_ovic_page_banner_lang`;
CREATE TABLE `PREFIX_ovic_page_banner_lang` (
  `id_page` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) unsigned NOT NULL,
  `description` TEXT,
  INDEX(`id_page`,`id_lang`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `PREFIX_ovic_banner_image`;
CREATE TABLE IF NOT EXISTS `PREFIX_ovic_banner_image` (
  `id_banner_image` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_page` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `url` varchar(255),
  INDEX (`id_banner_image`,`id_page`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `PREFIX_ovic_banner_image_lang`;
CREATE TABLE `PREFIX_ovic_banner_image_lang` (
  `id_banner_image` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `legend` varchar(255),
  `image` varchar(255) NOT NULL,
  INDEX (`id_banner_image`,`id_lang`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
