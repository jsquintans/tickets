/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$(function(){
    if ($('#categoryslider li').length > 1){
        if (typeof(categoryslider_speed) == 'undefined')
    		categoryslider_speed = 1500;
    	if (typeof(categoryslider_pause) == 'undefined')
    		categoryslider_pause = 3000;
    	if (typeof(categoryslider_loop) == 'undefined')
    		categoryslider_loop = true;
        $('#categoryslider').bxSlider({
    	    useCSS: false,
            mode: 'fade',
			maxSlides: 1,
			//slideWidth: homeslider_width,
			infiniteLoop: categoryslider_loop,
			hideControlOnEnd: true,
			pager: false,
			autoHover: true,
			auto: categoryslider_loop,
			speed: parseInt(categoryslider_speed),
			pause: categoryslider_pause,
			controls: true,
            nextText:'<i class="fa fa-angle-right"></i>',
            prevText:'<i class="fa fa-angle-left"></i>',
            onSliderLoad:function(currentIndex){
                var current = $('#categoryslider > li').eq(currentIndex);               
                setTimeout(function(){
                    //current.find('.sl-description').show();
                    current.find('.caption').each(function(){
                        $(this).show().addClass('animated fadeInDown');
                    })
                }, 500);                      
            },
            onSlideBefore:function(slideElement, oldIndex, newIndex){
                //slideElement.find('.sl-description').hide();
                slideElement.find('.caption').each(function(){                    
                   $(this).hide().removeClass('animated fadeInDown'); 
                });                
            },
            onSlideAfter: function(slideElement, oldIndex, newIndex){  
                //slideElement.find('.sl-description').show();
                setTimeout(function(){
                    slideElement.find('.caption').each(function(){                    
                       $(this).show().addClass('animated fadeInDown'); 
                    });
                }, 200);                                
            }
    	});
    }
});