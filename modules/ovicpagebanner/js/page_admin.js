$(document).ready(function () {
    $(document).on('change','.page_type',function(){
        if ($(this).val() == 'slider'){
            $('.parallax_info').slideUp('slow');
            $('.slider_info').slideDown('slow');
        }else{
            $('.slider_info').slideUp('slow');
            $('.parallax_info').slideDown('slow');
        }
    })
    $(document).on('change','.change_active',function(){
        if ($(this).val() == '1'){
            $('.use_default').slideUp('slow');
        }else{
            $('.use_default').slideDown('slow');
        }
    })
    $(document).on('change','#page_select',function(){
        window.location.href = postUrl+'&page_form=1&page_select='+$(this).val();
    });
    $(document).on('change','.link_select',function(){
        var ids = $(this).attr('id');
        $("#link_text").val($('#'+ids+' option:selected').text().trim());
        if (!$('#item_title').val().length > 0) {
            $('#item_title').val($('#'+ids+' option:selected').text().trim());
        }
        if (!$('#item_legend').val().length > 0) {
            $('#item_legend').val($('#'+ids+' option:selected').text().trim());
        }
        $('#link_value').val($(this).val());
    });
    $( ".ui-sortable" ).sortable({
        update: function (e, ui) {
            var order="";
            var id_page = $(this).data('page');
            $(this).find('.sortable').each(function () {
                order += $(this).data('id')+'::';
            })
            order = (order.substr(0, order.length - 2));
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: postUrl,
                data: 'updateposition&id_page='+id_page+'&order='+order,
                success:function(jsonData){
                    if (jsonData){
                        showSuccessMessage(update_success_msg);
                    }else{
                        showErrorMessage('Update error');
                    }
                }
            });
            /*if($("."+list_class).length >1){
                $("."+list_class).each(function(){
                    order += $(this).find(".hidden").text()+'::';
                });
                order = (order.substr(0, order.length - 2));
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: $('#ajaxUrl').val(),
                    data: 'action=updateposition&menu_order='+menu_order,
                    success:function(jsonData){
                        if (jsonData){
                            showSuccessMessage(update_success_msg);
                        }else{
                            showErrorMessage('Update error');
                        }
                    }
                });
            }*/

        }
    });
})
function displayList(idlist) {
    if ($('#'+idlist).is(":visible")){
        $('.banner_list').slideUp('slow');
    }else{
        $('.banner_list').slideUp('slow');
        $('#'+idlist).slideDown('slow');
    }
}