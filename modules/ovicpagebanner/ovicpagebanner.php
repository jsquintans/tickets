<?php if (!defined('_PS_VERSION_')) exit;
include_once (dirname(__file__) . '/class/PageBanner.php');
include_once (dirname(__file__) . '/class/BannerImage.php');
class OvicPageBanner extends Module{
    const INSTALL_SQL_FILE = 'install.sql';
    public $sameDatas = '';
    protected static $hookArr = Array(
        'displayTopColumn',
        'displayParalaxPage');
    public function __construct()
    {
        $this->name = 'ovicpagebanner';
        $this->tab = 'front_office_features';
        $this->version = '1.1';
        $this->author = 'OvicSoft';
        $this->need_instance = 0;
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Ovic page banner');
        $this->description = $this->l('Adds an image Banner (slider) for every page.');
        $this->ps_versions_compliancy = array('min' => '1.6.0.4', 'max' => _PS_VERSION_);
        //$this->sameDatas = dirname(__FILE__).'/samedatas/';
    }
    public function install($delete_params = true)
    {
        if (!file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE)) return false;
        else if (!$sql = file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE)) return false;
        $sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
        $sql = preg_split("/;\s*[\r\n]+/", trim($sql));
        foreach ($sql as $query){
            if (!Db::getInstance()->execute(trim($query))) return false;
        }

        if (!parent::install())
            return false;
        $result = true;
        foreach (self::$hookArr as $hookname){
            if (!$this->registerHook($hookname)){
                $result &= false;
                break;
            }
        }
        if (!$result ||
            !$this->registerHook('DisplayBackOfficeHeader') ||
            !$this->registerHook('displayHeader'))
            return $result;
        return true;
    }
    public function uninstall($delete_params = true)
    {
        if (!parent::uninstall())
            return false;
        return true;
    }
    public function getContent()
    {
        $output = '';
        $errors = array();
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $languages = Language::getLanguages();
        $id_lang = $this->context->language->id;
        $id_shop = $this->context->shop->id;
        if (Tools::getValue('confirm_msg'))
        {
            $this->context->smarty->assign('confirmation', Tools::getValue('confirm_msg'));
        }
        if (Tools::isSubmit('submitpagebanner')){
            $page_name = Tools::getValue('page_select');
            if ($page_name){
                if ($id_page = $this->getIdByPage($page_name,$id_shop)){
                    $update_banner = true;
                    $page_banner = new PageBanner($id_page);
                }else
                    $page_banner = new PageBanner();
            }else{
                return;
            }
            $page_banner->page_name = $page_name;
            $page_banner->type = Tools::getValue('page_type');
            if ($page_banner->type == 'parallax'){
                $setting = array();
                if (isset($_FILES['banner_img']) && strlen($_FILES['banner_img']['name']) > 0)
                {
                    if (!$img_file = $this->moveUploadedImage($_FILES['banner_img']))
                    {
                        $errors[] = 'An error occurred during the image upload.';
                    }
                    else
                    {
                        $setting['image'] = $img_file;
                        if (Tools::getValue('old_img') != '')
                        {
                            $filename = Tools::getValue('old_img');
                            if (file_exists(dirname(__file__) . '/img/' . $filename))
                            {
                                @unlink(dirname(__file__) . '/img/' . $filename);
                            }
                        }
                    }
                }
                else
                {
                    $setting['image'] = Tools::getValue('old_img');
                }
                $setting['custom_class'] = Tools::getValue('custom_class');
                $setting['ratio'] = (float)Tools::getValue('ratio');
                $page_banner->setting = Tools::jsonEncode($setting);
                foreach ($languages as $language) $page_banner->description[$language['id_lang']] = Tools::getValue('page_dec_' .
                    $language['id_lang']);
            }else{
                $setting = array();
                $setting['speed'] = (int)Tools::getValue('speed');
                $setting['pause'] = (int)Tools::getValue('pause');
                $setting['play'] = (int)Tools::getValue('autoplay');
                $setting['custom_class'] = Tools::getValue('custom_class');
                $page_banner->setting = Tools::jsonEncode($setting);
            }
            $page_banner->active = (int)Tools::getValue('active');
            $page_banner->usedefault = (int)Tools::getValue('usedefault');
            $page_banner->id_shop = $this->context->shop->id;
            if (!count($errors))
            {
                if ($update_banner)
                {
                    if (!$page_banner->update())
                        $errors[] = 'An error occurred while update data.';
                }
                else
                {
                    if (!$page_banner->add())
                    {
                        $errors[] = 'An error occurred while saving data.';
                    }
                }
                if (!count($errors))
                {

                    if ($update_banner)
                        $this->context->smarty->assign('confirmation', $this->l('Item successfully updated.'));
                    else
                    {
                        $confirm_msg = $this->l('New item successfully added.');
                        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('ovicpagebanner.tpl'),$page_banner->id_page);
                        Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                            Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg);
                    }
                }
            }
        }
        elseif (Tools::isSubmit('bannerstatus')){
            $id_page = Tools::getValue('id_page');
            if ($id_page && ValidateCore::isUnsignedId($id_page)){
                $page_banner = new PageBanner($id_page);
                $page_banner->active = !$page_banner->active;
                if (!$page_banner->update())
                {
                    $errors[] = $this->displayError('Could not change');
                }
                else
                {
                    $confirm_msg = $this->l('Banner successfully updated.');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('ovicpagebanner.tpl'),$id_page);
                    Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                        Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg);
                }
            }
        }
        elseif (Tools::isSubmit('removepage')){
            $id_page = Tools::getValue('id_page');

            if ($id_page && ValidateCore::isUnsignedId($id_page)){
                $page_banner = new PageBanner($id_page);
                if (!$page_banner->delete())
                {
                    $errors[] = $this->displayError('An error occurred while delete page info.');
                }
                else
                {
                    //$confirm_msg = $this->l('Delete successful.');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('ovicpagebanner.tpl'),$id_page);
                    /*Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                        Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg.'&page_active='.$page_active);*/
                    $this->context->smarty->assign('confirmation',$this->l('Delete successful.'));
                }
            }
        }
        elseif(Tools::isSubmit('submitbanneritem')){
            $id_banner = (int)Tools::getValue('id_banner');
            $update_image = false;
            if ($id_banner && ValidateCore::isUnsignedId($id_banner)){
                $update_image = true;
                $banner_item = new BannerImage($id_banner);
            }else{
                $banner_item = new BannerImage();
            }
            $id_page = (int)Tools::getValue('id_page');
            $page_active = Tools::getValue('page_active','');
            $banner_item->id_page = $id_page;
            $sql = 'SELECT MAX(`position`) AS position
    					FROM `'._DB_PREFIX_.'ovic_banner_image`
    					WHERE `id_page` = '.$id_banner;
            if (!$position = Db::getInstance()->getValue($sql))
                $position = 0;
            $banner_item->position = $position +1;
            $banner_item->active = (int)Tools::getValue('active');
            $itemtitle_set = false;
            foreach ($languages as $language)
            {
                $item_title = Tools::getValue('item_title_' . $language['id_lang']);
                if (strlen($item_title) > 0)
                {
                    $itemtitle_set = true;
                }
                $banner_item->title[$language['id_lang']] = $item_title;
                $banner_item->legend[$language['id_lang']] = Tools::getValue('item_legend_' . $language['id_lang']);
                $banner_item->description[$language['id_lang']] = Tools::getValue('item_dec_' . $language['id_lang']);
                if (isset($_FILES['banner_img_'.$language['id_lang']]) && strlen($_FILES['banner_img_'.$language['id_lang']]['name']) > 0)
                {
                    if (!$item_img = $this->moveUploadedImage($_FILES['banner_img_'.$language['id_lang']]))
                    {
                        $errors[] = 'An error occurred during the image upload.';
                    }
                    else
                    {
                        $banner_item->image[$language['id_lang']] = $item_img;
                        if (Tools::getValue('old_icon') != '')
                        {
                            $filename = Tools::getValue('old_icon');
                            if (file_exists(dirname(__file__) . '/img/' . $filename))
                                @unlink(dirname(__file__) . '/img/' . $filename);
                        }
                    }
                }
                else
                    $banner_item->image[$language['id_lang']] = Tools::getValue('old_img_'.$language['id_lang']);
            }
            if (!$itemtitle_set)
            {
                $lang_title = Language::getLanguage($this->context->language->id);
                $errors[] = 'This item title field is required at least in ' . $lang_title['name'];
            }

            $banner_item->url = Tools::getValue('link_value');
            if (!count($errors))
            {
                if ($update_image)
                {
                    if (!$banner_item->update())
                        $errors[] = 'An error occurred while update data.';
                }
                else
                {
                    if (!$banner_item->add())
                    {
                        $errors[] = 'An error occurred while saving data.';
                    }
                }
                if (!count($errors))
                {

                    if ($update_image)
                        $this->context->smarty->assign('confirmation', $this->l('Item successfully updated.'));
                    else
                    {
                        $confirm_msg = $this->l('New item successfully added.');
                        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('ovicpagebanner.tpl'),$id_page);
                        Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                            Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg.'&page_active='.$page_active);
                    }
                }
            }
        }
        elseif (Tools::isSubmit('itemstatus')){
            $id_banner = Tools::getValue('id_banner');
            if ($id_banner && ValidateCore::isUnsignedId($id_banner)){
                $banner_item = new BannerImage($id_banner);
                $banner_item->active = !$banner_item->active;
                if (!$banner_item->update())
                {
                    $errors[] = $this->displayError('Could not change');
                }
                else
                {
                    $confirm_msg = $this->l('Banner successfully updated.');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('ovicpagebanner.tpl'),$banner_item->id_page);
                    Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                        Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg.'&page_active='.$this->getPageNameById($banner_item->id_page,$id_shop));
                }
            }
        }
        elseif (Tools::isSubmit('removeitem')){
            $id_banner = Tools::getValue('id_banner');
            $page_active = Tools::getValue('page_active');
            if ($id_banner && ValidateCore::isUnsignedId($id_banner)){
                $banner_item = new BannerImage($id_banner);
                if (!$banner_item->delete())
                {
                    $errors[] = $this->displayError('An error occurred while delete banner image.');
                }
                else
                {
                    //$confirm_msg = $this->l('Delete successful.');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('ovicpagebanner.tpl'),$banner_item->id_page);
                    /*Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                        Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg.'&page_active='.$page_active);*/
                    $this->context->smarty->assign('confirmation',$this->l('Delete successful.'));
                }
            }
        }
        elseif (Tools::isSubmit('updateposition')){
            $order = Tools::getValue('order');
            $id_page = Tools::getValue('id_page');
            $position = explode('::', $order);
            $res = true;
            if (count($position) > 0)
                foreach ($position as $key => $id_item)
                {
                    $res &= Db::getInstance()->execute('
                    UPDATE `' . _DB_PREFIX_ . 'ovic_banner_image`
                    SET `position` = ' . $key . '
                    WHERE `id_banner_image` = ' . (int)$id_item);
                }
            Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('ovicpagebanner.tpl'),$id_page);
            die(Tools::jsonEncode($res)) ;
        }
        elseif (Tools::isSubmit('export')){
            $this->exportSameData();
            $this->context->smarty->assign('confirmation',$this->l('Export data success!'));
        }
        elseif (Tools::isSubmit('import')){
            $this->importSameData();
            $this->context->smarty->assign('confirmation',$this->l('Import data success!'));
        }

        if (Tools::getIsset('banner_form')) {
            //info banner of id page
            $id_page = Tools::getValue('id_page');
            $id_banner = Tools::getValue('id_banner');
            //$update_item = false;
            if ($id_banner && ValidateCore::isUnsignedId($id_banner)){
                //$update_item = true;
                $banner_item = new BannerImage($id_banner);
            }else{
                $banner_item = new BannerImage();
            }
            $lang_ul = '<ul class="dropdown-menu">';


            foreach ($languages as $lg)
            {
                $lang_ul .= '<li><a href="javascript:hideOtherLanguage(' . $lg['id_lang'] . ');" tabindex="-1">' . $lg['name'] .
                    '</a></li>';
                /*$default_link_option[$lg['id_lang']] = $this->getAllDefaultLink('',$lg['id_lang'],true);*/
            }
            $lang_ul .= '</ul>';
            $this->context->smarty->assign(array(
                'postUrl' => $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token=' . Tools::getAdminTokenLite('AdminModules'),
                'id_page' => $id_page,
                'page_active' => $this->getPageNameById($id_page,$id_shop),
                'link_text' => $this->fomartLink($banner_item->url),
                'banner_item' => $banner_item,
                'default_link_option' => $this->getAllDefaultLink($banner_item->url),
                'lang_ul' => $lang_ul,
                'image_baseurl' => $this->_path.'img/',
                'langguages' => array(
                    'default_lang' => $id_lang_default,
                    'all' => $languages,
                    'lang_dir' => _THEME_LANG_DIR_)
            ));
            $iso = Language::getIsoById((int)($id_lang));
            $isoTinyMCE = (file_exists(_PS_ROOT_DIR_ . '/js/tiny_mce/langs/' . $iso . '.js') ? $iso : 'en');
            $ad = dirname($_SERVER["PHP_SELF"]);
            $html = '<script type="text/javascript">
                var iso = \'' . $isoTinyMCE . '\' ;
                var pathCSS = \'' . _THEME_CSS_DIR_ . '\' ;
                var ad = \'' . $ad . '\' ;
                $(document).ready(function(){
                tinySetup({
                    editor_selector :"rte",
                    theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,fontselect,fontsizeselect",
                    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,codemagic,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                    theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                    theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
                    theme_advanced_toolbar_location : "top",
                    theme_advanced_toolbar_align : "left",
                    theme_advanced_statusbar_location : "bottom",
                    theme_advanced_resizing : false,
                    extended_valid_elements: \'pre[*],script[*],style[*]\',
                    valid_children: "+body[style|script],pre[script|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
                    valid_elements : \'*[*]\',
                    force_p_newlines : false,
                    cleanup: false,
                    forced_root_block : false,
                    force_br_newlines : true
                    });
                });</script>';
            $output = $html.$this->display(__file__, 'views/templates/admin/banner_form.tpl');
        }
        elseif (Tools::getIsset('page_form'))
        {
            //$update_banner = false;
            $page_name = Tools::getValue('page_select','default');
            if ($id_page = $this->getIdByPage($page_name,$id_shop)){
                //$update_banner = true;
                $page_banner = new PageBanner($id_page);
            }else
                $page_banner = new PageBanner();
            $lang_ul = '<ul class="dropdown-menu">';
            if ($page_banner->setting){
                $page_banner->setting = Tools::jsonDecode($page_banner->setting,true);
            }
            foreach ($languages as $lg)
            {
                $lang_ul .= '<li><a href="javascript:hideOtherLanguage(' . $lg['id_lang'] . ');" tabindex="-1">' . $lg['name'] .
                    '</a></li>';
            }
            $lang_ul .= '</ul>';
            $this->context->smarty->assign(array(
                'postUrl' => $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token=' . Tools::getAdminTokenLite('AdminModules'),
                'page_banner' => $page_banner,
                'default_link_option' => $this->getAllDefaultLink($page_name),
                'lang_ul' => $lang_ul,
                'image_baseurl' => $this->_path.'img/',
                'page_active' => $page_name,
                'langguages' => array(
                    'default_lang' => $id_lang_default,
                    'all' => $languages,
                    'lang_dir' => _THEME_LANG_DIR_)
            ));
            $iso = Language::getIsoById((int)($id_lang));
            $isoTinyMCE = (file_exists(_PS_ROOT_DIR_ . '/js/tiny_mce/langs/' . $iso . '.js') ? $iso : 'en');
            $ad = dirname($_SERVER["PHP_SELF"]);
            $html = '<script type="text/javascript">
                var iso = \'' . $isoTinyMCE . '\' ;
                var pathCSS = \'' . _THEME_CSS_DIR_ . '\' ;
                var ad = \'' . $ad . '\' ;
                $(document).ready(function(){
                tinySetup({
                    editor_selector :"rte",
                    theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,fontselect,fontsizeselect",
                    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,codemagic,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                    theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                    theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
                    theme_advanced_toolbar_location : "top",
                    theme_advanced_toolbar_align : "left",
                    theme_advanced_statusbar_location : "bottom",
                    theme_advanced_resizing : false,
                    extended_valid_elements: \'pre[*],script[*],style[*]\',
                    valid_children: "+body[style|script],pre[script|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
                    valid_elements : \'*[*]\',
                    force_p_newlines : false,
                    cleanup: false,
                    forced_root_block : false,
                    force_br_newlines : true
                    });
                });</script>';
            $output = $html.$this->display(__file__, 'views/templates/admin/page_form.tpl');
        }
        else{
            // list all page.
            $page_list = $this->getAllPageBanner();
            $page_banners = array();
            if ($page_list && count($page_list)> 0){
                foreach ($page_list as &$page) {
                    if ($page['setting']){
                        $page['setting'] = Tools::jsonDecode($page['setting'],true);
                    }
                    $page_banners[$page['page_name']]['id_page']  = $page['id_page'];
                    $page_banners[$page['page_name']]['list'] = $this->getBannerByPage($page['id_page']);
                    $page['title'] = $this->getFullPageName($page['page_name'],$id_lang);
                }
            }
            $page_active = Tools::getValue('page_active','');
            $this->context->smarty->assign(array(
                'postUrl' => $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token=' . Tools::getAdminTokenLite('AdminModules'),
                'page_active' => $page_active,
                'page_list' => $page_list,
                'image_baseurl' => $this->_path.'img/',
                'page_banners' => $page_banners,
            ));
            if (count($errors) > 0)
            {
                if (isset($errors) && count($errors)) $output .= $this->displayError(implode('<br />', $errors));
            }
            $output = $this->display(__file__, 'views/templates/admin/banner_list.tpl');
        }
        return $output;
    }

    private function addNoteCData(&$item, $text){
        $node= dom_import_simplexml($item);
        $doc = $node->ownerDocument;
        //$doc->appendChild($doc->createCDATASection(str_replace('\\', '', $text)));
        $node->appendChild($doc->createCDATASection(str_replace('\\', '', $text)));
        return true;
    }
    public function exportSameData($directory=''){
        $shopId = $this->context->shop->id;
        if($directory) $this->sameDatas = $directory;
        //$langId = Context::getContext()->language->id;
        $currentOption = Configuration::get('OVIC_CURRENT_DIR',null,null,$shopId);
        if($currentOption) $currentOption .= '.';
        else $currentOption = '';
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'ovic_page_banner` 
                WHERE `id_shop` = ' . $shopId ;
        $items = Db::getInstance()->executeS($sql);
        if($items){
            $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><pageBanner></pageBanner>');
            foreach($items as $item){
                $page_item = $xml->addChild('page');
                $this->exportPage($item,$page_item); 
                if ($item['type'] == 'slider') {
		    $slider =  $page_item->addChild('slide');
                    $this->exportSlider($item['id_page'], $slider);
		}
            }
            $file = $this->sameDatas.'store'.$shopId.'.'.$currentOption.$this->name.'.xml';
            $xml->asXML($file);
        }
        return true;
    }
    private function exportPage($item, &$parent){
        if($item && is_array($item)){
            //$parent->addChild('id_page', $item['id_page']);
            $parent->addChild('type', $item['type']);
            $parent->addChild('page_name', $item['page_name']);
            $setting = $parent->addChild('setting');
            $this->addNoteCData($setting, $item['setting']);
            $parent->addChild('usedefault', $item['usedefault']);
            $parent->addChild('active', $item['active']);
            $this->exportPageLang($item['id_page'], $parent);
        }
        return true;
    }
    private function exportPageLang($itemId=0, &$parent){
        //$langId = $this->context->language->id;
        //$result = '';
        if($itemId){
            $items = Db::getInstance()->executeS("Select * From "._DB_PREFIX_."ovic_page_banner_lang Where id_page =". $itemId);
            if($items){
                $languages_node = $parent->addChild('languages');
                foreach($items as $item){
                    $langIso = LanguageCore::getIsoById($item['id_lang']);
                    $language = $languages_node->addChild('language');
                    // lang iso
                    $language->addChild('lang_iso', $langIso);
                    // description
                    $description = $language->addChild('description');
                    $this->addNoteCData($description, $item['description']);
                }
            }
        }
        return true;
    }
    private function exportSlider($ItemId=0, &$parent){
        if($ItemId){
            $items = Db::getInstance()->executeS("Select * From "._DB_PREFIX_."ovic_banner_image Where id_page = $ItemId" . " ORDER BY `position` ASC, `id_banner_image` ASC" );
            if($items && is_array($items) && count($items>0)){
                foreach ($items as $item){
                    $slide_image = $parent->addChild('SlideImage');
                    $slide_image->addChild('position',$item['position']);
                    $slide_image->addChild('active',$item['active']);
                    $slide_image->addChild('url',$item['url']);
                    $this->exportSliderLang($item['id_banner_image'], $slide_image);
                }
            }
        }
        return true;
    }
    private function exportSliderLang($itemId=0, &$parent){
        /*$langId = $this->context->language->id;
        $result = '';*/
        if($itemId){
            $items = Db::getInstance()->executeS("Select * From "._DB_PREFIX_."ovic_banner_image_lang Where id_banner_image =". $itemId);
            if($items){
                $languages_node = $parent->addChild('languages');
                foreach($items as $item){
                    $langIso = LanguageCore::getIsoById($item['id_lang']);
                    $language = $languages_node->addChild('language');
                    // lang iso
                    $language->addChild('lang_iso', $langIso);
                    // description
                    $description = $language->addChild('description');
                    $this->addNoteCData($description, $item['description']);
                    // title
                    $title = $language->addChild('title');
                    $this->addNoteCData($title, $item['title']);
                    // legend
                    $legend = $language->addChild('legend');
                    $this->addNoteCData($legend, $item['legend']);
                    // image
                    $imagelink = $language->addChild('image');
                    $this->addNoteCData($imagelink, $item['image']);
                }
            }
        }
        return true;
    }
    public  function importSameData($directory='', $file=''){
        if($directory) $this->sameDatas = $directory;
        $shopId = $this->context->shop->id;
        $currentOption = Configuration::get('OVIC_CURRENT_DIR',null,null,$shopId);
        if($currentOption) $currentOption .= '.';
        else $currentOption = '';
        if(!$file){
            $file = $this->sameDatas.'store'.$shopId.'.'.$currentOption.$this->name.'.xml';
        }else{
            if(!file_exists($file))
                $file = $this->sameDatas.'store'.$shopId.'.'.$currentOption.$this->name.'.xml';
        }
        if(!file_exists($file))
            $file = $this->sameDatas.$currentOption.$this->name.'.xml';
        if(!file_exists($file))
            $file = $this->sameDatas.$this->name.'.xml';
        if(file_exists($file)){
            $db = Db::getInstance(_PS_USE_SQL_SLAVE_);
            $languages = $db->executeS("Select id_lang, iso_code From "._DB_PREFIX_."lang Where active = 1");
            $xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);
            if(isset($xml->page)){
                if(count($xml->page) >0){
                    $db->execute("Delete bil From `"._DB_PREFIX_."ovic_banner_image_lang` bil 
                                  INNER JOIN `"._DB_PREFIX_."ovic_banner_image` bi ON bi.id_banner_image = bil.id_banner_image     
                                  INNER JOIN `"._DB_PREFIX_."ovic_page_banner` pb ON pb.id_page = bi.id_page
                                  WHERE pb.id_shop =".$shopId);
                    $db->execute("Delete bi From `"._DB_PREFIX_."ovic_banner_image` bi 
                                  INNER JOIN `"._DB_PREFIX_."ovic_page_banner` pb ON pb.id_page = bi.id_page   
                                  WHERE pb.id_shop =".$shopId);
                    $db->execute("Delete pbl From `"._DB_PREFIX_."ovic_page_banner_lang` pbl 
                                  INNER JOIN `"._DB_PREFIX_."ovic_page_banner` pb ON pb.id_page = pbl.id_page   
                                  WHERE pb.id_shop =".$shopId);
                    $db->execute("Delete pb From `"._DB_PREFIX_."ovic_page_banner` pb
                                  WHERE pb.id_shop =".$shopId);

                    foreach($xml->page as $page_bn){
                        if($insertId = $this->importPage($page_bn,$languages,$shopId)){
                            if($page_bn->type ='slider' && isset($page_bn->slide)){
                                $this->importSlide($insertId,$page_bn->slide,$languages);
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
    private function importPage($page,$languages,$id_shop){
        $db = Db::getInstance();
        $pageInsert = array(
            'type'              =>  (string)$page->type,
            'page_name'         =>  (string)$page->page_name,
            'setting'           =>  (string)$page->setting,
            'usedefault'        =>  (string)$page->usedefault,
            'active'            =>  (int)$page->active,
            'id_shop'           =>  (int)$id_shop
        );
        $this_languages = array();
        $langDefault = array();
        if(isset($page->languages->language) && count($page->languages->language) >0){
            foreach($page->languages->language as $language){
                $ldescription = Tools::htmlentitiesDecodeUTF8($language->description);
                if(!$langDefault) $langDefault = array(
                    'description'=>Tools::htmlentitiesUTF8($ldescription),
                );
                $this_languages[(string)$language->lang_iso] = array(
                    'description'=>Tools::htmlentitiesUTF8($ldescription),
                );
            }
        }
        if($db->insert('ovic_page_banner', $pageInsert)){
            $insertId = $db->Insert_ID();
            $arrInsertLangs = array();
            foreach($languages as $language){
                $lang_iso = $language['iso_code'];
                if($this_languages){
                    if(key_exists($lang_iso, $this_languages)){
                        $arrInsertLangs[] = array(
                            'id_page'=>$insertId,
                            'id_lang'=>$language['id_lang'],
                            'description'=>$this_languages[$lang_iso]['description'],
                        );
                    }else{
                        $arrInsertLangs[] = array(
                            'id_item'=>$insertId,
                            'id_lang'=>$language['id_lang'],
                            'description'=>$langDefault['description'],
                        );
                    }
                }else{
                    $arrInsertLangs[] = array(
                        'id_item'=>$insertId,
                        'id_lang'=>$language['id_lang'],
                        'description'=>$langDefault['description'],
                    );
                }
            }
            if($arrInsertLangs) Db::getInstance()->insert('ovic_page_banner_lang', $arrInsertLangs);
            return $insertId;
        }else
            return false;
    }
    private function importSlide($parent_id,$slide,$languages){
        if (isset($slide->SlideImage) && count($slide->SlideImage)>0){
            $db = Db::getInstance();
            foreach ($slide->SlideImage as $slideImage) {
                $slideInsert = array(
                    'id_page'       =>  $parent_id,
                    'position'      =>  $slideImage->position,
                    'url'           =>  (string)$slideImage->url,
                    'active'        =>  (int)$slideImage->active,
                );
                $this_languages = array();
                $langDefault = array();
                if(isset($slideImage->languages->language) && count($slideImage->languages->language) >0){
                    foreach($slideImage->languages->language as $language){
                        $ldescription = Tools::htmlentitiesDecodeUTF8($language->description);
                        if(!$langDefault) $langDefault = array(
                            'description' =>Tools::htmlentitiesUTF8($ldescription),
                            'title'       => $language->title,
                            'legend'      => $language->legend,
                            'image'       => $language->image,
                        );
                        $this_languages[(string)$language->lang_iso] = array(
                            'description' =>Tools::htmlentitiesUTF8($ldescription),
                            'title'       => $language->title,
                            'legend'      => $language->legend,
                            'image'       => $language->image,
                        );
                    }
                }
                if($db->insert('ovic_banner_image', $slideInsert)){
                    $insertId = $db->Insert_ID();
                    $arrInsertLangs = array();
                    foreach($languages as $language){
                        $lang_iso = $language['iso_code'];
                        if($this_languages){
                            if(key_exists($lang_iso, $this_languages)){
                                $arrInsertLangs[] = array(
                                    'id_banner_image'=>$insertId,
                                    'id_lang'=>$language['id_lang'],
                                    'description'=>$this_languages[$lang_iso]['description'],
                                    'title'       => $this_languages[$lang_iso]['title'],
                                    'legend'      => $this_languages[$lang_iso]['legend'],
                                    'image'       => $this_languages[$lang_iso]['image'],
                                );
                            }else{
                                $arrInsertLangs[] = array(
                                    'id_banner_image'=>$insertId,
                                    'id_lang'=>$language['id_lang'],
                                    'description'=>$langDefault['description'],
                                    'title'       => $langDefault['title'],
                                    'legend'      => $langDefault['legend'],
                                    'image'       => $langDefault['image'],
                                );
                            }
                        }else{
                            $arrInsertLangs[] = array(
                                'id_banner_image'=>$insertId,
                                'id_lang'=>$language['id_lang'],
                                'description'=>$langDefault['description'],
                                'title'      => $langDefault['title'],
                                'legend'     => $langDefault['legend'],
                                'image'      => $langDefault['image'],
                            );
                        }
                    }
                    if($arrInsertLangs) Db::getInstance()->insert('ovic_banner_image_lang', $arrInsertLangs);
                }
            }
        }
    }
    /**
     * @param null $id_shop
     * @param bool $active
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    private function getAllPageBanner($id_shop = null, $active = false){
        if (is_null($id_shop))
            $id_shop = $this->context->shop->id;
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'ovic_page_banner` opb
                WHERE `id_shop` = ' . $id_shop .($active ? ' AND active = ' . $active : '');
        return Db::getInstance()->executeS($sql);
    }
    private function getPageNameById($id_page = null,$id_shop = null){
        if (is_null($id_page) || !ValidateCore::isUnsignedId($id_page))
            return false;
        if (is_null($id_shop))
            $id_shop = $this->context->shop->id;
        $sql = 'SELECT `page_name` FROM `' . _DB_PREFIX_ . 'ovic_page_banner`      
                WHERE `id_page` = ' .(int) $id_page.' 
                AND `id_shop` = '.(int)$id_shop;
        return Db::getInstance()->getValue($sql);
    }
    private function getIdByPage($page_name = null,$id_shop = null){
        if (is_null($page_name))
            return false;
        if (is_null($id_shop))
            $id_shop = $this->context->shop->id;
        $sql = 'SELECT `id_page` FROM `' . _DB_PREFIX_ . 'ovic_page_banner`               
                WHERE `page_name` = "' . $page_name.'" 
                AND `id_shop` = '.(int)$id_shop;
        return Db::getInstance()->getValue($sql);
    }
    /**
     * @param null $id_page
     * @param null $id_lang
     * @param bool $active
     * @return array|bool|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    private function getBannerByPage($id_page = null, $id_lang = null, $active = false){
        if (is_null($id_page))
            return false;
        if (is_null($id_lang))
            $id_lang = $this->context->language->id;
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'ovic_banner_image` obi
                LEFT JOIN `' . _DB_PREFIX_ . 'ovic_banner_image_lang` obil ON (obi.`id_banner_image` = obil.`id_banner_image`)
                WHERE `id_page` = "'.(int)$id_page.'" AND
                obil.`id_lang` = ' . $id_lang . ($active ? ' AND obi.`active` = ' . $active : '') . '
                ORDER BY  obi.`position` ASC, obi.`id_banner_image` ASC';
        return Db::getInstance()->executeS($sql);
    }
    /**
     * @return string
     */
    protected function getCurrentUrl()
    {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["HTTP_HOST"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
    /**
     * @param int $parent
     * @param int $depth
     * @param bool $id_lang
     * @param bool $link
     * @return string
     */
    private function getCMSOptions($parent = 0, $depth = 0, $id_lang = false, $link = false , $selected = '')
    {
        $html = '';
        $id_lang = $id_lang ? (int)$id_lang : (int)$this->context->language->id;
        $id_shop = (int)$this->context->shop->id;
        $categories = $this->getCMSCategories(false, (int)$parent, (int)$id_lang);
        $pages = $this->getCMSPages((int)$parent, $id_shop, (int)$id_lang);
        $spacer = str_repeat('&nbsp;', 3 * (int)$depth);
        foreach ($categories as $category)
        {
            $html .= $this->getCMSOptions($category['id_cms_category'], (int)$depth + 1, (int)$id_lang, $link,$selected);
        }
        foreach ($pages as $page)
            if ($link) $html .= '<option value="' . $this->context->link->getCMSLink($page['id_cms']) . '">' . (isset
                ($spacer) ? $spacer : '') . $page['meta_title'] . '</option>';
            else{
                $key = 'CMS'.(int)$page['id_cms'];
                $select_str = "";
                if ($selected == $key)
                    $select_str = ' selected = "selected"';
                $html .= '<option'.$select_str.' value="CMS' . $page['id_cms'] . '">' . $page['meta_title'] . '</option>';
            }
        return $html;
    }

    /**
     * @param bool $recursive
     * @param int $parent
     * @param bool $id_lang
     * @return array|bool|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    private function getCMSCategories($recursive = false, $parent = 1, $id_lang = false)
    {
        $id_lang = $id_lang ? (int)$id_lang : (int)$this->context->language->id;
        $id_shop = (int)$this->context->shop->id;
        if ($recursive === false)
        {
            $sql = 'SELECT bcp.`id_cms_category`, bcp.`id_parent`, bcp.`level_depth`, bcp.`active`, bcp.`position`, cl.`name`, cl.`link_rewrite`
				FROM `' . _DB_PREFIX_ . 'cms_category` bcp
				LEFT JOIN `' . _DB_PREFIX_ . 'cms_category_lang` cl
					ON (bcp.`id_cms_category` = cl.`id_cms_category`)
				WHERE bcp.`id_parent` = ' . (int)$parent.
                ' AND cl.id_shop = ' . $id_shop.
                ' AND cl.`id_lang` = ' . $id_lang;
            return Db::getInstance()->executeS($sql);
        }
        else
        {
            $sql = 'SELECT bcp.`id_cms_category`, bcp.`id_parent`, bcp.`level_depth`, bcp.`active`, bcp.`position`, cl.`name`, cl.`link_rewrite`
				FROM `' . _DB_PREFIX_ . 'cms_category` bcp
				LEFT JOIN `' . _DB_PREFIX_ . 'cms_category_lang` cl
					ON (bcp.`id_cms_category` = cl.`id_cms_category` AND cl.`id_lang` = ' . (int)$id_lang . ')
				WHERE bcp.`id_parent` = ' . (int)$parent.
                ' AND cl.id_shop = ' . $id_shop.
                ' AND cl.`id_lang` = ' . $id_lang;
            $results = Db::getInstance()->executeS($sql);
            foreach ($results as $result)
            {
                $sub_categories = $this->getCMSCategories(true, $result['id_cms_category'], (int)$id_lang);
                if ($sub_categories && count($sub_categories) > 0) $result['sub_categories'] = $sub_categories;
                $categories[] = $result;
            }
            return isset($categories) ? $categories : false;
        }
    }

    /**
     * @param $id_cms_category
     * @param bool $id_shop
     * @param bool $id_lang
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    private function getCMSPages($id_cms_category, $id_shop = false, $id_lang = false)
    {
        $id_shop = $id_shop ? (int)$id_shop : (int)$this->context->shop->id;
        $id_lang = $id_lang ? (int)$id_lang : (int)$this->context->language->id;
        $sql = 'SELECT c.`id_cms`, cl.`meta_title`, cl.`link_rewrite`
			FROM `' . _DB_PREFIX_ . 'cms` c
			LEFT JOIN `' . _DB_PREFIX_ . 'cms_lang` cl
				ON (c.`id_cms` = cl.`id_cms`)
			WHERE c.`id_cms_category` = ' . (int)$id_cms_category . '
				AND cl.`id_shop` = ' . (int)$id_shop . '
				AND cl.`id_lang` = ' . (int)$id_lang . '
				AND c.`active` = 1
			ORDER BY `position`';
        return Db::getInstance()->executeS($sql);
    }

    private function getCMSPageTitle($id_cms = null, $id_shop = false, $id_lang = false){
        //echo $id_cms;
        if (is_null($id_cms) || !ValidateCore::isUnsignedId($id_cms)){
            return false;
        }
        $id_shop = $id_shop ? (int)$id_shop : (int)$this->context->shop->id;
        $id_lang = $id_lang ? (int)$id_lang : (int)$this->context->language->id;
        $sql = 'SELECT cl.`meta_title`
			FROM `' . _DB_PREFIX_ . 'cms_lang` cl
			WHERE cl.`id_shop` = ' . (int)$id_shop . '
				AND cl.`id_lang` = ' . (int)$id_lang . '
				AND cl.`id_cms` = ' . (int)$id_cms;
        return Db::getInstance()->getValue($sql);
    }
    /**
     * @param null $id_lang
     * @param bool $link
     * @return string
     */
    private function getPagesOption($id_lang = null, $link = false,$selected)
    {
        if (is_null($id_lang)) $id_lang = (int)$this->context->cookie->id_lang;
        $files = Meta::getMetasByIdLang($id_lang);
        $html = '';
        foreach ($files as $file)
        {
            if ($file['page'] != 'index'){
                if ($link) $html .= '<option value="' . $this->context->link->getPageLink($file['page']) . '">' . (($file['title'] !=
                        '') ? $file['title'] : $file['page']) . '</option>';
                else{
                    $key = 'PAG'.$file['page'];
                    $select_str = "";
                    if ($selected == $key)
                        $select_str = ' selected = "selected"';
                    $html .= '<option'.$select_str.' value="PAG' . $file['page'] . '">' . (($file['title'] != '') ? $file['title'] :
                            $file['page']) . '</option>';
                }
            }
        }
        return $html;
    }

    /**
     * @param int $id_category
     * @param bool $id_lang
     * @param bool $id_shop
     * @param bool $recursive
     * @param bool $link
     * @return string|void
     */
    private function getCategoryOption($id_category = 1, $id_lang = false, $id_shop = false, $recursive = true, $link = false, $selected = "")
    {
        $html = '';
        $id_lang = $id_lang ? (int)$id_lang : (int)$this->context->language->id;
        $id_shop = $id_shop ? (int)$id_shop : (int)$this->context->shop->id;
        $category = new Category((int)$id_category, (int)$id_lang, (int)$id_shop);
        if (is_null($category->id)) return;
        if ($recursive)
        {
            $children = Category::getChildren((int)$id_category, (int)$id_lang, true, (int)$id_shop);
            $spacer = str_repeat('&nbsp;', 3 * (int)$category->level_depth);
        }
        $shop = (object)Shop::getShop((int)$category->getShopID());
        if (!in_array($category->id, array(Configuration::get('PS_ROOT_CATEGORY'))))
        {

            if ($link) $html .= '<option value="' . $this->context->link->getCategoryLink($category->id) . '">' . (isset
                ($spacer) ? $spacer : '') . str_repeat('&nbsp;', 3 * (int)$category->level_depth) . $category->name .
                '</option>';
            else{
                $key = 'CAT'.(int)$category->id;
                $select_str = "";
                if ($selected == $key)
                    $select_str = ' selected = "selected"';
                $html .= '<option'.$select_str.' value="' .$key. '">' . str_repeat('&nbsp;', 3 * (int)$category->level_depth) .
                    $category->name . '</option>';
            }
        }
        elseif ($category->id != Configuration::get('PS_ROOT_CATEGORY'))
        {
            $html .= '<optgroup label="' . str_repeat('&nbsp;', 3 * (int)$category->level_depth) . $category->name .
                '">';
        }
        if (isset($children) && count($children))
            foreach ($children as $child)
            {
                $html .= $this->getCategoryOption((int)$child['id_category'], (int)$id_lang, (int)$child['id_shop'],
                    $recursive, $link,$selected);
            }
        return $html;
    }

    /**
     * @param null $id_lang
     * @param bool $link
     * @return string
     */
    private function getAllDefaultLink($selected = "",$id_lang = null, $link = false)
    {
        if (is_null($id_lang)) $id_lang = (int)$this->context->language->id;
        $id_shop = (int)$this->context->shop->id;
        $html = '<optgroup label="' . $this->l('Category') . '">';
        $html .= $this->getCategoryOption(1, $id_lang, $id_shop, true, $link, $selected);
        $html .= '</optgroup>';
        //CMS option
        $html .= '<optgroup label="' . $this->l('Cms') . '">';
        $html .= $this->getCMSOptions(0, 0, $id_lang, $link, $selected);
        $html .= '</optgroup>';
        //Manufacturer option
        $html .= '<optgroup label="' . $this->l('Manufacturer') . '">';
        $manufacturers = Manufacturer::getManufacturers(false, $id_lang);
        foreach ($manufacturers as $manufacturer)
        {
            if ($link) $html .= '<option value="' . $this->context->link->getManufacturerLink($manufacturer['id_manufacturer']) .
                '">' . $manufacturer['name'] . '</option>';
            else{
                $key = 'MAN'.(int)$manufacturer['id_manufacturer'];
                $select_str = "";
                if ($selected == $key)
                    $select_str = ' selected = "selected"';
                $html .= '<option'.$select_str.' value="MAN' . (int)$manufacturer['id_manufacturer'] . '">' . $manufacturer['name'] .
                    '</option>';
            }
        }
        $html .= '</optgroup>';
        //Supplier option
        $html .= '<optgroup label="' . $this->l('Supplier') . '">';
        $suppliers = Supplier::getSuppliers(false, $id_lang);
        foreach ($suppliers as $supplier)
        {
            if ($link) $html .= '<option value="' . $this->context->link->getSupplierLink($supplier['id_supplier']) .
                '">' . $supplier['name'] . '</option>';
            else{
                $key = 'SUP'.(int)$supplier['id_supplier'];
                $select_str = "";
                if ($selected == $key)
                    $select_str = ' selected = "selected"';
                $html .= '<option'.$select_str.' value="SUP' . (int)$supplier['id_supplier'] . '">' . $supplier['name'] .
                    '</option>';
            }
        }
        $html .= '</optgroup>';
        //Page option
        $html .= '<optgroup label="' . $this->l('Page') . '">';
        $html .= $this->getPagesOption($id_lang, $link,$selected);
        $shoplink = Shop::getShops();
        if (count($shoplink) > 1)
        {
            $html .= '<optgroup label="' . $this->l('Shops') . '">';
            foreach ($shoplink as $sh)
            {
                $key = 'SHO'.(int)$sh['id_shop'];
                $select_str = "";
                if ($selected == $key)
                    $select_str = ' selected = "selected"';
                $html .= '<option'.$select_str.' value="SHO' . (int)$sh['id_shop'] . '">' . $sh['name'] . '</option>';
            }
        }
        $html .= '</optgroup>';
        return $html;
    }
    public function hookDisplayBackOfficeHeader()
    {
        if (Tools::getValue('configure') != $this->name) return;
        $this->context->controller->addCss($this->_path . 'css/page_admin.css');
        $this->context->controller->addJquery();
        $this->context->controller->addJqueryUi('ui.sortable');
        $this->context->controller->addJS(_PS_JS_DIR_ . 'tiny_mce/tiny_mce.js');
        if (_PS_VERSION_ <= "1.6.0.11") {
            $this->context->controller->addJS(_PS_JS_DIR_ . 'tinymce.inc.js');
        } else {
            $this->context->controller->addJS(($this->_path) . 'js/tinymce.inc.js');
        }
        $this->context->controller->addJS($this->_path . 'js/page_admin.js');
    }
    /**
     * Move an uploaded image to the module img/ folder
     */
    private function moveUploadedImage($file)
    {
        $img_name = time() . $file['name'];
        $img_name = preg_replace('/[^A-Za-z0-9\-.]/', '', $img_name);
        $main_name = _PS_ROOT_DIR_ . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $this->name . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . $img_name;
        if (!move_uploaded_file($file['tmp_name'], $main_name))
        {
            return false;
        }
        return $img_name;
    }

    /**
     * @param null $item
     * @param null $id_lang
     * @return array|void
     */
    private function fomartLink($link = null, $id_lang = null)
    {
        if (is_null($link)) return;
        /*if (!empty($this->context->controller->php_self)) $page_name = $this->context->controller->php_self;
        else
        {
            $page_name = Dispatcher::getInstance()->getController();
            $page_name = (preg_match('/^[0-9]/', $page_name) ? 'page_' . $page_name : $page_name);
        }*/
        $html = '';
        //$selected_item = false;
        if (is_null($id_lang)) $id_lang = (int)$this->context->language->id;
        $i_link =trim($link);
        $type = substr($i_link, 0, 3);
        $key = substr($i_link, 3, strlen($i_link) - 3);
        switch ($type)
        {
            case 'CAT':
                //if ($page_name == 'category' && (int)Tools::getValue('id_category') == (int)$key) $selected_item = true;
                $html = $this->context->link->getCategoryLink((int)$key, null, $id_lang);
                break;
            case 'CMS':
                //if ($page_name == 'cms' && (int)Tools::getValue('id_cms') == (int)$key) $selected_item = true;
                $html = $this->context->link->getCMSLink((int)$key, null, $id_lang);
                break;
            case 'MAN':
                //if ($page_name == 'manufacturer' && (int)Tools::getValue('id_manufacturer') == (int)$key) $selected_item = true;
                $man = new Manufacturer((int)$key, $id_lang);
                $html = $this->context->link->getManufacturerLink($man->id, $man->link_rewrite, $id_lang);
                break;
            case 'SUP':
                //if ($page_name == 'supplier' && (int)Tools::getValue('id_supplier') == (int)$key) $selected_item = true;
                $sup = new Supplier((int)$key, $id_lang);
                $html = $this->context->link->getSupplierLink($sup->id, $sup->link_rewrite, $id_lang);
                break;
            case 'PAG':
                $pag = Meta::getMetaByPage($key, $id_lang);
                $html = $this->context->link->getPageLink($pag['page'], true, $id_lang);
                //if ($page_name == $pag['page']) $selected_item = true;
                break;
            case 'SHO':
                $shop = new Shop((int)$key);
                $html = $shop->getBaseURL();
                break;
            default:
                $html = $link;
                break;
        }
        return $html;
    }
    private function getFullPageName($link = null, $id_lang = null)
    {
        if (is_null($link)) return;
        if (is_null($id_lang)) $id_lang = (int)$this->context->language->id;
        $i_link =trim($link);
        $type = substr($i_link, 0, 3);
        $key = substr($i_link, 3, strlen($i_link) - 3);
        switch ($type)
        {
            case 'CAT':
                $new_cate = new CategoryCore((int)$key,$id_lang);
                $html = $new_cate->name;
                break;
            case 'CMS':
                $html = $this->getCMSPageTitle((int)$key);
                break;
            case 'MAN':
                $man = new Manufacturer((int)$key, $id_lang);
                $html = $man->name;
                break;
            case 'SUP':
                $sup = new Supplier((int)$key, $id_lang);
                $html = $sup->name;
                break;
            case 'PAG':
                $pag = Meta::getMetaByPage($key, $id_lang);
                $html = $pag['title'];
                break;
            case 'SHO':
                $shop = new Shop((int)$key);
                $html = $shop->name;
                break;
            default:
                $html = $link;
                break;
        }
        return $html;
    }
    public function hookdisplayTopColumn($params){
        $module_name = '';
        if (Validate::isModuleName(Tools::getValue('module')))
            $module_name = Tools::getValue('module');
        if (!empty($this->context->controller->php_self))
            $page_name = $this->context->controller->php_self;
        elseif (Tools::getValue('fc') == 'module' && $module_name != '')
            $page_name = 'module-'.$module_name.'-'.Tools::getValue('controller');
        // @retrocompatibility Are we in a module ?
        elseif (preg_match('#^' . preg_quote($this->context->shop->physical_uri, '#') .
            'modules/([a-zA-Z0-9_-]+?)/(.*)$#', $_SERVER['REQUEST_URI'], $m))
            $page_name = 'module-' . $m[1] . '-' . str_replace(array('.php', '/'), array('',
                    '-'), $m[2]);
        else {
            $page_name = Dispatcher::getInstance()->getController();
            $page_name = (preg_match('/^[0-9]/', $page_name) ? 'page_' . $page_name : $page_name);
        }
        if (strlen($page_name) <= 0 || $page_name == 'index')
            return '';
        switch ($page_name){
            case 'category':
                $page_key = 'CAT'.(int)Tools::getValue('id_category');
                //$pageBanner = $this->getBannerFrontPage($page_key,$id_lang,$id_shop);
                break;
            case 'product':
                $id_product = (int)Tools::getValue('id_product');
                $product = new Product($id_product, true, $this->context->language->id, $this->context->shop->id);
                $page_key = 'CAT'.(int)$product->id_category_default;
                //$pageBanner = $this->getBannerFrontPage($page_key,$id_lang,$id_shop);
                break;
            case 'cms':
                $page_key = 'CMS'.(int)Tools::getValue('id_cms');
                break;
            case 'supplier':
                $page_key = 'SUP'.(int)Tools::getValue('id_supplier');
                break;
            case 'manufacturer':
                $page_key = 'MAN'.(int)Tools::getValue('id_manufacturer');
                break;
            default:
                $page_key = 'PAG'.$page_name;
                break;
        }
        $id_shop = (int)$this->context->shop->id;
        $id_page = $this->getIdByPage($page_key,$id_shop);
        if (!$this->isCached('ovicpagebanner.tpl', $this->getCacheId($id_page)))
        {
            $id_lang = (int)$this->context->language->id;
            $pageBanner = array();
            $use_default = false;
            if ($id_page){
                $page_info = new PageBanner($id_page,$id_lang);
                if ($page_info->active){
                    $pageBanner = get_object_vars($page_info);
                    if ($page_info->type == 'slider'){
                        $pageBanner['slider'] = $this->getBannerByPage($id_page,$id_lang,true);
                    }
                }else{
                    if ($page_info->usedefault)
                        $use_default = true;
                }
            }else{
                $use_default = true;
            }
            if($use_default && $id_page = $this->getIdByPage('default',$id_shop)){
                $page_info = new PageBanner($id_page,$id_lang);
                if ($page_info->active){
                    $pageBanner = get_object_vars($page_info);
                    if ($page_info->type == 'slider'){
                        $pageBanner['slider'] = $this->getBannerByPage($id_page,$id_lang,true);
                    }
                }
            }
            if (isset($pageBanner['setting']))
                $pageBanner['setting'] = Tools::jsonDecode($pageBanner['setting'],true);
            $this->context->smarty->assign(array(
                'pageBanner' => $pageBanner
            ));
        }
        return $this->display(__FILE__, 'ovicpagebanner.tpl', $this->getCacheId($id_page));
    }
    public function hookdisplayHeader($params)
    {
        //$this->context->controller->addCSS($this->_path.'css/parallaxpage.css');
        //$this->context->controller->addJS($this->_path.'js/jquery.stellar.min.js');
        //$this->context->controller->addJS($this->_path.'js/parallaxpage.js');
        $this->context->controller->addJS($this->_path.'js/categoryslider.js');
    }
}