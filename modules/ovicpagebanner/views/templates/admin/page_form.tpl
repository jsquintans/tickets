<div class="panel">
    <div class="panel-heading">
        {l s=' Page banner' mod='ovicpagebanner'}
    </div>
    <form method="post" action="{$postUrl|escape:'htmlall':'UTF-8'}" enctype="multipart/form-data" class="item-form defaultForm  form-horizontal">
        <input type="hidden" name="id_page" value="{if isset($page_banner->id_page)}{$page_banner->id_page}{/if}"/>
        <div class="item-field form-group">
            <label class="control-label col-lg-3 ">Pages</label>
            <div class="col-lg-9">
                <div class="form-group">
                    <div class="col-lg-9">
                        <select class="form-control fixed-width-lg" name="page_select" id="page_select" >
                            <option value="default">{l s=' Default' mod='ovicpagebanner'}</option>
                            {$default_link_option}
                        </select>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>
        </div>
        <div class="item-field form-group ">
            <label class="control-label col-lg-3 ">{l s='Custom class' mod='ovicpagebanner'}</label>
            <div class="col-lg-9">
                <div class="form-group">
                    <div class="col-lg-10">
                        <input class="form-control" type="text" name="custom_class" value="{if isset($page_banner->setting.custom_class)}{$page_banner->setting.custom_class}{/if}"/>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>
        </div>
        <div class="item-field form-group ">
            <label for="active" class="control-label col-lg-3">{l s='Type' mod='ovicpagebanner'}</label>
            <div class="col-lg-9">
                <div class="form-group">
                    <div class="col-lg-9">
							<span class="switch prestashop-switch fixed-width-lg">
								<input type="radio" class="page_type" name="page_type" id="slider_type" {if isset($page_banner->type)&&$page_banner->type == 'slider' || !isset($page_banner->type)}checked="checked"{/if} value="slider"/>
								<label for="slider_type">Slider</label>
								<input type="radio" class="page_type" name="page_type" id="parallax_type" {if isset($page_banner->type)&&$page_banner->type == 'parallax'}checked="checked"{/if} value="parallax" />
								<label for="parallax_type">Parallax</label>
								<a class="slide-button btn"></a>
							</span>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>
        </div>
        <div class="parallax_info" {if isset($page_banner->type)&&$page_banner->type == 'slider' || !isset($page_banner->type)}style="display: none"{/if}>
            <div class="item-field form-group">
                <label class="control-label col-lg-3">{l s='Image' mod='ovicpagebanner'}</label>
                <div class="col-lg-9">
                    <div class="form-group">
                        <div class="col-lg-9">
                            {if isset($page_banner->setting.image) && $page_banner->setting.image|count_characters>0}
                                <img class="img-thumbnail" src="{$image_baseurl}{$page_banner->setting.image}" alt="" />
                            {/if}
                            <input type="file" name="banner_img" />
                            <input type="hidden" name="old_img" value="{if isset($page_banner->setting.image)}{$page_banner->setting.image}{/if}"/>
                        </div>
                        <div class="col-lg-2">
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-field form-group">
                <label class="control-label col-lg-3 ">{l s='Ratio' mod='ovicpagebanner'}</label>
                <div class="col-lg-9">
                    <div class="form-group">
                        <div class="col-lg-10">
                            <input class="form-control" type="text" name="ratio" value="{if isset($page_banner->setting.ratio)}{$page_banner->setting.ratio}{else}0.1{/if}"/>
                            <p class="help-block newline">
                                {l s='As with parallax elements, the ratio is relative to the natural scroll speed. For ratios lower than 1, to avoid jittery scroll performance' mod='ovicpagebanner'}
                            </p>
                        </div>
                        <div class="col-lg-2">
                        </div>
                    </div>
                </div>

            </div>
            <div class="item-field form-group" >
                <label class="control-label col-lg-3">{l s='Content' mod='ovicpagebanner'}</label>
                <div class="col-lg-9">
                    <div class="form-group">
                        {foreach from=$langguages.all item=lang}
                            <div class="translatable-field lang-{$lang.id_lang|escape:'htmlall':'UTF-8'}" {if $langguages.default_lang != $lang.id_lang}style="display:none"{/if}>
                                <div class="col-lg-10">
                                    <textarea class="rte" name="page_dec_{$lang.id_lang}" style="margin-bottom:10px; height:200px;" >{if isset($page_banner->description[$lang.id_lang])}{$page_banner->description[$lang.id_lang]}{/if}</textarea>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                                        {$lang.iso_code|escape:'htmlall':'UTF-8'}
                                        <i class="icon-caret-down"></i>
                                    </button>
                                    {$lang_ul}
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
        <div class="slider_info" {if isset($page_banner->type)&&$page_banner->type == 'parallax'}style="display: none"{/if}>
            <div class="item-field form-group">
                <label class="control-label col-lg-3 ">{l s='Speed' mod='ovicpagebanner'}</label>
                <div class="col-lg-9">
                    <div class="form-group">
                        <div class="col-lg-10">
                            <input class="form-control" type="text" name="speed" value="{if isset($page_banner->setting['speed'])}{$page_banner->setting['speed']}{else}500{/if}"/>
                            <p class="help-block newline">
                                {l s='The duration of the transition between two slides.' mod='ovicpagebanner'}
                            </p>
                        </div>
                        <div class="col-lg-2">
                        </div>
                    </div>
                </div>

            </div>
            <div class="item-field form-group">
                <label class="control-label col-lg-3 ">{l s='Pause' mod='ovicpagebanner'}</label>
                <div class="col-lg-9">
                    <div class="form-group">
                        <div class="col-lg-10">
                            <input class="form-control" type="text" name="pause" value="{if isset($page_banner->setting['pause'])}{$page_banner->setting['pause']}{else}5000{/if}"/>
                            <p class="help-block newline">
                                {l s='The delay between two slides.' mod='ovicpagebanner'}
                            </p>
                        </div>
                        <div class="col-lg-2">
                        </div>
                    </div>
                </div>

            </div>
            <div class="item-field form-group ">
                <label for="active" class="control-label col-lg-3">{l s='Auto play' mod='ovicpagebanner'}</label>
                <div class="col-lg-9">
                    <div class="form-group">
                        <div class="col-lg-9">
							<span class="switch prestashop-switch fixed-width-lg">
								<input type="radio" name="autoplay" id="play_on" {if isset($page_banner->setting['play'])&&$page_banner->setting['play'] == 1 || !isset($page_banner->setting['play'])}checked="checked"{/if} value="1"/>
								<label for="play_on">{l s='Yes' mod='ovicpagebanner'}</label>
								<input type="radio" name="autoplay" id="play_off" {if isset($page_banner->setting['play'])&&$page_banner->setting['play'] == 0 }checked="checked"{/if} value="0" />
								<label for="play_off">{l s='No' mod='ovicpagebanner'}</label>
								<a class="slide-button btn"></a>
							</span>
                        </div>
                        <div class="col-lg-2">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item-field form-group ">
            <label for="active" class="control-label col-lg-3">{l s='Active' mod='ovicpagebanner'}</label>
            <div class="col-lg-9">
                <div class="form-group">
                    <div class="col-lg-9">
							<span class="switch prestashop-switch fixed-width-lg">
								<input type="radio" class="change_active" name="active" id="active_on" {if isset($page_banner->active)&&$page_banner->active == 1 || !isset($page_banner->active)}checked="checked"{/if} value="1"/>
								<label for="active_on">{l s='Yes' mod='ovicpagebanner'}</label>
								<input type="radio" class="change_active" name="active" id="active_off" {if isset($page_banner->active)&&$page_banner->active == 0 }checked="checked"{/if} value="0" />
								<label for="active_off">{l s='No' mod='ovicpagebanner'}</label>
								<a class="slide-button btn"></a>
							</span>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>
        </div>
        <div class="item-field form-group use_default" {if isset($page_banner->active)&&$page_banner->active == 1 || !isset($page_banner->active)}style="display: none"{/if}>
            <label for="active" class="control-label col-lg-3">{l s='Use default page' mod='ovicpagebanner'}</label>
            <div class="col-lg-9">
                <div class="form-group">
                    <div class="col-lg-9">
							<span class="switch prestashop-switch fixed-width-lg">
								<input type="radio" name="usedefault" id="default_on" {if isset($page_banner->usedefault)&&$page_banner->usedefault == 1 || !isset($page_banner->usedefault)}checked="checked"{/if} value="1"/>
								<label for="default_on">{l s='Yes' mod='ovicpagebanner'}</label>
								<input type="radio" name="usedefault" id="default_off" {if isset($page_banner->usedefault)&&$page_banner->usedefault == 0 }checked="checked"{/if} value="0" />
								<label for="default_off">{l s='No' mod='ovicpagebanner'}</label>
								<a class="slide-button btn"></a>
							</span>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-9 col-lg-offset-3">
                <input type="hidden" name="submitpagebanner" value="1"/>
                <a href="{$postUrl|escape:'htmlall':'UTF-8'}" class="btn btn-default button-new-item-cancel"><i class="icon-remove"></i>{l s=' Cancel' mod='ovicpagebanner'}</a>
                <button type="submit" class="button-new-item-save btn btn-default" onclick="this.form.submit();"><i class="icon-save"></i>{l s=' Save' mod='ovicpagebanner'}</button>
            </div>
        </div>
    </form>
</div>
{addJsDef postUrl=$postUrl}