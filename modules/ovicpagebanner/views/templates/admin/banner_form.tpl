<div class="panel">
    <div class="panel-heading">
        {l s=' Banner Image' mod='ovicpagebanner'}
    </div>
    <form method="post" action="{$postUrl|escape:'htmlall':'UTF-8'}" enctype="multipart/form-data" class="item-form defaultForm  form-horizontal">
        <input type="hidden" name="id_banner" value="{if isset($banner_item->id_banner_image)}{$banner_item->id_banner_image}{/if}"/>
        <input type="hidden" name="id_page" value="{if isset($id_page)}{$id_page}{/if}"/>
        <input type="hidden" name="page_active" value="{$page_active}"/>
        <div class="item-field form-group">
            <label class="control-label col-lg-3">{l s='Image' mod='ovicpagebanner'}</label>
            <div class="col-lg-9">
                <div class="form-group">
                    {foreach from=$langguages.all item=lang}
                        <div class="translatable-field lang-{$lang.id_lang|escape:'htmlall':'UTF-8'}" {if $langguages.default_lang != $lang.id_lang}style="display:none"{/if}>
                            <div class="col-lg-9">
                                {if isset($banner_item->image[$lang.id_lang]) && $banner_item->image[$lang.id_lang]|count_characters>0}
                                    <img class="img-thumbnail" src="{$image_baseurl}{$banner_item->image[$lang.id_lang]}" alt="" />
                                {/if}
                                <input type="file" name="banner_img_{$lang.id_lang}" />
                                <input type="hidden" name="old_img_{$lang.id_lang}" value="{if isset($banner_item->image[$lang.id_lang])}{$banner_item->image[$lang.id_lang]}{/if}"/>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                                    {$lang.iso_code|escape:'htmlall':'UTF-8'}
                                    <i class="icon-caret-down"></i>
                                </button>
                                {$lang_ul}
                            </div>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="title item-field form-group">
            <label id="title_lb" class="control-label col-lg-3 ">{l s='Title' mod='ovicpagebanner'}</label>
            <div class="col-lg-9">
                <div class="form-group">
                    {foreach from=$langguages.all item=lang}
                        <div class="translatable-field lang-{$lang.id_lang|escape:'htmlall':'UTF-8'}" {if $langguages.default_lang != $lang.id_lang}style="display:none"{/if}>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" id="item_title" name="item_title_{$lang.id_lang}" value="{if isset($banner_item->title[$lang.id_lang])}{$banner_item->title[$lang.id_lang]}{/if}"/>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                                    {$lang.iso_code|escape:'htmlall':'UTF-8'}
                                    <i class="icon-caret-down"></i>
                                </button>
                                {$lang_ul}
                            </div>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
        <div id="link_field" class="link_detail">
            <div class="item-field form-group">
                <label class="control-label col-lg-3 ">{l s='Url' mod='ovicpagebanner'}</label>
                <div class="col-lg-9">
                    <div class="form-group">
                        <input type="hidden" name="link_value" id="link_value" class="link_value" value="{if isset($banner_item->url)}{$banner_item->url}{/if}"/>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" id="link_text" placeholder="http://" name="link" value="{if isset($link_text)}{$link_text}{/if}"/>
                            </div>
                            <div class="col-lg-2">
                                <p class="help-block">{l s='or' mod='advancetopmenu'}</p>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item-field form-group">
            <label class="control-label col-lg-3 ">{l s='Prestashop Link' mod='ovicpagebanner'}</label>
            <div class="col-lg-9">
                <div class="form-group">
                    <div class="col-lg-9">
                        <select class="form-control fixed-width-lg link_select" name="link_select" id="link_select" >
                            <option selected="selected">--</option>
                            {$default_link_option}
                        </select>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>
        </div>
        <div class="item-field form-group">
            <label id="legend_lb" class="control-label col-lg-3 ">{l s='Caption' mod='ovicpagebanner'}</label>
            <div class="col-lg-9">
                <div class="form-group">
                    {foreach from=$langguages.all item=lang}
                        <div class="translatable-field lang-{$lang.id_lang|escape:'htmlall':'UTF-8'}" {if $langguages.default_lang != $lang.id_lang}style="display:none"{/if}>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" id="item_legend" name="item_legend_{$lang.id_lang}" value="{if isset($banner_item->legend[$lang.id_lang])}{$banner_item->legend[$lang.id_lang]}{/if}"/>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                                    {$lang.iso_code|escape:'htmlall':'UTF-8'}
                                    <i class="icon-caret-down"></i>
                                </button>
                                {$lang_ul}
                            </div>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="html item-field form-group">
            <label class="control-label col-lg-3">{l s='Description' mod='ovicpagebanner'}</label>
            <div class="col-lg-9">
                <div class="form-group">
                    {foreach from=$langguages.all item=lang}
                        <div class="translatable-field lang-{$lang.id_lang|escape:'htmlall':'UTF-8'}" {if $langguages.default_lang != $lang.id_lang}style="display:none"{/if}>
                            <div class="col-lg-9">
                                <textarea class="rte" name="item_dec_{$lang.id_lang}" style="margin-bottom:10px; height:300px;" >{if isset($banner_item->description[$lang.id_lang])}{$banner_item->description[$lang.id_lang]}{/if}</textarea>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                                    {$lang.iso_code|escape:'htmlall':'UTF-8'}
                                    <i class="icon-caret-down"></i>
                                </button>
                                {$lang_ul}
                            </div>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="item-field form-group ">
            <label for="active" class="control-label col-lg-3">{l s='Active' mod='ovicpagebanner'}</label>
            <div class="col-lg-9">
                <div class="form-group">
                    <div class="col-lg-9">
							<span class="switch prestashop-switch fixed-width-lg">
								<input type="radio" name="active" id="active_on" {if isset($page_banner->active)&&$page_banner->active == 1 || !isset($page_banner->active)}checked="checked"{/if} value="1"/>
								<label for="active_on">Yes</label>
								<input type="radio" name="active" id="active_off" {if isset($page_banner->active)&&$page_banner->active == 0 }checked="checked"{/if} value="0" />
								<label for="active_off">No</label>
								<a class="slide-button btn"></a>
							</span>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-9 col-lg-offset-3">
                <input type="hidden" name="submitbanneritem" value=""/>
                <a href="{$postUrl|escape:'htmlall':'UTF-8'}" class="btn btn-default button-new-item-cancel"><i class="icon-remove"></i> Cancel</a>
                <button type="submit" class="button-new-item-save btn btn-default" onclick="this.form.submit();"><i class="icon-save"></i> Save</button>
            </div>
        </div>
    </form>
</div>