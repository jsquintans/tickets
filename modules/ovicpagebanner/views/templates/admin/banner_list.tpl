{addJsDef postUrl=$postUrl}
<div class="panel clearfix" >
    <h3><i class="icon-list-ul"></i>{l s=' Pages banner list' mod='ovicpagebanner'}
        <span class="panel-heading-action">
            <a class="list-toolbar-btn" href="{$postUrl|escape:'htmlall':'UTF-8'}&export=1">
                <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Export data" data-html="true">
                    <i class="process-icon-export"></i> {*{l s=' Add new' mod='ovicpagebanner'}*}
                </span>
            </a>
            <a class="list-toolbar-btn" href="{$postUrl|escape:'htmlall':'UTF-8'}&import=1">
        			<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Import data" data-html="true">
        				<i class="process-icon-import"></i> {*{l s=' Add new' mod='ovicpagebanner'}*}
        			</span>
            </a>
            <a class="list-toolbar-btn" href="{$postUrl|escape:'htmlall':'UTF-8'}&page_form">
        			<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Add new" data-html="true">
        				<i class="process-icon-new "></i> {*{l s=' Add new' mod='ovicpagebanner'}*}
        			</span>
            </a>
        </span>
    </h3>
    {if isset($errors) && $errors}
        {foreach from=$errors item=error name=errors}
            <div {if !isset($error)}style="display:none;"{/if} class="message alert alert-danger">
                <div>{if isset($error)}{$error|escape:'htmlall':'UTF-8'}{/if}</div>
            </div>
        {/foreach}
    {/if}
    {if isset($confirmation) && $confirmation}
        <div {if !isset($confirmation)}style="display:none;"{/if} class="message alert alert-success">
            <div>{if isset($confirmation)}{$confirmation|escape:'htmlall':'UTF-8'}{/if}</div>
        </div>
    {/if}
    <div class="main-container">
        {if isset($page_list) && $page_list|count > 0}
            <table class="table">
                <thead>
                <th width="15%">Page name</th>
                <th width="35%">&nbsp;</th>
                <th class="" width="20%">Type</th>
                <th class="right_col" width="30%">{l s='Action' mod='ovicpagebanner'}</th>
                </thead>
                <tbody>
                {foreach $page_list as $page_info}
                    <tr>
                        <td>
                            {if $page_info.type == 'slider'}
                                <a href="javascript:displayList('{$page_info.page_name}_banner_list');">{$page_info.title}</a>
                            {else}
                                <a href="javascript:void()');">{$page_info.title}</a>
                            {/if}
                        </td>
                        <td>
                            {if isset($page_info.setting.image) && $page_info.setting.image|count_characters>0 && $page_info.type == 'parallax'}
                                <img class="img-thumbnail" src="{$image_baseurl}{$page_info.setting.image}" alt="" />
                            {/if}
                        </td>
                        <td>{$page_info.type}</td>
                        <td class="right_col">
                            {if $page_info.active}
                                <a class="btn btn-success" href="{$postUrl}&bannerstatus&id_page={$page_info.id_page}" title="Enabled"><i class="icon-check"></i>{l s='Enabled' mod='ovicpagebanner'}</a>
                            {else}
                                <a class="btn btn-danger" href="{$postUrl}&bannerstatus&id_page={$page_info.id_page}" title="Disabled"><i class="icon-remove"></i>{l s='Disabled' mod='ovicpagebanner'}</a>
                            {/if}
                            <a href="{$postUrl}&page_form=1&page_select={$page_info.page_name}" title="Edit" class="btn btn-default">
                                        <i class="icon-edit"></i>{l s=' Edit' mod='ovicpagebanner'}
                                    </a>
                            <a class="btn btn-default" href="{$postUrl|escape:'htmlall':'UTF-8'}&removepage&id_page={$page_info.id_page}">
                                <i class="icon-trash"></i>{l s=' Delete' mod='ovicpagebanner'}
                            </a>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        {/if}
    </div>
</div>
{if isset($page_banners) && $page_banners|count > 0}
    {foreach $page_banners as $page_name => $banner_list}
        <div id="{$page_name}_banner_list" class="banner_list" {if $page_name == $page_active}style="display: block" {/if}>
            <div class="panel clearfix" >
                <h3><i class="icon-list-ul"></i>{l s=' banner list' mod='ovicpagebanner'}
                    <span class="panel-heading-action">
                        <a class="list-toolbar-btn" href="{$postUrl|escape:'htmlall':'UTF-8'}&banner_form&id_page={$banner_list.id_page}">
                            <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Add new" data-html="true">
                                <i class="process-icon-new "></i> {*{l s=' Add new' mod='ovicpagebanner'}*}
                            </span>
                        </a>
                    </span>
                </h3>
                {*<pre>{$banner_list.list|print_r}</pre>*}
                <div id="bannerContent">
                    <div class="ui-sortable" data-page="{$banner_list.id_page}" style="cursor: auto;">
                    {if (isset($banner_list.list) && $banner_list.list|count > 0)}
                        {foreach $banner_list.list as $banner_item}
                            <div id="banner_{$banner_item.id_banner_image}" data-id="{$banner_item.id_banner_image}" class="panel sortable">
                                <div class="row">
                                    <div class="col-lg-1">
                                        <span><i class="icon-arrows "></i></span>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="{$image_baseurl}{$banner_item.image}" alt="" class="img-thumbnail">
                                    </div>
                                    <div class="col-md-8">
                                        <h4 class="pull-left">

                                        </h4>
                                        <div class="btn-group-action pull-right">
                                            {if $banner_item.active}
                                            <a class="btn btn-success" href="{$postUrl}&itemstatus&id_banner={$banner_item.id_banner_image}" title="Enabled"><i class="icon-check"></i>{l s=' Enabled' mod='ovicpagebanner'}</a>
                                            {else}
                                                <a class="btn btn-danger" href="{$postUrl}&itemstatus&id_banner={$banner_item.id_banner_image}" title="Disabled"><i class="icon-remove"></i>{l s=' Disabled' mod='ovicpagebanner'}</a>
                                            {/if}
                                            <a class="btn btn-default" href="{$postUrl|escape:'htmlall':'UTF-8'}&banner_form&id_page={$banner_list.id_page}&id_banner={$banner_item.id_banner_image}">
                                                <i class="icon-edit"></i>
                                                Edit
                                            </a>

                                            <a class="btn btn-default" href="{$postUrl|escape:'htmlall':'UTF-8'}&removeitem&page_active={$page_name}&id_banner={$banner_item.id_banner_image}">
                                                <i class="icon-trash"></i>{l s=' Delete' mod='ovicpagebanner'}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                    </div>
                </div>
            </div>
        </div>
    {/foreach}
{/if}
