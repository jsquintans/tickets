{if isset($pageBanner) && $pageBanner && isset($pageBanner.active) && $pageBanner.active}
    {if $pageBanner.type =='parallax'}
        <section class="banner bg-parallax parallax_page" style="background-image: url({$link->getMediaLink("`$smarty.const._MODULE_DIR_`ovicpagebanner/img/`$pageBanner.setting.image|escape:'htmlall':'UTF-8'`")})" data-stellar-background-ratio="{$pageBanner.setting.ratio}">
            <div class="overlay"></div>
            <div class="container">
                <div class="banner-content text-center paralax_page">
                    {$pageBanner.description|unescape:'html'}
                </div>
            </div>
        </section>
    {else}
        <script type="text/javascript">
            var categoryslider_loop={$pageBanner.setting.play|intval};
            var categoryslider_speed={$pageBanner.setting.speed|intval};
            var categoryslider_pause={$pageBanner.setting.pause|intval};
        </script>
        <div id="responsive_slides" class="div_full_width clearfix">
            <div class="inner_wrapper">
                <div class="callbacks_container clearBoth">
                    <ul id="categoryslider">
                        {foreach from=$pageBanner.slider item=slide}
                            {if $slide.active}
                                <li>
                                    <img class="img-responsive" src="{$smarty.const._MODULE_DIR_}/ovicpagebanner/img/{$slide.image|escape:'htmlall':'UTF-8'}" alt="{$slide.legend|escape:'htmlall':'UTF-8'}"  />
                                    <div class="cate-slide-decs">
                                        {$slide.description|unescape:'html'}
                                    </div>
                                </li>
                            {/if}
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
    {/if}
{/if}