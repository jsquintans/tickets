<?php
/**
 * User: HoangGia
 * Email: vn.hoanggia223@gmail.com
 */
class BannerImage extends ObjectModel
{
    public $id_banner_image;
    public $id_page;
    public $title;
    public $description;
    public $url;
    public $legend;
    public $image;
    public $active;
    public $position;
    public $id_shop;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'ovic_banner_image',
        'primary' => 'id_banner_image',
        'multilang' => true,
        'fields' => array(
            'id_page' =>        array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            'active' =>			array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'position' =>		array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            'url' =>			array('type' => self::TYPE_STRING, 'validate' => 'isUrl', 'size' => 255),
            // Lang fields
            'description' =>	array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 4000),
            'title' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255),
            'legend' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 255),
            'image' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 255),
        )
    );
}