<?php
    /**
     * User: HoangGia
     * Email: vn.hoanggia223@gmail.com
     */

class PageBanner extends ObjectModel
{
    public $id_page;
    public $type;
    public $page_name;
    public $active;
    public $id_shop;
    public $setting;
    public $usedefault;
    public $description;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'ovic_page_banner',
        'primary' => 'id_page',
        'multilang' => true,
        'fields' => array(
            'type' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true, 'size' => 20),
            'page_name' => array('type' => self::TYPE_STRING, 'validate' => 'isString','required' => true, 'size' => 255),
            'setting' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'size' => 4000),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'usedefault' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            // Lang fields
            'description' =>	array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 4000),
        )
    );
}
