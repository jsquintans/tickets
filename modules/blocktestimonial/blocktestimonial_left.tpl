{if isset($testimonials) && $testimonials && $testimonials|@count > 0}
{assign var="current_option" value=Configuration::get('OVIC_CURRENT_OPTION')}
<!-- MODULE Block otutestimonial -->
<div id="testimonial_block_left" class="block">
    <h4 class="title_block">{$TESTIMONIAL_TITLE}</h4>
    <div class="block_content" {*}{if $TESTIMONIAL_IMG && $TESTIMONIAL_IMG|count_characters > 0 && isset($current_option)&& ($current_option !=3)} style="background-image: url('{$imgpath}{$TESTIMONIAL_IMG}');"{/if}{*}>
        <div id="block_testimonial_block_slide" class="clearfix effect-zoomOut">	
    		{foreach from=$testimonials item=info}
    			<div class="slide_item"> 
                    <div class="wrapper" >
                        <div class="content">
                            <div class="align-image">
                                <img class="block_testtimonial_avat" src="{$module_dir}img/{$info.file_name}" alt="{$info.text|escape:html:'UTF-8'}" />
                            </div> 
                            <p class="block_testimonial_content">"{$info.text|escape:html:'UTF-8'}"</p>
                            <hr />
                            <p class="block_testimonial_name">{$info.name|escape:html:'UTF-8'}&nbsp;&ndash;&nbsp;<span class="block_testimonial_company">{$info.company|escape:html:'UTF-8'}</span></p>
                        </div>
                    </div>
                </div>
     		{/foreach} 
      	</div>
    </div>
</div>
<!-- /MODULE Block otutestimonial -->
{/if}