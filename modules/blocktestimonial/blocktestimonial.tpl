{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $testimonials|@count > 0}
<!-- MODULE Block otutestimonial -->
<div id="testimonial_block" class="col-sm-4 home-block clearfix">
    <h1 class="block-title">{$TESTIMONIAL_TITLE}</h1>
    <div class="block-content">
        <div id="block_testimonial_block_slide" class="effect-zoomOut">	
    		{foreach from=$testimonials item=info}
    			<div class="slide_item"> 
                    <div class="wrapper" >
                        <div class="content">
                            <p class="block_testimonial_content">"{$info.text|escape:html:'UTF-8'}"</p>
                            <hr />
                            <p class="block_testimonial_name">{$info.name|escape:html:'UTF-8'}&nbsp;&ndash;&nbsp;<span class="block_testimonial_company">{$info.company|escape:html:'UTF-8'}</span></p>
                        </div>
                    </div>
                </div>
     		{/foreach} 
      	</div>
    </div>
</div>
<!-- /MODULE Block otutestimonial -->
{/if}