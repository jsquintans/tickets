{if $testimonials|@count > 0}
<!-- MODULE Block ovictestimonial -->
<div class="block-testimonials div_full_width">
    <div class="container">
        <div class="testimonial" >
        {foreach from=$testimonials item=info}
            <div class="item">
                <div class="customer">
                    <img class="avatar" src="{$modules_dir}blocktestimonial/img/{$info.file_name}" alt="" title="" />
                </div> 
                <div class="testimonial_text">
                    <p>{$info.name|escape:html:'UTF-8'}</p>
                    <span>{$info.text|escape:html:'UTF-8'}</span>
                </div>
            </div>
        {/foreach}
        </div>			               
    </div>
</div>
<!-- /MODULE Block ovictestimonial -->
{/if}