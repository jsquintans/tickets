$(document).ready(function() {
    $("#block_testimonial_block_slide").owlCarousel({
      navigation : true, // Show next and prev buttons
      slideSpeed: 500,
      singleItem:true,
      addClassActive:true
    });
    $('.block-testimonials .testimonial').owlCarousel({
        items:1,  
        autoplay:true,
        autoplaySpeed: 1500,
        //animateIn: 'fadeInRight',
        margin:50,
        loop:true,
        autoHeight:true
    });
});