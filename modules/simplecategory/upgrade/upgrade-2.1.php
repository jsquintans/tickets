<?php

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_2_1($object)
{
	$db = Db::getInstance();
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."simplecategory_module_lang` LIKE 'id_shop'"))
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."simplecategory_module_lang` 	ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `module_id`");
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."simplecategory_module_product` LIKE 'id_shop'"))
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."simplecategory_module_product` ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `module_id`");
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."simplecategory_group` LIKE 'id_shop'"))
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."simplecategory_group` ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `id`");
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."simplecategory_group_lang` LIKE 'id_shop'"))
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."simplecategory_group_lang` ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `group_id`");
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."simplecategory_group_product` LIKE 'id_shop'"))
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."simplecategory_group_product` ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `group_id`");
		
	return true;
}
