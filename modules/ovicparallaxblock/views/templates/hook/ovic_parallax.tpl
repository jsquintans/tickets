<div class="container parallax-container">
    <div class=" a-center parallax newsletter-block1 small-block div_full_width" style="background-image: url({$link->getMediaLink("`$smarty.const._MODULE_DIR_`ovicparallaxblock/img/`$background|escape:'htmlall':'UTF-8'`")})" data-stellar-background-ratio="{$ratio}">
        {$content}
    </div>
</div>