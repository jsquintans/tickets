<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class BrandsSlider extends Module
{
    public function __construct()
    {
        $this->name = 'brandsslider';
        $this->tab = 'front_office_features';
        $this->version = 1.1;
		$this->author = 'OvicSoft';
		$this->bootstrap = true;
		$this->need_instance = 0;

        parent::__construct();

		$this->displayName = $this->l('Ovic - Brands slider block');
        $this->description = $this->l('Displays a block listing product manufacturers and/or brands.');
    }

	public function install()
	{
		Configuration::updateValue('OVIC_BRAND_TITLE','Our Brands');
		$BRAND_DESC = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				<p>Sed porttitor lectus nibh.</p>
				<p>Quisque velit nisi, pretium ut lacinia in, elementum id enim.</p>';
		Configuration::updateValue('OVIC_BRAND_DESC',$BRAND_DESC,true);
		return parent::install() && $this->registerHook('displayHeader') && $this->registerHook('displayFooter');
    }

    public function hookDisplayFooter($params)
	{
		if (!$this->isCached('brandsslider.tpl', $this->getCacheId())){
		    $manufacturers = Manufacturer::getManufacturers();
			foreach ($manufacturers as &$item)
				$item['image'] = $item['id_manufacturer'].'-manusize.jpg';
            $this->smarty->assign(array(
				'manufacturers' => $manufacturers,
				'brand_title' => ConfigurationCore::get('OVIC_BRAND_TITLE',$this->context->language->id),
				'brand_desc' => ConfigurationCore::get('OVIC_BRAND_DESC',$this->context->language->id)
			));
		}
		return $this->display(__FILE__, 'brandsslider.tpl', $this->getCacheId());
	}

	public function hookHeader($params)
	{
        // CSS in global.css file
        //$this->context->controller->addCSS(($this->_path).'brandsslider.css', 'all');
        $this->context->controller->addJS($this->_path.'brandsslider.js');
	}
	public function getContent()
	{
		if (Tools::isSubmit('submitBrands'))
		{
			$languages = Language::getLanguages(false);
			$BRAND_TITLE = array();
			$BRAND_DESC = array();
			foreach ($languages as $lang)
			{
				$BRAND_TITLE[$lang['id_lang']] = Tools::getValue('brand_title_'.$lang['id_lang']);
				$BRAND_DESC[$lang['id_lang']] = Tools::getValue('brand_desc_'.$lang['id_lang']);
			}
			Configuration::updateValue('OVIC_BRAND_TITLE',$BRAND_TITLE);
			Configuration::updateValue('OVIC_BRAND_DESC',$BRAND_DESC,true);
			//$PS_MYMODULE_OPTION1 = Tools::getValue('PS_BRANDSLIDER_TITLE');
			//Configuration::updateValue('PS_BRANDSLIDER_TITLE', Tools::getValue('PS_BRANDSLIDER_TITLE'));
		}
		return $this->displayForm();
	}

	public function displayForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Brands information'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Title'),
						'name' => 'brand_title',
						'lang' => true,
					),
					array(
						'type' => 'textarea',
						'label' => $this->l('Description'),
						'name' => 'brand_desc',
						'autoload_rte' => true,
						'lang' => true,
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->module = $this;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitBrands';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->tpl_vars = array(
			'base_url' => $this->context->shop->getBaseURL(),
			'language' => array(
				'id_lang' => $language->id,
				'iso_code' => $language->iso_code
			),
			'fields_value' => $this->getFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
		);

		return $helper->generateForm(array($fields_form));
	}
	public function getFieldsValues()
	{
		$fields = array();
		$languages = Language::getLanguages(false);
		foreach ($languages as $lang)
		{
			$fields['brand_title'][$lang['id_lang']] = Configuration::get('OVIC_BRAND_TITLE',$lang['id_lang']);
			$fields['brand_desc'][$lang['id_lang']] = Configuration::get('OVIC_BRAND_DESC',$lang['id_lang']);
		}
		return $fields;
	}
}
