$(document).ready(function() {
 
  // OWL 1.3.2
 /*
  $("#brand_list").owlCarousel({
      autoPlay: 3000, //Set AutoPlay to 3 seconds
      nav : true,
      items : 6,
      margin: 30
 
  });
  */
  $('#brand_list').owlCarousel({
    loop:true,
    margin:30,
    autoplay: true,
    nav:false,
    responsive:{
        0:{
            items:2
        },
        480:{
            items:4
        },
        768:{
            items:5
        },
        992:{
            items:6
        },
        1200:{
            items:6
        }
    }
})
  
  
  
 
});