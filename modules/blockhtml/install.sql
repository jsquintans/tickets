DROP TABLE IF EXISTS `PREFIX_htmlobject`;
CREATE TABLE `PREFIX_htmlobject` (
  `id_htmlobject` int(6) NOT NULL AUTO_INCREMENT,
  `hook_postition` int(2) NOT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id_htmlobject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `PREFIX_htmlobject_lang`;
CREATE TABLE `PREFIX_htmlobject_lang` (
  `id_htmlobject` int(6) NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id_htmlobject`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `PREFIX_htmlobject_shop`;
CREATE TABLE `PREFIX_htmlobject_shop` (
  `id_htmlobject` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_htmlobject`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;