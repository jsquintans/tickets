{if isset($item) && !empty($item)}
<div class="blockhtml_{$hook_position}">
    {if isset($item.content)}
        {$item.content|html_entity_decode}
    {/if}
</div>
{/if}