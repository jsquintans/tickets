<?php
if (!defined('_PS_VERSION_'))
	exit;
include_once (dirname(__file__) . '/class/HtmlObject.php');

class BlockHtml extends Module
{
    const INSTALL_SQL_FILE = 'install.sql';
    protected static $hookArr;
    public static $sameDatas = '';		
	protected static $tables = array('htmlobject'=>'position','htmlobject_lang'=>'lang',  'htmlobject_shop'=>'');
    public function __construct()
    {
        $this->name = 'blockhtml';
        $this->tab = 'front_office_features';
        $this->version = '2.0';
        $this->author = 'OvicSoft';
        parent::__construct();
        $this->displayName = $this->l('Ovic - Block HTML');
        $this->description = $this->l('With this module you can put the HTML/JavaScript/CSS code anywhere you want');
        $this->bootstrap = true;
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        if (!isset(BlockHtml::$hookArr)){
            BlockHtml::$hookArr = Array('displayHome','displayTop','displayLeftColumn','displayRightColumn','displayFooter','displayNav','displayTopColumn','CustomHtml','Contactform','CustomHtml2','CustomHtml3','CustomHtml4','CustomHtml5','CustomHtml6','CustomHtml7','CustomHtml8');
        }
        self::$sameDatas = dirname(__FILE__).'/samedatas/';
    }
    public function install()
    {//
        if (!file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE)) return false;
        else if (!$sql = file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE)) return false;        
        $sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
        $sql = preg_split("/;\s*[\r\n]+/", trim($sql));
        foreach ($sql as $query)
            if (!Db::getInstance()->execute(trim($query))) return false;
            
        if (!parent::install() ||
            !$this->registerHook('displayHeader') ||
            !$this->registerHook('displayBackOfficeHeader') ||
            !$this->registerHook('displayHome') ||
            !$this->registerHook('displayTop') ||
			!$this->registerHook('displayLeftColumn') ||
			!$this->registerHook('displayRightColumn') ||
			!$this->registerHook('displayFooter') ||
            !$this->registerHook('displayNav') ||
            !$this->registerHook('displayTopColumn') ||
            !$this->registerHook('CustomHtml') ||
            !$this->registerHook('Contactform') ||
            !$this->registerHook('CustomHtml2') ||
            !$this->registerHook('CustomHtml3') ||
            !$this->registerHook('CustomHtml4') ||
            !$this->registerHook('CustomHtml5') ||
            !$this->registerHook('CustomHtml6') ||
            !$this->registerHook('CustomHtml7') ||
            !$this->registerHook('CustomHtml8')            
        ){
            return false;
        }
        //$this->importSameData();
        return true;
    }
    
    public function uninstall()
    {
        if (!parent::uninstall() || !$this->uninstallDB()) return false;
        return true;
    }
    private function uninstallDb()
    {
        Db::getInstance()->execute('DROP TABLE `' . _DB_PREFIX_ . 'htmlobject`');
        Db::getInstance()->execute('DROP TABLE `' . _DB_PREFIX_ . 'htmlobject_lang`');
        Db::getInstance()->execute('DROP TABLE `' . _DB_PREFIX_ . 'htmlobject_shop`');
        return true;
    }
    
    public function getContent()
    {	
        if(Tools::getValue('data-export')){
            $this->exportSameData();
			//$this->exportDataDemo();
			echo $this->l('Export data success!');
			die;
		}
		if(Tools::getValue('data-import')){
			$this->importSameData();
            //$this->installDataDemo();
			echo $this->l('Install data demo success!');
			die;
		}
        $output = '';
        $errors = array();
        $languages = Language::getLanguages();
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $id_lang = $this->context->language->id;
        if (Tools::getValue('confirm_msg')){
            $output .= $this->displayConfirmation(Tools::getValue('confirm_msg'));
        }
        if (Tools::isSubmit('submitGlobal'))
        {
                $id_position = (int)Tools::getValue('id_position',1);
                $html_id = $this->getHtmlIdByPosition($id_position);
                if ($html_id && count($html_id)>0)
                    $htmlObject = new HtmlObject($html_id['id_htmlobject']);
                else
                    $htmlObject = new HtmlObject();

                $htmlObject->active = (int)Tools::getValue('active');
                //$hook_postition
                $htmlObject->hook_postition = (int)Tools::getValue('id_position');
                foreach ($languages as $language)
                {
                    $htmlObject->title[$language['id_lang']] = Tools::getValue('item_title_' . $language['id_lang']);
                    $htmlObject->content[$language['id_lang']] = Tools::getValue('item_html_' . $language['id_lang']);
                }
                if (!$errors || count($errors) < 1)
                {
                    /* Update */
                    if ($html_id && count($html_id)>0)
                    {
                        if (!$htmlObject->update()) $errors[] = $this->displayError($this->l('The Advertising slide could not be updated.'));
                    } elseif (!$htmlObject->add()) $errors[] = $this->displayError($this->l('The Advertising slide could not be add.'));
                    /* Adds */
                }
                if (!isset($errors) || count($errors) < 1){
                    if ($html_id && count($html_id)>0)
                        $confirm_msg = $this->l('Slide successfully updated.');
                    else
                        $confirm_msg = $this->l('New slide successfully added.');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('blockhtml.tpl'));
                    Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->
                                name . '&token=' . Tools::getAdminTokenLite('AdminModules').'&confirm_msg='.$confirm_msg.'&id_position='.$id_position);
                }
        }
        if (isset($errors) && count($errors)) $output .= $this->displayError(implode('<br />',
                $errors));
        return $output.$this->displayForm();
    }
    private function addNoteCData(&$item, $text){ 
	   $node= dom_import_simplexml($item); 
	   $doc = $node->ownerDocument; 
	   $node->appendChild($doc->createCDATASection($text)); 
	}	
	
	private function exportWidgetLang($itemId=0, &$parent){
		$langId = Context::getContext()->language->id;
		$result = '';
		if($itemId){
			$items = Db::getInstance()->executeS("Select * From "._DB_PREFIX_."htmlobject_lang Where id_htmlobject = $itemId");
			if($items){
				foreach($items as $item){
					$langIso = LanguageCore::getIsoById($item['id_lang']);
					$language = $parent->addChild('language');
					// lang iso
					$language->addChild('lang_iso', $langIso);
					// item content				
					$content = $language->addChild('content');
					$this->addNoteCData($content, $item['content']); 
				}
			}
		}
		return true;
	} 
	
	public function exportSameData($directory=''){
		$shopId = $this->context->shop->id;
	   	if($directory) self::$sameDatas = $directory;
        $langId = Context::getContext()->language->id;		
        $currentOption = Configuration::get('OVIC_CURRENT_DIR');
        if($currentOption) $currentOption .= '.';
        else $currentOption = '';
		$items = Db::getInstance()->executeS("
        Select h.* From "._DB_PREFIX_."htmlobject as h 
        INNER JOIN `"._DB_PREFIX_."htmlobject_shop` hs ON h.id_htmlobject = hs.id_htmlobject  
        where hs.id_shop = $shopId");
		if($items){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><widgets></widgets>'); 			
			foreach($items as $item){
				$widget = $xml->addChild('widget');				
				$widget->addChild('hook_postition', $item['hook_postition']); 
                $widget->addChild('active', $item['active']);								
				$languages = $widget->addChild('languages'); 
				$this->exportWidgetLang($item['id_htmlobject'], $languages); 				
			}						
			$file = self::$sameDatas.'store'.$shopId.'.'.$currentOption.'blockhtml.xml';
			$xml->asXML($file);			
		}
		
		
		return true;
	}
    public  function importSameData($directory='', $file=''){
        if($directory) self::$sameDatas = $directory;
        $shopId = $this->context->shop->id;
        $currentOption = Configuration::get('OVIC_CURRENT_DIR');
        if($currentOption) $currentOption .= '.';
        else $currentOption = '';
        if(!$file){
            $file = self::$sameDatas.'store'.$shopId.'.'.$currentOption.$this->name.'.xml';           
        }else{
            if(!file_exists($file))
                $file = self::$sameDatas.'store'.$shopId.'.'.$currentOption.$this->name.'.xml';
        } 
        if(!file_exists($file))
            $file = self::$sameDatas.$currentOption.$this->name.'.xml';
        if(!file_exists($file))
            $file = self::$sameDatas.$this->name.'.xml';
                
        if(file_exists($file)){
            $db = Db::getInstance(_PS_USE_SQL_SLAVE_);
            $languages = $db->executeS("Select id_lang, iso_code From "._DB_PREFIX_."lang Where active = 1");           
            $xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);
         
            if(isset($xml->widget)){ 
                if(count($xml->widget) >0){
                    $db->execute("Delete h From `"._DB_PREFIX_."htmlobject` h 
                                  INNER JOIN `"._DB_PREFIX_."htmlobject_shop` hs ON h.id_htmlobject = hs.id_htmlobject      
                                  WHERE hs.id_shop =".$shopId);
                    $db->execute("Delete hl From `"._DB_PREFIX_."htmlobject_lang` hl 
                                  INNER JOIN `"._DB_PREFIX_."htmlobject_shop` hs ON hl.id_htmlobject = hs.id_htmlobject      
                                  WHERE hs.id_shop =".$shopId);
                    $db->execute("Delete From `"._DB_PREFIX_."htmlobject_shop` 
                                  WHERE id_shop =".$shopId);
                    foreach($xml->widget as $widget){ 
                        $widgetInsert = array( 
                            'hook_postition' =>  (string)$widget->hook_postition, 
                            'active'         =>  (int)$widget->active, 
                        );
                        
                        if($db->insert('htmlobject',$widgetInsert)){ 
                            $insertId = $db->Insert_ID();
                            if(isset($widget->languages->language) && (count($widget->languages->language) > 0)){
                                $this_languages = array();
                                $langDefault = array();                     
                                if(isset($widget->languages->language) && count($widget->languages->language) >0){
                                    foreach($widget->languages->language as $language){
                                        $lcontent = Tools::htmlentitiesDecodeUTF8($language->content);
                                        $lcontent = Tools::htmlentitiesUTF8($lcontent);
                                        if(!$langDefault) $langDefault = array(
                                            'content'=>$lcontent,
                                            'title'=>(string)$language->title, 
                                        );
                                        $this_languages[(string)$language->lang_iso] = array(
                                            'content'=>$lcontent,
                                            'title'=>(string)$language->title,  
                                        );  
                                    }
                                }           
                                $arrInsertLangs = array(); 
                                foreach($languages as $language){
                                    $lang_iso = $language['iso_code'];
                                    if($this_languages){
                                        if(key_exists($lang_iso, $this_languages)){
                                            $arrInsertLangs[] = array(
                                                'id_htmlobject'=>$insertId,
                                                'id_lang'=>$language['id_lang'],
                                                'title'=>$this_languages[$lang_iso]['title'],
                                                'content'=>$this_languages[$lang_iso]['content'] 
                                            );
                                        }else{
                                            $arrInsertLangs[] = array(
                                                'id_htmlobject'=>$insertId,
                                                'id_lang'=>$language['id_lang'],
                                                'title'=>$this_languages[$lang_iso]['title'],
                                                'content'=>$this_languages[$lang_iso]['content'], 
                                            );
                                        }
                                    }else{
                                        $arrInsertLangs[] = array(
                                            'id_htmlobject'=>$insertId,
                                            'id_lang'=>$language['id_lang'],
                                            'title'=>$this_languages[$lang_iso]['title'],
                                            'content'=>$this_languages[$lang_iso]['content'], 
                                        );
                                    }
                                }  
                                if($arrInsertLangs) $db->insert('htmlobject_lang', $arrInsertLangs);
                                $db->insert('htmlobject_shop',array('id_htmlobject'=>$insertId, 'id_shop' => $shopId));
                            } 
                        }                       
                    }
                }
            }           
        }
        return true;
    }
    /* Function for v1.0
    public function importSameData($directory=''){
		//foreach(self::$tables as $table=>$value){
			//Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.$table);
		//}
        //foreach(self::$tables as $table=>$value) Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.$table);
        if($directory) self::$sameDatas = $directory;
		$langs = Db::getInstance()->executeS("Select id_lang From "._DB_PREFIX_."lang Where active = 1");			
		if(self::$tables){
		  
          $curent_option = Configuration::get('OVIC_CURRENT_DIR');
            if($curent_option) $curent_option .= '.';         
            else $curent_option = '';
			
            foreach(self::$tables as $table=>$value){
                
				if (file_exists(self::$sameDatas.$curent_option.$table.'.sql')){
				    Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.$table);
					$sql = file_get_contents(self::$sameDatas.$curent_option.$table.'.sql');
					$sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
					$sql = preg_split("/;\s*[\r\n]+/", trim($sql));
					if($value == 'lang'){
						foreach ($sql as $query){
							foreach($langs as $lang){								
								$query_result = str_replace('id_lang', $lang['id_lang'], trim($query));
								Db::getInstance()->execute($query_result);
							}
						}				
                    }else if($value == 'position-test'){
                        foreach ($sql as $query){                            
                            if(strpos($query, 'displayHeader')){
								$hookId = hook::getIdByName('displayHeader');                                        
								$query = str_replace('displayHeader', $hookId, trim($query));	
							}elseif(strpos($query, 'displayBackOfficeHeader')){
								$hookId = hook::getIdByName('displayBackOfficeHeader');                                        
								$query = str_replace('displayBackOfficeHeader', $hookId, trim($query));	
							}elseif(strpos($query, 'displayHome')){
								$hookId = hook::getIdByName('displayHome');                                        
								$query = str_replace('displayHome', $hookId, trim($query));	
							}elseif(strpos($query, 'displayLeftColumn')){
								$hookId = hook::getIdByName('displayLeftColumn');
								$query = str_replace('displayLeftColumn', $hookId, trim($query));	
							}elseif(strpos($query, 'displayRightColumn')){
								$hookId = hook::getIdByName('displayRightColumn');                                        
								$query = str_replace('displayRightColumn', $hookId, trim($query));	
							}elseif(strpos($query, 'displayFooter')){
								$hookId = hook::getIdByName('displayFooter');                                        
								$query = str_replace('displayFooter', $hookId, trim($query));	
							}elseif(strpos($query, 'displayNav')){
								$hookId = hook::getIdByName('displayNav');                                        
								$query = str_replace('displayNav', $hookId, trim($query));	
							}elseif(strpos($query, 'displayTopColumn')){
								$hookId = hook::getIdByName('displayTopColumn');                                        
								$query = str_replace('displayTopColumn', $hookId, trim($query));	
							}elseif(strpos($query, 'CustomHtml')){
								$hookId = hook::getIdByName('CustomHtml');                                        
								$query = str_replace('CustomHtml', $hookId, trim($query));	
							}elseif(strpos($query, 'Contactform')){
								$hookId = hook::getIdByName('Contactform');
								$query = str_replace('Contactform', $hookId, trim($query));	
							}elseif(strpos($query, 'displayTop')){
								$hookId = hook::getIdByName('displayTop');
								$query = str_replace('displayTop', $hookId, trim($query));	
							}elseif(strpos($query, 'CustomHtml2')){
								$hookId = hook::getIdByName('CustomHtml2');                                        
								$query = str_replace('CustomHtml2', $hookId, trim($query));	
							}elseif(strpos($query, 'CustomHtml3')){
								$hookId = hook::getIdByName('CustomHtml3');                                        
								$query = str_replace('CustomHtml3', $hookId, trim($query));	
							}elseif(strpos($query, 'CustomHtml4')){
								$hookId = hook::getIdByName('CustomHtm4l');                                        
								$query = str_replace('CustomHtml4', $hookId, trim($query));	
							}elseif(strpos($query, 'CustomHtml5')){
								$hookId = hook::getIdByName('CustomHtml5');                                        
								$query = str_replace('CustomHtml5', $hookId, trim($query));	
							}elseif(strpos($query, 'CustomHtml6')){
								$hookId = hook::getIdByName('CustomHtml6');                                        
								$query = str_replace('CustomHtml6', $hookId, trim($query));	
							}elseif(strpos($query, 'CustomHtml7')){
								$hookId = hook::getIdByName('CustomHtml7');                                        
								$query = str_replace('CustomHtml7', $hookId, trim($query));	
							}elseif(strpos($query, 'CustomHtml8')){
								$hookId = hook::getIdByName('CustomHtml8');                                        
								$query = str_replace('CustomHtml8', $hookId, trim($query));	
                            }
                            if($query)
                                Db::getInstance()->execute($query);								
						}
					}else{
						foreach ($sql as $query){
							if (!Db::getInstance()->execute(trim($query))) return false;
						}
					}
				}
				
			}
		}
		return true;
	}  
    */
    
    public function AjaxCall($id_position){
        $html_id = $this->getHtmlIdByPosition($id_position);
        if ($html_id && count($html_id)>0)
            $htmlObject = new HtmlObject($html_id['id_htmlobject']);
        else
            $htmlObject = new HtmlObject();
        $iso = Language::getIsoById((int)($this->context->language->id));
        $isoTinyMCE = (file_exists(_PS_ROOT_DIR_ . '/js/tiny_mce/langs/' . $iso . '.js') ?
            $iso : 'en');
        $ad = dirname($_SERVER["PHP_SELF"]);
        $html ='<script type="text/javascript">
    			var iso = \'' . $isoTinyMCE . '\' ;
    			var pathCSS = \'' . _THEME_CSS_DIR_ . '\' ;
    			var ad = \'' . $ad . '\' ;
    			$(document).ready(function(){
    			tinySetup({
    				editor_selector :"rte",
            		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,fontselect,fontsizeselect",
            		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,codemagic,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
            		theme_advanced_toolbar_location : "top",
            		theme_advanced_toolbar_align : "left",
            		theme_advanced_statusbar_location : "bottom",
            		theme_advanced_resizing : false,
                    extended_valid_elements: \'pre[*],script[*],style[*]\',
                    valid_children: "+body[style|script],pre[script|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
                    valid_elements : \'*[*]\',
                    force_p_newlines : false,
                    cleanup: false,
                    forced_root_block : false,
                    force_br_newlines : true
    				});
    			});</script>';
        $languages = Language::getLanguages();
        $lang_ul = '<ul class="dropdown-menu">';
        foreach ($languages as $lg){
            $lang_ul .='<li><a href="javascript:hideOtherLanguage('.$lg['id_lang'].');" tabindex="-1">'.$lg['name'].'</a></li>';
        }
        $lang_ul .='</ul>';
        $this->context->smarty->assign(array(
            'item' => $htmlObject,
            'lang_ul' => $lang_ul,
            'langguages' => array(
				'default_lang' => (int)Configuration::get('PS_LANG_DEFAULT'),
				'all' => $languages,
				'lang_dir' => _THEME_LANG_DIR_)
        ));
        return Tools::jsonEncode($html.$this->display(__file__, 'views/templates/admin/html_content.tpl'));
    }
    private function displayForm()
    {
        $id_position = (int)Tools::getValue('id_position',1);
        $html_id = $this->getHtmlIdByPosition($id_position);
        if ($html_id && count($html_id)>0)
            $htmlObject = new HtmlObject($html_id['id_htmlobject']);
        else
            $htmlObject = new HtmlObject();
        $languages = Language::getLanguages();

        $lang_ul = '<ul class="dropdown-menu">';
        foreach ($languages as $lg){
            $lang_ul .='<li><a href="javascript:hideOtherLanguage('.$lg['id_lang'].');" tabindex="-1">'.$lg['name'].'</a></li>';
        }

        $lang_ul .='</ul>';
        $this->context->smarty->assign(array(
            'item' => $htmlObject,
            'lang_ul' => $lang_ul,
            'hookArr' => BlockHtml::$hookArr,
            'ajaxUrl' => $this->_path.'ajaxhtml.php',
            'default_position' => $id_position,
            'admin_templates' => _PS_MODULE_DIR_.$this->name.'/views/templates/admin/',
            'postAction' => AdminController::$currentIndex .'&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'),
            'langguages' => array(
				'default_lang' => (int)Configuration::get('PS_LANG_DEFAULT'),
				'all' => $languages,
				'lang_dir' => _THEME_LANG_DIR_)
            ));
        $iso = Language::getIsoById((int)($this->context->language->id));
        $isoTinyMCE = (file_exists(_PS_ROOT_DIR_ . '/js/tiny_mce/langs/' . $iso . '.js') ?
            $iso : 'en');
        $ad = dirname($_SERVER["PHP_SELF"]);
        $html ='<script type="text/javascript">
    			var iso = \'' . $isoTinyMCE . '\' ;
    			var pathCSS = \'' . _THEME_CSS_DIR_ . '\' ;
    			var ad = \'' . $ad . '\' ;
    			$(document).ready(function(){
    			tinySetup({
    				editor_selector :"rte",
            		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,fontselect,fontsizeselect",
            		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,codemagic,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
            		theme_advanced_toolbar_location : "top",
            		theme_advanced_toolbar_align : "left",
            		theme_advanced_statusbar_location : "bottom",
            		theme_advanced_resizing : false,
                    extended_valid_elements: \'pre[*],script[*],style[*]\',
                    valid_children: "+body[style|script],pre[script|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
                    valid_elements : \'*[*]\',
                    force_p_newlines : false,
                    cleanup: false,
                    forced_root_block : false,
                    force_br_newlines : true
    				});
    			});</script>';
        return $html.$this->display(__file__, 'views/templates/admin/main.tpl');
    }
    private function getHtmlIdByPosition($id_position = null,$active = null){
        if (is_null($id_position))
            return;
        $id_shop = $this->context->shop->id;
        $id_lang = $this->context->language->id;
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'htmlobject h
        LEFT JOIN ' . _DB_PREFIX_ .
            'htmlobject_lang hl ON (h.id_htmlobject = hl.id_htmlobject)
			LEFT JOIN ' . _DB_PREFIX_ .
            'htmlobject_shop hs ON (h.id_htmlobject = hs.id_htmlobject)
			WHERE (hs.`id_shop` = ' . (int)$id_shop . ')
			AND hl.`id_lang` = ' . (int)$id_lang
            .(!is_null($active) && $active ? ' AND h.`active` = 1':'').
            ' AND h.`hook_postition` = '.(int)$id_position;
        return Db::getInstance()->getRow($sql);
    }

    public function hookdisplayTopColumn($params){
        if ($this->psversion() == 6 )
            return $this->prehook('displayTopColumn');
        else
            return;
    }

    public function hookdisplayNav($params){
        if ($this->psversion() == 6 ){
            //return $this->prehook('displayNav');
            $id_position = array_search('displayNav',BlockHtml::$hookArr)+1;
            if (!$this->isCached('displaynav.tpl', $this->getCacheId($id_position,true))){
                $htmlObject = $this->getHtmlIdByPosition($id_position,true);
                $this->context->smarty->assign(array(
                    'item' => $htmlObject,
                    'hook_position' => 'displayNav'
                ));
            }
            return $this->display(__file__, 'displaynav.tpl', $this->getCacheId($id_position));
            
        }else
            return;
    }

    public function hookdisplayFooter($params){
        //return $this->prehook('displayFooter');
        $id_position = array_search('displayFooter',BlockHtml::$hookArr)+1;
        if (!$this->isCached('displayfooter.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => 'displayFooter'
            ));
        }
        return $this->display(__file__, 'displayfooter.tpl', $this->getCacheId($id_position));
    }

    public function hookdisplayRightColumn($params){
        return $this->prehook('displayRightColumn');
    }

    public function hookleftColumn($params){
        return $this->prehook('displayLeftColumn');
    }

    public function hookdisplayTop($params){
        //return $this->prehook('displayTop');
        $id_position = array_search('displayTop',BlockHtml::$hookArr)+1;
        if (!$this->isCached('displaytop.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => 'displayTop'
            ));
        }
        return $this->display(__file__, 'displaytop.tpl', $this->getCacheId($id_position));
    }

    public function hookdisplayHome($params)
    {
        $id_position = array_search('displayHome',BlockHtml::$hookArr)+1;
        if (!$this->isCached('displayhome.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => 'displayHome'
            ));
        }
        return $this->display(__file__, 'displayhome.tpl', $this->getCacheId($id_position));
    }

    public function hookDisplayBackOfficeHeader()
	{
		if (Tools::getValue('configure') != $this->name)
			return;
        $iso = Language::getIsoById((int)($this->context->language->id));        
        $ad = dirname($_SERVER["PHP_SELF"]);
        $this->smarty->assign(array('ad' => $ad, 'iso'=>$iso));
		$this->context->controller->addJquery();
        $this->context->controller->addJS(_PS_JS_DIR_.'tiny_mce/tiny_mce.js');
        $this->context->controller->addJS($this->_path.'js/tinymce.inc.js');
		$this->context->controller->addJS($this->_path.'js/blockhtml_admin.js');
	}
    public function hookCustomHtml($params){
        $id_position = array_search('CustomHtml',BlockHtml::$hookArr)+1;
        if (!$this->isCached('customhtml.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => 'CustomHtml'
            ));
        }
        return $this->display(__file__, 'customhtml.tpl', $this->getCacheId($id_position));
    }
    public function hookCustomHtml2($params){
        $id_position = array_search('CustomHtml2',BlockHtml::$hookArr)+1;
        if (!$this->isCached('customhtml2.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => 'CustomHtml2'
            ));
        }
        return $this->display(__file__, 'customhtml2.tpl', $this->getCacheId($id_position));
    }
    public function hookCustomHtml3($params){ 
        $id_position = array_search('CustomHtml3',BlockHtml::$hookArr)+1; 
        if (!$this->isCached('customhtml3.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => 'CustomHtml3'
            ));
        }
        return $this->display(__file__, 'customhtml3.tpl', $this->getCacheId($id_position)); 
    }
    public function hookCustomHtml4($params){
        $id_position = array_search('CustomHtml4',BlockHtml::$hookArr)+1;
        if (!$this->isCached('customhtml4.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => 'CustomHtml4'
            ));
        }
        return $this->display(__file__, 'customhtml4.tpl', $this->getCacheId($id_position));
    }
    public function hookCustomHtml5($params){
        $id_position = array_search('CustomHtml5',BlockHtml::$hookArr)+1;
        if (!$this->isCached('customhtml5.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => 'CustomHtml5'
            ));
        }
        return $this->display(__file__, 'customhtml5.tpl', $this->getCacheId($id_position));
    }
    public function hookCustomHtml6($params){
        $id_position = array_search('CustomHtml6',BlockHtml::$hookArr)+1;
        if (!$this->isCached('customhtml6.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => 'CustomHtml6'
            ));
        }
        return $this->display(__file__, 'customhtml6.tpl', $this->getCacheId($id_position));
    }
    public function hookCustomHtml7($params){
        $id_position = array_search('CustomHtml7',BlockHtml::$hookArr)+1;
        if (!$this->isCached('customhtml7.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => 'CustomHtml7'
            ));
        }
        return $this->display(__file__, 'customhtml7.tpl', $this->getCacheId($id_position));
    }
    public function hookCustomHtml8($params){
        $id_position = array_search('CustomHtml8',BlockHtml::$hookArr)+1;
        if (!$this->isCached('customhtml8.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => 'CustomHtml8'
            ));
        }
        return $this->display(__file__, 'customhtml8.tpl', $this->getCacheId($id_position));
    }
     

    public function hookContactForm($params){
        return $this->prehook('Contactform');
    }
    

    public function prehook($position){
        $id_position = array_search($position,BlockHtml::$hookArr)+1;
        if (!$this->isCached('blockhtml.tpl', $this->getCacheId($id_position,true))){
            $htmlObject = $this->getHtmlIdByPosition($id_position,true);
            $this->context->smarty->assign(array(
                'item' => $htmlObject,
                'hook_position' => $position
            ));
        }
        return $this->display(__file__, 'blockhtml.tpl', $this->getCacheId($id_position));
    }
    public function hookHeader()
    {
        $this->context->controller->addCSS(($this->_path) . 'css/blockhtml.css', 'all');
        //$this->context->controller->addJS(($this->_path) . 'js/homeadvslide.js');
    }

    public function psversion() {
		$version=_PS_VERSION_;
		$exp=$explode=explode(".",$version);
		return $exp[1];
	}
}
