<?php

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_2_1($object)
{
	$db = Db::getInstance();
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."pagelink_module_lang` LIKE 'id_shop'"))
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."pagelink_module_lang` 		ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `module_id`");
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."pagelink_item` LIKE 'id_shop'"))
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."pagelink_item` 	ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `id`");
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."pagelink_item_lang` LIKE 'id_shop'"))
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."pagelink_item_lang` 			ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `menuitem_id`");		
	
	return true;
}
