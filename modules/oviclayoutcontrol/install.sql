DROP TABLE IF EXISTS `PREFIX_ovic_backup_hook_module`;
CREATE TABLE `PREFIX_ovic_backup_hook_module` (
  `id_module` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_hook` int(10) unsigned NOT NULL,
  `position` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`id_module`,`id_hook`,`id_shop`),
  KEY `id_hook` (`id_hook`),
  KEY `id_module` (`id_module`),
  KEY `position` (`id_shop`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `PREFIX_ovic_options`;
CREATE TABLE `PREFIX_ovic_options` (
  `id_option` int(6) NOT NULL AUTO_INCREMENT,
  `image` varchar(254) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `theme` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `column` varchar(10) NOT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id_option`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `PREFIX_ovic_options_hook_module`;
CREATE TABLE `PREFIX_ovic_options_hook_module` (
  `theme` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `hookname` varchar(64) NOT NULL,
  `modules` text NOT NULL,
  PRIMARY KEY (`theme`,`alias`,`hookname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `PREFIX_ovic_options_sidebar`;
CREATE TABLE `PREFIX_ovic_options_sidebar` (
  `theme` varchar(50) NOT NULL,
  `page` varchar(50) NOT NULL,
  `left` text,
  `right` text,
  `id_shop` int(6) NOT NULL,
  PRIMARY KEY (`theme`,`page`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `PREFIX_ovic_options_style`;
CREATE TABLE `PREFIX_ovic_options_style` (
  `theme` varchar(50) NOT NULL,
  `alias` varchar(50) NOT NULL,
  `font` text,
  `color` text,
  PRIMARY KEY (`theme`,`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


TRUNCATE `PREFIX_ovic_options`;
INSERT INTO `PREFIX_ovic_options` (`id_option`, `image`, `name`, `theme`, `alias`, `column`, `active`) VALUES
(1,	'1446519193index1.jpg',	'Index1',	'leka',	'index1',	'3',	1),
(2,	'1447747397index2.jpg',	'Index2',	'leka',	'index2',	'3',	1),
(3,	'1447819369index3.jpg',	'Index3',	'leka',	'index3',	'3',	1),
(4,	'1447838650index4.jpg',	'Index4',	'leka',	'index4',	'3',	1),
(5,	'1447838701index5.jpg',	'Index5',	'leka',	'index5',	'3',	1),
(6,	'1447838766index6.jpg',	'Index6',	'leka',	'index6',	'1',	1),
(7,	'1470801805index7.jpg',	'Index7',	'leka',	'index7',	'3',	1);

TRUNCATE `PREFIX_ovic_options_hook_module`;
INSERT INTO `PREFIX_ovic_options_hook_module` (`theme`, `alias`, `hookname`, `modules`) VALUES
('leka',	'index1',	'displayBeforeLogo',	'[[\"imagesearchblock\",\"displayTop\"]]'),
('leka',	'index1',	'displayFooter',	'[["advancefooter","displayFooter"],["blocknewsletter","displayFooter"],["ovicnewsletter","displayFooter"]]'),
('leka',	'index1',	'displayHome',	'[[\"flexgroupbanners\",\"displayHome\"],[\"simplecategory\",\"displayHome\"]]'),
('leka',	'index1',	'displayHomeBottomColumn',	'[[\"brandsslider\",\"displayFooter\"],[\"smartbloghomelatestnews\",\"displayHome\"],[\"ovicparallaxblock\",\"displayHomeBottomColumn\"],[\"blocktestimonial\",\"displayHome\"]]'),
('leka',	'index1',	'displayHomeBottomContent',	'[[\"flexgroupbanners\",\"displayHomeBottomContent\"],[\"ovicparallaxblock\",\"displayHomeBottomContent\"],[\"simplecategory\",\"displayHomeBottomContent\"]]'),
('leka',	'index1',	'displayHomeTab',	'[]'),
('leka',	'index1',	'displayHomeTabContent',	'[]'),
('leka',	'index1',	'displayHomeTopColumn',	'[[\"homeslider\",\"displayTopColumn\"]]'),
('leka',	'index1',	'displayHomeTopContent',	'[[\"simplecategory\",\"displayHomeTopContent\"]]'),
('leka',	'index1',	'displayNav',	'[[\"blockuserinfo\",\"displayNav\"],[\"pagelink\",\"displayNav\"],[\"blockcurrencies\",\"displayNav\"],[\"blocklanguages\",\"displayNav\"]]'),
('leka',	'index1',	'displayNavTop',	'[[\"blockcontact\",\"displayNav\"]]'),
('leka',	'index1',	'displayTop',	'[[\"advancetopmenu\",\"displayTop\"],[\"blockcart\",\"displayTop\"],[\"pagesnotfound\",\"displayTop\"],[\"sekeywords\",\"displayTop\"],[\"blockwishlist\",\"displayTop\"]]'),
('leka',	'index1',	'displayTopColumn',	'[]'),
('leka',	'index2',	'displayBeforeLogo',	'[[\"imagesearchblock\",\"displayTop\"]]'),
('leka',	'index2',	'displayBottomColumn',	'[]'),
('leka',	'index2',	'displayFooter',	'[["advancefooter","displayFooter"],["blocknewsletter","displayFooter"],["ovicnewsletter","displayFooter"]]'),
('leka',	'index2',	'displayHome',	'[[\"flexgroupbanners\",\"displayHome\"],[\"simplecategory\",\"displaySimpleCategory1\"],[\"ovicparallaxblock\",\"displayParalax2\"],[\"blockhtml\",\"CustomHtml\"]]'),
('leka',	'index2',	'displayHomeBottomColumn',	'[[\"blocktestimonial\",\"displayHome\"],[\"smartbloghomelatestnews\",\"displayHome\"],[\"brandsslider\",\"displayFooter\"]]'),
('leka',	'index2',	'displayHomeBottomContent',	'[]'),
('leka',	'index2',	'displayHomeTab',	'[]'),
('leka',	'index2',	'displayHomeTabContent',	'[]'),
('leka',	'index2',	'displayHomeTopColumn',	'[[\"homeslider\",\"displayTopColumn\"]]'),
('leka',	'index2',	'displayHomeTopContent',	'[[\"flexgroupbanners\",\"displayHomeTopContent\"],[\"simplecategory\",\"displaySimpleCategory\"],[\"ovicparallaxblock\",\"displayParalax1\"]]'),
('leka',	'index2',	'displayNav',	'[[\"blockuserinfo\",\"displayNav\"],[\"pagelink\",\"displayNav\"],[\"blockcurrencies\",\"displayNav\"],[\"blocklanguages\",\"displayNav\"]]'),
('leka',	'index2',	'displayNavTop',	'[[\"blockcontact\",\"displayNav\"]]'),
('leka',	'index2',	'displayTop',	'[[\"advancetopmenu\",\"displayTop\"],[\"blockcart\",\"displayTop\"],[\"pagesnotfound\",\"displayTop\"],[\"sekeywords\",\"displayTop\"],[\"blockwishlist\",\"displayTop\"]]'),
('leka',	'index2',	'displayTopColumn',	'[]'),
('leka',	'index3',	'displayBeforeLogo',	'[[\"imagesearchblock\",\"displayTop\"],[\"blockhtml\",\"displayTop\"]]'),
('leka',	'index3',	'displayBottomColumn',	'[]'),
('leka',	'index3',	'displayFooter',	'[["advancefooter","displayFooter"],["blocknewsletter","displayFooter"],["ovicnewsletter","displayFooter"]]'),
('leka',	'index3',	'displayHome',	'[[\"ovicparallaxblock\",\"displayParalax3\"]]'),
('leka',	'index3',	'displayHomeBottomColumn',	'[[\"flexgroupbanners\",\"displayHomeBottomColumn\"],[\"brandsslider\",\"displayFooter\"]]'),
('leka',	'index3',	'displayHomeBottomContent',	'[[\"simplecategory\",\"displayHomeBottomContent\"],[\"flexgroupbanners\",\"displayHomeBottomContent\"]]'),
('leka',	'index3',	'displayHomeTab',	'[]'),
('leka',	'index3',	'displayHomeTabContent',	'[]'),
('leka',	'index3',	'displayHomeTopColumn',	'[[\"homeslider\",\"displayTopColumn\"]]'),
('leka',	'index3',	'displayHomeTopContent',	'[[\"flexgroupbanners\",\"displayHomeTopContent\"],[\"simplecategory\",\"displayHomeTopContent\"]]'),
('leka',	'index3',	'displayNav',	'[[\"blockuserinfo\",\"displayNav\"],[\"pagelink\",\"displayNav\"],[\"blockcurrencies\",\"displayNav\"],[\"blocklanguages\",\"displayNav\"]]'),
('leka',	'index3',	'displayNavTop',	'[[\"blockcontact\",\"displayNav\"]]'),
('leka',	'index3',	'displayTop',	'[[\"advancetopmenu\",\"displayTop\"],[\"blockcart\",\"displayTop\"],[\"pagesnotfound\",\"displayTop\"],[\"sekeywords\",\"displayTop\"],[\"blockwishlist\",\"displayTop\"]]'),
('leka',	'index3',	'displayTopColumn',	'[]'),
('leka',	'index4',	'displayBeforeLogo',	'[[\"imagesearchblock\",\"displayTop\"]]'),
('leka',	'index4',	'displayFooter',	'[["advancefooter","displayFooter"],["blocknewsletter","displayFooter"],["ovicnewsletter","displayFooter"]]'),
('leka',	'index4',	'displayHome',	'[[\"simplecategory\",\"displaySimpleCategory4\"]]'),
('leka',	'index4',	'displayHomeBottomColumn',	'[]'),
('leka',	'index4',	'displayHomeBottomContent',	'[[\"flexgroupbanners\",\"displayHomeBottomContent\"],[\"blocktestimonial\",\"displayHome\"],[\"smartbloghomelatestnews\",\"displayHome\"]]'),
('leka',	'index4',	'displayHomeTab',	'[]'),
('leka',	'index4',	'displayHomeTabContent',	'[]'),
('leka',	'index4',	'displayHomeTopColumn',	'[[\"ovicparallaxblock\",\"displayParalax5\"],[\"flexgroupbanners\",\"displayGroupBanner1\"]]'),
('leka',	'index4',	'displayHomeTopContent',	'[]'),
('leka',	'index4',	'displayNav',	'[]'),
('leka',	'index4',	'displayNavTop',	'[[\"blockcontact\",\"displayNav\"]]'),
('leka',	'index4',	'displayTop',	'[[\"blockcart\",\"displayTop\"],[\"pagesnotfound\",\"displayTop\"],[\"sekeywords\",\"displayTop\"],[\"blockwishlist\",\"displayTop\"],[\"blockuserinfo\",\"displayNav\"],[\"blockcurrencies\",\"displayNav\"],[\"blocklanguages\",\"displayNav\"],[\"advancetopmenu\",\"displayTop\"]]'),
('leka',	'index4',	'displayTopColumn',	'[]'),
('leka',	'index5',	'displayBeforeLogo',	'[[\"imagesearchblock\",\"displayTop\"]]'),
('leka',	'index5',	'displayBottomColumn',	'[]'),
('leka',	'index5',	'displayFooter',	'[["advancefooter","displayFooter"],["blocknewsletter","displayFooter"],["ovicnewsletter","displayFooter"]]'),
('leka',	'index5',	'displayHome',	'[[\"ovicparallaxblock\",\"displayParalax4\"],[\"simplecategory\",\"displaySimpleCategory1\"]]'),
('leka',	'index5',	'displayHomeBottomColumn',	'[[\"smartbloghomelatestnews\",\"displayHome\"]]'),
('leka',	'index5',	'displayHomeBottomContent',	'[[\"brandsslider\",\"displayFooter\"],[\"flexgroupbanners\",\"displayHomeBottomContent\"]]'),
('leka',	'index5',	'displayHomeTab',	'[]'),
('leka',	'index5',	'displayHomeTabContent',	'[]'),
('leka',	'index5',	'displayHomeTopColumn',	'[[\"homeslider\",\"displayTopColumn\"],[\"flexgroupbanners\",\"displayHomeTopColumn\"]]'),
('leka',	'index5',	'displayHomeTopContent',	'[[\"simplecategory\",\"displaySimpleCategory\"],[\"flexgroupbanners\",\"displayHomeTopContent\"]]'),
('leka',	'index5',	'displayNav',	'[[\"blockuserinfo\",\"displayNav\"],[\"pagelink\",\"displayNav\"],[\"blockcurrencies\",\"displayNav\"],[\"blocklanguages\",\"displayNav\"]]'),
('leka',	'index5',	'displayNavTop',	'[[\"blockcontact\",\"displayNav\"]]'),
('leka',	'index5',	'displayTop',	'[[\"advancetopmenu\",\"displayTop\"],[\"blockcart\",\"displayTop\"],[\"pagesnotfound\",\"displayTop\"],[\"sekeywords\",\"displayTop\"],[\"blockwishlist\",\"displayTop\"]]'),
('leka',	'index5',	'displayTopColumn',	'[]'),
('leka',	'index6',	'displayBeforeLogo',	'[[\"imagesearchblock\",\"displayTop\"]]'),
('leka',	'index6',	'displayBottomColumn',	'[]'),
('leka',	'index6',	'displayFooter',	'[["advancefooter","displayFooter"],["blocknewsletter","displayFooter"],["ovicnewsletter","displayFooter"]]'),
('leka',	'index6',	'displayHome',	'[[\"flexgroupbanners\",\"displayHome\"]]'),
('leka',	'index6',	'displayHomeBottomColumn',	'[]'),
('leka',	'index6',	'displayHomeBottomContent',	'[[\"simplecategory\",\"displaySimpleCategory6\"],[\"blockhtml\",\"CustomHtml6\"],[\"brandsslider\",\"displayFooter\"]]'),
('leka',	'index6',	'displayHomeTab',	'[]'),
('leka',	'index6',	'displayHomeTabContent',	'[]'),
('leka',	'index6',	'displayHomeTopColumn',	'[[\"homeslider\",\"displayTopColumn\"]]'),
('leka',	'index6',	'displayHomeTopContent',	'[[\"flexgroupbanners\",\"displayGroupBanner1\"],[\"simplecategory\",\"displayHomeTopContent\"]]'),
('leka',	'index6',	'displayLeftColumn',	'[[\"simplecategory\",\"displaySimpleCategory7\"],[\"blocktestimonial\",\"displayLeftColumn\"],[\"smartbloghomelatestnews\",\"displayHome\"],[\"flexgroupbanners\",\"displayLeftColumn\"]]'),
('leka',	'index6',	'displayNav',	'[[\"blockuserinfo\",\"displayNav\"],[\"pagelink\",\"displayNav\"],[\"blockcurrencies\",\"displayNav\"],[\"blocklanguages\",\"displayNav\"]]'),
('leka',	'index6',	'displayNavTop',	'[[\"blockcontact\",\"displayNav\"]]'),
('leka',	'index6',	'displayTop',	'[[\"advancetopmenu\",\"displayTop\"],[\"blockcart\",\"displayTop\"],[\"pagesnotfound\",\"displayTop\"],[\"sekeywords\",\"displayTop\"],[\"blockwishlist\",\"displayTop\"]]'),
('leka',	'index6',	'displayTopColumn',	'[[\"megamenus\",\"displayVerticalMenu\"]]'),
('leka',	'index7',	'displayBeforeLogo',	'[[\"imagesearchblock\",\"displayTop\"],[\"blockhtml\",\"CustomHtml5\"]]'),
('leka',	'index7',	'displayFooter',	'[[\"statsdata\",\"displayFooter\"],[\"blockcontact\",\"displayNav\"],[\"blocksocial\",\"displayFooter\"],[\"blocknewsletter\",\"displayFooter\"],[\"blockhtml\",\"displayFooter\"],[\"ovicnewsletter\",\"displayFooter\"]]'),
('leka',	'index7',	'displayHome',	'[]'),
('leka',	'index7',	'displayHomeTab',	'[]'),
('leka',	'index7',	'displayHomeTabContent',	'[]'),
('leka',	'index7',	'displayHomeTopColumn',	'[[\"homeslider\",\"displayTopColumn\"]]'),
('leka',	'index7',	'displayNav',	'[[\"blockuserinfo\",\"displayNav\"],[\"blocklanguages\",\"displayNav\"]]'),
('leka',	'index7',	'displayNavTop',	'[[\"pagelink\",\"displayNav\"]]'),
('leka',	'index7',	'displayTop',	'[[\"advancetopmenu\",\"displayTop\"],[\"blockcart\",\"displayTop\"],[\"pagesnotfound\",\"displayTop\"],[\"sekeywords\",\"displayTop\"]]'),
('leka',	'index7',	'displayTopColumn',	'[]');

TRUNCATE `PREFIX_ovic_options_sidebar`;
INSERT INTO `PREFIX_ovic_options_sidebar` (`theme`, `page`, `left`, `right`, `id_shop`) VALUES
('leka',	'category',	'[[\"blocklayered\",\"displayLeftColumn\"],[\"blocktags\",\"displayLeftColumn\"],[\"flexgroupbanners\",\"displayLeftColumn\"]]',	NULL,	1),
('leka',	'index',	'[[\"simplecategory\",\"displaySimpleCategory7\"],[\"blocktestimonial\",\"displayLeftColumn\"],[\"smartbloghomelatestnews\",\"displayHome\"],[\"flexgroupbanners\",\"displayLeftColumn\"]]',	NULL,	1),
('leka',	'module-productcomments-default',	'[[\"blockbestsellers\",\"displayLeftColumn\"],[\"blockcategories\",\"displayLeftColumn\"],[\"blocklayered\",\"displayLeftColumn\"],[\"blockcms\",\"displayLeftColumn\"],[\"blockmanufacturer\",\"displayLeftColumn\"],[\"blockmyaccount\",\"displayLeftColumn\"],[\"blocknewproducts\",\"displayLeftColumn\"],[\"blockpaymentlogo\",\"displayLeftColumn\"],[\"blockspecials\",\"displayLeftColumn\"],[\"blockstore\",\"displayLeftColumn\"],[\"blocksupplier\",\"displayLeftColumn\"],[\"blocktags\",\"displayLeftColumn\"],[\"blockviewed\",\"displayLeftColumn\"],[\"themeconfigurator\",\"displayLeftColumn\"],[\"simplecategory\",\"displayLeftColumn\"],[\"flexgroupbanners\",\"displayLeftColumn\"]]',	NULL,	1);

TRUNCATE `PREFIX_ovic_options_style`;
INSERT INTO `PREFIX_ovic_options_style` (`theme`, `alias`, `font`, `color`) VALUES
('leka',	'index1',	'{\"font1\":\"&lt;link href=&#039;https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900,900italic&#039; rel=&#039;stylesheet&#039; type=&#039;text/css&#039;&gt;\"}',	'{\"main\":\"#7e883a\",\"button\":\"\",\"button_text\":\"\",\"button_hover\":\"\",\"button_text_hover\":\"\",\"text\":\"\",\"text_hover\":\"\"}'),
('leka',	'index2',	'{\"font1\":\"&lt;link href=&#039;https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900,900italic&#039; rel=&#039;stylesheet&#039; type=&#039;text/css&#039;&gt;\"}',	'{\"main\":\"#7e883a\",\"button\":\"\",\"button_text\":\"\",\"button_hover\":\"\",\"button_text_hover\":\"\",\"text\":\"\",\"text_hover\":\"\"}'),
('leka',	'index3',	'{\"font1\":\"&lt;link href=&#039;https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900,900italic&#039; rel=&#039;stylesheet&#039; type=&#039;text/css&#039;&gt;\"}',	'{\"main\":\"#7e883a\",\"button\":\"\",\"button_text\":\"\",\"button_hover\":\"\",\"button_text_hover\":\"\",\"text\":\"\",\"text_hover\":\"\"}'),
('leka',	'index4',	'{\"font1\":\"&lt;link href=&#039;https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900,900italic&#039; rel=&#039;stylesheet&#039; type=&#039;text/css&#039;&gt;\"}',	'{\"main\":\"#7e883a\",\"button\":\"\",\"button_text\":\"\",\"button_hover\":\"\",\"button_text_hover\":\"\",\"text\":\"\",\"text_hover\":\"\"}'),
('leka',	'index5',	'{\"font1\":\"&lt;link href=&#039;https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900,900italic&#039; rel=&#039;stylesheet&#039; type=&#039;text/css&#039;&gt;\"}',	'{\"main\":\"#7e883a\",\"button\":\"\",\"button_text\":\"\",\"button_hover\":\"\",\"button_text_hover\":\"\",\"text\":\"\",\"text_hover\":\"\"}'),
('leka',	'index6',	'{\"font1\":\"&lt;link href=&#039;https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900,900italic&#039; rel=&#039;stylesheet&#039; type=&#039;text/css&#039;&gt;\"}',	'{\"main\":\"#7e883a\",\"button\":\"\",\"button_text\":\"\",\"button_hover\":\"\",\"button_text_hover\":\"\",\"text\":\"\",\"text_hover\":\"\"}'),
('leka',	'index7',	'{\"font1\":\"&lt;link href=&#039;https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900,900italic&#039; rel=&#039;stylesheet&#039; type=&#039;text/css&#039;&gt;\"}',	'{\"main\":\"#7e883a\",\"button\":\"\",\"button_text\":\"\",\"button_hover\":\"\",\"button_text_hover\":\"\",\"text\":\"\",\"text_hover\":\"\"}');