function tinyRemove(elParent){	
	var i, t = tinyMCE.editors;
	for (i in t){
	    if (t.hasOwnProperty(i)){
	        t[i].remove();
	    }
	}	 
}
function tinySetup(config)
{
	if(!config)
		config = {};
	//var editor_selector = 'rte';
	//if (typeof config['editor_selector'] !== 'undefined')
		//var editor_selector = config['editor_selector'];
	if (typeof config['editor_selector'] != 'undefined')
		config['selector'] = '.'+config['editor_selector'];

//    safari,pagebreak,style,table,advimage,advlink,inlinepopups,media,contextmenu,paste,fullscreen,xhtmlxtras,preview
	default_config = {
		selector: ".editor" ,
		height: 160,
		plugins : "colorpicker link image paste pagebreak table contextmenu filemanager table code media textcolor module deal product",
		toolbar1 : "code,|,bold,italic,underline,strikethrough,|,alignleft,aligncenter,alignright,alignfull,formatselect,|,blockquote,colorpicker,pasteword,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,cleanup,|,media,image",
		toolbar2: "",
		external_filemanager_path: ad+"/filemanager/",
		filemanager_title: "File manager" ,
		external_plugins: { "filemanager" : ad+"/filemanager/plugin.min.js"},
		language: iso,
		skin: "prestashop",
		statusbar: false,
		relative_urls : false,
		convert_urls: false,
		extended_valid_elements : "em[class|name|id],div[*],ul[*]",
		
		menu: {
			edit: {title: 'Edit', items: 'undo redo | cut copy paste | selectall'},
			insert: {title: 'Insert', items: 'media image link | pagebreak'},
			view: {title: 'View', items: 'visualaid'},
			format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
			table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
			tools: {title: 'Tools', items: 'code'},
			shortcodes: {title: 'Shortcodes', items: 'product module deal'}
		}
	};
	tinyMCE.PluginManager.add('module', function(editor, url) {
		editor.addButton('module', {
			text: 'Module',
			icon: true,
			onclick: function() {
				editor.windowManager.open({
					title: 'Module',
					width: 400,
					height: 120,
					body: [
						{
							type: 'textbox', 
							name: 'module_name', 
							label: 'Enter module name',								
    					},
    					{
							type: 'textbox', 
							name: 'hook_name', 
							label: 'Enter hook name',								
    					}
					],
					onsubmit: function(e) {
						editor.insertContent('{module}{"mod":"'+e.data.module_name+'", "hook":"'+e.data.hook_name+'"}{/module}');
					}
				});
			}
		});	
		// Adds a menu item to the tools menu
		editor.addMenuItem('module', {
			text: 'Module',
			context: 'shortcodes',
			onclick: function() {
				editor.windowManager.open({
					title: 'Module Shortcode',
					width: 400,
					height: 120,
					body: [
						{
							type: 'textbox', 
							name: 'module_name', 
							label: 'Enter module name',								
    					},
    					{
							type: 'textbox', 
							name: 'hook_name', 
							label: 'Enter hook name',								
    					}
					],
					onsubmit: function(e) {
						editor.insertContent('{module}{"mod":"'+e.data.module_name+'", "hook":"'+e.data.hook_name+'"}{/module}');
					}
				});				
			}
		});
	});
	tinyMCE.PluginManager.add('product', function(editor, url) {
		editor.addButton('product', {
			text: 'Product item',
			icon: true,
			onclick: function() {
				editor.insertContent('{product}{"id":"1"}{/product}');				
			}
		});
		editor.addMenuItem('product', {
			text: 'Product item',
			context: 'shortcodes',
			onclick: function() {
				editor.insertContent('{product}{"id":"1"}{/product}');			
			}
		});
	});
	tinyMCE.PluginManager.add('deal', function(editor, url) {
		editor.addButton('deal', {
			text: 'Product deal',
			icon: true,
			onclick: function() {
				editor.insertContent('{deal}{"limit":"3"}{/deal}');				
			}
		});
		editor.addMenuItem('deal', {
			text: 'Product deal',
			context: 'shortcodes',
			onclick: function() {
				editor.insertContent('{deal}{"limit":"3"}{/deal}');			
			}
		});
	});
	$.each(default_config, function(index, el)
	{
		if (config[index] === undefined )
			config[index] = el;
	});

	tinyMCE.init(config);

};