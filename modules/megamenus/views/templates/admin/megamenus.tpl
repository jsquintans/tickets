<div class="panel abacana-panel">    
    <div class="panel-heading">
        {l s='List Mega Menus' mod='megamenus'}
        <span class="panel-heading-action ">
        	<!--
        	<a href="javascript:void(0)" class="link-trash c-red" title="{l s='Delete' mod='megamenus'}">{l s='Delete' mod='megamenus'}</a>
            <a href="javascript:void(0)" class="link-deactive c-org" title="{l s='Disable' mod='megamenus'}">{l s='Disable' mod='megamenus'}</a>
            <a href="javascript:void(0)" class="link-active" title="{l s='Enable' mod='megamenus'}">{l s='Enable' mod='megamenus'}</a>
           -->
           
           <a href="{$url_export}" class="list-toolbar-btn " title="{l s='Export data' mod='megamenus'}"><i class="process-icon-export"></i></a>
           <a href="javascript:void(0)" class="list-toolbar-btn link-megamenus-import-data" title="{l s='Import data' mod='megamenus'}"><i class="process-icon-import"></i></a>
           
           <a href="javascript:void(0)" class="list-toolbar-btn link-add link-add-megamenu" title="{l s='Add new' mod='megamenus'}"><i class="process-icon-new"></i></a>
           <!--
            <a href="javascript:void(0)" class="link-add link-add-megamenu" title="{l s='Add new' mod='megamenus'}">{l s='Add new' mod='megamenus'}</a>
           -->
        </span>
    </div>
    <div class="panel-body" style="padding:0">
        <div class="table-responsive">
            <table class="table" id="moduleList">
                <thead>
                    <tr class="nodrag nodrop">
                        <th width="100" class="center">{l s='Ordering' mod='megamenus'}</th>
                        <th>{l s='Name' mod='megamenus'}</th>
                        <th>{l s='Position' mod='megamenus'}</th>
                        <th>{l s='Theme/Option' mod='megamenus'}</th>                        
                        <th width="250">{l s='Layout' mod='megamenus'}</th>                        
                        <th class="center" width="120">{l s='Actions' mod='megamenus'}</th>
                    </tr>
                </thead>
                <tbody>
                    {if $items|@count >0}
                    {foreach from=$items item=item}
                    <tr id="megamenu_{$item.id}" class="list-megamenu status-{$item.status}">
                        <td class="pointer dragHandle center" ><div class="dragGroup"><div class="positions">{$item.ordering}</div></div></td>
                        <td><a href="javascript:void(0)" class="link-megamenu" title="{l s='Load menu content'}" data-id="{$item.id}">{$item.name}</a></td>                
                        <td>{$item.position_name}</td>
                        <td>{$item.theme_directory} / {$item.option_directory}</td>
                        <td>{MegaMenus::_getLayoutName($item.layout)}</td>                                            
                        
                        <td class="center">
                        	<div class="btn-group pull-right">
								<a class="btn btn-default link-edit-megamenu" data-id="{$item.id}"><i class="icon-pencil"></i>&nbsp; {l s='Edit' mod='megamenus'}</a>
								<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" aria-expanded="true"><i class="icon-caret-down"></i>&nbsp;</button>
								<ul class="dropdown-menu">
									<li>
							        	{if $item.status == '1'}
		                            		<a href="javascript:void(0)" class="link-status-megamenu" data-id="{$item.id}" data-value="{$item.status}" data-action="changeMegamenuStatus"><i class="icon-off"></i>&nbsp;{l s='Disable' mod='megamenus'}</a>                                
			                            {else}
			                            	<a href="javascript:void(0)" class="link-status-megamenu c-org" data-id="{$item.id}" data-value="{$item.status}" data-action="changeMegamenuStatus"><i class="icon-off"></i>&nbsp;{l s='Enable' mod='megamenus'}</a>
			                            {/if} 							         	
									</li>                        	
	                        		<li><a href="javascript:void(0)" data-id="{$item.id}" class="link-copy-megamenu" title="{l s='Copy megamenu' mod='megamenus'}"><i class="icon-copy"></i>&nbsp;{l s='Copy' mod='megamenus'}</a></li>
									<li class="divider"></li>
	                        		<li><a href="javascript:void(0)" data-id="{$item.id}" class="link-trash-megamenu c-red" title="{l s='Delete megamenu' mod='megamenus'}"><i class="icon-trash"></i>&nbsp;{l s='Delete' mod='megamenus'}</a></li>	                        	
								</ul>
							</div>
                        </td>
                    </tr>
                    {/foreach}
                    {/if}    
                </tbody>    
           </table>            
        </div>        
    </div> 
</div>




<div class="panel abacana-panel" style="display: none" id="panel-menus">    
    <div class="panel-heading">
        {l s=' List menus of ' mod='megamenus'}<span id="header-megamenu-name"></span> 
        <span class="panel-heading-action">    
        	<a href="javascript:void(0)" class="list-toolbar-btn link-add link-add-menu" title="{l s='Add new' mod='megamenus'}"><i class="process-icon-new"></i></a>
        	{*}
            <a href="javascript:void(0)" class="link-add link-add-menu" title="{l s='Add new' mod='megamenus'}">{l s='Add new' mod='megamenus'}</a>
            {*}
        </span>      
    </div>
    <div class="panel-body" style="padding:0" >
        <div class="table-responsive">
        	<div class="" id="menuList">        		
        		<div class="list-body menu-sortable" data-parent="0"></div>
        	</div>
        	
        </div>
    </div> 
</div>

<div class="panel abacana-panel" style="display: none" id="row_of_menu">    
    <div class="panel-heading">
        {l s=' rows of menu ' mod='megamenus'}<span id="header-menu-name"></span>
        <span class="panel-heading-action">        	
        	<a href="javascript:void(0)" class="list-toolbar-btn link-add link-add-row" title="{l s='Add new' mod='megamenus'}"><i class="process-icon-new"></i></a>
        	{*}
            <a href="javascript:void(0)" class="link-add link-add-row" title="{l s='Add new' mod='megamenus'}">{l s='Add new' mod='megamenus'}</a>
            {*}            
        </span>
        {*}
        <span class="panel-heading-action">            
            <a href="javascript:void(0)" onclick="showModal('modalRow')" class="list-toolbar-btn link-add" title="{l s='Add new row'}"><i class="process-icon-new"></i></a>            
        </span>
        {*}
    </div>
    <div class="panel-body" style="padding:0" id="menu-content">
        
    </div> 
</div>




<div id="modalModule" class="modal fade dts-modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <span class="modal-title">{l s=' Add or edit box megamenu' mod='megamenus'}</span>
            </div>
            <div class="modal-body form-horizontal" id="forgotBody">
                <form id="frmMegamenu">
                	<div class="clearfix">
                        <div class="tab-megamenus text-center tab-groups" id="tab-megamenu">
                            <a href="#megamenu-config" class="tab-item active">{l s='Geneal config' mod="megamenus"}</a>
                            <a href="#megamenu-style" class="tab-item">{l s='Style config' mod="megamenus"}</a>                                       
                        </div>
                    </div>     
                    <div class="tab-content">                                
                        <div id="megamenu-config" class="tab-pane fade in active">
                            {$formMegamenu.config}                                    
                        </div>
                        <div id="megamenu-style" class="tab-pane fade">
                            {$formMegamenu.style}                           
                        </div>
                    </div> 	
                    {*}
                	{$formMegamenu}
                	{*}
                </form>           
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary btnForgot" onclick="saveModule()"><i class="icon-save"></i> {l s='Save' mod='megamenus'}</button>
            </div>
        </div>
    </div>
</div>
<div id="modalMenu" class="modal fade dts-modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <span class="modal-title">{l s=' Add or edit menu' mod='megamenus'}</span>
            </div>
            <div class="modal-body form-horizontal" id="forgotBody">
                <form id="frmMenu">{$formMenu}</form>           
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary btnForgot" onclick="saveMenu()"><i class="icon-save"></i> {l s='Save' mod='megamenus'}</button>
            </div>
        </div>
    </div>
</div>
<div id="modalSubMenu" class="modal fade dts-modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <span class="modal-title">{l s=' Add or edit sub menu' mod='megamenus'}</span>
            </div>
            <div class="modal-body form-horizontal" id="forgotBody">
                <form id="frmSubMenu">{$formSubMenu}</form>           
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary btnForgot" onclick="saveSubMenu()"><i class="icon-save"></i> {l s='Save' mod='megamenus'}</button>
            </div>
        </div>
    </div>
</div>
<div id="modalRow" class="modal fade dts-modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <span class="modal-title">{l s='Add or edit row' mod='megamenus'}</span>
            </div>
            <div class="modal-body form-horizontal">
                <form id="frmRow">{$formRow}</form>           
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary btnForgot" onclick="saveRow()"><i class="icon-save"></i> {l s='Save' mod='megamenus'}</button>
            </div>
        </div>
    </div>
</div>
<div id="modalGroup" class="modal fade dts-modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <span class="modal-title">{l s=' Add or edit group' mod='megamenus'}</span>
            </div>
            <div class="modal-body form-horizontal">
                <form id="frmGroup">
                    <div class="clearfix">
                        <div class="tab-groups" id="tab-groups">
                            <a href="#group-config" class="tab-item active">{l s='General config' mod="megamenus"}</a>
                            <a href="#group-description" class="tab-item">{l s='Description' mod="megamenus"}</a>                                       
                        </div>
                    </div>     
                    <div class="tab-content">                                
                        <div id="group-config" class="tab-pane fade in active">
                            {$formGroup.config}                                    
                        </div>
                        <div id="group-description" class="tab-pane fade">
                            {$formGroup.description}                           
                        </div>
                    </div>                    
                </form>           
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary btnForgot" onclick="saveGroup()"><i class="icon-save"></i> {l s='Save' mod='megamenus'}</button>
            </div>
        </div>
    </div>
</div>
<div id="modalMenuItem" class="modal fade dts-modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <span class="modal-title">{l s=' Add or edit menu item' mod='megamenus'}</span>
            </div>
            <div class="modal-body form-horizontal">
                <form id="frmMenuItem">{$formMenuItem}</form>           
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary btnForgot" onclick="saveMenuItem()"><i class="icon-save"></i> {l s='Save' mod='megamenus'}</button>
            </div>
        </div>
    </div>
</div>
<div id="modalSubMenuItem" class="modal fade dts-modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <span class="modal-title">{l s=' Add or edit sub menu item' mod='megamenus'}</span>
            </div>
            <div class="modal-body form-horizontal">
                <form id="frmSubMenuItem">{$formSubMenuItem}</form>           
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary btnForgot" onclick="saveSubMenuItem()"><i class="icon-save"></i> {l s='Save' mod='megamenus'}</button>
            </div>
        </div>
    </div>
</div>
<div id="modalImport" class="modal fade dts-modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <span class="modal-title">{l s=' Select file megamenus data' mod='megamenus'}</span>
            </div>
            <div class="modal-body form-horizontal" id="forgotBody">
                <form id="frmImport">
                	 <div class="form-group">
                	 	<label class="control-label col-sm-3">
	                    	<span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Only support file format .zip or .xml">
								 {l s='Select file' mod='megamenus'}
							</span>
	                    </label>
					   
                        <div class="col-sm-9">
                        	<div class="input-group">
								<input type="text" class="form-control" readonly="readonly" value="" name="icon" id="import-file" />
								<span class="input-group-btn">
									<button id="import-file-uploader" type="button" class="btn btn-default">
										<i class="icon-folder-open"></i>
									</button>
								</span>
							</div>
                        </div>
                	 </div>
                </form>           
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default btnForgot" data-dismiss="modal"><i class="icon-off"></i> {l s='Cancel' mod='megamenus'}</button>
                <button type="button" class="btn btn-primary btnForgot" onclick="importData()"><i class="icon-save"></i> {l s='Import' mod='megamenus'}</button>
            </div>
        </div>
    </div>
</div>
<div id="modalLayout" class="modal fade dts-modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <span class="modal-title">{l s=' Select layout' mod='megamenus'}</span>
            </div>
            <div class="modal-body form-horizontal">
                <table class="table-responsive table">
                	{foreach from=$layouts item=layout}
	                	<tr>
	                		<td width="60"><input type="radio" class="layout-item" name="layout_item" value="{$layout.key}" data-value="{$layout.value}" id="{$layout.key}" /></td>
	                		<td><label for="{$layout.key}" rel="popover" data-img="{$layout.src}" >{$layout.value}</label></td>
	                	</tr>
                	{/foreach}                	
                </table>           
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default btnForgot" data-dismiss="modal"><i class="icon-off"></i> {l s='Cancel' mod='megamenus'}</button>
                <button type="button" class="btn btn-primary btnForgot" onclick="selectedLayout()"><i class="icon-save"></i> {l s='Ok' mod='megamenus'}</button>
            </div>            
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="alert text-center">
	<div>Mega menus</div>
	<div>Copyright 2015 abacana.com</div>
	<div>Create by Jack Stanham (abacaza@gmail.com)</div>
</div>

{include file="$dialog_product"}
{addJsDefL name=uploadfile_not_setup}{l s='Error, not find element upload file!' mod='megamenus' js=1}{/addJsDefL}
{addJsDefL name=lab_delete}{l s='Delete' mod='megamenus' js=1}{/addJsDefL}
{addJsDefL name=lab_disable}{l s='Disable' mod='megamenus' js=1}{/addJsDefL}
{addJsDefL name=lab_enable}{l s='Enable' mod='megamenus' js=1}{/addJsDefL}
{addJsDefL name=confirm_delete_group}{l s='Are you sure you want to delete this group?' mod='megamenus' js=1}{/addJsDefL}
{addJsDefL name=confirm_delete_menu}{l s='Are you sure you want to delete this menu?' mod='megamenus' js=1}{/addJsDefL}
{addJsDefL name=confirm_delete_row}{l s='Are you sure you want to delete this row?' mod='megamenus' js=1}{/addJsDefL}
{addJsDefL name=confirm_delete_menu_item}{l s='Are you sure you want to delete this menu item?' mod='megamenus' js=1}{/addJsDefL}

<script type="text/javascript">
	$(document).on('focusin', function(e) {
        if ($(e.target).closest(".mce-window").length) {
    		e.stopImmediatePropagation();
    	}
    });
    var secure_key			= "{$secure_key}";
    var baseModuleUrl 		= "{$baseModuleUrl}";
    var currentUrl 			= "{$currentUrl}";
    var currentLanguage 	= "{$langId}";
    var megamenuId 			= '0';
    var menuId 				= '0';
    var parentMenuId 		= '0';
    var parentMenuItemId 	= '0';
    var rowId 				= '0';    
    var groupId 			= '0';    
    var iso 				= '{$iso}';    
    var ad 					= "{$ad}";
    var formNewMegamenu 	= '';
    var formNewMenu 		= '';
    var formNewRow 			= '';
    var formNewGroup 		= '';    
    var formNewMenuItem 	= '';
    var formNewSubMenu 		= '';
    var formNewSubMenuItem	='';
</script>