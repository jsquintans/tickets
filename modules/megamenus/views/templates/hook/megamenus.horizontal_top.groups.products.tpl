{$before}
<div class="col-md-{$width} {$custom_class}">
	{if $description != ''}<div class="megamenu-group-description">{$description}</div>{/if}
	{if $display_title == '1'}
		<h4 class="mega-group-header"><a href="{$href}" class="group-title megamenu-title {$submenu_color} {$submenu_hover_color}">{$name}</a></h4>
	{/if}	
	{if isset($megamenus_products) && $megamenus_products|@count >0}
		<div class="megamenus-group-products clearfix row">
	        {assign var='products_totalwidth' value=0}
	        {foreach from=$megamenus_products item=product name=products}
	            {assign var='products_totalwidth' value=$products_totalwidth+$item_width}            
	            {if $products_totalwidth > 12 && !$smarty.foreach.products.last}             
	                <div class="clearfix"></div>                
	                {assign var='products_totalwidth' value=0}            
	            {/if}                                                                         	            
	            <div class="product-container {if $item_width >0} col-sm-{$item_width} {else} col-sm-12 {/if}" >
	                <div class="product" itemscope itemtype="http://schema.org/Product">
		                <div class="left-block">
		                    <div class="product-image-container">
		                        <a class="product_img_link" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">
		                            <img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$product.name|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="image" />
		                        </a>    
		                        {if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
			                        {if $product.specific_prices.reduction_type == 'percentage'}
			                        	<a class="specific-box" href="{$product.link|escape:'html':'UTF-8'}">
			                                <span class="specific-label">-{$product.specific_prices.reduction * 100}%</span>
			                            </a>									
	                                {else}
	                                	<a class="specific-box" href="{$product.link|escape:'html':'UTF-8'}">
			                                <span class="specific-label">-{$product.specific_prices.reduction}</span>
			                                {hook h="displayProductPriceBlock" product=$product type="unit_price"}
			                            </a>	                                
	                                {/if} 
		                        {else}
		                        	{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
			                            <a class="sale-box" href="{$product.link|escape:'html':'UTF-8'}">
			                                <span class="sale-label">{l s='Sale!'}</span>
			                            </a>
			                        {else}
			                    		{if isset($product.new) && $product.new == 1}
				                            <a class="new-box" href="{$product.link|escape:'html':'UTF-8'}">
				                                <span class="new-label">{l s='New'}</span>
				                            </a>
				                        {/if}    
			                        {/if}
		                        {/if}
		                        {*}
		                        <div class="container-product-action">
			                        {if isset($quick_view) && $quick_view}
			                        	<a href="{$product.link|escape:'html':'UTF-8'}" rel="{$product.link|escape:'html':'UTF-8'}" title="{l s='Quick view' mod='megamenus'}" class="quick-view product-btn "><i class="ti-eye"></i></a>
									{/if} 	                        
			                        <a class="addToWishlist wishlistProd_{$product.id_product|intval} product-btn " title="{l s='Add to wishlist' mod='megamenus'}" href="#" rel="{$product.id_product|intval}" onclick="WishlistCart('wishlist_block_list', 'add', '{$product.id_product|intval}', false, 1); return false;"><i class="ti-bookmark-alt"></i></a>	                        
									{if isset($comparator_max_item) && $comparator_max_item}
										<a href="{$product.link|escape:'html':'UTF-8'}" title="{l s='Add to compare' mod='megamenus'}" class="add_to_compare link-compare product-btn" data-id-product="{$product.id_product}"><i class="ti-bar-chart"></i></a>
									{/if}
		                        </div>
		                        {*}
		                    </div>                                                                                    
		                </div>
		                <div class="right-block">
		                    <h5 itemprop="name">
		                        {if isset($product.pack_quantity) && $product.pack_quantity}{$product.pack_quantity|intval|cat:' x '}{/if}
		                        <a class="product-name {$submenu_color} {$submenu_hover_color}" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url" >
		                            {$product.name|truncate:45:'...'|escape:'html':'UTF-8'}
		                        </a>
		                    </h5>
		                    
		                    {if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
		                    <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="content_price">
		                        {if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
		                            <span itemprop="price" class="price product-price">
		                                {if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
		                            </span>
		                            <meta itemprop="priceCurrency" content="{$currency->iso_code}" />
		                            {if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
		                                {hook h="displayProductPriceBlock" product=$product type="old_price"}
		                                <span class="old-price product-price pull-right">
		                                    {displayWtPrice p=$product.price_without_reduction}
		                                </span>
		                                {hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
		                                {*}
		                                {if $product.specific_prices.reduction_type == 'percentage'}
		                                    <span class="price-percent-reduction pull-left">-{$product.specific_prices.reduction * 100}%</span>
		                                {/if}
		                                {*}
		                            {/if}
		                            {hook h="displayProductPriceBlock" product=$product type="price"}
		                            {hook h="displayProductPriceBlock" product=$product type="unit_price"}
		                        {/if}
		                    </div>
		                    {/if}  
		                    <div class="add-to-cart">
		                    	{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.customizable != 2 && !$PS_CATALOG_MODE}
									{if (!isset($product.customization_required) || !$product.customization_required) && ($product.allow_oosp || $product.quantity > 0)}
										{capture}add=1&amp;id_product={$product.id_product|intval}{if isset($static_token)}&amp;token={$static_token}{/if}{/capture}
										<a class="ajax_add_to_cart_button product-add-btn" href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product="{$product.id_product|intval}" data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity > 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">
											<span class="add-btn-text">{l s='Add to Cart' mod='megamenus'}</span> <span class="product-btn product-cart">{l s='Cart' mod='megamenus'}</span>
										</a>
									{else}
										<span class="ajax_add_to_cart_button product-add-btn disabled">
											<span class="add-btn-text">{l s='Add to Cart' mod='megamenus'}</span> <span class="product-btn product-cart">{l s='Cart' mod='megamenus'}</span>
										</span>
									{/if}
								{/if}
		                    </div>                                                                                  
		                </div>
	                </div>                                                                                
	            </div>                                                                    
	        {/foreach}
	    </div> 
	{/if}
</div>
{$after}
