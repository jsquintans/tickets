{if isset($megamenus_menus) && $megamenus_menus|@count >0}
{$before}
<nav id="" class="megamenus nav-horizontal-top-megamenu {$custom_class}" role="navigation">
	<div id="" class="responsive-nav">
		<a class="responsive-btn {$root_second_background} {$root_second_color}" href="#">
			<span class="responsive-btn-icon ti-menu pull-left"></span>
			<span class="responsive-btn-text">{$module_name}</span>
		</a>
		<div class="responsive-menu-container"></div>
	</div>
	<ul class="desktop-nav horizontal-top-megamenu {$root_background} menu clearfix" data-background="{$root_background}" data-color="{$root_color}">
		{foreach from=$megamenus_menus item=menu name=menus}
			{if !isset($menu.rows) || $menu.rows == ''}
				{if isset($menu.subs) && $menu.subs}
					<li class="simplemenu-container {$menu.custom_class} menu-parent">
						<a class="{$root_color} {$root_hover_background} {$root_hover_color}" href="{$menu.link}" data-rel="{$menu.link}" title="{$menu.name}">{$menu.name}</a>
						{$menu.subs}						
					</li>
				{else}
					<li class="{$menu.custom_class}">
						<a class="{$root_color} {$root_hover_background} {$root_hover_color}" href="{$menu.link}" title="{$menu.name}">{$menu.name}</a>
					</li>
				{/if}
			{else}
			<li class="megamenu-container megamenu-rows {$menu.custom_class} menu-parent">
				<a class="{$root_color} {$root_hover_background} {$root_hover_color}" data-rel="{$menu.link}" href="{$menu.link}" title="{$menu.name}">{$menu.name}</a>
				{$menu.rows}
			</li>
			{/if}				
		{/foreach}
	</ul>     
</nav>
{$after}
{/if}

