{if isset($megamenus_rows) && $megamenus_rows|@count >0}
	<div class="megamenu">
		<div class="container">
		{foreach from=$megamenus_rows item=row name=rows}
			{$before} 
			<div class="row {$row.custom_class} clearfix">
				{if $row.display_name == '1'}<h4 class="row-title"><a href="{$href}" class="{$submenu_color} {$submenu_hover_color}" >{$row.name}</a></h4>{/if}				
				{$row.groups}
			</div>
			{$after}		
		{/foreach}
		</div>
	</div>	
{/if}
