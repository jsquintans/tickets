{if isset($items) && $items|@count >0}							
<ul class="sub_level_1 sub-menus">
	{foreach from=$items item=submenu1 name=submenu1s}
		{if isset($submenu1.subs) && $submenu1.subs|@count >0}
			<li class="{$submenu1.custom_class} menu-parent">
				<a class="{$submenu_color} {$submenu_hover_color}" data-rel="{$submenu1.link}" href="{$submenu1.link}" title="{$submenu1.name}">{$submenu1.name}</a>
				<ul class="sub_level_2 sub-menus">
					{foreach from=$submenu1.subs item=submenu2 name=submenu2s}
						{if isset($submenu2.subs) && $submenu2.subs|@count >0}
							<li class="{$submenu2.custom_class} menu-parent">
								<a class="{$submenu_color} {$submenu_hover_color}" data-rel="{$submenu2.link}" href="{$submenu2.link}" title="{$submenu2.name}">{$submenu2.name}</a>								
								<ul class="sub_level_3 sub-menus">
									{foreach from=$submenu2.subs item=submenu3 name=submenu3s}
										{if isset($submenu3.subs) && $submenu3.subs|@count >0}
											<li class="{$submenu3.custom_class} menu-parent">
												<a class="{$submenu_color} {$submenu_hover_color}" data-rel="{$submenu3.link}" href="{$submenu3.link}" title="{$submenu3.name}">{$submenu3.name}</a>												
												<ul class="sub_level_3 sub-menus">
												{foreach from=$submenu3.subs item=submenu4 name=submenu4s}
													<li class="{$submenu4.custom_class}">
														<a class="{$submenu_color} {$submenu_hover_color}" href="{$submenu4.link}" title="{$submenu4.name}">{$submenu4.name}</a>			
													</li>				
												{/foreach}
												</ul>
											</li>
										{else}
											<li class="{$submenu3.custom_class}">
												<a class="{$submenu_color} {$submenu_hover_color}" href="{$submenu3.link}" title="{$submenu3.name}">{$submenu3.name}</a>
											</li>
										{/if}
									{/foreach}		
								</ul>													
							</li>
						{else}
							<li class="{$submenu2.custom_class}">
								<a class="{$submenu_color} {$submenu_hover_color}" href="{$submenu2.link}" title="{$submenu2.name}">{$submenu2.name}</a>
							</li>
						{/if}
					{/foreach}
				</ul>														
			</li>
		{else}
			<li class="{$submenu1.custom_class}">
				<a class="{$submenu_color} {$submenu_hover_color}" href="{$submenu1.link}" title="{$submenu1.name}">{$submenu1.name}</a>				
			</li>
		{/if}
	
		
	{/foreach}
</ul>
{/if}