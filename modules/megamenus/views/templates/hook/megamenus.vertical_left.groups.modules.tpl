{$before}
<div class="col-md-{$width} {$custom_class}">
	{if $display_title == '1'}
		<h4 class="mega-group-header"><a href="javascript:void(0)" class="group-title megamenu-title">{$name}</a></h4>
	{/if}
	{if isset($megamenus_module) && $megamenus_module != ''}
		<div class="menu-group-module">{$megamenus_module}</div>	
	{/if}
</div>
{$after}
