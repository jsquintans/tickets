<?php
/*
*  @author 
*/

class OvicHomeSlideData extends Module
{
    protected static $tablesHomeSlider = array('homeslider'=>'', 'homeslider_slides_lang'=>'lang', 'homeslider_slides'=>'');
    protected static $sameDatas = '';
    public function __construct()
	{
		$this->name = 'ovichomeslidedata';
		$this->tab = 'administration';
		$this->version = '1.0';
		$this->author = 'OVICSOFT';		
		$this->secure_key = Tools::encrypt($this->name);
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Ovic - Home slider DataSample');
		$this->description = $this->l('Ovic - Home slider DataSample');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);		
		self::$sameDatas = dirname(__FILE__).'/samedatas/';		
	}
    
    public function getContent()
	{
	   if (Tools::isSubmit('data-export')){
	       $this->exportSameData();
			echo $this->l('Export data home slider success!');
			die;
	   }elseif(Tools::isSubmit('data-import')){
	       $this->importSameData();
			echo $this->l('Install data home silder success!');
			die;
	   }
	}
    private function exportHomeSliderLang($itemId=0, &$parent){
		$langId = Context::getContext()->language->id;
		$result = '';
		if($itemId){
			$items = Db::getInstance()->executeS("Select * From "._DB_PREFIX_."homeslider_slides_lang Where id_homeslider_slides = $itemId");			
			if($items){
				foreach($items as $item){
					$langIso = LanguageCore::getIsoById($item['id_lang']);
					$language = $parent->addChild('language');
					// lang iso
					$language->addChild('lang_iso', $langIso);
					$language->addChild('image', $item['image']);
					
					// name
					$title = $language->addChild('title');
					$this->addNoteCData($title, $item['title']);
					// name
					$description = $language->addChild('description');
					$this->addNoteCData($description, str_replace('\\', '', $item['description']));
					// description
					$legend = $language->addChild('legend');
					$this->addNoteCData($legend, $item['legend']);
					// link_text
					$url = $language->addChild('url');
					$this->addNoteCData($url, $item['url']);				
				}
			}
		}
		return true;
	}
    private function addNoteCData(&$item, $text){ 
	   $node= dom_import_simplexml($item); 
	   $doc = $node->ownerDocument; 
	   $node->appendChild($doc->createCDATASection($text)); 
	}    
    public function exportSameData($directory=''){
        $shopId = Context::getContext()->shop->id;
        if($directory) self::$sameDatas = $directory;
        $langId = Context::getContext()->language->id;
		$link = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_);
		mysql_select_db(_DB_NAME_,$link);
        $currentOption = Configuration::get('OVIC_CURRENT_DIR');
        if($currentOption) $currentOption .= '.';
        else $currentOption = '';
		
        // homeslider
		$items = Db::getInstance()->executeS("Select DISTINCT * From "._DB_PREFIX_."homeslider_slides as s Inner join "._DB_PREFIX_."homeslider as s1 On s.id_homeslider_slides = s1.id_homeslider_slides  where s1.id_shop = $shopId");		
		if($items){
			$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><!-- Copyright DTS Team --><widgets></widgets>'); 			
			foreach($items as $item){
				$widget = $xml->addChild('widget');				
				$widget->addChild('position', $item['position']);
				$widget->addChild('active', $item['active']);
				$languages = $widget->addChild('languages'); 
				$this->exportHomeSliderLang($item['id_homeslider_slides'], $languages);
			}						
			$file = self::$sameDatas.'store'.$shopId.'.'.$currentOption.'homeslider.xml';
			$xml->asXML($file);
		}
	}
    public function importSameData($directory=''){
        $shopId = Context::getContext()->shop->id;
        $currentOption = Configuration::get('OVIC_CURRENT_DIR');
        if($currentOption) $currentOption .= '.';
        else $currentOption = '';
		if($directory) self::$sameDatas = $directory;
		$languages = DB::getInstance()->executeS("Select * From "._DB_PREFIX_."lang Where active = 1");		
		$file = self::$sameDatas.'store'.$shopId.'.'.$currentOption.'homeslider.xml';
        $db = Db::getInstance(_PS_USE_SQL_SLAVE_);		
		if(!file_exists($file))
			$file = self::$sameDatas.$currentOption.'homeslider.xml'; 
		if(file_exists($file)){
			$xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);
            //echo '<pre>'; print_r($xml->widget);die;
			if(isset($xml->widget)){
				if(count($xml->widget) >0){
					$db->execute("Delete From "._DB_PREFIX_."homeslider_slides_lang Where id_homeslider_slides IN (Select id_homeslider_slides From "._DB_PREFIX_."homeslider Where id_shop = ".$shopId.") ");
					$db->execute("Delete From "._DB_PREFIX_."homeslider_slides Where id_homeslider_slides IN (Select id_homeslider_slides From "._DB_PREFIX_."homeslider Where id_shop = ".$shopId.") ");
					$db->execute("Delete From "._DB_PREFIX_."homeslider Where id_shop = ".$shopId);										
					foreach($xml->widget as $widget){
						$widgetInsert = array(
							'position'			=>	(int)$widget->position,
							'active'			=>	(int)$widget->active,
						);
						$this_languages = array();
						$langDefault = array();						
						if(isset($widget->languages->language) && count($widget->languages->language) >0){
							foreach($widget->languages->language as $language){
							    
								if(!$langDefault) $langDefault = array(
									'title'=>(string)$language->title,
									'description'=>(string)$language->description,
									'legend'=>(string)$language->legend,
									'url'=>(string)$language->url,
									'image'=>(string)$language->image,
								);
								$this_languages[(string)$language->lang_iso] = array(
									'title'=>(string)$language->title,
									'description'=>(string)$language->description,
									'legend'=>(string)$language->legend,
									'url'=>(string)$language->url,
									'image'=>(string)$language->image,
								); 	
							}
						}						
						$arrInsertLangs = array();						
						if($db->insert('homeslider_slides', $widgetInsert)){
							$insertId = $db->Insert_ID();
							foreach($languages as $language){
								$lang_iso = $language['iso_code'];
								if($this_languages){
									if(key_exists($lang_iso, $this_languages)){
										$arrInsertLangs[] = array(
											'id_homeslider_slides'=>$insertId,
											'id_lang'=>$language['id_lang'],
											'title'=>$db->escape($this_languages[$lang_iso]['title']),											
											'legend'=>$db->escape($this_languages[$lang_iso]['legend']),
											'description'=>$db->escape($this_languages[$lang_iso]['description'], true),
											'url'=>$db->escape($this_languages[$lang_iso]['url']),
											'image'=>$db->escape($this_languages[$lang_iso]['image']),
										);
									}else{
										$arrInsertLangs[] = array(
											'id_homeslider_slides'=>$insertId,
											'id_lang'=>$language['id_lang'],
//											'id_shop'=>$shopId,
											'title'=>$db->escape($langDefault['title']),
											'legend'=>$db->escape($langDefault['legend']),
											'description'=>$db->escape($langDefault['description'], true),
											'url'=>$db->escape($langDefault['url']),
											'image'=>$db->escape($langDefault['image']),
										);
									}
								}else{
									$arrInsertLangs[] = array(
										'id_homeslider_slides'=>$insertId,
										'id_lang'=>$language['id_lang'],
										//'id_shop'=>$shopId,
										'title'=>$db->escape($langDefault['title']),
										'legend'=>$db->escape($langDefault['legend']),
										'description'=>$db->escape($langDefault['description'], true),
										'url'=>$db->escape($langDefault['url']),
										'image'=>$db->escape($langDefault['image']),
									);
								}
							}
							if($arrInsertLangs) Db::getInstance()->insert('homeslider_slides_lang', $arrInsertLangs);							
							if(isset($widget->items->item) && count($widget->items->item) >0){
								$this->importItems($insertId, $languages, $widget->items);
							}
							Db::getInstance()->insert('homeslider', array('id_homeslider_slides'=>$insertId, 'id_shop'=>$shopId));
						}

					}
				}
			}			
		}
		return true;
    }
}