<?php
if (!defined('_PS_VERSION_')) exit;
class OvicContactForm extends Module{
    public function __construct()
	{
		$this->name = 'oviccontactform';
		$this->tab = 'front_office_features';
		$this->need_instance = 0;
		$this->controllers = array('verification');
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Ovic - Contact Form');
		$this->description = $this->l('Contact form.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->version = '1.0';
		$this->author = 'OvicSoft';
	}
    public function install()
	{
		if (!parent::install() || !$this->registerHook(array('header', 'footer')))
			return false;
        return true;
    }
    public function uninstall()
	{
		return parent::uninstall();
	}
    public function getContent(){
        $html = '';
        if (Tools::isSubmit('submitUpdate'))
		{
    		$r_email = Tools::getValue('OVIC_RECEIVE_EMAIL');
            if (Validate::isEmail($r_email)){
                Configuration::updateValue('OVIC_RECEIVE_EMAIL',$r_email);
                $html .= $this->displayConfirmation($this->l('Settings updated'));
            }else{
                $html .= $this->displayError($this->l('Invalid email address.'));
            }
		}
        $html .= $this->renderForm();
        return $html;
    }
    
    public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Receive email'),
						'name' => 'OVIC_RECEIVE_EMAIL',
						'class' => 'fixed-width-xxl'
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitUpdate';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => array('OVIC_RECEIVE_EMAIL' => Tools::getValue('OVIC_RECEIVE_EMAIL', Configuration::get('OVIC_RECEIVE_EMAIL'))),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
    public function hookFooter($params){
        
        if (Tools::isSubmit('submitcfform'))
		{
            $output = '';
            $name = Tools::getValue('cf_name');
            $message = Tools::getValue('cf_message');
            $from = trim(Tools::getValue('cf_email'));
            $nw_error = true;
            if (!($from) || !Validate::isEmail($from))
				$output .= Tools::displayError('Invalid email address.');
			elseif (!$message)
				$output .= Tools::displayError('The message cannot be blank.');
			elseif (!Validate::isCleanHtml($message))
				$output .= Tools::displayError('Invalid message');
            if (sizeof($errors)<1){
                $r_email = Configuration::get('OVIC_RECEIVE_EMAIL');
                if (!Validate::isEmail($r_email))
                    $r_email = Configuration::get('PS_SHOP_EMAIL');
                if (!$this->sendMail($name,$from,$message,$r_email)){
                    $output .= Tools::displayError('An error occurred while sending an email to '.$r_email.'.');
                }else{
                    $nw_error = false;
                    $output .= $this->l('You have successfully sent a contact info.');
                }   
            } 
            if (strlen($output)>0){
                $this->context->smarty->assign('nw_error',$nw_error);
            }  
            $this->context->smarty->assign('msg',$output);         
        }
        return $this->display(__FILE__, 'ovic_contactform.tpl');
    }
    public function sendMail($name,$emailAddr,$comment,$to_email)
    {
			$name =  Tools::stripslashes($name);
			$e_body ='You have Received a New Contact from '. $name . '. Comment: '.$comment.'';
			$emailAddr =  Tools::stripslashes($semailAddr);
			$comment =  Tools::stripslashes($comment);
			$subject =  'New Contact Posted';
			$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');	
			$to =  $to_email;	
			$contactMessage =  
				"
				$comment 
				Name: $name
				IP: ".((version_compare(_PS_VERSION_, '1.3.0.0', '<'))?$_SERVER['REMOTE_ADDR']:Tools::getRemoteAddr());
				if(Mail::Send($id_lang,
					'contact',
					$subject,
					array(
						'{message}' => nl2br($e_body),
						'{email}' =>  $emailAddr,
					),
					$to,
					null,
					$emailAddr,
					$name
				))
				return true;
    }
}