<section class="conact-us">
<h3 class="hidden" style="padding:0;">&nbsp;</h3>
<div class="contact"> 
    <div class="contact-form">
        {if isset($msg) && $msg}
            <p class="{if $nw_error}warning_inline{else}success_inline{/if}">{$msg}</p>
        {/if}
    	<form class="contactform" id="contact_form" novalidate="" action="{$link->getPageLink('index', true)|escape:'html'}" method="post">
            <div class="row">
                <div class="col-md-6">
                    <ul>
            			<li class="row">
            				<div class="col-xs-12">
            					<input type="text" class="form-control" required="" title="Name" name="cf_name" placeholder="{l s='* NAME' mod='oviccontactform'}" />
            				</div>
            			</li>
            			<li class="row">
            				<div class="col-xs-12">
            					<input type="email" class="form-control" id="cf_email_input" required="" title="Email" name="cf_email" placeholder="{l s='* EMAIL' mod='oviccontactform'}" />
            				</div>
            			</li> 
            		</ul>
                </div>
                <div class="col-md-6">
                    <ul> 
            			<li class="row">
            				<div class="col-xs-12">
            					<textarea placeholder="{l s='* MESSAGE' mod='oviccontactform'}" class="form-control" required="" title="Message" name="cf_message" rows="6"></textarea>
            				</div>
            			</li>
            			<li class="row">
            				<div class="col-xs-12">
            					<button type="submit" name="submitcfform" class="btn middle_btn">{l s='SEND MESSAGE' mod='oviccontactform'}</button>
            				</div>
            			</li>
            		</ul>
                </div>
            </div>
    	</form>
    </div>
</div>
</section>
<script type="text/javascript">
    var placeholder = "{l s='Your address' mod='blocknewsletter' js=1}";
    {literal}
        $(document).ready(function() {
            $('#cf_email_input').on({
                focus: function() {
                    if ($(this).val() == placeholder) {
                        $(this).val('');
                    }
                },
                blur: function() {
                    if ($(this).val() == '') {
                        $(this).val(placeholder);
                    }
                }
            });
        });
    {/literal}
</script>
