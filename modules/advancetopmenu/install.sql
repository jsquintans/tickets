DROP TABLE IF EXISTS `PREFIX_advance_topmenu_blocks`;
CREATE TABLE `PREFIX_advance_topmenu_blocks` (
  `id_block` int(6) NOT NULL AUTO_INCREMENT,
  `position` int(3) DEFAULT NULL,
  `id_sub` int(6) NOT NULL,
  `width` int(6) DEFAULT NULL,
  `class` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_block`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `PREFIX_advance_topmenu_items`;
CREATE TABLE `PREFIX_advance_topmenu_items` (
  `id_item` int(6) NOT NULL AUTO_INCREMENT,
  `id_block` int(6) NOT NULL,
  `position` int(3) DEFAULT NULL,
  `type` varchar(30) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `target` varchar(30) DEFAULT NULL,
  `class` varchar(200) DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `PREFIX_advance_topmenu_items_lang`;
CREATE TABLE `PREFIX_advance_topmenu_items_lang` (
  `id_item` int(6) NOT NULL,
  `id_lang` int(6) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id_item`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `PREFIX_advance_topmenu_main_shop`;
CREATE TABLE `PREFIX_advance_topmenu_main_shop` (
  `id_item` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_item`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `PREFIX_advance_topmenu_sub`;
CREATE TABLE `PREFIX_advance_topmenu_sub` (
  `id_sub` int(6) NOT NULL AUTO_INCREMENT,
  `id_parent` int(6) NOT NULL,
  `width` int(6) DEFAULT NULL,
  `class` varchar(200) DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id_sub`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
