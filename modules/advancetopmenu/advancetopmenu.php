<?php if (!defined('_PS_VERSION_')) exit;
include_once (dirname(__file__) . '/class/Item.php');
include_once (dirname(__file__) . '/class/Block.php');
include_once (dirname(__file__) . '/class/Submenu.php');
class AdvanceTopMenu extends Module
{
	const INSTALL_SQL_FILE = 'install.sql';	
    public $absoluteUrl;
    private $absolutePath;
    private $admin_tpl_path;
	public $sameDatas = '';		
	protected static $tables = array('advance_topmenu_items'=>'','advance_topmenu_blocks'=>'',  'advance_topmenu_main_shop'=>'',  'advance_topmenu_sub'=>'', 'advance_topmenu_items_lang'=>'lang');
    public function __construct()
    {
        $this->name = 'advancetopmenu';
        $this->tab = 'front_office_features';
        $this->version = '2.4';
        $this->author = 'OvicSoft';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Ovic - Advanced Top Menu');
        $this->description = $this->l('Advanced Top Menu.');
        $this->secure_key = Tools::encrypt($this->name);
        if(Configuration::get('PS_SSL_ENABLED'))
			$this->absoluteUrl = _PS_BASE_URL_SSL_.__PS_BASE_URI__.'modules/'.$this->name.'/'; 
		else
			$this->absoluteUrl = _PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/';
		/*
        $this->absoluteUrl = $this->is_https() ? 'https://' : 'http://' .Tools::getShopDomainSsl().__PS_BASE_URI__.
            'modules/' . $this->name . '/';
		*/ 
        $this->absolutePath = _PS_ROOT_DIR_ . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $this->name .
            DIRECTORY_SEPARATOR;
        $this->admin_tpl_path = _PS_MODULE_DIR_ . $this->name . '/views/templates/admin/';
		$this->sameDatas = dirname(__FILE__).'/samedatas/';
    }
    // this also works, and is more future-proof
    public function install()
    {
    	if (!file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
			return false;
		else if (!$sql = file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
			return false;
		$sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
		$sql = preg_split("/;\s*[\r\n]+/", trim($sql));
							
		foreach ($sql as $query){
			if (!Db::getInstance()->execute(trim($query))) return false;
		}
		
        if (!parent::install() || !$this->registerHook('header') || !$this->registerHook('displayBackOfficeHeader') || !$this->registerHook('displayHomeTopMenu')) return false;
        //$this->importSameData();
        return true;
    }
	protected function installDataDemo(){
		foreach(self::$tables as $table=>$value){
			Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.$table);
		}
		$langs = Db::getInstance()->executeS("Select id_lang From "._DB_PREFIX_."lang Where active = 1");			
		if(self::$tables){
			foreach(self::$tables as $table=>$value){					
				if (file_exists($this->sameDatas.$table.'.sql')){
					$sql = file_get_contents($this->sameDatas.$table.'.sql');
					$sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
					$sql = preg_split("/;\s*[\r\n]+/", trim($sql));
					if($value == 'lang'){
						foreach ($sql as $query){
							foreach($langs as $lang){								
								$query_result = str_replace('id_lang', $lang['id_lang'], trim($query));
								Db::getInstance()->execute($query_result);
							}
						}					
					}else{
						foreach ($sql as $query){
							if (!Db::getInstance()->execute(trim($query))) return false;
						}
					}
				}
				
			}
		}
		return true;
	}
    public function exportSameData($directory=''){
        $shopId = $this->context->shop->id;
        if($directory) $this->sameDatas = $directory;
        $langId = Context::getContext()->language->id;      
        $currentOption = Configuration::get('OVIC_CURRENT_DIR');
        if($currentOption) $currentOption .= '.';
        else $currentOption = '';
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'advance_topmenu_items` ti
                LEFT JOIN `' . _DB_PREFIX_ .
            'advance_topmenu_main_shop` tis ON (ti.`id_item` = tis.`id_item`)
                WHERE `id_block` = 0 AND
                tis.`id_shop` = ' . $shopId .'
                ORDER BY  ti.`position` ASC';
        $items = Db::getInstance()->executeS($sql);
        if($items){
            $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><menuItems></menuItems>');            
            foreach($items as $item){
                $menuItem = $xml->addChild('menuItem');             
                $this->exportItems($item,$menuItem);
                // groups
                //$subs = $menuItem->addChild('subs');
                $this->exportSubMenu($item['id_item'], $menuItem);              
            }                       
            $file = $this->sameDatas.'store'.$shopId.'.'.$currentOption.$this->name.'.xml';
            $xml->asXML($file);
        }
        return true;
    }
    private function exportItems($item, &$parent){
        if($item && is_array($item)){
            $parent->addChild('id_block', $item['id_block']);
            $parent->addChild('position', $item['position']);
            $parent->addChild('type', $item['type']);
            $ico = $parent->addChild('icon');              
            $this->addNoteCData($ico, $item['icon']);
            $parent->addChild('target', $item['target']);             
            $parent->addChild('class', $item['class']);
            $parent->addChild('active', $item['active']); 
            $ilink = $parent->addChild('link');              
            $this->addNoteCData($ilink, $item['link']);
            $this->exportmenuItemLang($item['id_item'], $parent);  
        }
    
        return true;
    }  
    private function addNoteCData(&$item, $text){ 
       $node= dom_import_simplexml($item); 
       $doc = $node->ownerDocument; 
       $doc->appendChild($doc->createCDATASection(str_replace('\\', '', $text)));
       //$node->appendChild($doc->createCDATASection($text)); 
       return true;
    }
    private function exportSubMenu($menuItemId=0, &$parent){
        if($menuItemId){
            $items = Db::getInstance()->getRow("Select * From "._DB_PREFIX_."advance_topmenu_sub Where id_parent = $menuItemId");
            if($items){                  
                $sub = $parent->addChild('sub');
                $sub->addChild('width', $items['width']);
                $sub->addChild('class', $items['class']);
                $sub->addChild('active', $items['active']);          
                $this->exportBlock($items['id_sub'], $sub);
            }
        }
        return true;
    }  
    private function exportBlock($menuItemId=0, &$parent){
        if($menuItemId){
            $blocks = Db::getInstance()->executeS("Select * From "._DB_PREFIX_."advance_topmenu_blocks Where id_sub = $menuItemId");
            if($blocks){
                $blocks_node = $parent->addChild('blocks');
                foreach($blocks as $block){                   
                    $block_node = $blocks_node->addChild('block');
                    $block_node->addChild('width', $block['width']);
                    $block_node->addChild('class', $block['class']);
                    $block_node->addChild('position', $block['position']);          
                    $items = Db::getInstance()->executeS("Select * From "._DB_PREFIX_."advance_topmenu_items Where id_block = ".$block['id_block']);
                    if ($items && count($items)>0){
                        $sub_node = $block_node->addChild('subs');
                        foreach ($items as $item) {
                            $items_node = $sub_node->addChild('subitem');
                            $this->exportItems($item,$items_node);
                        }
                    }
                }
            }
        }
        return true;
    }  
    // export menuItem language
    private function exportmenuItemLang($itemId=0, &$parent){
        $langId = Context::getContext()->language->id;
        $result = '';
        if($itemId){
            $items = Db::getInstance()->executeS("Select * From "._DB_PREFIX_."advance_topmenu_items_lang Where id_item =". $itemId);
            if($items){
                $languages_node = $parent->addChild('languages'); 
                foreach($items as $item){
                    $langIso = LanguageCore::getIsoById($item['id_lang']);
                    $language = $languages_node->addChild('language');
                    // lang iso
                    $language->addChild('lang_iso', $langIso);
                    // title
                    $title = $language->addChild('title');
                    $this->addNoteCData($title, $item['title']);
                    // text
                    $text = $language->addChild('text');
                    $this->addNoteCData($text, $item['text']);                  
                }
            }
        }
        return true;
    }   
 
		
    public  function importSameData($directory='', $file=''){
        if($directory) $this->sameDatas = $directory;
        $shopId = $this->context->shop->id;
        $currentOption = Configuration::get('OVIC_CURRENT_DIR');
        if($currentOption) $currentOption .= '.';
        else $currentOption = '';
        if(!$file){
            $file = $this->sameDatas.'store'.$shopId.'.'.$currentOption.$this->name.'.xml';           
        }else{
            if(!file_exists($file))
                $file = $this->sameDatas.'store'.$shopId.'.'.$currentOption.$this->name.'.xml';
        }
        if(!file_exists($file))
            $file = $this->sameDatas.$currentOption.$this->name.'.xml';
        if(!file_exists($file))
            $file = $this->sameDatas.$this->name.'.xml';
                
        if(file_exists($file)){
            $db = Db::getInstance(_PS_USE_SQL_SLAVE_);
            $languages = $db->executeS("Select id_lang, iso_code From "._DB_PREFIX_."lang Where active = 1");           
            $xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);  
            if(isset($xml->menuItem)){
                if(count($xml->menuItem) >0){
                    $db->execute("Delete il From `"._DB_PREFIX_."advance_topmenu_items_lang` il 
                                  INNER JOIN `"._DB_PREFIX_."advance_topmenu_items` i ON i.id_item = il.id_item     
                                  INNER JOIN `"._DB_PREFIX_."advance_topmenu_blocks` b ON b.id_block = i.id_block   
                                  INNER JOIN `"._DB_PREFIX_."advance_topmenu_sub` s ON b.id_sub = s.id_sub 
                                  INNER JOIN `"._DB_PREFIX_."advance_topmenu_main_shop` m on s.id_parent = m.id_item 
                                  WHERE m.id_shop =".$shopId);
                    $db->execute("Delete i From `"._DB_PREFIX_."advance_topmenu_items` i 
                                  INNER JOIN `"._DB_PREFIX_."advance_topmenu_blocks` b ON b.id_block = i.id_block   
                                  INNER JOIN `"._DB_PREFIX_."advance_topmenu_sub` s ON b.id_sub = s.id_sub 
                                  INNER JOIN `"._DB_PREFIX_."advance_topmenu_main_shop` m on s.id_parent = m.id_item 
                                  WHERE m.id_shop =".$shopId);
                    $db->execute("Delete b From `"._DB_PREFIX_."advance_topmenu_blocks` b
                                  INNER JOIN `"._DB_PREFIX_."advance_topmenu_sub` s ON b.id_sub = s.id_sub 
                                  INNER JOIN `"._DB_PREFIX_."advance_topmenu_main_shop` m on s.id_parent = m.id_item 
                                  WHERE m.id_shop =".$shopId);
                    $db->execute("Delete s From `"._DB_PREFIX_."advance_topmenu_sub` s
                                  INNER JOIN `"._DB_PREFIX_."advance_topmenu_main_shop` m on s.id_parent = m.id_item 
                                  WHERE m.id_shop =".$shopId);
                    $db->execute("Delete m From `"._DB_PREFIX_."advance_topmenu_main_shop` m
                                  WHERE m.id_shop =".$shopId);

                    foreach($xml->menuItem as $menuItem){
                        if($insertId = $this->importItem(0,$menuItem,$languages)){
                            Db::getInstance()->insert('advance_topmenu_main_shop',array('id_item'=>$insertId, 'id_shop' => $shopId));
                            if(isset($menuItem->sub)){
                                $this->importSub($insertId,$menuItem->sub,$languages);
                            } 
                        }                       
                    }
                }
            }           
        }
        return true;
    }
    private function importItem($block_id,$menuItem,$languages){ 
        $db = Db::getInstance(_PS_USE_SQL_SLAVE_);        
        $menuItemInsert = array(
            'id_block'          =>  (int)$block_id,
            'position'          =>  (int)$menuItem->position,
            'type'              =>  (string)$menuItem->type,
            'icon'              =>  (string)$menuItem->icon,
            'target'            =>  (string)$menuItem->target,
            'class'             =>  (string)$menuItem->class,
            'active'            =>  (int)$menuItem->active,
            'link'              =>  (string)$menuItem->link,
        );
        $this_languages = array();
        $langDefault = array();                     
        if(isset($menuItem->languages->language) && count($menuItem->languages->language) >0){
            foreach($menuItem->languages->language as $language){
                if(!$langDefault) $langDefault = array(
                    'title'=>(string)$language->title,
                    'text'=>(string)$language->text,
                );
                $this_languages[(string)$language->lang_iso] = array(
                    'title'=>(string)$language->title,
                    'text'=>(string)$language->text,
                );  
            }
        }                       
        if($db->insert('advance_topmenu_items', $menuItemInsert)){
            $insertId = $db->Insert_ID();
            $arrInsertLangs = array(); 
            foreach($languages as $language){
                $lang_iso = $language['iso_code'];
                if($this_languages){
                    if(key_exists($lang_iso, $this_languages)){
                        $arrInsertLangs[] = array(
                            'id_item'=>$insertId,
                            'id_lang'=>$language['id_lang'],
                            'title'=>$db->escape($this_languages[$lang_iso]['title']),
                            'text'=>$this_languages[$lang_iso]['text'],
                        );
                    }else{
                        $arrInsertLangs[] = array(
                            'id_item'=>$insertId,
                            'id_lang'=>$language['id_lang'],
                            'title'=>$db->escape($this_languages[$lang_iso]['title']),
                            'text'=>$this_languages[$lang_iso]['text'],
                        );
                    }
                }else{
                    $arrInsertLangs[] = array(
                        'id_item'=>$insertId,
                        'id_lang'=>$language['id_lang'],
                        'title'=>$this_languages[$lang_iso]['title'],
                        'text'=>$this_languages[$lang_iso]['text'],
                    );
                }
            }  
            if($arrInsertLangs) Db::getInstance()->insert('advance_topmenu_items_lang', $arrInsertLangs);
            return $insertId;
        }else
            return false;
    }
    private function importSub($parent_id,$sub,$languages){
        $db = Db::getInstance(_PS_USE_SQL_SLAVE_);        
        $arrInsert = array(
            'id_parent' => $parent_id,
            'width'  => $sub->width,
            'class'  => $sub->class,
            'active' => (int)$sub->active,
        );
        if($db->insert('advance_topmenu_sub', $arrInsert)){
            $insertId = $db->Insert_ID();
            if(isset($sub->blocks->block) && count($sub->blocks->block) >0){
                $this->importBlock($insertId, $sub->blocks,$languages);
            }
        }
    }
    private function importBlock($id_sub,$blocks,$languages){         
        $db = Db::getInstance(_PS_USE_SQL_SLAVE_);        
        foreach ($blocks->block as $block) {
            $arrInsert = array(
                'id_sub' => $id_sub,
                'width' => $block->width,
                'class' => $block->class,
                'position' => $block->position,
            );
            if($db->insert('advance_topmenu_blocks', $arrInsert)){
                $insertId = $db->Insert_ID(); 
                if(isset($block->subs->subitem) && count($block->subs->subitem) >0){           
                    foreach ($block->subs->subitem as $s_item) {
                        $this->importItem($insertId, $s_item,$languages);
                    }
                }
            }
        }
    }
    public function uninstall()
    {
        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('topmenu.tpl'));
        if (!parent::uninstall() || !$this->uninstallDB()) return false;
        return true;
    }
    private function uninstallDb()
    {
    	foreach(self::$tables as $table=>$value) Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.$table);
        return true;
    }
    public function getContent()
    {
    	if(Tools::getValue('data-export')){
			$this->exportSameData();
			echo $this->l('Export data success!');
			die;
		}
		if(Tools::getValue('data-import')){
			$this->importSameData();
			echo $this->l('Install data demo success!');
			die;
		}
		
        $output = '';
        $errors = array();
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $languages = Language::getLanguages(false);
        if (Tools::getValue('confirm_msg'))
        {
            $this->context->smarty->assign('confirmation', Tools::getValue('confirm_msg'));
        }
        if (Tools::isSubmit('submitnewItem'))
        {
            $id_item = (int)Tools::getValue('item_id');
            if ($id_item && Validate::isUnsignedId($id_item))
            {
                $new_item = new Item($id_item);
            }
            else
            {
                $new_item = new Item();
            }
            $new_item->id_block = Tools::getValue('block_id');
            $new_item->type = Tools::getValue('linktype');
            $new_item->active = (int)Tools::getValue('active');
            $itemtitle_set = false;
            foreach ($languages as $language)
            {
                $item_title = Tools::getValue('item_title_' . $language['id_lang']);
                if (strlen($item_title) > 0)
                {
                    $itemtitle_set = true;
                }
                $new_item->title[$language['id_lang']] = $item_title;
            }
            if (!$itemtitle_set)
            {
                $lang_title = Language::getLanguage($this->context->language->id);
                if ($new_item->type == 'img') $errors[] = 'This Alt text field is required at least in ' . $lang_title['name'];
                else  $errors[] = 'This item title field is required at least in ' . $lang_title['name'];
            }
            $new_item->class = Tools::getValue('custom_class');
            if ($new_item->type == 'link')
            {
                $new_item->icon = Tools::getValue('item_icon');
                $new_item->link = Tools::getValue('link_value');
            }
            elseif ($new_item->type == 'img')
            {
                if (isset($_FILES['item_img']) && strlen($_FILES['item_img']['name']) > 0)
                {
                    if (!$img_file = $this->moveUploadedImage($_FILES['item_img']))
                    {
                        $errors[] = 'An error occurred during the image upload.';
                    }
                    else
                    {
                        $new_item->icon = $img_file;
                        if (Tools::getValue('old_img') != '')
                        {
                            $filename = Tools::getValue('old_img');
                            if (file_exists(dirname(__file__) . '/img/' . $filename))
                            {
                                @unlink(dirname(__file__) . '/img/' . $filename);
                            }
                        }
                    }
                }
                else
                {
                    $new_item->icon = Tools::getValue('old_img');
                }
                $new_item->link = Tools::getValue('link_value');
            }
            elseif ($new_item->type == 'html')
            {
                foreach ($languages as $language) $new_item->text[$language['id_lang']] = Tools::getValue('item_html_' .
                        $language['id_lang']);
            }
            if (!count($errors))
            {
                if ($id_item && Validate::isUnsignedId($id_item))
                {
                    if (!$new_item->update())
                    {
                        $errors[] = 'An error occurred while update data.';
                    }
                }
                else
                {
                    if (!$new_item->add())
                    {
                        $errors[] = 'An error occurred while saving data.';
                    }
                }
                if (!count($errors))
                {
                    if ($id_item && Validate::isUnsignedId($id_item))
                    {
                        $this->context->smarty->assign('confirmation', $this->l('Item successfully updated.'));
                    }
                    else
                    {
                        $confirm_msg = $this->l('New item successfully added.');
                        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('topmenu.tpl'));
                        Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                            Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg);
                    }
                }
            }
        }
        elseif (Tools::isSubmit('submit_del_item'))
        {
            $item_id = Tools::getValue('item_id');
            if ($item_id && Validate::isUnsignedId($item_id))
            {
                $subs = $this->getSupMenu($item_id);
                $del = true;
                if ($subs && count($subs) > 0)
                {
                }
                foreach ($subs as $sub)
                {
                    $del &= $this->deleteSub($sub['id_sub']);
                }
                $item = new Item($item_id);
                if (!$item->delete() || !$del)
                {
                    $errors[] = 'An error occurred while delete item.';
                }
                else
                {
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('topmenu.tpl'));
                    $this->context->smarty->assign('confirmation', $this->l('Delete successful.'));
                }
            }
        }
        elseif (Tools::isSubmit('submitnewsub'))
        {
            $id_sub = Tools::getValue('id_sub');
            if ($id_sub && Validate::isUnsignedId($id_sub))
            {
                $sub = new Submenu($id_sub);
            }
            else
            {
                $sub = new Submenu();
            }
            $sub->id_parent = Tools::getValue('id_parent');
            $sub->width = Tools::getValue('subwidth');
            $sub->class = Tools::getValue('sub_class');
            $sub->active = Tools::getValue('active');
            if ($id_sub && Validate::isUnsignedId($id_sub))
            {
                if (!$sub->update())
                {
                    $errors[] = 'An error occurred while update data.';
                }
            }
            else
            {
                if (!$sub->checkAvaiable())
                {
                    if (!$sub->add())
                    {
                        $errors[] = 'An error occurred while saving data.';
                    }
                }
                else
                {
                    $parent_item = new Item($sub->id_parent);
                    $errors[] = $parent_item->title[$this->context->language->id] . ' already have a sub.';
                }
            }
            if (!count($errors))
            {
                if ($id_sub && Validate::isUnsignedId($id_sub))
                {
                    $this->context->smarty->assign('confirmation', $this->l('Submenu successfully updated.'));
                }
                else
                {
                    $confirm_msg = $this->l('New sub successfully added.');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('topmenu.tpl'));
                    Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                        Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg);
                }
            }
        }
        elseif (Tools::isSubmit('submit_del_sub'))
        {
            $id_sub = (int)Tools::getValue('id_sub');
            if ($id_sub && Validate::isUnsignedId($id_sub))
            {
                if (!$this->deleteSub($id_sub))
                {
                    $errors[] = 'An error occurred while delete sub menu.';
                }
                else
                {
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('topmenu.tpl'));
                    $this->context->smarty->assign('confirmation', $this->l('Delete successful.'));
                }
            }
        }
        elseif (Tools::isSubmit('submitnewblock'))
        {
            $id_block = Tools::getValue('id_block');
            if ($id_block && Validate::isUnsignedId($id_block))
            {
                $block = new Block($id_block);
            }
            else
            {
                $block = new Block();
            }
            $block->id_sub = Tools::getValue('id_sub');
            $block->width = Tools::getValue('block_widh');
            $block->class = Tools::getValue('block_class');
            if ($id_block && Validate::isUnsignedId($id_block))
            {
                if (!$block->update())
                {
                    $errors[] = 'An error occurred while update block.';
                }
            }
            else
            {
                if (!$block->add())
                {
                    $errors[] = 'An error occurred while saving data.';
                }
            }
            if (!count($errors))
            {
                if ($id_block && Validate::isUnsignedId($id_block))
                {
                    $this->context->smarty->assign('confirmation', $this->l('Block successfully updated.'));
                }
                else
                {
                    $confirm_msg = $this->l('New block successfully added.');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('topmenu.tpl'));
                    Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                    Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg);
                }
            }
        }
        elseif (Tools::isSubmit('submit_del_block'))
        {
            $id_block = Tools::getValue('id_block');
            if ($id_block && Validate::isUnsignedId($id_block))
            {
                if (!$this->deleteBlock($id_block))
                {
                    $errors[] = 'An error occurred while delete block.';
                }
                else
                {
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('topmenu.tpl'));
                    $this->context->smarty->assign('confirmation', $this->l('Delete successful.'));
                }
            }
        }
        elseif (Tools::isSubmit('changeactive'))
        {
            $id_item = (int)Tools::getValue('item_id');
            if ($id_item && Validate::isUnsignedId($id_item))
            {
                $item = new Item($id_item);
                $item->active = !$item->active;
                if (!$item->update())
                {
                    $errors[] = $this->displayError('Could not change');
                }
                else
                {
                    $confirm_msg = $this->l('Successfully updated.');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('topmenu.tpl'));
                    Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                        Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg);
                }
            }
        }
        elseif (Tools::isSubmit('changestatus'))
        {
            $id_sub = (int)Tools::getValue('id_sub');
            if ($id_sub && Validate::isUnsignedId($id_sub))
            {
                $sub_menu = new Submenu($id_sub);
                $sub_menu->active = !$sub_menu->active;
                if (!$sub_menu->update())
                {
                    $errors[] = $this->displayError('Could not change');
                }
                else
                {
                    $confirm_msg = $this->l('Submenu successfully updated.');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('topmenu.tpl'));
                    Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                        Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg);
                }
            }
        }
        $this->context->smarty->assign(array(
            'admin_tpl_path' => $this->admin_tpl_path,
            'postAction' => AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite
                ('AdminModules'),
            ));
        if (count($errors) > 0)
        {
            if (isset($errors) && count($errors)) $output .= $this->displayError(implode('<br />', $errors));
        }
        if (Tools::isSubmit('submit_edit_item') || (Tools::isSubmit('submitnewItem') && count($errors) > 0))
        {
            $output .= $this->displayItemForm();
        }
        elseif (Tools::isSubmit('submit_edit_sub'))
        {
            $output .= $this->displaySubForm();
        }
        elseif (Tools::isSubmit('submit_new_block'))
        {
            $output .= $this->displayBlockForm();
        }
        else
        {
            $output .= $this->displayForm();
        }
        return $output;
    }
    private function deleteSub($id_sub = null)
    {
        if (is_null($id_sub)) return false;
        $blocks = $this->getAllBlocks($id_sub);
        $del = true;
        if ($blocks && count($blocks) > 0)
            foreach ($blocks as $bl)
            {
                $del &= $this->deleteBlock($bl['id_block']);
            }
        if ($del)
        {
            $sub = new Submenu($id_sub);
            return $sub->delete();
        }
        return false;
    }
    private function deleteBlock($id_block = null)
    {
        if (is_null($id_block)) return false;
        $items = $this->getItemByBlock($id_block);
        $del = true;
        if ($items && count($items) > 0)
            foreach ($items as $it)
            {
                $item = new Item($it['id_item']);
                $del &= $item->delete();
            }
        if ($del)
        {
            $block = new Block($id_block);
            return $block->delete();
        }
        return false;
    }
    private function deleteItem($id_item = null)
    {
        if (is_null($id_item)) return false;
        $item = new Item($id_item);
        return $item->delete();
    }
    public function updateMenuPosition($order = null)
    {
        if (is_null($order)) return false;
        $position = explode('::', $order);
        $res = false;
        if (count($position) > 0)
            foreach ($position as $key => $id_item)
            {
                $res = Db::getInstance()->execute('
                    UPDATE `' . _DB_PREFIX_ . 'advance_topmenu_items`
                    SET `position` = ' . $key . '
                    WHERE `id_item` = ' . (int)$id_item);
                if (!$res) break;
            }
        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('topmenu.tpl'));
        return $res;
    }
    public function updateBlockPosition($order = null)
    {
        if (is_null($order)) return false;
        $position = explode('::', $order);
        $res = false;
        if (count($position) > 0)
            foreach ($position as $key => $id_block)
            {
                $res = Db::getInstance()->execute('
                    UPDATE `' . _DB_PREFIX_ . 'advance_topmenu_blocks`
                    SET `position` = ' . $key . '
                    WHERE `id_block` = ' . (int)$id_block);
                if (!$res) break;
            }
        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('topmenu.tpl'));
        return $res;
    }
    private function displayBlockForm()
    {
        $id_sub = Tools::getValue('id_sub');
        if (!strlen($id_sub) > 0)
        {
            return;
        }
        $id_block = Tools::getValue('id_block');
        if (strlen($id_block) > 0)
        {
            $block = new Block($id_block);
        }
        else
        {
            $block = new Block($id_block);
        }
        $block->id_sub = (int)$id_sub;
        $this->context->smarty->assign(array(
            'form' => 'block',
            'block' => $block,
            ));
        return $this->display(__file__, 'views/templates/admin/block_form.tpl');
    }
    private function displaySubForm()
    {
        $id_sub = Tools::getValue('id_sub');
        if (strlen($id_sub) > 0)
        {
            $sub = new Submenu($id_sub);
        }
        else
        {
            $sub = new Submenu($id_sub);
        }
        $main_items = $this->getMainItem();
        $this->context->smarty->assign(array(
            'form' => 'sub',
            'submenu' => $sub,
            'main_items' => $main_items));
        return $this->display(__file__, 'views/templates/admin/sub_form.tpl');
    }
    private function displayForm()
    {
        $main_items = $this->getMainItem();
        $supmenu = $this->getSupMenu();
        foreach ($supmenu as &$sub)
        {
            $sub['blocks'] = $this->getAllBlocks($sub['id_sub']);
            if (count($sub['blocks']))
            {
                foreach ($sub['blocks'] as &$block)
                {
                    $block['items'] = $this->getItemByBlock($block['id_block']);
                }
            }
        }
        $this->context->smarty->assign(array(
            'form' => 'main',
            'imgpath' => $this->_path . 'img/',
            'supmenu' => $supmenu,
            'ajaxUrl' => $this->absoluteUrl . 'ajax.php?secure_key=' . $this->secure_key,
            'list_items' => $main_items));
        return $this->display(__file__, 'views/templates/admin/admin.tpl');
    }
    private function displayItemForm()
    {
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $languages = Language::getLanguages();
        $id_lang = $this->context->language->id;
        $item_id = (int)Tools::getValue('item_id');
        $link_texts = array();
        if ($item_id && Validate::isUnsignedId($item_id))
        {
            $Item = new Item($item_id);
        }
        else
        {
            $Item = new Item();
            $block_id = (int)Tools::getValue('block');
            $Item->id_block = $block_id;
        }
        if (Tools::isSubmit('submitnewItem'))
        {
            $Item->id_block = Tools::getValue('block_id');
            $Item->type = Tools::getValue('linktype');
            $Item->active = (int)Tools::getValue('active');
            $Item->class = Tools::getValue('custom_class');
            if ($Item->type == 'link')
            {
                $Item->icon = Tools::getValue('item_icon');
                $Item->link = Tools::getValue('link_value');
            }
            elseif ($Item->type == 'img')
            {
                $item_img = Tools::getValue('old_img');
                if (strlen($item_img) > 0) $Item->icon = $item_img;
                $Item->link = Tools::getValue('link_value');
            }
            elseif ($Item->type == 'html')
            {
                foreach ($languages as $language) $Item->text[$language['id_lang']] = Tools::getValue('item_html_' .
                        $language['id_lang']);
            }
        }
        $lang_ul = '<ul class="dropdown-menu">';
        $default_link_option = array();
        foreach ($languages as $lg)
        {
            $link_text = $this->fomartLink((array )$Item, $lg['id_lang']);
            $link_texts[$lg['id_lang']] = $link_text['link'];
            $lang_ul .= '<li><a href="javascript:hideOtherLanguage(' . $lg['id_lang'] . ');" tabindex="-1">' . $lg['name'] .
                '</a></li>';
            $default_link_option[$lg['id_lang']] = $this->getAllDefaultLink($lg['id_lang']);
        }
        $lang_ul .= '</ul>';
        $this->context->smarty->assign(array(
            'form' => 'item',
            'item' => $Item,
            'link_text' => $link_texts,
            'absoluteUrl' => $this->absoluteUrl,
            'default_link_option' => $default_link_option,
            'lang_ul' => $lang_ul,
            'langguages' => array(
                'default_lang' => $id_lang_default,
                'all' => $languages,
                'lang_dir' => _THEME_LANG_DIR_)));
        $iso = Language::getIsoById((int)($id_lang));
        $isoTinyMCE = (file_exists(_PS_ROOT_DIR_ . '/js/tiny_mce/langs/' . $iso . '.js') ? $iso : 'en');
        $ad = dirname($_SERVER["PHP_SELF"]);
        $html = '<script type="text/javascript">
    			var iso = \'' . $isoTinyMCE . '\' ;
    			var pathCSS = \'' . _THEME_CSS_DIR_ . '\' ;
    			var ad = \'' . $ad . '\' ;
    			$(document).ready(function(){
    			tinySetup({
    				editor_selector :"rte",
            		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,fontselect,fontsizeselect",
            		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,codemagic,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
            		theme_advanced_toolbar_location : "top",
            		theme_advanced_toolbar_align : "left",
            		theme_advanced_statusbar_location : "bottom",
            		theme_advanced_resizing : false,
                    extended_valid_elements: \'pre[*],script[*],style[*]\',
                    valid_children: "+body[style|script],pre[script|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
                    valid_elements : \'*[*]\',
                    force_p_newlines : false,
                    cleanup: false,
                    forced_root_block : false,
                    force_br_newlines : true
    				});
    			});</script>';
        return $html . $this->display(__file__, 'views/templates/admin/item_form.tpl');
    }
    private function getAllBlocks($id_sub = null)
    {
        if (is_null($id_sub)) return;
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'advance_topmenu_blocks`
                WHERE `id_sub` = ' . $id_sub . '
                ORDER BY  `position` ASC';
        $results = Db::getInstance()->executeS($sql);
        return $results;
    }
    private function getSupMenu($id_main = null, $active = false)
    {
        $id_lang = $this->context->language->id;
        $id_shop = (int)$this->context->shop->id;
        $sql = 'SELECT s.*, til.`title` FROM `' . _DB_PREFIX_ . 'advance_topmenu_sub` s

                LEFT JOIN `' . _DB_PREFIX_ .
            'advance_topmenu_items_lang` til ON (s.`id_parent` = til.`id_item`)

                LEFT JOIN `' . _DB_PREFIX_ .
            'advance_topmenu_main_shop` tms ON (s.`id_parent` = tms.`id_item`)

                WHERE til.`id_lang` = ' . (int)$id_lang . '

                 AND id_shop = ' . $id_shop . ($active ? ' AND active = ' . $active : '') . (is_null
            ($id_main) ? '' : ' AND s.`id_parent` = ' . (int)$id_main);
        $results = Db::getInstance()->executeS($sql);
        return $results;
    }
    private function getItemByBlock($id_block = null, $active = false)
    {
        if (is_null($id_block)) return false;
        $id_lang = (int)$this->context->language->id;
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'advance_topmenu_items` ti

                LEFT JOIN `' . _DB_PREFIX_ .
            'advance_topmenu_items_lang` til ON (ti.`id_item` = til.`id_item`)

                WHERE `id_block` = ' . (int)$id_block . ' AND

                id_lang = ' . $id_lang . ($active ? ' AND active = ' . $active : '') . '

                ORDER BY  ti.`position` ASC';
        $results = Db::getInstance()->executeS($sql);
        return $results;
    }
    private function getMainItem($active = false)
    {
        $id_lang = (int)$this->context->language->id;
        $id_shop = (int)$this->context->shop->id;
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'advance_topmenu_items` ti

                LEFT JOIN `' . _DB_PREFIX_ .
            'advance_topmenu_items_lang` til ON (ti.`id_item` = til.`id_item`)

                LEFT JOIN `' . _DB_PREFIX_ .
            'advance_topmenu_main_shop` tis ON (ti.`id_item` = tis.`id_item`)

                WHERE `id_block` = 0 AND

                tis.`id_shop` = ' . $id_shop . ' AND

                til.`id_lang` = ' . $id_lang . ($active ? ' AND active = ' . $active : '') . '

                ORDER BY  ti.`position` ASC';
        $results = Db::getInstance()->executeS($sql);
        return $results;
    }
    private function getCMSOptions($parent = 0, $depth = 0, $id_lang = false, $link = false)
    {
        $html = '';
        $id_lang = $id_lang ? (int)$id_lang : (int)Context::getContext()->language->id;
        $categories = $this->getCMSCategories(false, (int)$parent, (int)$id_lang);
        $pages = $this->getCMSPages((int)$parent, false, (int)$id_lang);
        $spacer = str_repeat('&nbsp;', 3 * (int)$depth);
        foreach ($categories as $category)
        {
            //$html .= '<option value="CMS_CAT'.$category['id_cms_category'].'" style="font-weight: bold;">'.$spacer.$category['name'].'</option>';
            $html .= $this->getCMSOptions($category['id_cms_category'], (int)$depth + 1, (int)$id_lang, $link);
            //$spacer = str_repeat('&nbsp;', 3 * (int)$depth);
        }
        foreach ($pages as $page)
            if ($link) $html .= '<option value="' . $this->context->link->getCMSLink($page['id_cms']) . '">' . (isset
                    ($spacer) ? $spacer : '') . $page['meta_title'] . '</option>';
            else  $html .= '<option value="CMS' . $page['id_cms'] . '">' . $page['meta_title'] . '</option>';
        return $html;
    }
    private function getCMSCategories($recursive = false, $parent = 1, $id_lang = false)
    {
        $id_lang = $id_lang ? (int)$id_lang : (int)Context::getContext()->language->id;
        if ($recursive === false)
        {
            $sql = 'SELECT bcp.`id_cms_category`, bcp.`id_parent`, bcp.`level_depth`, bcp.`active`, bcp.`position`, cl.`name`, cl.`link_rewrite`
				FROM `' . _DB_PREFIX_ . 'cms_category` bcp
				INNER JOIN `' . _DB_PREFIX_ . 'cms_category_lang` cl
				ON (bcp.`id_cms_category` = cl.`id_cms_category`)
				WHERE cl.`id_lang` = ' . (int)$id_lang . '
				AND bcp.`id_parent` = ' . (int)$parent;
            return Db::getInstance()->executeS($sql);
        }
        else
        {
            $sql = 'SELECT bcp.`id_cms_category`, bcp.`id_parent`, bcp.`level_depth`, bcp.`active`, bcp.`position`, cl.`name`, cl.`link_rewrite`
				FROM `' . _DB_PREFIX_ . 'cms_category` bcp
				INNER JOIN `' . _DB_PREFIX_ . 'cms_category_lang` cl
				ON (bcp.`id_cms_category` = cl.`id_cms_category`)
				WHERE cl.`id_lang` = ' . (int)$id_lang . '
				AND bcp.`id_parent` = ' . (int)$parent;
            $results = Db::getInstance()->executeS($sql);
            foreach ($results as $result)
            {
                $sub_categories = $this->getCMSCategories(true, $result['id_cms_category'], (int)$id_lang);
                if ($sub_categories && count($sub_categories) > 0) $result['sub_categories'] = $sub_categories;
                $categories[] = $result;
            }
            return isset($categories) ? $categories : false;
        }
    }
    private function getCMSPages($id_cms_category, $id_shop = false, $id_lang = false)
    {
        $id_shop = ($id_shop !== false) ? (int)$id_shop : (int)Context::getContext()->shop->id;
        $id_lang = $id_lang ? (int)$id_lang : (int)Context::getContext()->language->id;
        $sql = 'SELECT c.`id_cms`, cl.`meta_title`, cl.`link_rewrite`
			FROM `' . _DB_PREFIX_ . 'cms` c
			INNER JOIN `' . _DB_PREFIX_ . 'cms_shop` cs
			ON (c.`id_cms` = cs.`id_cms`)
			INNER JOIN `' . _DB_PREFIX_ . 'cms_lang` cl
			ON (c.`id_cms` = cl.`id_cms`)
			WHERE c.`id_cms_category` = ' . (int)$id_cms_category . '
			AND cs.`id_shop` = ' . (int)$id_shop . '
			AND cl.`id_lang` = ' . (int)$id_lang . '
			AND c.`active` = 1
			ORDER BY `position`';
        return Db::getInstance()->executeS($sql);
    }
    private function getPagesOption($id_lang = null, $link = false)
    {
        if (is_null($id_lang)) $id_lang = (int)$this->context->cookie->id_lang;
        $files = Meta::getMetasByIdLang($id_lang);
        $html = '';
        foreach ($files as $file)
        {
            if ($link) $html .= '<option value="' . $this->context->link->getPageLink($file['page']) . '">' . (($file['title'] !=
                    '') ? $file['title'] : $file['page']) . '</option>';
            else  $html .= '<option value="PAG' . $file['page'] . '">' . (($file['title'] != '') ? $file['title'] :
                    $file['page']) . '</option>';
        }
        return $html;
    }
    private function getCategoryOption($id_category = 1, $id_lang = false, $id_shop = false, $recursive = true,
        $link = false)
    {
        $html = '';
        $id_lang = $id_lang ? (int)$id_lang : (int)Context::getContext()->language->id;
        $category = new Category((int)$id_category, (int)$id_lang, (int)$id_shop);
        if (is_null($category->id)) return;
        if ($recursive)
        {
            $children = Category::getChildren((int)$id_category, (int)$id_lang, true, (int)$id_shop);
            $spacer = str_repeat('&nbsp;', 3 * (int)$category->level_depth);
        }
        $shop = (object)Shop::getShop((int)$category->getShopID());
        if (!in_array($category->id, array(Configuration::get('PS_HOME_CATEGORY'), Configuration::get('PS_ROOT_CATEGORY'))))
        {
            if ($link) $html .= '<option value="' . $this->context->link->getCategoryLink($category->id) . '">' . (isset
                    ($spacer) ? $spacer : '') . str_repeat('&nbsp;', 3 * (int)$category->level_depth) . $category->name .
                    '</option>';
            else  $html .= '<option value="CAT' . (int)$category->id . '">' . str_repeat('&nbsp;', 3 * (int)$category->level_depth) .
                    $category->name . '</option>';
        }
        elseif ($category->id != Configuration::get('PS_ROOT_CATEGORY'))
        {
            $html .= '<optgroup label="' . str_repeat('&nbsp;', 3 * (int)$category->level_depth) . $category->name .
                '">';
        }
        if (isset($children) && count($children))
            foreach ($children as $child)
            {
                $html .= $this->getCategoryOption((int)$child['id_category'], (int)$id_lang, (int)$child['id_shop'],
                    $recursive, $link);
            }
        return $html;
    }
    private function getAllDefaultLink($id_lang = null, $link = false)
    {
        if (is_null($id_lang)) $id_lang = (int)$this->context->language->id;
        $html = '<optgroup label="' . $this->l('Category') . '">';
        $html .= $this->getCategoryOption(1, $id_lang, false, true, $link);
        $html .= '</optgroup>';
        //CMS option
        $html .= '<optgroup label="' . $this->l('Cms') . '">';
        $html .= $this->getCMSOptions(0, 0, $id_lang, $link);
        $html .= '</optgroup>';
        //Manufacturer option
        $html .= '<optgroup label="' . $this->l('Manufacturer') . '">';
        $manufacturers = Manufacturer::getManufacturers(false, $id_lang);
        foreach ($manufacturers as $manufacturer)
        {
            if ($link) $html .= '<option value="' . $this->context->link->getManufacturerLink($manufacturer['id_manufacturer']) .
                    '">' . $manufacturer['name'] . '</option>';
            else  $html .= '<option value="MAN' . (int)$manufacturer['id_manufacturer'] . '">' . $manufacturer['name'] .
                    '</option>';
        }
        $html .= '</optgroup>';
        //Supplier option
        $html .= '<optgroup label="' . $this->l('Supplier') . '">';
        $suppliers = Supplier::getSuppliers(false, $id_lang);
        foreach ($suppliers as $supplier)
        {
            if ($link) $html .= '<option value="' . $this->context->link->getSupplierLink($supplier['id_supplier']) .
                    '">' . $supplier['name'] . '</option>';
            else  $html .= '<option value="SUP' . (int)$supplier['id_supplier'] . '">' . $supplier['name'] .
                    '</option>';
        }
        $html .= '</optgroup>';
        //Page option
        $html .= '<optgroup label="' . $this->l('Page') . '">';
        $html .= $this->getPagesOption($id_lang, $link);
        $shoplink = Shop::getShops();
        if (count($shoplink) > 1)
        {
            $html .= '<optgroup label="' . $this->l('Shops') . '">';
            foreach ($shoplink as $sh)
            {
                $html .= '<option value="SHO' . (int)$sh['id_shop'] . '">' . $sh['name'] . '</option>';
            }
        }
        $html .= '</optgroup>';
        return $html;
    }
    /**
     * Move an uploaded image to the module img/ folder
     */
    private function moveUploadedImage($file)
    {
        $img_name = time() . $file['name'];
        $main_name = $this->absolutePath . 'img' . DIRECTORY_SEPARATOR . $img_name;
        if (!move_uploaded_file($file['tmp_name'], $main_name))
        {
            return false;
        }
        return $img_name;
    }
    private function is_https()
    {
        return strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https' ? true : false;
    }
    private function fomartLink($item = null, $id_lang = null)
    {
        if (is_null($item)) return;
        if (!empty($this->context->controller->php_self)) $page_name = $this->context->controller->php_self;
        else
        {
            $page_name = Dispatcher::getInstance()->getController();
            $page_name = (preg_match('/^[0-9]/', $page_name) ? 'page_' . $page_name : $page_name);
        }
        $html = '';
        $selected_item = false;
        if (is_null($id_lang)) $id_lang = (int)$this->context->language->id;
        $type = substr($item['link'], 0, 3);
        $key = substr($item['link'], 3, strlen($item['link']) - 3);
        switch ($type)
        {
            case 'CAT':
                if ($page_name == 'category' && (int)Tools::getValue('id_category') == (int)$key) $selected_item = true;
                $html = $this->context->link->getCategoryLink((int)$key, null, $id_lang);
                break;
            case 'CMS':
                if ($page_name == 'cms' && (int)Tools::getValue('id_cms') == (int)$key) $selected_item = true;
                $html = $this->context->link->getCMSLink((int)$key, null, $id_lang);
                break;
            case 'MAN':
                if ($page_name == 'manufacturer' && (int)Tools::getValue('id_manufacturer') == (int)$key) $selected_item = true;
                $man = new Manufacturer((int)$key, $id_lang);
                $html = $this->context->link->getManufacturerLink($man->id, $man->link_rewrite, $id_lang);
                break;
            case 'SUP':
                if ($page_name == 'supplier' && (int)Tools::getValue('id_supplier') == (int)$key) $selected_item = true;
                $sup = new Supplier((int)$key, $id_lang);
                $html = $this->context->link->getSupplierLink($sup->id, $sup->link_rewrite, $id_lang);
                break;
            case 'PAG':
                $pag = Meta::getMetaByPage($key, $id_lang);
                $html = $this->context->link->getPageLink($pag['page'], true, $id_lang);
                if ($page_name == $pag['page']) $selected_item = true;
                break;
            case 'SHO':
                $shop = new Shop((int)$key);
                $html = $shop->getBaseURL();
                break;
            default:
                $html = $item['link'];
                break;
        }
        return array('link' => $html, 'selected_item' => $selected_item);
    }
    private function preHook()
    {
        $results = $this->getMainItem(true);
        if (count($results) > 0)
            foreach ($results as &$result)
            {
                $submenu = $this->getSupMenu($result['id_item'], true);
                if (count($submenu) > 0)
                {
                    $submenu = $submenu[0];
                    $blocks = $this->getAllBlocks($submenu['id_sub']);
                    if (count($blocks) > 0)
                    {
                        foreach ($blocks as &$block)
                        {
                            $items = $this->getItemByBlock($block['id_block'], true);
                            if (count($items) > 0)
                                foreach ($items as &$item)
                                {
                                    $checklink = $this->fomartLink($item);
                                    $item['link'] = $checklink['link'];
                                    $item['active'] = $checklink['selected_item'];
                                }
                            $block['items'] = $items;
                        }
                        $submenu['blocks'] = $blocks;
                        $result['submenu'] = $submenu;
                    }
                }
                $mainlink = $this->fomartLink($result);
                $result['link'] = $mainlink['link'];
                $result['active'] = $mainlink['selected_item'];
            }

        return $results;
    }
    public function hookDisplayBackOfficeHeader()
    {
        if (Tools::getValue('configure') != $this->name) return;
        $this->context->controller->addCSS($this->_path . 'css/admin.css');
        $this->context->controller->addJquery();
        $this->context->controller->addJS(_PS_JS_DIR_ . 'tiny_mce/tiny_mce.js');
        if (_PS_VERSION_ <= "1.6.0.11") {
            $this->context->controller->addJS(_PS_JS_DIR_ . 'tinymce.inc.js');
        }else {
            $this->context->controller->addJS(($this->_path) . 'js/tinymce.inc.js');
        }
        $this->context->controller->addJS($this->_path . 'js/jquery-ui.js');
        $this->context->controller->addJS($this->_path . 'js/topmenu_admin.js');
    }
	public function hookDisplayHomeTopMenu($params){
	    $page_name = Dispatcher::getInstance()->getController();
		$page_name = (preg_match('/^[0-9]/', $page_name) ? 'page_'.$page_name : $page_name);
        $this->context->smarty->assign('page_name', $page_name);
       
		$this->smarty->assign(array('MENU' => $this->preHook(), 'absoluteUrl' => $this->absoluteUrl));
  
        return $this->display(__file__, 'topmenu.tpl');
	}	
    public function hookDisplayTop($param)
    {
        $page_name = Dispatcher::getInstance()->getController();
		$page_name = (preg_match('/^[0-9]/', $page_name) ? 'page_'.$page_name : $page_name);
        $this->context->smarty->assign('page_name', $page_name);
        
        $this->smarty->assign(array('MENU' => $this->preHook(), 'absoluteUrl' => $this->absoluteUrl));
        
        return $this->display(__file__, 'topmenu.tpl');
    }
    public function hookDisplayHeader($params)
    {
        $this->hookHeader($params);
    }
    public function hookHeader($params)
    {
        $this->context->controller->addCSS(($this->_path) . 'css/topmenu.css', 'all');
        $this->context->controller->addJS(($this->_path) . 'js/top_menu.js');
    }
}
