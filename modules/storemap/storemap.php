<?php
/*
* 2014 Fashion
*
*  @author Fashion modules
*  @copyright  2014 Fashion modules
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_'))
	exit;

class StoreMap extends Module
{
	public function __construct()
	{
		$this->name = 'storemap';
		$this->tab = 'front_office_features';
		$this->version = 1.0;
		$this->author = 'OvicSoft';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('Ovic - Store Map');
		$this->description = $this->l('Displays store location with google map.');
	}

	public function install()
	{
		return parent::install() &&
		$this->registerHook('displayStoreMap') &&
		$this->registerHook('header');
	}

	public function hookdisplayStoreMap($params)
	{
		$distanceUnit = Configuration::get('PS_DISTANCE_UNIT');
		if (!in_array($distanceUnit, array('km', 'mi')))
			$distanceUnit = 'km';
		$this->context->smarty->assign(array(
			'defaultLat' => (float)Configuration::get('PS_STORES_CENTER_LAT'),
			'defaultLong' => (float)Configuration::get('PS_STORES_CENTER_LONG'),
			'storeName' => Configuration::get('PS_SHOP_NAME')
		));
		return $this->display(__FILE__, 'storemap.tpl');
	}

	public function hookHeader($params)
	{
		// CSS in global.css file
		$module_name = '';
		if (Validate::isModuleName(Tools::getValue('module')))
			$module_name = Tools::getValue('module');
		if (!empty($this->context->controller->php_self))
			$page_name = $this->context->controller->php_self;
		elseif (Tools::getValue('fc') == 'module' && $module_name != '')
			$page_name = 'module-' . $module_name . '-' . Tools::getValue('controller');
		elseif (preg_match('#^' . preg_quote($this->context->shop->physical_uri, '#') .
			'modules/([a-zA-Z0-9_-]+?)/(.*)$#', $_SERVER['REQUEST_URI'], $m))
			$page_name = 'module-' . $m[1] . '-' . str_replace(array('.php', '/'), array('',
					'-'), $m[2]);
		else {
			$page_name = Dispatcher::getInstance()->getController();
			$page_name = (preg_match('/^[0-9]/', $page_name) ? 'page_' . $page_name : $page_name);
		}
		if ($page_name != 'stores') {
			$default_country = new Country((int)Configuration::get('PS_COUNTRY_DEFAULT'));
			$this->context->controller->addJS('http' . ((Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? 's' : '') . '://maps.google.com/maps/api/js');
			$this->context->controller->addJS($this->_path . 'storemap.js');
			$this->context->controller->addCSS(($this->_path) . 'css/storemap.css', 'all');
		}
	}
}
