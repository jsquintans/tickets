<section class="div_full_width {$custom_class}">
	{if isset($rowContents) && $rowContents|@count >0}
		<div class="flexgroupbanners-rows">	
		{foreach from=$rowContents item=row name=rows}
			<ul class="flexgroupbanners-row {$row.custom_class} {if $row.width >0}col-sm-{$row.width}{/if} clearfix">
				{if isset($row.groups) && $row.groups|@count >0}
				{assign var='totalwidth' value=0}
							
					{foreach from=$row.groups item=group name=groups}
											
			            <!-- group banners -->                                                        
                        <li class="{$group.custom_class} {if $group.width >0} col-sm-{$group.width}{/if}">                            
                            {if isset($group.items) && $group.items|@count >0}
                                <!-- banner items -->
                                <div class="flexgroupbanners-banners">
                                    {foreach from=$group.items item=banneritem name=banneritems}
										<div class="banner-item {$banneritem.custom_class}">
											{if $banneritem.full_path}
								                <a href="{$banneritem.link}" target="_blank" title="{$banneritem.alt}">
								                	<img class="img-responsive" src="{$banneritem.full_path}" alt="{$banneritem.alt}" />
                                                    {if $banneritem.description}
        							                <span class="banner-description">{$banneritem.description|html_entity_decode}</span>
        							                {/if}
								                </a>
							                {/if}
							                
							            </div> 
                                    {/foreach}
                                </div>
                                <!-- end banner item -->
                            {/if}  
                        </li>
                        <!-- end group banners -->
					{/foreach}	
				{/if}			
			</ul>	
		{/foreach}
		</div>	
	{/if}
</section>