<section class="furniture-large {$custom_class}">
	{if isset($rowContents) && $rowContents|@count >0}
		<div class="flexgroupbanners-rows">
		{foreach from=$rowContents item=row name=rows}
			<div class="flexgroupbanners-row {$row.custom_class} {if $row.width >0}col-sm-{$row.width}{/if} clearfix">
                <div class="fur-slide">
				{if isset($row.groups) && $row.groups|@count >0}
				    {assign var='totalwidth' value=0}
					{foreach from=$row.groups item=group name=groups}						
			            <!-- group banners -->                                                        
                        <div class="item {$group.custom_class} {if $group.width >0} col-sm-{$group.width}{/if}">                            
                            {if isset($group.items) && $group.items|@count >0}
                                <!-- banner items -->
                                <div class="flexgroupbanners-banners">
                                    {foreach from=$group.items item=banneritem name=banneritems}
										<div class="banner-item {$banneritem.custom_class}">
											{if $banneritem.full_path}
								                <a href="{$banneritem.link}" target="_blank" title="{$banneritem.alt}">
								                	<img class="img-responsive" src="{$banneritem.full_path}" alt="{$banneritem.alt}" />
								                </a>
                                                {if $banneritem.description}
        							                <div class="item-inn">
                                                        <div class="item-hover">
                                                            {$banneritem.description|html_entity_decode}
                                                        </div>
                                                    </div>
    							                {/if}
							                {/if}
							                
							            </div> 
                                    {/foreach}
                                </div>
                                <!-- end banner item -->
                            {/if}  
                        </div>
                        <!-- end group banners -->
					{/foreach}	
				{/if}	
                </div>		
			</div>	
		{/foreach}
		</div>	
	{/if}
</section>