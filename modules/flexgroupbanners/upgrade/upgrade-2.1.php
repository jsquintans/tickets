<?php

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_2_1($object)
{
	$db = Db::getInstance(_PS_USE_SQL_SLAVE_);
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."flexgroupbanners_module_lang` LIKE 'id_shop'")){
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."flexgroupbanners_module_lang` ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `module_id`");
	}
		
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."flexgroupbanners_row` LIKE 'id_shop'")){
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."flexgroupbanners_row` ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `id`");
	}
		
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."flexgroupbanners_row_lang` LIKE 'id_shop'")){
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."flexgroupbanners_row_lang` ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `row_id`");
	}
		
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."flexgroupbanners_group` LIKE 'id_shop'")){
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."flexgroupbanners_group` ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `id`");
	}
		
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."flexgroupbanners_group_lang` LIKE 'id_shop'")){
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."flexgroupbanners_group_lang` ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `group_id`");
	}
		
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."flexgroupbanners_banner` LIKE 'id_shop'")){
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."flexgroupbanners_banner` ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `id`");
	}
		
	
	if(!$db->executeS("SHOW COLUMNS FROM `"._DB_PREFIX_."flexgroupbanners_banner_lang` LIKE 'id_shop'")){
		$db->execute("ALTER TABLE  `"._DB_PREFIX_."flexgroupbanners_banner_lang` ADD  `id_shop` TINYINT(3) UNSIGNED NOT NULL DEFAULT  '1' AFTER  `banner_id`");
	}
		
	
	return true;
}
