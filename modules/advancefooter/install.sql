DROP TABLE IF EXISTS `PREFIX_advance_footer_block_items`;
CREATE TABLE `PREFIX_advance_footer_block_items` (
  `id_item` int(6) NOT NULL AUTO_INCREMENT,
  `id_block` int(6) NOT NULL,
  `display_title` tinyint(1) DEFAULT '1',
  `position` int(3) DEFAULT NULL,
  `target` varchar(50) DEFAULT NULL,
  `itemtype` varchar(10) DEFAULT NULL,
  `content_key` varchar(50) DEFAULT NULL,
  `content_value` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_item`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `PREFIX_advance_footer_block_items_lang`;
CREATE TABLE `PREFIX_advance_footer_block_items_lang` (
  `id_item` int(6) NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id_item`,`id_lang`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `PREFIX_advance_footer_blocks`;
CREATE TABLE `PREFIX_advance_footer_blocks` (
  `id_block` int(6) NOT NULL AUTO_INCREMENT,
  `id_row` int(6) NOT NULL DEFAULT '1',
  `display_title` tinyint(1) DEFAULT '1',
  `position` int(3) DEFAULT NULL,
  `bclass` varchar(200) DEFAULT NULL,
  `width` int(6) DEFAULT NULL,
  PRIMARY KEY (`id_block`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `PREFIX_advance_footer_blocks_lang`;
CREATE TABLE `PREFIX_advance_footer_blocks_lang` (
  `id_block` int(6) NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_block`,`id_lang`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8; 


DROP TABLE IF EXISTS `PREFIX_advance_footer_row`;
CREATE TABLE `PREFIX_advance_footer_row` (
  `id_row` int(6) NOT NULL AUTO_INCREMENT,
  `rclass` varchar(200) DEFAULT NULL,
  `position` int(3) DEFAULT '0',
  `active` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id_row`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `PREFIX_advance_footer_shop`;
CREATE TABLE `PREFIX_advance_footer_shop` (
  `id_row` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_row`,`id_shop`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
