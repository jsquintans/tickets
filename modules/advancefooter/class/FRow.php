<?php
if (!defined('_PS_VERSION_'))
	exit;
class FRow extends ObjectModel{
	/** @var integer id_row */
	public $id_row;
    /** @var String custom class  */
    public $rclass;
    /** @var integer position  */
    public $position;
    /** @var Boolean active */
	public $active;

    public static $definition = array(
		'table' => 'advance_footer_row',
		'primary' => 'id_row',
		'fields' => array(
            'active' =>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
            'position'      =>  array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'rclass'         =>	array('type' => self::TYPE_STRING, 'validate' => 'isMessage')
		)
	);
    public function delete(){
        $res = Db::getInstance()->execute('
			DELETE FROM `'._DB_PREFIX_.'advance_footer_shop`
			WHERE `id_row` = '.(int)$this->id
		);

		$res &= parent::delete();
		return $res;
    }
	public function add($autodate = true, $null_values = false)
	{
		$context = Context::getContext();
		$id_shop = $context->shop->id;

		$res = parent::add($autodate, $null_values);
		$res &= Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'advance_footer_shop` (`id_shop`, `id_row`)
			VALUES('.(int)$id_shop.', '.(int)$this->id.')'
		);
		return $res;
	}
}