<?php if (!defined('_PS_VERSION_')) exit;
include_once (dirname(__file__) . '/class/FBlock.php');
include_once (dirname(__file__) . '/class/FItem.php');
include_once (dirname(__file__) . '/class/FRow.php');
class AdvanceFooter extends Module
{
    const INSTALL_SQL_FILE = 'install.sql'; 
    public $hookAssign = array();
    public $sameDatas = '';     
    protected static $tables = array(   'advance_footer_block_items'=>'', 
                                        'advance_footer_block_items_lang'=>'lang', 
                                        'advance_footer_blocks'=>'', 
                                        'advance_footer_row'=>'', 
                                        'advance_footer_blocks_lang'=>'lang', 
                                        'advance_footer_shop'=>''
                                        );
    public function __construct()
    {
        $this->name = 'advancefooter';
        $this->tab = 'front_office_features';
        $this->version = '2.0';
        $this->author = 'OvicSoft';
        parent::__construct();
        $this->displayName = $this->l('Ovic - Advanced Footer');
        $this->description = $this->l('Advanced Footer Module.');
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;
        $this->hookAssign = array(
            'rightcolumn',
            'leftcolumn',
            'home',
            'top',
            'footer');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        $this->sameDatas = dirname(__FILE__).'/samedatas/';
    }
    // this also works, and is more future-proof
    public function install()
    {
        if (!file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
            return false;
        else if (!$sql = file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE))
            return false;
        $sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
        $sql = preg_split("/;\s*[\r\n]+/", trim($sql));
                            
        foreach ($sql as $query){
            if (!Db::getInstance()->execute(trim($query))) return false;
        }
        if (!parent::install() || !$this->registerHook('header') || !$this->registerHook('footer') || !$this->registerHook ('displayBackOfficeHeader')) return false;
        //$this->importSameData();
        return true;
    }
    
       
                
     public  function importSameData($directory='', $file=''){
        if($directory) $this->sameDatas = $directory;
        $shopId = $this->context->shop->id;
        $currentOption = Configuration::get('OVIC_CURRENT_DIR',null,null,$shopId);
        if($currentOption) $currentOption .= '.';
        else $currentOption = '';
        if(!$file){
            $file = $this->sameDatas.'store'.$shopId.'.'.$currentOption.$this->name.'.xml';           
        }else{
            if(!file_exists($file))
                $file = $this->sameDatas.'store'.$shopId.'.'.$currentOption.$this->name.'.xml';
        }
        if(!file_exists($file))
            $file = $this->sameDatas.$currentOption.$this->name.'.xml';
        if(!file_exists($file))
            $file = $this->sameDatas.$this->name.'.xml';
        if(file_exists($file)){
            $db = Db::getInstance(_PS_USE_SQL_SLAVE_);
            $languages = $db->executeS("Select id_lang, iso_code From "._DB_PREFIX_."lang Where active = 1");           
            $xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);
            if(isset($xml->row)){
                if(count($xml->row) >0){
                    $db->execute("Delete il From `"._DB_PREFIX_."advance_footer_block_items_lang` il 
                                  INNER JOIN `"._DB_PREFIX_."advance_footer_block_items` i ON i.id_item = il.id_item     
                                  INNER JOIN `"._DB_PREFIX_."advance_footer_blocks` b ON b.id_block = i.id_block   
                                  INNER JOIN `"._DB_PREFIX_."advance_footer_shop` s ON b.id_row = s.id_row 
                                  WHERE s.id_shop =".$shopId);
                    $db->execute("Delete i From `"._DB_PREFIX_."advance_footer_block_items` i 
                                  INNER JOIN `"._DB_PREFIX_."advance_footer_blocks` b ON b.id_block = i.id_block   
                                  INNER JOIN `"._DB_PREFIX_."advance_footer_shop` s ON b.id_row = s.id_row 
                                  WHERE s.id_shop =".$shopId);
                    $db->execute("Delete bl From `"._DB_PREFIX_."advance_footer_blocks_lang` bl 
                                  INNER JOIN `"._DB_PREFIX_."advance_footer_blocks` b ON b.id_block = bl.id_block   
                                  INNER JOIN `"._DB_PREFIX_."advance_footer_shop` s ON b.id_row = s.id_row 
                                  WHERE s.id_shop =".$shopId);
                    $db->execute("Delete b From `"._DB_PREFIX_."advance_footer_blocks` b 
                                  INNER JOIN `"._DB_PREFIX_."advance_footer_shop` s ON b.id_row = s.id_row 
                                  WHERE s.id_shop =".$shopId);
                    $db->execute("Delete r From `"._DB_PREFIX_."advance_footer_row` r 
                                  INNER JOIN `"._DB_PREFIX_."advance_footer_shop` s ON r.id_row = s.id_row 
                                  WHERE s.id_shop =".$shopId);
                    $db->execute("Delete s From `"._DB_PREFIX_."advance_footer_shop` s 
                                  WHERE s.id_shop =".$shopId);
                    $r_pos = 0;
                    foreach($xml->row as $row){
                        if($insertId = $this->importRow($row,$r_pos)){
                            Db::getInstance()->insert('advance_footer_shop',array('id_row'=>$insertId, 'id_shop' => $shopId));
                            if(isset($row->blocks->block) && count($row->blocks->block)){
                                $this->importBlock($insertId,$row->blocks,$languages);
                            } 
                        }                       
                        $r_pos++;
                    }
                }
            }           
        }
        return true;
    }
    private function importRow($row,$pos){
        $db = Db::getInstance();
        $rowInsert = array(
            'rclass'          =>  (string)$row->rclass,
            'position'          =>  (int)$pos,
            'active'            =>  (int)$row->active,
            );
        if($db->insert('advance_footer_row', $rowInsert)){
            return $db->Insert_ID();
        }else
            return false;
    }
    private function importBlock($id_row,$blocks,$languages){
        $db = Db::getInstance();
        $pos = 0;
        foreach ($blocks->block as $block) {
        $arrInsert = array(
            'id_row' => $id_row,
            'display_title'  => (int)$block->display_title,
                'position' => (int)$pos,
            'bclass'  => $block->bclass,
            'width' => (int)$block->width,
        );
            $pos++;
        $this_languages = array();
        $langDefault = array();                     
        if(isset($block->langguages->language) && count($block->langguages->language) >0){
            foreach($block->langguages->language as $language){
                if(!$langDefault) $langDefault = array(
                    'title'=>(string)$language->title,
                );
                $this_languages[(string)$language->lang_iso] = array(
                    'title'=>(string)$language->title,
                );  
            }
        }                       
        if($db->insert('advance_footer_blocks', $arrInsert)){
            $insertId = $db->Insert_ID();               
            $arrInsertLangs = array(); 
            foreach($languages as $language){
                $lang_iso = $language['iso_code'];
                if($this_languages){
                        if (array_key_exists($lang_iso, $this_languages)) {
                        $arrInsertLangs[] = array(
                            'id_block'=>$insertId,
                            'id_lang'=>$language['id_lang'],
                            'title'=>$this_languages[$lang_iso]['title'],
                        );
                    }else{
                        $arrInsertLangs[] = array(
                            'id_block'=>$insertId,
                            'id_lang'=>$language['id_lang'],
                            'title'=>$langDefault['title'],
                        );
                    }
                }else{
                    $arrInsertLangs[] = array(
                        'id_block'=>$insertId,
                        'id_lang'=>$language['id_lang'],
                        'title'=>$langDefault['title'],
                    );
                }
            }  
            if($arrInsertLangs) Db::getInstance()->insert('advance_footer_blocks_lang', $arrInsertLangs);
            if(isset($block->items->item) && count($block->items->item) >0){
                foreach ($block->items->item as $item) {
                    $this->importItems($insertId, $item,$languages);
                    }
                }
            }
        }
    }
    private function importItems($block_id,$item,$languages){
        $db = Db::getInstance();
        $itemInsert = array(
            'id_block'          =>  (int)$block_id,
            'position'          =>  (int)$item->position,
            'display_title'     =>  (int)$item->display_title,
            'itemtype'          =>  (string)$item->itemtype,
            'target'            =>  (string)$item->target,
            'content_key'       =>  (string)$item->content_key,
            'content_value'     =>  (string)$item->content_value,
        );
        $this_languages = array();
        $langDefault = array();    
        if(isset($item->langguages->language) && count($item->langguages->language) >0){
            foreach($item->langguages->language as $language){
                $ltext = Tools::htmlentitiesDecodeUTF8($language->text);
                if(!$langDefault) $langDefault = array(
                    'title'=>(string)$language->title,
                    'text'=>Tools::htmlentitiesUTF8($ltext),
                );
                $this_languages[(string)$language->lang_iso] = array(
                    'title'=>(string)$language->title,
                    'text'=>Tools::htmlentitiesUTF8($ltext),
                );  
            }
        }                       
        if($db->insert('advance_footer_block_items', $itemInsert)){
            $insertId = $db->Insert_ID();
            $arrInsertLangs = array(); 
            foreach($languages as $language){
                $lang_iso = $language['iso_code'];
                if($this_languages){
                    if(array_key_exists($lang_iso, $this_languages)){
                        $arrInsertLangs[] = array(
                            'id_item'=>$insertId,
                            'id_lang'=>$language['id_lang'],
                            'title'=>$this_languages[$lang_iso]['title'],
                            'text'=>$this_languages[$lang_iso]['text'],
                        );
                    }else{
                        $arrInsertLangs[] = array(
                            'id_item'=>$insertId,
                            'id_lang'=>$language['id_lang'],
                            'title'=>$langDefault['title'],
                            'text'=>$langDefault['text'],
                        );
                    }
                }else{
                    $arrInsertLangs[] = array(
                        'id_item'=>$insertId,
                        'id_lang'=>$language['id_lang'],
                        'title'=>$langDefault['title'],
                        'text'=>$langDefault['text'],
                    );
                }
            }  
            if($arrInsertLangs) Db::getInstance()->insert('advance_footer_block_items_lang', $arrInsertLangs);
            return $insertId;
        }else
            return false;
    }
    public function exportSameData($directory=''){
        $shopId = $this->context->shop->id;
        if($directory) $this->sameDatas = $directory;
        //$langId = Context::getContext()->language->id;
        $currentOption = Configuration::get('OVIC_CURRENT_DIR',null,null,$shopId);
        if($currentOption) $currentOption .= '.';
        else $currentOption = '';       
        $rows = $this->getFooterRow();
        if($rows){
            $db = Db::getInstance();
            $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><rows></rows>');            
            foreach($rows as $row){
                $footerRows = $xml->addChild('row');             
                $footerRows->addChild('rclass',$row['rclass']);
                $footerRows->addChild('position',$row['position']);
                $footerRows->addChild('active',$row['active']);
                $sql = 'SELECT * FROM `'._DB_PREFIX_.'advance_footer_blocks` WHERE id_row='.$row['id_row'];
                $blocks = $db->executeS($sql);
                if (count($blocks) > 0)
                    {
                        $blocks_node = $footerRows->addChild('blocks');
                        foreach ($blocks as $block){
                            $block_node = $blocks_node->addChild('block');
                            $block_node->addChild('display_title', $block['display_title']);
                            $block_node->addChild('position', $block['position']);
                            $block_node->addChild('bclass', $block['bclass']); 
                            $block_node->addChild('width', $block['width']); 
                            $sql = 'SELECT * FROM `'._DB_PREFIX_.'advance_footer_blocks_lang` WHERE id_block='.$block['id_block'];
                            $block_lang = $db->executeS($sql);
                            if ($block_lang && count($block_lang) > 0)
                            {
                                $langguages = $block_node->addChild('langguages');
                                foreach ($block_lang as $bl) {
                                    $langs = $langguages->addChild('language');
                                    $langIso = LanguageCore::getIsoById($bl['id_lang']);
                                    $langs->addChild('lang_iso', $langIso);
                                    $langs->addChild('title',$bl['title']);
                                }
                            }
                            $sql = 'SELECT * FROM `'._DB_PREFIX_.'advance_footer_block_items` WHERE id_block='.$block['id_block'];
                            $block_items = $db->executeS($sql);
                            if ($block_items && count($block_items) > 0)
                            {
                                $items = $block_node->addChild('items');
                                foreach ($block_items as $bi) {
                                    $item =  $items->addChild('item');
                                    $item->addChild('display_title',$bi['display_title']);
                                    $item->addChild('position',$bi['position']);
                                    $item->addChild('target',$bi['target']);
                                    $item->addChild('itemtype',$bi['itemtype']);
                                    $ikey = $item->addChild('content_key');              
                                    $this->addNoteCData($ikey, $bi['content_key']);
                                    $ival = $item->addChild('content_value');              
                                    $this->addNoteCData($ival, $bi['content_value']);
                                    $sql = 'SELECT * FROM `'._DB_PREFIX_.'advance_footer_block_items_lang` WHERE id_item='.$bi['id_item'];
                                    $item_langs = $db->executeS($sql);
                                    if ($item_langs && count($item_langs) > 0)
                                    {
                                        $langguages = $item->addChild('langguages');
                                        foreach ($item_langs as $il) {
                                            $langs = $langguages->addChild('language');
                                            $langIso = LanguageCore::getIsoById($il['id_lang']);
                                            $langs->addChild('lang_iso', $langIso);
                                            $iltitle = $langs->addChild('title');
                                            $this->addNoteCData($iltitle,$il['title']);
                                            $iltext = $langs->addChild('text');              
                                            $this->addNoteCData($iltext, $il['text']);
                                        }
                                    }
                                }
                            }
                        }
                    }
            }                       
            $file = $this->sameDatas.'store'.$shopId.'.'.$currentOption.$this->name.'.xml';
            $xml->asXML($file);
        }
        return true;
    }
    private function addNoteCData(&$item, $text){ 
       $node= dom_import_simplexml($item); 
       $doc = $node->ownerDocument; 
       $node->appendChild($doc->createCDATASection($text)); 
    }
    
        
        
    public function uninstall()
    {
        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
        
        if (!parent::uninstall() || !$this->delConfig() || !$this->uninstallDB()) return false;
        return true;
    }
    private function delConfig()
    {
        Configuration::deleteByName('NB_ROWS');
        return true;
    }
    private function uninstallDb()
    {
        
        foreach(self::$tables as $table=>$value) Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.$table);
        return true;
    }
    public function getContent()
    {
        if(Tools::getValue('data-export')){
            $this->exportSameData();
            echo $this->l('Export data success!');
            die;
        }
        if(Tools::getValue('data-import')){
            $this->importSameData();
            echo $this->l('Import data success!');
            die;
        }
        
        $output = '';
        $errors = array();
        if (Tools::getValue('confirm_msg'))
        {
            $output .= $this->displayConfirmation(Tools::getValue('confirm_msg'));
        }
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $languages = Language::getLanguages(false);
        $lang_ul = '<ul class="dropdown-menu">';
        foreach ($languages as $lg)
        {
            $lang_ul .= '<li><a href="javascript:hideOtherLanguage(' . $lg['id_lang'] . ');" tabindex="-1">' . $lg['name'] .
                '</a></li>';
        }
        $lang_ul .= '</ul>';
        $this->context->smarty->assign(array(
            'postAction' => AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite
                ('AdminModules'),
            'lang_ul' => $lang_ul,
            'langguages' => array(
                'default_lang' => $id_lang_default,
                'all' => $languages,
                'lang_dir' => _THEME_LANG_DIR_)));
        if (Tools::isSubmit('submitnewrow'))
        {
            $id_row = (int)Tools::getValue('id_row');
            if ($id_row && Validate::isUnsignedId($id_row))
            {
                $footer_row = new FRow($id_row);
            }
            else
            {
                $footer_row = new FRow();
            }
            $footer_row->rclass = Tools::getValue('row_class');
            $footer_row->active = (int)Tools::getValue('active');
            if ($id_row && Validate::isUnsignedId($id_row))
            {
                if (!$footer_row->update())
                {
                    $errors[] = 'An error occurred while update data.';
                }
                else
                {
                    $output .= $this->displayConfirmation('Row successfully updated');
                }
            }
            else
            {
                if (!$footer_row->add())
                {
                    $errors[] = 'An error occurred while saving data.';
                }
                else
                {
                    $confirm_msg = $this->l('New row successfully added');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
                    Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                        Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg);
                }
            }
        }
        elseif (Tools::isSubmit('submitSaveBlock'))
        {
            $id_block = (int)Tools::getValue('id_block');
            if ($id_block && Validate::isUnsignedId($id_block))
            {
                $block = new FBlock($id_block);
            }
            else
            {
                $block = new FBlock();
            }
            $block->id_row = Tools::getValue('block_row');
            $block->display_title = Tools::getValue('title_show');
            $block->bclass = Tools::getValue('block_class');
            $block->width = Tools::getValue('block_width');
            $blocktitle_set = false;
            foreach ($languages as $language)
            {
                $blocktitle = Tools::getValue('blocktitle_' . $language['id_lang']);
                if (strlen($blocktitle) > 0)
                {
                    $blocktitle_set = true;
                }
                $block->title[$language['id_lang']] = $blocktitle;
            }
            if (!$blocktitle_set)
            {
                $lang_title = Language::getLanguage($this->context->language->id);
                $errors[] = 'This block title field is required at least in ' . $lang_title['name'];
            }
            if (!count($errors))
                if ($id_block && Validate::isUnsignedId($id_block))
                {
                    if ($block->update())
                    {
                        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
                        $output .= $this->displayConfirmation('Block Updated');
                    }
                    else
                    {
                        $errors[] = 'An error occurred while saving block.';
                    }
                }
                else
                {
                    if ($block->add())
                    {
                        $confirm_msg = $this->l('Block successfully saved.');
                        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
                        Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                            Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg);
                    }
                    else
                    {
                        $errors[] = 'An error occurred while update block.';
                    }
                }
        }
        elseif (Tools::isSubmit('submitRemoveBlock'))
        {
            $block_id = Tools::getValue('id_block');
            if ($this->deleteBlock($block_id))
            {
                Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
                $output .= '<div class="alert alert-success">' . $this->l('Block deleted') . '</div>';
            }
            else
            {
                $errors[] = 'An error occurred while delete block.';
            }
        }
        elseif (Tools::isSubmit('submitSaveItem'))
        {
            $id_item = Tools::getValue('id_item');
            if ($id_item && Validate::isUnsignedId($id_item))
            {
                $id_item = Tools::getValue('id_item');
                $item = new FItem($id_item);
            }
            else  $item = new FItem();
            $item->id_block = Tools::getValue('block_id');
            $itemtitle_set = false;
            foreach ($languages as $language)
            {
                $item_title = Tools::getValue('itemtitle_' . $language['id_lang']);
                if (strlen($item_title) > 0)
                {
                    $itemtitle_set = true;
                }
                $item->title[$language['id_lang']] = $item_title;
            }
            if (!$itemtitle_set)
            {
                $lang_title = Language::getLanguage($this->context->language->id);
                $errors[] = 'This item title field is required at least in ' . $lang_title['name'];
            }
            $item->display_title = Tools::getValue('title_show');
            $item->itemtype = Tools::getValue('item_type');
            switch ($item->itemtype)
            {
                case 'link':
                    $item->target = Tools::getValue('target');
                    $item->content_key = Tools::getValue('linktype');
                    $k = 'id_' . trim($item->content_key);
                    $item->content_value = Tools::getValue($k);
                    break;
                case 'html':
                    foreach ($languages as $language) $item->text[$language['id_lang']] = Tools::getValue('htmlbody_' .
                            $language['id_lang']);
                    break;
                case 'module':
                    $item->content_key = Tools::getValue('module');
                    $item->content_value = Tools::getValue('hook');
                    break;
            }
            if (!count($errors))
                if ($id_item && Validate::isUnsignedId($id_item))
                {
                    if ($item->update())
                    {
                        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
                        $output .= $this->displayConfirmation('Item Updated');
                    }
                    else
                    {
                        $errors[] = 'An error occurred while update item.';
                    }
                }
                else
                {
                    if ($item->add())
                    {
                        $confirm_msg = $this->l('Item successfully saved.');
                        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
                        Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                            Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg);
                    }
                    else
                    {
                        $errors[] = 'An error occurred while add new item.';
                    }
                }
        }
        elseif (Tools::getValue('removeitem') == 1)
        {
            $id_item = Tools::getValue('id_item');
            if ($id_item && Validate::isUnsignedId($id_item))
            {
                $item = new FItem($id_item);
                if ($item->delete())
                {
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
                    $output .= $this->displayConfirmation('Item deleted');
                }
                else
                {
                    $errors[] = 'An error occurred while delete item.';
                }
            }
        }
        elseif (Tools::isSubmit('changestatus'))
        {
            $id_row = (int)Tools::getValue('id_row');
            if ($id_row && Validate::isUnsignedId($id_row))
            {
                $f_row = new FRow($id_row);
                $f_row->active = !$f_row->active;
                if (!$f_row->update())
                {
                    $errors[] = 'An error occurred while chanage status.';
                }
                else
                {
                    $confirm_msg = $this->l('Row successfully updated.');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
                    Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&token=' .
                        Tools::getAdminTokenLite('AdminModules') . '&confirm_msg=' . $confirm_msg);
                }
            }
        }
        elseif (Tools::isSubmit('submit_del_row'))
        {
            $id_row = (int)Tools::getValue('id_row');
            if ($id_row && Validate::isUnsignedId($id_row))
            {
                if (!$this->deleteRow($id_row))
                {
                    $errors[] = 'An error occurred while delete row.';
                }
                else
                {
                    $output .= $this->displayConfirmation('Delete successful');
                    Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
                }
            }
        }
        if (count($errors) > 0)
        {
            if (isset($errors) && count($errors)) $output .= $this->displayError(implode('<br />', $errors));
        }
        if (Tools::isSubmit('submitItem') || (Tools::isSubmit('submitSaveItem') && count($errors) > 0))
                return $output . $this->displayItemForm();
        elseif (Tools::isSubmit('submitBlock') || (Tools::isSubmit('submitSaveBlock') && count($errors) > 0))
                return $output . $this->displayBlockForm();
        elseif (Tools::isSubmit('submitRow')) return $output . $this->displayRowForm();
        else  return $output . $this->displayForm();
    }
    private function getItemByBlock($id_block = null)
    {
        $results = array();
        if (is_null($id_block) || !Validate::isUnsignedId($id_block))
        {
            return $results;
        }
        $block = new FBlock($id_block);
        $items = $block->getItems();
        if ($items && count($items) > 0)
        {
            foreach ($items as $it)
            {
                $item = new FItem($it['id_item']);
                $results[] = $item;
            }
        }
        return $results;
    }
    public function updateRowPosition($order = null)
    {
        if (is_null($order) || strlen($order) < 1) return false;
        $position = explode('::', $order);
        $res = false;
        if (count($position) > 0)
            foreach ($position as $key => $id_row)
            {
                $res = Db::getInstance()->execute('
                    UPDATE `' . _DB_PREFIX_ . 'advance_footer_row`
                    SET `position` = ' . $key . '
                    WHERE `id_row` = ' . (int)$id_row);
                if (!$res) break;
            }
        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
        return $res;
    }
    public function updateItemPosition($order = null)
    {
        if (is_null($order) || strlen($order) < 1) return false;
        $position = explode('::', $order);
        $res = false;
        if (count($position) > 0)
            foreach ($position as $key => $id_item)
            {
                $res = Db::getInstance()->execute('
                    UPDATE `' . _DB_PREFIX_ . 'advance_footer_block_items`
                    SET `position` = ' . $key . '
                    WHERE `id_item` = ' . (int)$id_item);
                if (!$res) break;
            }
        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
        return $res;
    }
    public function updateBlockPosition($order = null)
    {
        if (is_null($order) || strlen($order) < 1) return false;
        $position = explode('::', $order);
        $res = false;
        if (count($position) > 0)
            foreach ($position as $key => $id_block)
            {
                $res = Db::getInstance()->execute('
                    UPDATE `' . _DB_PREFIX_ . 'advance_footer_blocks`
                    SET `position` = ' . $key . '
                    WHERE `id_block` = ' . (int)$id_block);
                if (!$res) break;
            }
        Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('advfooter.tpl'));
        return $res;
    }
    private function deleteRow($id_row = null)
    {
        if (is_null($id_row) || !Validate::isUnsignedId($id_row)) return false;
        $blocks = $this->getBlocks($id_row);
        $del = true;
        if ($blocks && count($blocks) > 0)
            foreach ($blocks as $bl)
            {
                $del &= $this->deleteBlock($bl['id_block']);
            }
        if ($del)
        {
            $frow = new FRow($id_row);
            return $frow->delete();
        }
        return false;
    }
    private function deleteBlock($id_block = null)
    {
        if (is_null($id_block) || !Validate::isUnsignedId($id_block)) return false;
        $items = $this->getItemByBlock($id_block);
        $del = true;
        if ($items && count($items) > 0)
            foreach ($items as $it)
            {
                $del &= $it->delete();
            }
        if ($del)
        {
            $block = new FBlock($id_block);
            return $block->delete();
        }
        return false;
    }
    /**
     * Render Configuration From for user making settings.
     *
     * @return context
     */
    private function displayForm()
    {
        $footer_rows = $this->getFooterRow();
        if (count($footer_rows))
        {
            //$footer_data = array();
            foreach ($footer_rows as &$row)
            {
                $blocks = $this->getBlocks($row['id_row']);
                if ($blocks && count($blocks) > 0)
                    foreach ($blocks as &$bl)
                    {
                        $block_obj = new FBlock($bl['id_block']);
                        $bl = (array )$block_obj;
                        $items = $this->getItemByBlock($block_obj->id);
                        $bl['items'] = $items;
                    }
                $row['blocks'] = $blocks;
                //$footer_data[$i]['blocks']= $blocks;
            }
        }
        $this->context->smarty->assign(array(
            'footer_data' => $footer_rows,
            'imgpath' => $this->_path . 'img/',
            'ajaxUrl' => $this->_path . 'ajax.php?secure_key=' . $this->secure_key,
            ));
        return $this->display(__file__, 'views/templates/admin/main.tpl');
    }
    public function displayRowForm()
    {
        $id_row = Tools::getValue('id_row');
        if ($id_row && Validate::isUnsignedId($id_row))
        {
            $footer_row = new FRow($id_row);
        }
        else
        {
            $footer_row = new FRow();
        }
        $this->context->smarty->assign(array('footer_row' => $footer_row));
        return $this->display(__file__, 'views/templates/admin/row_form.tpl');
    }
    public function displayBlockForm()
    {
        $id_block = (int)Tools::getValue('id_block');
        if ($id_block && Validate::isUnsignedId($id_block))
        {
            $fblock = new FBlock($id_block);
        }
        else
        {
            $fblock = new FBlock();
        }
        $fblock->id_row = Tools::getValue('block_row');
        if (Tools::isSubmit('submitSaveBlock'))
        {
            $languages = Language::getLanguages(false);
            $fblock->display_title = Tools::getValue('title_show');
            $fblock->bclass = Tools::getValue('block_class');
            $fblock->width = Tools::getValue('block_width');
            foreach ($languages as $language)
            {
                $fblock->title[$language['id_lang']] = Tools::getValue('blocktitle_' . $language['id_lang']);
            }
        }
        $this->context->smarty->assign(array('block_obj' => $fblock));
        return $this->display(__file__, 'views/templates/admin/block_form.tpl');
    }
    public function displayItemForm()
    {
        $id_item = Tools::getValue('id_item');
        if ($id_item && Validate::isUnsignedId($id_item))
        {
            $edit = true;
            $item = new FItem($id_item);
        }
        else
        {
            $edit = false;
            $item = new FItem();
        }
        $item->id_block = (int)Tools::getValue('block_id');
        if (Tools::isSubmit('submitSaveItem'))
        {
            $languages = Language::getLanguages(false);
            foreach ($languages as $language)
            {
                $item->title[$language['id_lang']] = Tools::getValue('itemtitle_' . $language['id_lang']);
            }
            $item->display_title = Tools::getValue('title_show');
            $item->itemtype = Tools::getValue('item_type');
            switch ($item->itemtype)
            {
                case 'link':
                    $item->target = Tools::getValue('target');
                    $item->content_key = Tools::getValue('linktype');
                    $k = 'id_' . trim($item->content_key);
                    $item->content_value = Tools::getValue($k);
                    break;
                case 'html':
                    foreach ($languages as $language) $item->text[$language['id_lang']] = Tools::getValue('htmlbody_' .
                            $language['id_lang']);
                    break;
                case 'module':
                    $item->content_key = Tools::getValue('module');
                    $item->content_value = Tools::getValue('hook');
                    break;
            }
        }
        if ($edit && strlen($item->content_value) > 0)
        {
            $categoryOption = $this->getCategoryOption($item->content_value);
            $cmsOption = $this->getCMSOptions($item->content_value);
            $supplierOption = $this->getSupplierOption($item->content_value);
            $manufacturerOption = $this->getManufacturerOption($item->content_value);
            $pageOption = $this->getPagesOption($item->content_value);
        }
        else
        {
            $categoryOption = $this->getCategoryOption();
            $cmsOption = $this->getCMSOptions();
            $supplierOption = $this->getSupplierOption();
            $manufacturerOption = $this->getManufacturerOption();
            $pageOption = $this->getPagesOption();
        }
        $hookOption = '';
        if ($edit && strlen($item->content_key) > 0)
        {
            $moduleOption = $this->getModulesOption($item->content_key);
            if (strlen($item->content_value) > 0)
            {
                $hookOption = $this->getHookOptionByModuleName($item->content_key, $item->content_value);
            }
            else
            {
                $hookOption = $this->getHookOptionByModuleName($item->content_key);
            }
        }
        else
        {
            $moduleOption = $this->getModulesOption();
        }
        $this->context->smarty->assign(array(
            'item' => $item,
            'categoryOption' => $categoryOption,
            'cmsOption' => $cmsOption,
            'supplierOption' => $supplierOption,
            'manufacturerOption' => $manufacturerOption,
            'pageOption' => $pageOption,
            'moduleOption' => $moduleOption,
            'hookOption' => $hookOption,
            'ajaxPath' => $this->_path . 'ajax.php?secure_key=' . $this->secure_key));
        $iso = Language::getIsoById((int)($this->context->language->id));
        $isoTinyMCE = (file_exists(_PS_ROOT_DIR_ . '/js/tiny_mce/langs/' . $iso . '.js') ? $iso : 'en');
        $ad = dirname($_SERVER["PHP_SELF"]);
        $html = '<script type="text/javascript">
                        var iso = \'' . $isoTinyMCE . '\' ;
                        var pathCSS = \'' . _THEME_CSS_DIR_ . '\' ;
                        var ad = \'' . $ad . '\' ;
                        $(document).ready(function(){
                            tinySetup({
                                editor_selector :"rte",
                                theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,fontselect,fontsizeselect",
                                theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,codemagic,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                                theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                                theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
                                theme_advanced_toolbar_location : "top",
                                theme_advanced_toolbar_align : "left",
                                theme_advanced_statusbar_location : "bottom",
                                theme_advanced_resizing : false,
                                extended_valid_elements: \'pre[*],script[*],style[*]\',
                                valid_children: "+body[style|script],pre[script|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
                                valid_elements : \'*[*]\',
                                force_p_newlines : false,
                                cleanup: false,
                                forced_root_block : false,
                                force_br_newlines : true
                            });
                        });
                    </script>';
        return $html . $this->display(__file__, 'views/templates/admin/item_form.tpl');
    }
    private function getFooterRow($active = null)
    {
        $id_shop = $this->context->shop->id;
        $sql = 'SELECT r.* FROM `' . _DB_PREFIX_ . 'advance_footer_row` r  
                LEFT JOIN `' . _DB_PREFIX_ . 'advance_footer_shop` s on s.id_row = r.id_row 
                WHERE s.id_shop =' .$id_shop. 
                (!is_null($active) && $active ?' AND active = 1' : '') . ' ORDER BY  `position` ASC, `id_row` ASC';
        $result = Db::getInstance()->executeS($sql);
        return $result;
    }
    private function getSupplierOption($selected_id = null)
    {
        $html = '';
        $suppliers = Supplier::getSuppliers(false, $this->context->language->id);
        foreach ($suppliers as $supplier)
        {
            $html .= '<option value="' . $supplier['id_supplier'] . '" ' . ((!is_null($selected_id) && $selected_id ==
                $supplier['id_supplier']) ? 'selected = "selected"' : '') . '>' . $supplier['name'] . '</option>';
        }
        return $html;
    }
    private function getManufacturerOption($selected_id = null)
    {
        $html = '';
        $manufacturers = Manufacturer::getManufacturers(false, $this->context->language->id);
        foreach ($manufacturers as $manufacturer)
        {
            $html .= '<option value="' . $manufacturer['id_manufacturer'] . '"' . ((!is_null($selected_id) && $selected_id ==
                $manufacturer['id_manufacturer']) ? 'selected = "selected"' : '') . '>' . $manufacturer['name'] .
                '</option>';
        }
        return $html;
    }
    private function getBlocks($row, $total = false)
    {
        $id_lang = $this->context->language->id;
        if ($total)
        {
            $sql = 'SELECT count(id) FROM `' . _DB_PREFIX_ . 'advance_footer_blocks` b
LEFT JOIN `' . _DB_PREFIX_ . 'advance_footer_blocks_lang` bl ON (b.`id_block` = bl.`id_block`)
WHERE `id_row` = ' . (int)$row . ' AND
bl.`id_lang` = ' . (int)$id_lang;
        }
        else
        {
            $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'advance_footer_blocks` b
LEFT JOIN `' . _DB_PREFIX_ . 'advance_footer_blocks_lang` bl ON (b.`id_block` = bl.`id_block`)
WHERE `id_row` = ' . (int)$row . ' AND
bl.`id_lang` = ' . (int)$id_lang . ' ORDER BY  b.`position` ASC';
        }
        $result = Db::getInstance()->executeS($sql);
        return $result;
    }
    public function getCategoryOption($selected = null, $id_category = 1, $id_lang = false, $id_shop = false,
        $recursive = true)
    {
        $html = '';
        $id_lang = $id_lang ? (int)$id_lang : (int)Context::getContext()->language->id;
        $category = new Category((int)$id_category, (int)$id_lang, (int)$id_shop);
        if (is_null($category->id)) return '';
        if ($recursive)
        {
            $children = Category::getChildren((int)$id_category, (int)$id_lang, true, (int)$id_shop);
        }
        $shop = (object)Shop::getShop((int)$category->getShopID());
        if ($category->id != 1) $html .= '<option ' . ($selected == $category->id ? 'selected="selected"' :
                '') . ' value="' . (int)$category->id . '">' . $category->name . '</option>';
        if (isset($children) && count($children))
            foreach ($children as $child)
            {
                $html .= $this->getCategoryOption($selected, (int)$child['id_category'], (int)$id_lang, (int)$child['id_shop']);
            }
        return $html;
    }
    public function getCMSOptions($selected = null, $parent = 0, $depth = 1, $id_lang = false,$id_shop = false)
    {
        $html = '';
        $id_lang = $id_lang ? (int)$id_lang : (int)$this->context->language->id;
        $id_shop = $id_shop ? (int)$id_shop : (int)$this->context->shop->id;
        $categories = $this->getCMSCategories(false, (int)$parent, (int)$id_lang);
        $pages = $this->getCMSPages((int)$parent, $id_shop, (int)$id_lang);
        foreach ($pages as $page) $html .= '<option ' . ($selected == $page['id_cms'] ?
                'selected="selected"' : '') . ' value="' . $page['id_cms'] . '">' . $page['meta_title'] .
                '</option>';
        foreach ($categories as $category)
        {
            $html .= $this->getCMSOptions($selected, $category['id_cms_category'], (int)$depth + 1, (int)$id_lang,$id_shop);
        }
        return $html;
    }
    private function getCMSCategories($recursive = false, $parent = 1, $id_lang = false)
    {
        $id_lang = $id_lang ? (int)$id_lang : (int)$this->context->language->id;
        $id_shop = (int)$this->context->shop->id;
        if ($recursive === false)
        {
            $sql = 'SELECT bcp.`id_cms_category`, bcp.`id_parent`, bcp.`level_depth`, bcp.`active`, bcp.`position`, cl.`name`, cl.`link_rewrite`
FROM `' . _DB_PREFIX_ . 'cms_category` bcp
                LEFT JOIN `' . _DB_PREFIX_ . 'cms_category_lang` cl
ON (bcp.`id_cms_category` = cl.`id_cms_category`)
                WHERE bcp.`id_parent` = ' . (int)$parent.
                ' AND cl.id_shop = ' . $id_shop.
                ' AND cl.`id_lang` = ' . $id_lang;
            return Db::getInstance()->executeS($sql);
        }
        else
        {
            $sql = 'SELECT bcp.`id_cms_category`, bcp.`id_parent`, bcp.`level_depth`, bcp.`active`, bcp.`position`, cl.`name`, cl.`link_rewrite`
FROM `' . _DB_PREFIX_ . 'cms_category` bcp
                LEFT JOIN `' . _DB_PREFIX_ . 'cms_category_lang` cl
                    ON (bcp.`id_cms_category` = cl.`id_cms_category` AND cl.`id_lang` = ' . (int)$id_lang . ')
                WHERE bcp.`id_parent` = ' . (int)$parent.
                ' AND cl.id_shop = ' . $id_shop.
                ' AND cl.`id_lang` = ' . $id_lang;
            $results = Db::getInstance()->executeS($sql);
            foreach ($results as $result)
            {
                $sub_categories = $this->getCMSCategories(true, $result['id_cms_category'], (int)$id_lang);
                if ($sub_categories && count($sub_categories) > 0) $result['sub_categories'] = $sub_categories;
                $categories[] = $result;
            }
            return isset($categories) ? $categories : false;
        }
    }
    private function getCMSPages($id_cms_category, $id_shop = false, $id_lang = false)
    {
        $id_shop = $id_shop ? (int)$id_shop : (int)$this->context->shop->id;
        $id_lang = $id_lang ? (int)$id_lang : (int)$this->context->language->id;
        $sql = 'SELECT c.`id_cms`, cl.`meta_title`, cl.`link_rewrite`
FROM `' . _DB_PREFIX_ . 'cms` c
            LEFT JOIN `' . _DB_PREFIX_ . 'cms_lang` cl
ON (c.`id_cms` = cl.`id_cms`)
WHERE c.`id_cms_category` = ' . (int)$id_cms_category . '
                AND cl.`id_shop` = ' . (int)$id_shop . '
AND cl.`id_lang` = ' . (int)$id_lang . '
AND c.`active` = 1
ORDER BY `position`';
        return Db::getInstance()->executeS($sql);
    }
    private function getModules()
    {
        $id_shop = (int)Context::getContext()->shop->id;
        $results = Db::getInstance()->ExecuteS('
SELECT m.*
FROM `' . _DB_PREFIX_ . 'module` m
JOIN `' . _DB_PREFIX_ . 'module_shop` ms ON (m.`id_module` = ms.`id_module` AND ms.`id_shop` = ' . (int)
            ($id_shop) . ')
WHERE m.`name` <> \'' . $this->name . '\'');
        if (count($results) > 0)
        {
            $modules = array();
            foreach ($results as $result)
            {
                if ($this->getHooksByModuleName($result['name'])) $modules[] = $result;
            }
        }
        return $modules;
    }
    private function getModulById($id_module)
    {
        return Db::getInstance()->getRow('
SELECT m.*
FROM `' . _DB_PREFIX_ . 'module` m
JOIN `' . _DB_PREFIX_ . 'module_shop` ms ON (m.`id_module` = ms.`id_module` AND ms.`id_shop` = ' . (int)
            ($this->context->shop->id) . ')
WHERE m.`id_module` = ' . $id_module);
    }
    private function getModulesOption($selected = null)
    {
        $modules = $this->getModules();
        $html = '';
        if (count($modules) > 0)
        {
            foreach ($modules as $m)
            {
                if (is_null($selected)) $html .= '<option value="' . $m['name'] . '">' . $m['name'] . '</option>';
                else  $html .= '<option ' . ($selected == $m['name'] ? 'selected="selected"' : '') . ' value="' . $m['name'] .
                        '">' . $m['name'] . '</option>';
            }
        }
        return $html;
    }
    private function getHookByArrName($arrName)
    {
        $result = Db::getInstance()->ExecuteS('
SELECT `id_hook`, `name`
FROM `' . _DB_PREFIX_ . 'hook`
WHERE `name` IN (\'' . implode("','", $arrName) . '\')');
        return $result;
    }
    private function getHooksByModuleName($module_name)
    {
        $moduleInstance = Module::getInstanceByName($module_name);
        $hooks = array();
        if ($this->hookAssign)
        {
            foreach ($this->hookAssign as $hook)
            {
                if (_PS_VERSION_ < "1.5")
                {
                    if (is_callable(array($moduleInstance, 'hook' . $hook)))
                    {
                        $hooks[] = $hook;
                    }
                }
                else
                {
                    $retro_hook_name = Hook::getRetroHookName($hook);
                    if (is_callable(array($moduleInstance, 'hook' . $hook)) || is_callable(array($moduleInstance, 'hook' .
                            $retro_hook_name)))
                    {
                        $hooks[] = $retro_hook_name;
                    }
                }
            }
        }
        $results = self::getHookByArrName($hooks);
        return $results;
    }
    public function getHookOptionByModuleName($module_name, $selected = null)
    {
        $hooks = $this->getHooksByModuleName($module_name);
        if (count($hooks) > 0)
        {
            $html = '';
            foreach ($hooks as $h)
            {
                $html .= '<option ' . ($selected == $h['name'] ? 'selected="selected"' : '') . ' value="' . $h['name'] .
                    '">' . $h['name'] . '</option>';
            }
            return $html;
        }
        return '';
    }
    /**
     * Execute modules for specified hook
     * @param module $moduleInstance Execute hook for this module only
     * @param string $hook_name Hook Name
     * @return string modules output
     */
     private function ModuleHookExec($module_name = '', $hook_name = ''){
        $output ='';
        if ($module_name == '' || !Validate::isModuleName($module_name)){
            return '';
        }
        $moduleInstance = Module::getInstanceByName($module_name);
        if (Validate::isLoadedObject($moduleInstance) && $moduleInstance->id) {
            $altern = 0;
            $id_hook = Hook::getIdByName($hook_name);
            $retro_hook_name = Hook::getRetroHookName($hook_name);
            $disable_non_native_modules = (bool)Configuration::get('PS_DISABLE_NON_NATIVE_MODULE');
            if ($disable_non_native_modules && Hook::$native_module && count(Hook::$native_module) && !in_array($moduleInstance->name, self::$native_module))
                return '';
            //check disable module
            $device = (int)$this->context->getDevice();
           if (Db::getInstance()->getValue('
            SELECT COUNT(`id_module`) FROM '._DB_PREFIX_.'module_shop
            WHERE enable_device & '.(int)$device.' AND id_module='.(int)$moduleInstance->id.
            Shop::addSqlRestriction()) == 0)
                return '';
            // Check permissions
            
            $exceptions = $moduleInstance->getExceptions($id_hook);

            $controller = Dispatcher::getInstance()->getController();
            $controller_obj = Context::getContext()->controller;
            
            //check if current controller is a module controller
            if (isset($controller_obj->module) && Validate::isLoadedObject($controller_obj->module))
                $controller = 'module-'.$controller_obj->module->name.'-'.$controller;
            
            if (in_array($controller, $exceptions))
                return '';
            
            //retro compat of controller names
            $matching_name = array(
                'authentication' => 'auth',
                'productscomparison' => 'compare'
            );
            if (isset($matching_name[$controller]) && in_array($matching_name[$controller], $exceptions))
                return '';
            if (Validate::isLoadedObject($this->context->employee) && !$moduleInstance->getPermission('view', $this->context->employee))
                return '';
            
            if (!isset($hook_args['cookie']) or !$hook_args['cookie'])
                $hook_args['cookie'] = $this->context->cookie;
            if (!isset($hook_args['cart']) or !$hook_args['cart'])
                $hook_args['cart'] = $this->context->cart;
            $hook_callable = is_callable(array($moduleInstance, 'hook'.$hook_name));
            $hook_retro_callable = is_callable(array($moduleInstance, 'hook'.$retro_hook_name));
            if (($hook_callable || $hook_retro_callable) && Module::preCall($moduleInstance->name))
            {
                $hook_args['altern'] = ++$altern;
                // Call hook method
                if ($hook_callable)
                    $display = $moduleInstance->{'hook'.$hook_name}($hook_args);
                elseif ($hook_retro_callable)
                    $display = $moduleInstance->{'hook'.$retro_hook_name}($hook_args);
                $output .= $display;
            }
            
        }
        return $output;
     }
    
    public function getPagesOption($selected = null)
    {
        $files = Meta::getMetasByIdLang((int)$this->context->cookie->id_lang);
        $html = '';
        foreach ($files as $file)
        {
            $html .= '<option ' . ($selected == $file['page'] ? 'selected="selected"' : '') . ' value="' . $file['page'] .
                '">' . (($file['title'] != '') ? $file['title'] : $file['page']) . '</option>';
        }
        return $html;
    }
    public function hookFooter($params)
    {

        if (!$this->isCached('advfooter.tpl', $this->getCacheId()))
        {
            $footer_rows = $this->getFooterRow(true);
            if (count($footer_rows))
            {
                foreach ($footer_rows as &$row)
                {
                    $blocks = $this->getBlocks($row['id_row']);
                    if (count($blocks) > 0)
                    {
                        foreach ($blocks as &$block)
                        {
                            $block_obj = new FBlock($block['id_block']);
                            //$block['items'] = $block_obj->getItems();
                            $items = $block_obj->getItems();
                            if (count($items) > 0)
                            {
                                $it = array();
                                foreach ($items as $item)
                                {
                                    $it[$item['id_item']]['type'] = $item['itemtype'];
                                    switch ($item['itemtype'])
                                    {
                                        case 'link':
                                            switch ($item['content_key'])
                                            {
                                                case 'category':
                                                    $it[$item['id_item']]['html'] = '<a href="' . $this->context->link->getCategoryLink($item['content_value']) .
                                                        '" title="' . $item['title'] . '">' . ($item['display_title'] ? $item['title'] : '') . '</a>';
                                                    break;
                                                case 'cms':
                                                    $it[$item['id_item']]['html'] = '<a href="' . $this->context->link->getCMSLink($item['content_value']) .
                                                        '" title="' . $item['title'] . '">' . ($item['display_title'] ? $item['title'] : '') . '</a>';
                                                    break;
                                                case 'manufacturer':
                                                    $it[$item['id_item']]['html'] = '<a href="' . $this->context->link->getManufacturerLink($item['content_value']) .
                                                        '" title="' . $item['title'] . '">' . ($item['display_title'] ? $item['title'] : '') . '</a>';
                                                    break;
                                                case 'supplier':
                                                    $it[$item['id_item']]['html'] = '<a href="' . $this->context->link->getSupplierLink($item['content_value']) .
                                                        '" title="' . $item['title'] . '">' . ($item['display_title'] ? $item['title'] : '') . '</a>';
                                                    break;
                                                case 'page':
                                                    $it[$item['id_item']]['html'] = '<a href="' . $this->context->link->getPageLink($item['content_value']) .
                                                        '" title="' . $item['title'] . '">' . ($item['display_title'] ? $item['title'] : '') . '</a>';
                                                    break;
                                                case 'other':
                                                    $it[$item['id_item']]['html'] = '<a href="' . $item['content_value'] . '" title="' . $item['title'] .
                                                        '">' . ($item['display_title'] ? $item['title'] : '') . '</a>';
                                                    break;
                                            }
                                            break;
                                        case 'html':
                                            $it[$item['id_item']]['html'] = Tools::htmlentitiesDecodeUTF8($item['text']);
                                            break;
                                        case 'module':
                                            $it[$item['id_item']]['html'] = $this->ModuleHookExec($item['content_key'], $item['content_value']);
                                            break;
                                    }
                                }
                                $block['items'] = $it;
                            }
                        }
                        $row['blocks'] = $blocks;
                    }
                }
            }
            $this->smarty->assign(array('footers' => $footer_rows));
        }
        return $this->display(__file__, 'advfooter.tpl', $this->getCacheId());
    }
    public function hookDisplayHeader($params)
    {
        $this->context->controller->addCSS(($this->_path) . 'css/advancefooter.css','all');
        $this->context->controller->addJS(($this->_path) . 'js/advancefooter.js');
    }
    public function hookDisplayBackOfficeHeader()
    {
        if (Tools::getValue('configure') != $this->name) return;
        $this->context->controller->addCSS($this->_path . 'css/admin.css');
        $this->context->controller->addJquery();
        $this->context->controller->addJS(_PS_JS_DIR_ . 'tiny_mce/tiny_mce.js');
        if (_PS_VERSION_ <= "1.6.0.11") {
            $this->context->controller->addJS(_PS_JS_DIR_ . 'tinymce.inc.js');
        }else {
            $this->context->controller->addJS(($this->_path) . 'js/tinymce.inc.js');
        }
        $this->context->controller->addJqueryUi('ui.sortable');
        //$this->context->controller->addJS($this->_path . 'js/jquery-ui.js');
        $this->context->controller->addJS($this->_path . 'js/admin_footer.js');
    }
}
?>