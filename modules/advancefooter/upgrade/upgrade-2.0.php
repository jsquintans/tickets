<?php

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_2_0($object)
{
	$db = Db::getInstance(_PS_USE_SQL_SLAVE_);
	$db->execute("ALTER TABLE  `"._DB_PREFIX_."advance_footer_shop` CHANGE  `id_block` `id_row` INT(10) UNSIGNED NOT NULL");
	
	return true;
}
