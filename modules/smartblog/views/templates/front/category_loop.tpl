<div id="smartblogpost-{$post.id_post}" class="row">
    {assign var="options" value=null}
    {$options.id_post = $post.id_post} 
    {$options.slug = $post.link_rewrite} 
    {assign var="options" value=null}
    {$options.id_post = $post.id_post}
    {$options.slug = $post.link_rewrite}
    {assign var="catlink" value=null}
    {assign var="activeimgincat" value='0'}
    {$activeimgincat = $smartshownoimg}
    {$catlink.id_category = $post.id_category}
    {$catlink.slug = $post.cat_link_rewrite}
    {assign var="options" value=null}
    {$options.id_post = $post.id_post}  
    {$options.slug = $post.link_rewrite}
    <div class="col-sm-5"> 
        <a itemprop="url" title="{$post.meta_title}">
             
            {if ($post.post_img != "no" && $activeimgincat == 0) || $activeimgincat == 1}
                <img alt="{$post.meta_title}" src="{$modules_dir}/smartblog/images/{$post.post_img}-single-default.jpg" class="img-responsive" />
            {/if}
        </a>
    </div>
    <div class="col-sm-7">
        <span class="tags"><a href="{smartblog::GetSmartBlogLink('smartblog_category',$catlink)}">{if $title_category != ''}{$title_category}{else}{$post.cat_name}{/if}</a></span> 
        <a title="{$post.meta_title}" class="tittle-post font-playfair" href='{smartblog::GetSmartBlogLink('smartblog_post',$options)}'>{$post.meta_title}</a>
        <p class="post-des">{$post.short_description}</p>
        <ul class="info"> 
            <li><i class="fa fa-user"></i> {if $post.firstname}{$post.firstname}{/if} {if $post.lastname}{$post.lastname}{/if}</li>
            <li><i class="fa fa-calendar-o"></i> {if $post.created}{$post.created}{/if}</li>
            <li><i class="fa fa-eye"></i> {if $smartshowviewed ==1}{$post.viewed}{/if}</li>
        </ul> 
        <a title="{$post.meta_title}" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}" class="btn btn-small btn-dark">{l s='READ MORE' mod='smartblog'} </a>
    </div> 
</div>