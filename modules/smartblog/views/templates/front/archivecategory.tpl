{capture name=path}<a href="{smartblog::GetSmartBlogLink('smartblog')}">{l s='All Blog News' mod='smartblog'}</a>
     {if $title_category != ''}
    <span class="navigation-pipe">{$navigationPipe}</span>{$title_category}{/if}{/capture}
    {if $postcategory == ''}
             <p class="error">{l s='No Post in Archive' mod='smartblog'}</p>
    {else}   
    <div id="smartblog_cat" class="blog-page">
        <div class="blog-posts medium-images">
            <ul>
            {foreach from=$postcategory item=post}
                <li> 
                    {include file="./category_loop.tpl" postcategory=$postcategory} 
                </li>   
            {/foreach}
            </ul>
        </div>
    </div>
 {/if}
 {if isset($smartcustomcss)}
    <style>
        {$smartcustomcss}
    </style>
{/if}

