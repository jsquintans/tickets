<?php
class SimpleCategoryOverride extends SimpleCategory
{


    public function frontBuildModuleContent($item, $hookName='', $shopId=0, $langId=0){
    	if(!$item) return '';
    	$cache_id = Tools::encrypt('simplecategory::'.$hookName.'::'.$item['id']);
    	if($item['layout'] == 'simple' || $item['layout'] == 'owl' || $item['layout'] == 'slider' ||$item['layout'] == 'grid' || $item['layout'] == 'left' || $item['layout'] == 'leftowl'){    		
    		$homeCategory = Configuration::get('PS_HOME_CATEGORY');        
			$results = array();            
	    	// module banner    	
	    	if($item['banners']){
		    	$banners = json_decode($item['banners']);
				$item['banners'] = array();
				if($banners){
					foreach($banners as $banner){
						if($banner){
							$fullPath = $this->getBannerSrc($banner->image, true);
							if($fullPath){										
								$banner->image = $fullPath;								
								$item['banners'] = get_object_vars($banner);
							}	
						}							
					}
				}	
	    	}else $item['banners'] = array();		    	
			// end module banner
            /* 
			if($item['params']){
				$item['params'] = get_object_vars(json_decode($item['params']));
				$item['params']['icon'] = $this->getIconSrc($item['params']['icon']);
				$item['params']['icon_active'] = $this->getIconSrc($item['params']['icon_active']);
			}
            */
			$id_category = 0;				
			if($item['category_id'] == 0){
				if($this->page_name == 'category'){
					$id_category = intval(Tools::getValue('id_category', 0));	
				}elseif($this->page_name == 'product'){
					$productId = (int)Tools::getValue('id_product');
	            	$id_category = Db::getInstance()->getValue("Select id_category_default From "._DB_PREFIX_."product Where id_shop = '$shopId' AND id_product = ".$productId);
				}else{
					$id_category = $homeCategory;
				}
			}else{
				$id_category = $item['category_id'];
			}
			$arrCategoryId = $this->getCategoryIds($id_category, array($id_category));		
			$item['products'] = array();
			if($item['type'] == 'auto'){
				if($item['layout'] == 'deal'){
					$item['products'] = $this->frontGetProducts2($arrCategoryId, $item['on_condition'], $item['on_sale'], $item['on_new'], $item['on_discount'], $langId, 1, $item['maxItem'], $item['order_by'], $item['order_way'], null, null, true);
				}else{
					$item['products'] = $this->frontGetProducts2($arrCategoryId, $item['on_condition'], $item['on_sale'], $item['on_new'], $item['on_discount'], $langId, 1, $item['maxItem'], $item['order_by'], $item['order_way'], null, null);
				}
			}else{
				$rows = Db::getInstance()->executeS("Select product_id, ordering From "._DB_PREFIX_."simplecategory_module_product Where module_id = ".$item['id']." Order By ordering");
	            if($rows){                
	                foreach($rows as $row){
	                    $item['products'][] = $this->frontGetProductById($row['product_id'], $langId);		                    
	                }    
	            }
			}
            // Get product images
        foreach($item['products'] as &$product) { 
            $product_images = $this->getProductImages($product['id_product'],$langId); 
            $arr_image = array();  
            foreach ($product_images as $image) {
                $arr_image[] =  $product['id_product'].'-'.$image['id_image'];
            } 
            $product['images']= $arr_image; 
        }     
	        // short codes
	        //if($item['description']) $item['description'] = Tools::htmlentitiesDecodeUTF8($item['description']);
	        $moduleDescription = $item['description'];
			if($moduleDescription){
				// short code deal
		        $pattern = '/\{deal\}(.*?)\{\/deal\}/';
		        $checkDeal = preg_match_all($pattern, $moduleDescription, $match);
		        if($checkDeal){
		            $deals = $match[1];
		            if($deals){
		                foreach($deals as $deal){                    
		                    $dealConfig = json_decode(str_replace(array('\\', '\''), array('', '"'), $deal));                    
		                    if($dealConfig){                        
		                        $html = $this->buildDescriptionDeal($arrCategoryId, $dealConfig, $item['layout']);
		                        $item['description'] = str_replace('{deal}'.$deal.'{/deal}', $html, $item['description']);
		                    }else{
		                        $item['description'] = str_replace('{deal}'.$deal.'{/deal}', '', $item['description']);
		                    }
		                    
		                }
		            }
		        }	
			} // end if check module description
    	} // end if check layout 
        $item['groups'] = $this->frontBuildContentGroups($item['id'], $item['layout'], $shopId, $langId, Tools::encrypt('simplecategory::'.$hookName.'::'.$this->page_name.'::'.$item['id'])); 
		$this->context->smarty->assign(array(
			'simplecategory_item'=> $item,
		));		  
		return $this->display(__FILE__, 'simplecategory.'.$item['layout'].'.module.tpl');        
    }
	public function frontBuildContentGroups($moduleId, $layout, $shopId, $langId, $cache_id=''){
		
		$items = array();        
		$items = DB::getInstance()->executeS("Select g.*, gl.name, gl.banners, gl.description  
			From "._DB_PREFIX_."simplecategory_group AS g 
			Inner Join "._DB_PREFIX_."simplecategory_group_lang AS gl On g.id = gl.group_id 
			Where g.status = '1' AND module_id = '".$moduleId."' AND gl.id_lang = '".$langId."' 
			Order By g.ordering");
            
		if($items){			
			$homeCategory = Configuration::get('PS_HOME_CATEGORY');
			foreach($items as &$item){
				//$item['description'] = Tools::htmlentitiesDecodeUTF8($item['description']);
				$item['icon'] = $this->getIconSrc($item['icon']);
				$item['icon_active'] = $this->getIconSrc($item['icon_active']);				
				// banners	
				if($item['banners']){
					$banners = json_decode($item['banners']);
					if($banners){
						foreach($banners as &$banner){
							$fullPath = $this->getBannerSrc($banner->image, true);
							if(!$fullPath) unset($banner);
							else {
								$banner->fullPath = $fullPath;
								$banner = get_object_vars($moduleBanner);	
							}
						}
					}
					$item['banners'] = $banners;
				}else $item['banners'] = array();
				// end banners
				if($item['category_id'] == 0){
					if($this->page_name == 'category'){
						$id_category = intval(Tools::getValue('id_category', 0));	
					}elseif($this->page_name == 'product'){
						$productId = (int)Tools::getValue('id_product');
		            	$id_category = Db::getInstance()->getValue("Select id_category_default From "._DB_PREFIX_."product Where id_shop = '$shopId' AND id_product = ".$productId);
					}else{
						$id_category = $homeCategory;
					}
				}else{
					$id_category = $item['category_id'];
				}
				$arrCategoryId = $this->getCategoryIds($id_category, array($id_category));
				$item['products'] = array();
				if($item['type'] == 'auto'){					
					$item['products'] = $this->frontGetProducts2($arrCategoryId, $item['on_condition'], $item['on_sale'], $item['on_new'], $item['on_discount'], $langId, 1, $item['maxItem'], $item['order_by'], $item['order_way'], null, null);					
				}else{
					$rows = Db::getInstance()->executeS("Select product_id, ordering From "._DB_PREFIX_."simplecategory_group_product Where group_id = ".$item['id']." Order By ordering");
		            if($rows){                
		                foreach($rows as $row){
		                    $item['products'][] = $this->frontGetProductById($row['product_id'], $langId);		                    
		                }    
		            }
				}	

				// short codes
		        $moduleDescription = $item['description'];
				if($moduleDescription){
					// short code deal
			        $pattern = '/\{deal\}(.*?)\{\/deal\}/';
			        $checkDeal = preg_match_all($pattern, $moduleDescription, $match);
			        if($checkDeal){
			            $deals = $match[1];
			            if($deals){
			                foreach($deals as $deal){                    
			                    $dealConfig = json_decode(str_replace(array('\\', '\''), array('', '"'), $deal));                    
			                    if($dealConfig){                        
			                        $html = $this->buildDescriptionDeal($arrCategoryId, $dealConfig, $layout);
			                        $item['description'] = str_replace('{deal}'.$deal.'{/deal}', $html, $item['description']);
			                    }else{
			                        $item['description'] = str_replace('{deal}'.$deal.'{/deal}', '', $item['description']);
			                    }
			                    
			                }
			            }
			        }
					// end short code deal	
				}		
			}			
			$this->context->smarty->assign(array(
				'simplecategory_groups'	=> $items,				
			));		 
            return $items;
		}else 
            return "";  
		return $this->display(__FILE__, 'simplecategory.'.$layout.'.groups.tpl');
	}
    function buildDescriptionDeal($arrCategoryId, $config, $layout='default'){
        $langId = $this->context->language->id;
	    $shopId = $this->context->shop->id;        
        
        if(isset($config->on_condition)) $on_condition = $config->on_condition;
        else $on_condition = 'all';
        
        if(isset($config->on_sale)) $on_sale = intval($config->on_sale);
        else $on_sale = 2;
        
        if(isset($config->on_new)) $on_new = intval($config->on_new);
        else $on_new = 2;
        
        if(isset($config->count)) $count = intval($config->count);
        else $count = 1;
        
        $products = $this->frontGetProducts2($arrCategoryId, $on_condition, $on_sale, $on_new, 1, $langId, 1, $count, 'discount', 'DESC', null, null, true);
        $this->context->smarty->assign(
            array(
    			'description_products'=>$products,
    			'livePath'=>$this->liveImage,			
            )
        );		
		return $this->display(__FILE__, 'simplecategory.'.$layout.'.descriptiondeal.tpl');               
    }
	function getCacheId($name=null){
		return parent::getCacheId('simplecategory|'.$name);
	}
	function clearCache($cacheKey)
	{
		if(!$cacheKey){
			parent::_clearCache('simplecategory.tpl');
			parent::_clearCache('simplecategory.default.tpl');
		}else{
			parent::_clearCache('simplecategory.tpl', $this->getCacheId($cacheKey));
			parent::_clearCache('simplecategory.default.tpl', $this->getCacheId($cacheKey));	
		} 		
       return true;
	}
	public static function getProductRatings($id_product)
	{
		$validate = Configuration::get('PRODUCT_COMMENTS_MODERATE');
		$sql = 'SELECT (SUM(pc.`grade`) / COUNT(pc.`grade`)) AS avg,
				MIN(pc.`grade`) AS min,
				MAX(pc.`grade`) AS max,
				COUNT(pc.`grade`) AS review
			FROM `'._DB_PREFIX_.'product_comment` pc
			WHERE pc.`id_product` = '.(int)$id_product.'
			AND pc.`deleted` = 0'.
			($validate == '1' ? ' AND pc.`validate` = 1' : '');


		$item = DB::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
		if($item){
			$item['avg'] = (int) $item['avg'] * 20;
			$item['min'] = (int) $item['min'] * 20;
			$item['max'] = (int) $item['max'] * 20;
			return $item;
		}
		else return array('avg'=>0, 'min'=>0, 'max'=>0);
	}
    
	
	
	public static function toItemDateTime($date_time){
		$result = array();
		$strTime = strtotime($date_time);
		$result['year'] = date('Y', $strTime);
		$result['month'] = date('m', $strTime);
		$result['day'] = date('d', $strTime);
		$result['hour'] = date('H', $strTime);
		$result['minute'] = date('m', $strTime);
		$result['second'] = date('s', $strTime);
        $result['untilTime'] = $strTime - time();
		return $result;
	}
	
	public function frontCheckAccess($id_customer)
	{
		$cache_id = 'Category::checkAccess_'.(int)$this->id.'-'.$id_customer.(!$id_customer ? '-'.(int)Group::getCurrent()->id : '');
		if (!Cache::isStored($cache_id))
		{
			if (!$id_customer)
				$result = (bool)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
				SELECT ctg.`id_group`
				FROM '._DB_PREFIX_.'category_group ctg
				WHERE ctg.`id_category` = '.(int)$this->id.' AND ctg.`id_group` = '.(int)Group::getCurrent()->id);
			else
				$result = (bool)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
				SELECT ctg.`id_group`
				FROM '._DB_PREFIX_.'category_group ctg
				INNER JOIN '._DB_PREFIX_.'customer_group cg on (cg.`id_group` = ctg.`id_group` AND cg.`id_customer` = '.(int)$id_customer.')
				WHERE ctg.`id_category` = '.(int)$this->id);
			Cache::store($cache_id, $result);
		}
		return Cache::retrieve($cache_id);
	}
	protected function getProductIdByDate($id_shop, $id_currency, $id_country, $id_group, $beginning, $ending, $id_customer = 0, $with_combination_id = false, $deal=false)
	{
		if (!SpecificPrice::isFeatureActive())
			return array();
		if($deal === true){
			$where = '(`from` = \'0000-00-00 00:00:00\' OR \''.pSQL($beginning).'\' >= `from`) AND (`to` != \'0000-00-00 00:00:00\' AND \''.pSQL($ending).'\' <= `to`)';
		}else{
			$where = '(`from` = \'0000-00-00 00:00:00\' OR \''.pSQL($beginning).'\' >= `from`) AND (`to` = \'0000-00-00 00:00:00\' OR \''.pSQL($ending).'\' <= `to`)';
		}
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT `id_product`, `id_product_attribute`
			FROM `'._DB_PREFIX_.'specific_price`
			WHERE	`id_shop` IN(0, '.(int)$id_shop.') AND
					`id_currency` IN(0, '.(int)$id_currency.') AND
					`id_country` IN(0, '.(int)$id_country.') AND
					`id_group` IN(0, '.(int)$id_group.') AND
					`id_customer` IN(0, '.(int)$id_customer.') AND
					`from_quantity` = 1 AND
					('.$where.') 
					AND
					`reduction` > 0
		', false);
		$ids_product = array();
		while ($row = Db::getInstance()->nextRow($result))
			$ids_product[] = $with_combination_id ? array('id_product' => (int)$row['id_product'], 'id_product_attribute' => (int)$row['id_product_attribute']) : (int)$row['id_product'];
		return $ids_product;
	}
	protected function _getProductIdByDate($beginning, $ending, Context $context = null, $with_combination_id = false, $id_customer=0, $deal=false)
	{
		if (!$context)
			$context = Context::getContext();

		$id_address = $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
		$ids = Address::getCountryAndState($id_address);
		$id_country = $ids['id_country'] ? (int)$ids['id_country'] : (int)Configuration::get('PS_COUNTRY_DEFAULT');
		if (!SpecificPrice::isFeatureActive())
			return array();
		if($deal == true){
			$where = '(`from` = \'0000-00-00 00:00:00\' OR \''.pSQL($beginning).'\' >= `from`) AND (`to` != \'0000-00-00 00:00:00\' AND \''.pSQL($ending).'\' <= `to`)';
		}else{
			$where = '(`from` = \'0000-00-00 00:00:00\' OR \''.pSQL($beginning).'\' >= `from`) AND (`to` = \'0000-00-00 00:00:00\' OR \''.pSQL($ending).'\' <= `to`)';
		}
		
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT `id_product`, `id_product_attribute`
			FROM `'._DB_PREFIX_.'specific_price`
			WHERE	`id_shop` IN(0, '.(int)$context->shop->id.') AND
					`id_currency` IN(0, '.(int)$context->currency->id.') AND
					`id_country` IN(0, '.(int)$id_country.') AND
					`id_group` IN(0, '.(int)$context->customer->id_default_group.') AND
					`id_customer` IN(0, '.(int)$id_customer.') AND
					`from_quantity` = 1 AND
					('.$where.') 
					AND
					`reduction` > 0
		', false);
		$ids_product = array();
		while ($row = Db::getInstance()->nextRow($result))
			$ids_product[] = $with_combination_id ? array('id_product' => (int)$row['id_product'], 'id_product_attribute' => (int)$row['id_product_attribute']) : (int)$row['id_product'];
		return $ids_product;
		/*
		return self::getProductIdByDate(
			$context->shop->id,
			$context->currency->id,
			$id_country,
			$context->customer->id_default_group,
			$beginning,
			$ending,
			0,
			$with_combination,
			$deal
		);
		 * 
		 */
	}
	protected function frontGetProducts2($categoryIds = array(), $on_condition='all', $on_sale=2, $on_new=2, $on_discount=2, $id_lang, $p, $n, $order_by = null, $order_way = null, $beginning=null, $ending=null, $deal=false, $get_total = false, $active = true, $random = false, $random_number_products = 1, Context $context = null){		
		if(!$categoryIds) return array();		
		$where = "";
		if($on_condition != 'all'){
             $where .= " AND p.condition = '".$on_condition."' ";                
        }
		if($on_sale != 2){
			$where .= " AND p.on_sale = '".$on_sale."' ";
		}
        Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? $PS_NB_DAYS_NEW_PRODUCT = (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : $PS_NB_DAYS_NEW_PRODUCT = 20;
		if($on_new == 0){
			$where .= " AND product_shop.`date_add` <= '".date('Y-m-d', strtotime('-'.$PS_NB_DAYS_NEW_PRODUCT.' DAY'))."' ";
		}elseif($on_new == 1){
			$where .= " AND product_shop.`date_add` > '".date('Y-m-d', strtotime('-'.$PS_NB_DAYS_NEW_PRODUCT.' DAY'))."' ";
		}
		$ids_product = '';
		if($on_discount == 0){
			$current_date = date('Y-m-d H:i:s');
			$product_reductions = $this->_getProductIdByDate((!$beginning ? $current_date : $beginning), (!$ending ? $current_date : $ending), $context, true, 0, $deal);		
			if ($product_reductions){
				$ids_product = ' AND (';
				foreach ($product_reductions as $product_reduction)
					$ids_product .= '( product_shop.`id_product` != '.(int)$product_reduction['id_product'].($product_reduction['id_product_attribute'] ? ' OR product_attribute_shop.`id_product_attribute`='.(int)$product_reduction['id_product_attribute'] :'').') AND';
				$ids_product = rtrim($ids_product, 'AND').')';
			}
		}elseif($on_discount == 1){
			$current_date = date('Y-m-d H:i:s');
			$product_reductions = $this->_getProductIdByDate((!$beginning ? $current_date : $beginning), (!$ending ? $current_date : $ending), $context, true, 0, $deal);            		
			if ($product_reductions)
			{
				$ids_product = ' AND (';
				foreach ($product_reductions as $product_reduction)
					$ids_product .= '( product_shop.`id_product` = '.(int)$product_reduction['id_product'].($product_reduction['id_product_attribute'] ? ' AND product_attribute_shop.`id_product_attribute`='.(int)$product_reduction['id_product_attribute'] :'').') OR';
				$ids_product = rtrim($ids_product, 'OR').')';
			}else{
		      if($deal == true) return array();
			}
		}else{
			if($order_by == 'discount'){
				$current_date = date('Y-m-d H:i:s');
				$product_reductions = $this->_getProductIdByDate((!$beginning ? $current_date : $beginning), (!$ending ? $current_date : $ending), $context, true, 0, $deal);		
				if ($product_reductions){
					$ids_product = ' AND (';
					foreach ($product_reductions as $product_reduction)
						$ids_product .= '( product_shop.`id_product` = '.(int)$product_reduction['id_product'].($product_reduction['id_product_attribute'] ? ' AND product_attribute_shop.`id_product_attribute`='.(int)$product_reduction['id_product_attribute'] :'').') OR';
					$ids_product = rtrim($ids_product, 'OR').')';
				}				
			}
		}		
		if($ids_product) $where .= $ids_product;
		if (!$context) $context = Context::getContext();
		$front = true;
		if (!in_array($context->controller->controller_type, array('front', 'modulefront'))) $front = false;		
		if ($p < 1) $p = 1;
		
		if (empty($order_by)){
			$order_by = 'position';
		}else{
			$order_by = strtolower($order_by);
		}			
		if (empty($order_way)) $order_way = 'ASC';		
		$order_by_prefix = false;
		
		$addJoin = '';
		$addSelect = '';	
		if ($order_by == 'id_product' || $order_by == 'date_add' || $order_by == 'date_upd'){
			$order_by_prefix = 'p';
		}elseif ($order_by == 'name'){
			$order_by_prefix = 'pl';
		}elseif ($order_by == 'manufacturer' || $order_by == 'manufacturer_name'){
			$order_by_prefix = 'm';
			$order_by = 'name';
		}elseif ($order_by == 'position'){
			$order_by_prefix = 'cp';
		}elseif($order_by == 'discount'){
			$order_by_prefix = 'sp';
			$order_by = 'reduction';
			$addJoin = ' LEFT JOIN `'._DB_PREFIX_.'specific_price` sp On p.`id_product` = sp.`id_product` ';
			$addSelect = ', sp.reduction, sp.`from`, sp.`to`';
		}elseif($order_by == 'review'){
			$order_by_prefix = '';
			$order_by = 'total_review';
			$addJoin = ' LEFT JOIN `'._DB_PREFIX_.'product_comment` pr ON pr.`id_product` = p.`id_product` ';
			$addSelect = ', COUNT(pr.grade) as total_review';
		}elseif($order_by == 'view'){
			$order_by_prefix = '';
			$order_by = 'total_view';
			$addJoin = ' LEFT JOIN '._DB_PREFIX_.'simplecategory_product_view as pv ON pv.`product_id` = p.`id_product` ';
			$addSelect = ', pv.total as total_view';
		}elseif($order_by == 'rate'){
			$order_by_prefix = '';
			$order_by = 'total_avg';
			$addJoin = ' LEFT JOIN `'._DB_PREFIX_.'product_comment` pr ON pr.`id_product` = p.`id_product` ';
			$addSelect = ', (SUM(pr.`grade`) / COUNT(pr.`grade`)) AS total_avg';
		}elseif($order_by == 'seller'){
			$order_by_prefix = '';
			$order_by = 'sales';
			$addJoin = ' LEFT JOIN `'._DB_PREFIX_.'product_sale` ps ON ps.`id_product` = p.`id_product` ';
			$addSelect = ', ps.`quantity` AS sales';
		} 
		if($order_by != 'reduction' && $on_discount != 2){
			$addJoin = ' LEFT JOIN `'._DB_PREFIX_.'specific_price` sp On p.`id_product` = sp.`id_product` ';
			$addSelect = ', sp.reduction, sp.`from`, sp.`to`';
		}
		if ($order_by == 'price') $order_by = 'orderprice';
		
		
		
		
		if (!Validate::isBool($active) || !Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) die (Tools::displayError());
		$id_supplier = (int)Tools::getValue('id_supplier');
		
		if ($get_total)
		{
			$sql = 'SELECT COUNT(cp.`id_product`) AS total
					FROM `'._DB_PREFIX_.'product` p 					
					'.Shop::addSqlAssociation('product', 'p').' '.$addJoin.'
					LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`
					WHERE cp.`id_category` IN ('.implode(', ', $categoryIds).') '.$where. 
					($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
					($active ? ' AND product_shop.`active` = 1' : '').
					(($ids_product) ? $ids_product : '').
					($id_supplier ? 'AND p.id_supplier = '.(int)$id_supplier : '');
			return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
		}
        
		$sql = 'SELECT DISTINCT 
				p.id_product,  MAX(product_attribute_shop.id_product_attribute) id_product_attribute, pl.`link_rewrite`, pl.`name`, pl.`description_short`, product_shop.`id_category_default`,
				MAX(image_shop.`id_image`) id_image, il.`legend`, p.`ean13`, p.`upc`, cl.`link_rewrite` AS category, p.show_price, p.available_for_order, IFNULL(stock.quantity, 0) as quantity, p.customizable,
				IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity, stock.out_of_stock, p.`fecha_encuentro` as `fecha_encuentro`, DATE(p.`fecha_encuentro`) as `fe`, 
									fplocal_l.`value` as `equipo_local`,
									fplocal.`id_feature_value` as `id_equipo_local`,
									fpvisitante_l.`value` as `equipo_visitante`,
									fpvisitante.`id_feature_value` as `id_equipo_visitante`,
									fptorneo_l.`value` as `torneo_del_encuentro`,
									p.`nombre_estadio` as `nombre_estadio`,
									fpais_l.`value` as `pais`,
									fciudad_l.`value` as `ciudad`,
				product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.$PS_NB_DAYS_NEW_PRODUCT.' DAY')).'" as `new`,
				product_shop.`on_sale`, MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity, product_shop.price AS orderprice '.$addSelect.'
				FROM `'._DB_PREFIX_.'category_product` cp 
				LEFT JOIN `'._DB_PREFIX_.'product` p 
					ON p.`id_product` = cp.`id_product` 
				'.Shop::addSqlAssociation('product', 'p').
				$addJoin.
				(Combination::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa 
				ON (p.`id_product` = pa.`id_product`) 
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').' 
				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop) :  Product::sqlStock('p', 'product', false, Context::getContext()->shop)).' 
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl 
					ON (product_shop.`id_category_default` = cl.`id_category` 
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl 
					ON (p.`id_product` = pl.`id_product` 
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image` i 
					ON (i.`id_product` = p.`id_product`) '.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').' 
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il 
					ON (image_shop.`id_image` = il.`id_image` 
					AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m 
					ON m.`id_manufacturer` = p.`id_manufacturer` 
				LEFT JOIN `'._DB_PREFIX_.'feature_product` fpvisitante
					ON ( fpvisitante.`id_product` = p.id_product AND fpvisitante.`id_feature` = 2 )
				LEFT JOIN `'._DB_PREFIX_.'feature_product` fplocal
					ON ( fplocal.`id_product` = p.id_product AND fplocal.`id_feature` = 1 )
				LEFT JOIN `'._DB_PREFIX_.'feature_value_lang` fpvisitante_l
					ON ( fpvisitante.`id_feature_value` = fpvisitante_l.`id_feature_value` AND fpvisitante_l.`id_lang` = 1)
				LEFT JOIN `'._DB_PREFIX_.'feature_value_lang` fplocal_l
					ON ( fplocal.`id_feature_value` = fplocal_l.`id_feature_value` AND fplocal_l.`id_lang` = 1)
				LEFT JOIN `'._DB_PREFIX_.'feature_product` fptorneo
					ON ( fptorneo.`id_product` = p.id_product AND fptorneo.`id_feature` = 3 )
				LEFT JOIN `'._DB_PREFIX_.'feature_value_lang` fptorneo_l
					ON ( fptorneo.`id_feature_value` = fptorneo_l.`id_feature_value` AND fptorneo_l.`id_lang` = 1)
				LEFT JOIN `'._DB_PREFIX_.'feature_product` fpais
					ON ( fpais.`id_product` = p.id_product AND fpais.`id_feature` = 7 )
				LEFT JOIN `'._DB_PREFIX_.'feature_value_lang` fpais_l
					ON ( fpais.`id_feature_value` = fpais_l.`id_feature_value` AND fpais_l.`id_lang` = 1)
				LEFT JOIN `'._DB_PREFIX_.'feature_product` fciudad
					ON ( fciudad.`id_product` = p.id_product AND fciudad.`id_feature` = 8 )
				LEFT JOIN `'._DB_PREFIX_.'feature_value_lang` fciudad_l
					ON ( fciudad.`id_feature_value` = fciudad_l.`id_feature_value` AND fciudad_l.`id_lang` = 1)

				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.' 
					AND cp.`id_category` IN ('.implode(', ', $categoryIds).') '
					.$where 
					.($active ? ' AND product_shop.`active` = 1' : '')
					.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')
					.($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')
					.' GROUP BY product_shop.id_product';
				$sql.=' ORDER BY fe ASC';
		/*if ($random === true) $sql .= ' ORDER BY RAND() LIMIT '.(int)$random_number_products;		
		else $sql .= ' ORDER BY '.(!empty($order_by_prefix) ? $order_by_prefix.'.' : '').'`'.bqSQL($order_by).'` '.pSQL($order_way).' LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;    */


		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		if ($order_by == 'orderprice') Tools::orderbyPrice($result, $order_way);
		if (!$result) return array();
		return Product::getProductsProperties($id_lang, $result);
	}
	
	public function frontGetProducts($categoryIds = array(), $display_only='', $id_lang, $p, $n, $order_by = null, $order_way = null, $get_total = false, $active = true, $random = false, $random_number_products = 1, Context $context = null){
		if(!$categoryIds) return array();
		$where = "";
		if($display_only){
            if($display_only == 'condition-new') $where .= " AND p.condition = 'new'";
            elseif($display_only == 'condition-used') $where .= " AND p.condition = 'used'";
            elseif($display_only == 'condition-refurbished') $where .= " AND p.condition = 'refurbished'";    
        }
		if (!$context) $context = Context::getContext();
		$front = true;
		if (!in_array($context->controller->controller_type, array('front', 'modulefront'))) $front = false;		
		if ($p < 1) $p = 1;
		if (empty($order_by)) $order_by = 'position';

		else $order_by = strtolower($order_by);
		if (empty($order_way)) $order_way = 'ASC';
		$order_by_prefix = false;
		if ($order_by == 'id_product' || $order_by == 'date_add' || $order_by == 'date_upd') $order_by_prefix = 'p';
		elseif ($order_by == 'name') $order_by_prefix = 'pl';
		elseif ($order_by == 'manufacturer' || $order_by == 'manufacturer_name'){
			$order_by_prefix = 'm';
			$order_by = 'name';
		}elseif ($order_by == 'position') $order_by_prefix = 'cp';
		if ($order_by == 'price') $order_by = 'orderprice';
		if (!Validate::isBool($active) || !Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) die (Tools::displayError());
		$id_supplier = (int)Tools::getValue('id_supplier');
		if ($get_total)
		{
			$sql = 'SELECT COUNT(cp.`id_product`) AS total
					FROM `'._DB_PREFIX_.'product` p
					'.Shop::addSqlAssociation('product', 'p').'
					LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`
					WHERE cp.`id_category` IN ('.implode(', ', $categoryIds).') '.$where. 
					($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
					($active ? ' AND product_shop.`active` = 1' : '').
					($id_supplier ? 'AND p.id_supplier = '.(int)$id_supplier : '');
			return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
		}
		$sql = 'SELECT
				p.id_product,  MAX(product_attribute_shop.id_product_attribute) id_product_attribute, pl.`link_rewrite`, pl.`name`, pl.`description_short`, product_shop.`id_category_default`,
				MAX(image_shop.`id_image`) id_image, il.`legend`, p.`ean13`, p.`upc`, cl.`link_rewrite` AS category, p.show_price, p.available_for_order, IFNULL(stock.quantity, 0) as quantity, p.customizable,
				IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity, stock.out_of_stock,
				product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'" as new,
				product_shop.`on_sale`, MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity, product_shop.price AS orderprice 
				FROM `'._DB_PREFIX_.'category_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p
					ON p.`id_product` = cp.`id_product`
				'.Shop::addSqlAssociation('product', 'p').
				(Combination::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop) :  Product::sqlStock('p', 'product', false, Context::getContext()->shop)).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
					ON (product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON (p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image` i
					ON (i.`id_product` = p.`id_product`)'.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il
					ON (image_shop.`id_image` = il.`id_image`
					AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
					ON m.`id_manufacturer` = p.`id_manufacturer`
				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'
					AND cp.`id_category` IN ('.implode(', ', $categoryIds).')'
					.($active ? ' AND product_shop.`active` = 1' : '')
					.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')
					.($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')
					.' GROUP BY product_shop.id_product';
		if ($random === true) $sql .= ' ORDER BY RAND() LIMIT '.(int)$random_number_products;
		else $sql .= ' ORDER BY '.(!empty($order_by_prefix) ? $order_by_prefix.'.' : '').'`'.bqSQL($order_by).'` '.pSQL($order_way).' LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;
			
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		if ($order_by == 'orderprice') Tools::orderbyPrice($result, $order_way);
		if (!$result) return array();
		return Product::getProductsProperties($id_lang, $result);
	}
	public function frontGetProductByIds($ids = array(), $id_lang, $order_by = null, $order_way = null, $active = true, Context $context = null){
		if(!$ids) return array();
		if (!$context) $context = Context::getContext();
		//if($check_access && !$this->frontCheckAccess($context->customer->id)) return array();
		$front = true;
		if (!in_array($context->controller->controller_type, array('front', 'modulefront'))) $front = false;		
		
		if (empty($order_by)) $order_by = 'position';
		else $order_by = strtolower($order_by);
		if (empty($order_way)) $order_way = 'ASC';		
		$order_by_prefix = false;
		if ($order_by == 'id_product' || $order_by == 'date_add' || $order_by == 'date_upd') $order_by_prefix = 'p';
		elseif ($order_by == 'name') $order_by_prefix = 'pl';
		elseif ($order_by == 'manufacturer' || $order_by == 'manufacturer_name'){
			$order_by_prefix = 'm';
			$order_by = 'name';
		}elseif ($order_by == 'position') $order_by_prefix = 'cp';
		if ($order_by == 'price') $order_by = 'orderprice';
		if (!Validate::isBool($active) || !Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) die (Tools::displayError());
		$id_supplier = (int)Tools::getValue('id_supplier');		
		$sql = 'SELECT
				p.id_product,  MAX(product_attribute_shop.id_product_attribute) id_product_attribute, pl.`link_rewrite`, pl.`name`, pl.`description_short`, product_shop.`id_category_default`,
				MAX(image_shop.`id_image`) id_image, il.`legend`, p.`ean13`, p.`upc`, cl.`link_rewrite` AS category, p.show_price, p.available_for_order, IFNULL(stock.quantity, 0) as quantity, p.customizable,
				IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity, stock.out_of_stock,
				product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'" as new,
				product_shop.`on_sale`, MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity, product_shop.price AS orderprice 
				FROM `'._DB_PREFIX_.'category_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p
					ON p.`id_product` = cp.`id_product`
				'.Shop::addSqlAssociation('product', 'p').
				(Combination::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop) :  Product::sqlStock('p', 'product', false, Context::getContext()->shop)).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
					ON (product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON (p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image` i
					ON (i.`id_product` = p.`id_product`)'.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il
					ON (image_shop.`id_image` = il.`id_image`
					AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
					ON m.`id_manufacturer` = p.`id_manufacturer`
				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'
					AND product_shop.id_product IN ('.implode(', ', $ids).') '.
					' AND product_shop.`active` = 1'.
					' AND product_shop.`visibility` IN ("both", "catalog")'.
					($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '').
					' GROUP BY product_shop.id_product';
		$sql .= ' ORDER BY '.(!empty($order_by_prefix) ? $order_by_prefix.'.' : '').'`'.bqSQL($order_by).'` '.pSQL($order_way);
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		if ($order_by == 'orderprice') Tools::orderbyPrice($result, $order_way);
		if (!$result) return array();
		return Product::getProductsProperties($id_lang, $result);
	}
	public function frontGetProductById($productId = 0, $id_lang, $active = true, Context $context = null){
		if(!$productId) return array();
		if (!$context) $context = Context::getContext();
		//if($check_access && !$this->frontCheckAccess($context->customer->id)) return array();
		$front = true;
		if (!in_array($context->controller->controller_type, array('front', 'modulefront'))) $front = false;		
		
		
		if (!Validate::isBool($active)) die (Tools::displayError());
		$id_supplier = (int)Tools::getValue('id_supplier');		
		$sql = 'SELECT
				p.id_product,  MAX(product_attribute_shop.id_product_attribute) id_product_attribute, pl.`link_rewrite`, pl.`name`, pl.`description_short`, product_shop.`id_category_default`,
				MAX(image_shop.`id_image`) id_image, il.`legend`, p.`ean13`, p.`upc`, cl.`link_rewrite` AS category, p.show_price, p.available_for_order, IFNULL(stock.quantity, 0) as quantity, p.customizable,
				IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity, stock.out_of_stock,
				product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'" as new,
				product_shop.`on_sale`, MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity, product_shop.price AS orderprice 
				FROM `'._DB_PREFIX_.'category_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p
					ON p.`id_product` = cp.`id_product`
				'.Shop::addSqlAssociation('product', 'p').
				(Combination::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop) :  Product::sqlStock('p', 'product', false, Context::getContext()->shop)).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
					ON (product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON (p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image` i
					ON (i.`id_product` = p.`id_product`)'.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il
					ON (image_shop.`id_image` = il.`id_image`
					AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
					ON m.`id_manufacturer` = p.`id_manufacturer`
				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'
					AND product_shop.id_product =  '.$productId.
					' AND product_shop.`active` = 1'.
					' AND product_shop.`visibility` IN ("both", "catalog")'.
					($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '').
					' GROUP BY product_shop.id_product';
		
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);		
		if (!$result) return array();		
		return Product::getProductProperties($id_lang, $result);
	}
	public function frontGetMostReview($categoryIds = array(), $display_only='', $id_lang, $p, $n, $order_way = null, $get_total = false, $active = true, $random = false, $random_number_products = 1, Context $context = null){
		if(!$categoryIds) return array();
		$where = "";
		if($display_only){
            if($display_only == 'condition-new') $where .= " AND p.condition = 'new'";
            elseif($display_only == 'condition-used') $where .= " AND p.condition = 'used'";
            elseif($display_only == 'condition-refurbished') $where .= " AND p.condition = 'refurbished'";    
        }
		if (!$context) $context = Context::getContext();
		$front = true;
		if (!in_array($context->controller->controller_type, array('front', 'modulefront'))) $front = false;		
		if ($p < 1) $p = 1;
		$order_by = 'total_review';
		//if (empty($order_by)) $order_by = 'position';
		//else $order_by = strtolower($order_by);
		if (empty($order_way)) $order_way = 'DESC';
		//$order_by_prefix = false;
		//if ($order_by == 'id_product' || $order_by == 'date_add' || $order_by == 'date_upd') $order_by_prefix = 'p';
		//elseif ($order_by == 'name') $order_by_prefix = 'pl';
		//elseif ($order_by == 'manufacturer' || $order_by == 'manufacturer_name'){
		//	$order_by_prefix = 'm';
		//	$order_by = 'name';
		//}elseif ($order_by == 'position') $order_by_prefix = 'cp';
		//if ($order_by == 'price') $order_by = 'orderprice';
		if (!Validate::isBool($active) || !Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) die (Tools::displayError());
		$id_supplier = (int)Tools::getValue('id_supplier');
		if ($get_total)
		{
			$sql = 'SELECT COUNT(cp.`id_product`) AS total
					FROM `'._DB_PREFIX_.'product` p
					'.Shop::addSqlAssociation('product', 'p').'
					LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`
					WHERE cp.`id_category` IN ('.implode(', ', $categoryIds).') '.$where. 
					($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
					($active ? ' AND product_shop.`active` = 1' : '').
					($id_supplier ? 'AND p.id_supplier = '.(int)$id_supplier : '');
			return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
		}
		$sql = 'SELECT
				p.id_product,  MAX(product_attribute_shop.id_product_attribute) id_product_attribute, pl.`link_rewrite`, pl.`name`, pl.`description_short`, product_shop.`id_category_default`,
				MAX(image_shop.`id_image`) id_image, il.`legend`, p.`ean13`, p.`upc`, cl.`link_rewrite` AS category, p.show_price, p.available_for_order, IFNULL(stock.quantity, 0) as quantity, p.customizable,
				IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity, stock.out_of_stock, COUNT(pr.grade) as total_review, 
				product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'" as new,
				product_shop.`on_sale`, MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity, product_shop.price AS orderprice 
				FROM `'._DB_PREFIX_.'category_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p
					ON p.`id_product` = cp.`id_product`
				'.Shop::addSqlAssociation('product', 'p').
				(Combination::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop) :  Product::sqlStock('p', 'product', false, Context::getContext()->shop)).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
					ON (product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON (p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image` i
					ON (i.`id_product` = p.`id_product`)'.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il
					ON (image_shop.`id_image` = il.`id_image`
					AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
					ON m.`id_manufacturer` = p.`id_manufacturer`
				LEFT JOIN `'._DB_PREFIX_.'product_comment` pr
					ON pr.`id_product` = p.`id_product`
				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'
					AND cp.`id_category` IN ('.implode(', ', $categoryIds).')'
					.($active ? ' AND product_shop.`active` = 1' : '')
					.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')
					.($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')
					.' GROUP BY product_shop.id_product';
					
		
		$sql .= ' ORDER BY `'.bqSQL($order_by).'` '.pSQL($order_way).' LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;		
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		//if ($order_by == 'orderprice') Tools::orderbyPrice($result, $order_way);
		if (!$result) return array();
		return Product::getProductsProperties($id_lang, $result);
	}
	public function frontGetMostRates($categoryIds = array(), $display_only='', $id_lang, $p, $n, $order_way = null, $get_total = false, $active = true, $random = false, $random_number_products = 1, Context $context = null){
		if(!$categoryIds) return array();
		$where = "";
		if($display_only){
            if($display_only == 'condition-new') $where .= " AND p.condition = 'new'";
            elseif($display_only == 'condition-used') $where .= " AND p.condition = 'used'";
            elseif($display_only == 'condition-refurbished') $where .= " AND p.condition = 'refurbished'";    
        }
		if (!$context) $context = Context::getContext();
		$front = true;
		if (!in_array($context->controller->controller_type, array('front', 'modulefront'))) $front = false;		
		if ($p < 1) $p = 1;
		$order_by = 'total_avg';
		//if (empty($order_by)) $order_by = 'position';
		//else $order_by = strtolower($order_by);
		if (empty($order_way)) $order_way = 'DESC';
		//$order_by_prefix = false;
		//if ($order_by == 'id_product' || $order_by == 'date_add' || $order_by == 'date_upd') $order_by_prefix = 'p';
		//elseif ($order_by == 'name') $order_by_prefix = 'pl';
		//elseif ($order_by == 'manufacturer' || $order_by == 'manufacturer_name'){
		//	$order_by_prefix = 'm';
		//	$order_by = 'name';
		//}elseif ($order_by == 'position') $order_by_prefix = 'cp';
		//if ($order_by == 'price') $order_by = 'orderprice';
		if (!Validate::isBool($active) || !Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) die (Tools::displayError());
		$id_supplier = (int)Tools::getValue('id_supplier');
		if ($get_total)
		{
			$sql = 'SELECT COUNT(cp.`id_product`) AS total
					FROM `'._DB_PREFIX_.'product` p
					'.Shop::addSqlAssociation('product', 'p').'
					LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`
					WHERE cp.`id_category` IN ('.implode(', ', $categoryIds).') '.$where. 
					($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
					($active ? ' AND product_shop.`active` = 1' : '').
					($id_supplier ? 'AND p.id_supplier = '.(int)$id_supplier : '');
			return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
		}
		$sql = 'SELECT
				p.id_product,  MAX(product_attribute_shop.id_product_attribute) id_product_attribute, pl.`link_rewrite`, pl.`name`, pl.`description_short`, product_shop.`id_category_default`,
				MAX(image_shop.`id_image`) id_image, il.`legend`, p.`ean13`, p.`upc`, cl.`link_rewrite` AS category, p.show_price, p.available_for_order, IFNULL(stock.quantity, 0) as quantity, p.customizable,
				IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity, stock.out_of_stock, (SUM(pr.`grade`) / COUNT(pr.`grade`)) AS total_avg, 
				product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'" as new,
				product_shop.`on_sale`, MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity, product_shop.price AS orderprice 
				FROM `'._DB_PREFIX_.'category_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p
					ON p.`id_product` = cp.`id_product`
				'.Shop::addSqlAssociation('product', 'p').
				(Combination::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop) :  Product::sqlStock('p', 'product', false, Context::getContext()->shop)).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
					ON (product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON (p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image` i
					ON (i.`id_product` = p.`id_product`)'.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il
					ON (image_shop.`id_image` = il.`id_image`
					AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
					ON m.`id_manufacturer` = p.`id_manufacturer`
				LEFT JOIN `'._DB_PREFIX_.'product_comment` pr
					ON pr.`id_product` = p.`id_product`
				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'
					AND cp.`id_category` IN ('.implode(', ', $categoryIds).')'
					.($active ? ' AND product_shop.`active` = 1' : '')
					.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')
					.($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')
					.' GROUP BY product_shop.id_product';
					
		
		$sql .= ' ORDER BY `'.bqSQL($order_by).'` '.pSQL($order_way).' LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;		
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		//if ($order_by == 'orderprice') Tools::orderbyPrice($result, $order_way);
		if (!$result) return array();
		return Product::getProductsProperties($id_lang, $result);
	}
	public function frontGetMostView($categoryIds = array(), $display_only='', $id_lang, $p, $n, $order_way = null, $get_total = false, $active = true, $random = false, $random_number_products = 1, Context $context = null){
		if(!$categoryIds) return array();
		$where = "";
		if($display_only){
            if($display_only == 'condition-new') $where .= " AND p.condition = 'new'";
            elseif($display_only == 'condition-used') $where .= " AND p.condition = 'used'";
            elseif($display_only == 'condition-refurbished') $where .= " AND p.condition = 'refurbished'";    
        }
		if (!$context) $context = Context::getContext();
		$front = true;
		if (!in_array($context->controller->controller_type, array('front', 'modulefront'))) $front = false;		
		if ($p < 1) $p = 1;
		$order_by = 'total_view';
		
		//$order_way = 'DESC';
		
		//if (empty($order_by)) $order_by = 'position';
		//else $order_by = strtolower($order_by);
		if (empty($order_way)) $order_way = 'DESC';
		//$order_by_prefix = false;
		//if ($order_by == 'id_product' || $order_by == 'date_add' || $order_by == 'date_upd') $order_by_prefix = 'p';
		//elseif ($order_by == 'name') $order_by_prefix = 'pl';
		//elseif ($order_by == 'manufacturer' || $order_by == 'manufacturer_name'){
		//	$order_by_prefix = 'm';
		//	$order_by = 'name';
		//}elseif ($order_by == 'position') $order_by_prefix = 'cp';
		//if ($order_by == 'price') $order_by = 'orderprice';
		if (!Validate::isBool($active) || !Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) die (Tools::displayError());
		$id_supplier = (int)Tools::getValue('id_supplier');
		if ($get_total)
		{
			$sql = 'SELECT COUNT(cp.`id_product`) AS total
					FROM `'._DB_PREFIX_.'product` p
					'.Shop::addSqlAssociation('product', 'p').'
					LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`
					WHERE cp.`id_category` IN ('.implode(', ', $categoryIds).') '.$where. 
					($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
					($active ? ' AND product_shop.`active` = 1' : '').
					($id_supplier ? 'AND p.id_supplier = '.(int)$id_supplier : '');
			return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
		}
		$sql = 'SELECT
				p.id_product,  MAX(product_attribute_shop.id_product_attribute) id_product_attribute, pl.`link_rewrite`, pl.`name`, pl.`description_short`, product_shop.`id_category_default`,
				MAX(image_shop.`id_image`) id_image, il.`legend`, p.`ean13`, p.`upc`, cl.`link_rewrite` AS category, p.show_price, p.available_for_order, IFNULL(stock.quantity, 0) as quantity, p.customizable,
				IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity, stock.out_of_stock, pv.total as total_view, 
				product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'" as new,
				product_shop.`on_sale`, MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity, product_shop.price AS orderprice 
				FROM `'._DB_PREFIX_.'category_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p
					ON p.`id_product` = cp.`id_product`
				'.Shop::addSqlAssociation('product', 'p').
				(Combination::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop) :  Product::sqlStock('p', 'product', false, Context::getContext()->shop)).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
					ON (product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON (p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image` i
					ON (i.`id_product` = p.`id_product`)'.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il
					ON (image_shop.`id_image` = il.`id_image`
					AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
					ON m.`id_manufacturer` = p.`id_manufacturer`
				LEFT JOIN `'._DB_PREFIX_.'product_comment` pr
					ON pr.`id_product` = p.`id_product` 
				LEFT JOIN '._DB_PREFIX_.'simplecategory_product_view as pv 
                    ON pv.`product_id` = p.`id_product` 
				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'
					AND cp.`id_category` IN ('.implode(', ', $categoryIds).')'
					.($active ? ' AND product_shop.`active` = 1' : '')
					.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')
					.($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')
					.' GROUP BY product_shop.id_product';
					
		
		$sql .= ' ORDER BY `'.bqSQL($order_by).'` '.pSQL($order_way).' LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;		
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		//if ($order_by == 'orderprice') Tools::orderbyPrice($result, $order_way);
		if (!$result) return array();
		return Product::getProductsProperties($id_lang, $result);
	}
	public function frontGetSpecial($categoryIds = array(), $display_only = '', $id_lang, $p, $n, $order_way = null, $beginning=null, $ending=null, $get_total = false, $active = true, Context $context = null){
		if(!$categoryIds) return array();
		if (!$context) $context = Context::getContext();
		$where = "";
		if($display_only){
            if($display_only == 'condition-new') $where .= " AND p.condition = 'new'";
            elseif($display_only == 'condition-used') $where .= " AND p.condition = 'used'";
            elseif($display_only == 'condition-refurbished') $where .= " AND p.condition = 'refurbished'";    
        }
		$current_date = date('Y-m-d H:i:s');
		$product_reductions = Product::_getProductIdByDate((!$beginning ? $current_date : $beginning), (!$ending ? $current_date : $ending), $context, true);		
		if ($product_reductions)
		{
			$ids_product = ' AND (';
			foreach ($product_reductions as $product_reduction)
				$ids_product .= '( product_shop.`id_product` = '.(int)$product_reduction['id_product'].($product_reduction['id_product_attribute'] ? ' AND product_attribute_shop.`id_product_attribute`='.(int)$product_reduction['id_product_attribute'] :'').') OR';
			$ids_product = rtrim($ids_product, 'OR').')';
		}	
				
		$front = true;
		if (!in_array($context->controller->controller_type, array('front', 'modulefront'))) $front = false;		
		if ($p < 1) $p = 1;
		$order_by = 'sp.reduction';
		//$order_way = 'DESC';
		if (empty($order_way)) $order_way = 'DESC';
		if (!Validate::isBool($active) || !Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) die (Tools::displayError());
		$id_supplier = (int)Tools::getValue('id_supplier');
		if ($get_total)
		{
			$sql = 'SELECT COUNT(cp.`id_product`) AS total
					FROM (`'._DB_PREFIX_.'product` p INNER JOIN `'._DB_PREFIX_.'specific_price` sp On p.id_product = sp.id_product)
					'.Shop::addSqlAssociation('product', 'p').'
					LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`
					WHERE cp.`id_category` IN ('.implode(', ', $categoryIds).') '.$where. 
					' AND product_shop.`visibility` IN ("both", "catalog")'.
					' AND product_shop.`active` = 1'.
					(($ids_product) ? $ids_product : '').
					($id_supplier ? 'AND p.id_supplier = '.(int)$id_supplier : '');
			return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
		}
		$sql = 'SELECT
				p.id_product,  MAX(product_attribute_shop.id_product_attribute) id_product_attribute, pl.`link_rewrite`, pl.`name`, pl.`description_short`, product_shop.`id_category_default`,
				MAX(image_shop.`id_image`) id_image, il.`legend`, p.`ean13`, p.`upc`, cl.`link_rewrite` AS category, p.show_price, p.available_for_order, IFNULL(stock.quantity, 0) as quantity, p.customizable,
				IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity, stock.out_of_stock,
				product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'" as new,
				product_shop.`on_sale`, MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity, product_shop.price AS orderprice, sp.reduction 
				FROM `'._DB_PREFIX_.'category_product` cp
				LEFT JOIN (`'._DB_PREFIX_.'product` p INNER JOIN `'._DB_PREFIX_.'specific_price` sp On p.id_product = sp.id_product)
					ON p.`id_product` = cp.`id_product`
				'.Shop::addSqlAssociation('product', 'p').
				(Combination::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop) :  Product::sqlStock('p', 'product', false, Context::getContext()->shop)).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
					ON (product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON (p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image` i
					ON (i.`id_product` = p.`id_product`)'.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il
					ON (image_shop.`id_image` = il.`id_image`
					AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
					ON m.`id_manufacturer` = p.`id_manufacturer`
				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.(($ids_product) ? $ids_product : '').
					' AND cp.`id_category` IN ('.implode(', ', $categoryIds).')'. $where .					
					' AND product_shop.`active` = 1'.
					(($ids_product) ? $ids_product : '').
					' AND product_shop.`visibility` IN ("both", "catalog")'.
					($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '').
					' GROUP BY product_shop.id_product'.
					' ORDER BY `'.bqSQL($order_by).'` '.pSQL($order_way).' LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;		
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		if (!$result) return array();
		return Product::getProductsProperties($id_lang, $result);
	}
	
	public function frontGetBestSeller($categoryIds=array(), $on_condition = 'all', $on_sale=2, $on_new=2, $on_discount = 2, $id_lang, $page_number = 0, $nb_products = 10, Context $context = null)
	{
		if(!$categoryIds) return array();
		$where = "";
		if($on_condition != 'all'){
			$where .= " AND p.condition = '".$on_condition."'";			
            //if($display_only == 'condition-new') $where .= " AND p.condition = 'new'";
            //elseif($display_only == 'condition-used') $where .= " AND p.condition = 'used'";
            //elseif($display_only == 'condition-refurbished') $where .= " AND p.condition = 'refurbished'";    
        }
		if (!$context) $context = Context::getContext();
		if ($page_number < 0) $page_number = 0;
		if ($nb_products < 1) $nb_products = 10;
		//FROM `'._DB_PREFIX_.'category_product` cp
		$sql = '
		SELECT
			p.id_product,  MAX(product_attribute_shop.id_product_attribute) id_product_attribute, pl.`link_rewrite`, pl.`name`, pl.`description_short`, product_shop.`id_category_default`,
			MAX(image_shop.`id_image`) id_image, il.`legend`,
			ps.`quantity` AS sales, p.`ean13`, p.`upc`, cl.`link_rewrite` AS category, p.show_price, p.available_for_order, IFNULL(stock.quantity, 0) as quantity, p.customizable,
			IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity, stock.out_of_stock,
			product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'" as new,
			product_shop.`on_sale`, MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity
		FROM `'._DB_PREFIX_.'product_sale` ps
		LEFT JOIN `'._DB_PREFIX_.'product` p ON ps.`id_product` = p.`id_product`
		'.Shop::addSqlAssociation('product', 'p').'
		LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
			ON (p.`id_product` = pa.`id_product`)
		'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
		'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).'
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
			ON p.`id_product` = pl.`id_product`
			AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
		Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
			ON cl.`id_category` = product_shop.`id_category_default`
			AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl');
				
		if (Group::isFeatureActive())
		{
			$groups = FrontController::getCurrentCustomerGroups();
			$sql .= '
				JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_product` = p.`id_product`)
				JOIN `'._DB_PREFIX_.'category_group` cg ON (cp.id_category = cg.id_category AND cg.`id_group` '.(count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1').')';
		}else{
			$sql .= ' JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_product` = p.`id_product`)';
		}

		$sql.= '
		WHERE product_shop.`active` = 1
		AND cp.`id_category` IN ('.implode(', ', $categoryIds).') '.$where.'  
		
		AND p.`visibility` != \'none\'
		GROUP BY product_shop.id_product
		ORDER BY sales DESC
		LIMIT '.(int)($page_number * $nb_products).', '.(int)$nb_products;

		if (!$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql))
			return false;

		return Product::getProductsProperties($id_lang, $result);
	}
	
	
	
	
	public static function _getLayoutName($layout){
		return self::$layout[$layout];
	}
	
	
	
	
	
	
	
    function getProductList($id_lang, $arrCategory = array(), $notIn = '', $keyword = '', $getTotal = false, $offset=0, $limit=10){
        
        $where = "";
        if($arrCategory){
            $catIds = implode(', ', $arrCategory);
        }
        if (Group::isFeatureActive())
		{
			$groups = FrontController::getCurrentCustomerGroups();
			$where .= ' AND p.`id_product` IN (
				SELECT cp.`id_product`
				FROM `'._DB_PREFIX_.'category_group` cg
				LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_category` = cg.`id_category`)
				WHERE  cp.id_category IN ('.$catIds.') AND cg.`id_group` '.(count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1').'
			)';
		}else{
            $where .= ' AND p.`id_product` IN (
				SELECT cp.`id_product`
				FROM `'._DB_PREFIX_.'category_product` cp 
				WHERE cp.id_category IN ('.$catIds.'))';
		}
        if($keyword != '') $where .= " AND (p.id_product) LIKE '%".$keyword."%' OR pl.name LIKE '%".$keyword."%'";
        $sqlTotal = 'SELECT COUNT(p.`id_product`) AS nb
					FROM `'._DB_PREFIX_.'product` p
					'.Shop::addSqlAssociation('product', 'p').' 
                    LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					   ON p.`id_product` = pl.`id_product`
					   AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
					WHERE product_shop.`active` = 1 AND product_shop.`active` = 1 AND p.`visibility` != \'none\' '.$where;
        $total = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sqlTotal);
        if($getTotal == true) return $total;
        if($total <=0) return false;                    
        $sql = 'Select p.*, pl.`name`, pl.`link_rewrite`, IFNULL(stock.quantity, 0) as quantity_all, MAX(image_shop.`id_image`) id_image 
                FROM  `'._DB_PREFIX_.'product` p 
                '.Shop::addSqlAssociation('product', 'p', false).'				
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
				LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
				LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (product_shop.`id_tax_rules_group` = tr.`id_tax_rules_group`)
					AND tr.`id_country` = '.(int)Context::getContext()->country->id.'
					AND tr.`id_state` = 0
				LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
				'.Product::sqlStock('p').'
				WHERE product_shop.`active` = 1
					AND p.`visibility` != \'none\'  '.$where.'			
				GROUP BY product_shop.id_product Limit '.$offset.', '.$limit;
			
                $result = DB::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
                return Product::getProductsProperties($id_lang, $result);            
    }
    
    
    public function paginationAjaxEx($total, $page_size, $current = 1, $index_limit = 10, $func='loadPage'){
		$total_pages=ceil($total/$page_size);
		$start=max($current-intval($index_limit/2), 1);
		$end=$start+$index_limit-1;
		$output = '';                       
		$output = '<ul class="pagination">';
		if($current==1) {
			$output .= '<li><span>Prev</span></li>';
		}else{
			$i = $current-1;
			$output .= '<li><a href="javascript:void(0)" onclick="'.$func.'(\''.$i.'\')">Prev</a></li>';
		}
		if($start>1){
			$i = 1;
			$output .= '<li><a href="javascript:void(0)" onclick="'.$func.'(\''.$i.'\')">'.$i.'</a></li>';
			$output .= '<li><span>...</span></li>';
		}	
		for ($i=$start;$i<=$end && $i<= $total_pages;$i++) {
			if($i==$current) 
				$output .= '<li class="active"><span >'.$i.'</span></li>';
			else 
				$output .= '<li><a  href="javascript:void(0)" onclick="'.$func.'(\''.$i.'\')">'.$i.'</a></li>';
		}		
		if($total_pages>$end) {
			$i = $total_pages;
			$output .= '<li><span>...</span></li>';
			$output .= '<li><a href="javascript:void(0)" onclick="'.$func.'(\''.$i.'\')">'.$i.'</a></li>';
		}		
		if($current<$total_pages) {
			$i = $current+1;
			$output .= '<li><a href="javascript:void(0)" onclick="'.$func.'(\''.$i.'\')">Next</a></li>';
		} else {
			$output .= '<li><span>Next</span></li>';
		}
		$output .= '</ul>';		
		return $output;		
	}
    /**
     * Assign template vars related to images
     */
    private function getProductImages($id_product, $langId)
    { 
        $images = Image::getImages($langId, $id_product);
        
        return $images;
        
    }
   
}
