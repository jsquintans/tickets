<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class ProductsCategoryOverride extends ProductsCategory
{
	

	public function hookProductFooter($params)
	{
		$id_product = (int)$params['product']->id;
		$product = $params['product'];

		$cache_id = 'productscategory|'.$id_product.'|'.(isset($params['category']->id_category) ? (int)$params['category']->id_category : (int)$product->id_category_default);

		if (!$this->isCached('productscategory.tpl', $this->getCacheId($cache_id)))
		{

			$category = false;
			if (isset($params['category']->id_category))
				$category = $params['category'];
			else
			{
				if (isset($product->id_category_default) && $product->id_category_default > 1)
					$category = new Category((int)$product->id_category_default);
			}


			//Aquí empezamos a modificar las sentencias //
				
			//print_r($product);
			//print_r($product);

			$product_o = new Product($product->id);
			//print_r($product_o);

			$featus = $product_o->getFeatures();
			//print_r($featus);
			$categorias = $product_o->getCategories();

			//print_r($categorias);

			/*
			foreach ($featus as $featu) {
				if($featu['id_feature'] == 6 ){
					$equipos_cross[] = $featu['id_feature_value'];
				}
			}*/

		/*	foreach ($categorias as $categoria) {
				$sql_padre_categoria = "SELECT id_parent FROM ps_category WHERE id_category = ".$categoria.";";

				$res_padre_categoria = Db::getInstance()->getValue($sql_padre_categoria);

				if($res_padre_categoria == 15)
				{
					$cats_fin[] = $categoria;
				}
			}

			foreach ($cats_fin as $cat_fin) {
				$category_1 = new Category((int)$cat_fin);
				print_r($category);

				$category_products = $category_1->getProducts($this->context->language->id, 1, 100); 

			} */
			
			//print_r($equipos_cross);


			/*foreach ($equipos_cross as $equipo_cross) {
				$sql_nombre = "SELECT value FROM ps_feature_value_lang
						WHERE  id_lang = 1 AND id_feature_value = $equipo_cross ";
				$nombre_equipo =   Db::getInstance()->getValue($sql_nombre);
				//echo $nombre_equipo;

				$sql_productos = "SELECT id_product FROM ps_feature_product WHERE id_feature_value = $equipo_cross";

				$equipos_con = Db::getInstance()->executeS($sql_productos);
				print_r($equipos_con);

			}*/





			if (!Validate::isLoadedObject($category) || !$category->active)
				return false;

			// Get infos
			$category_products = $category->getProducts($this->context->language->id, 1, 100); /* 100 products max. */

			//print_r($category_products);

			$nb_category_products = (int)count($category_products);
			$middle_position = 0;

			// Remove current product from the list
			if (is_array($category_products) && count($category_products))
			{
				foreach ($category_products as $key => $category_product)
				{
					if ($category_product['id_product'] == $id_product)
					{
						unset($category_products[$key]);
						break;
					}
				}

				$taxes = Product::getTaxCalculationMethod();
				if (Configuration::get('PRODUCTSCATEGORY_DISPLAY_PRICE'))
				{
					foreach ($category_products as $key => $category_product)
					{
						if ($category_product['id_product'] != $id_product)
						{
							if ($taxes == 0 || $taxes == 2)
							{
								$category_products[$key]['displayed_price'] = Product::getPriceStatic(
									(int)$category_product['id_product'],
									true,
									null,
									2
								);
							} elseif ($taxes == 1)
							{
								$category_products[$key]['displayed_price'] = Product::getPriceStatic(
									(int)$category_product['id_product'],
									false,
									null,
									2
								);
							}
						}
					}
				}

				// Get positions
				$middle_position = (int)round($nb_category_products / 2, 0);
				$product_position = $this->getCurrentProduct($category_products, (int)$id_product);

				// Flip middle product with current product
				if ($product_position)
				{
					$tmp = $category_products[$middle_position - 1];
					$category_products[$middle_position - 1] = $category_products[$product_position];
					$category_products[$product_position] = $tmp;
				}

				// If products tab higher than 30, slice it
				if ($nb_category_products > 30)
				{
					$category_products = array_slice($category_products, $middle_position - 15, 30, true);
					$middle_position = 15;
				}
			}

			// Display tpl
			$this->smarty->assign(
				array(
					'categoryProducts' => $category_products,
					'middlePosition' => (int)$middle_position,
					'ProdDisplayPrice' => Configuration::get('PRODUCTSCATEGORY_DISPLAY_PRICE')
				)
			);
		}

		return $this->display(__FILE__, 'productscategory.tpl', $this->getCacheId($cache_id));
	}


}
