<?php
/*
* 2014 Fashion
*
*  @author Fashion modules
*  @copyright  2014 Fashion modules
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_'))
	exit;

class StoreMapOverride extends StoreMap
{


	public function hookHeader($params)
	{
		// CSS in global.css file
		$module_name = '';
		if (Validate::isModuleName(Tools::getValue('module')))
			$module_name = Tools::getValue('module');
		if (!empty($this->context->controller->php_self))
			$page_name = $this->context->controller->php_self;
		elseif (Tools::getValue('fc') == 'module' && $module_name != '')
			$page_name = 'module-' . $module_name . '-' . Tools::getValue('controller');
		elseif (preg_match('#^' . preg_quote($this->context->shop->physical_uri, '#') .
			'modules/([a-zA-Z0-9_-]+?)/(.*)$#', $_SERVER['REQUEST_URI'], $m))
			$page_name = 'module-' . $m[1] . '-' . str_replace(array('.php', '/'), array('',
					'-'), $m[2]);
		else {
			$page_name = Dispatcher::getInstance()->getController();
			$page_name = (preg_match('/^[0-9]/', $page_name) ? 'page_' . $page_name : $page_name);
		}
		if ($page_name != 'stores') {
			$default_country = new Country((int)Configuration::get('PS_COUNTRY_DEFAULT'));
			$this->context->controller->addJS('http' . ((Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? 's' : '') . '://maps.google.com/maps/api/js?key=AIzaSyDvtzV3dKDa1J2RXMdgIzFnLG6M2s66u20');
			$this->context->controller->addJS($this->_path . 'storemap.js');
			$this->context->controller->addCSS(($this->_path) . 'css/storemap.css', 'all');
		}
	}
}
