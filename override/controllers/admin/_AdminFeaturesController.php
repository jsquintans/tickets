<?php

class AdminFeaturesController extends AdminFeaturesControllerCore
{
     public function __construct()
    {
        $this->table = 'feature';
        $this->className = 'Feature';
        $this->list_id = 'feature';
        $this->identifier = 'id_feature';
        $this->lang = true;

         $this->fieldImageSettings = array(
            'name' => 'logo',
            'dir' => 'f'
        );


        $this->fields_list = array(
            'id_feature' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'logo' => array(
                'title' => $this->l('Logo'),
                'image' => 'f',
                'orderby' => false,
                'search' => false,
                'align' => 'center',
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'width' => 'auto',
                'filter_key' => 'b!name'
            ),
            'value' => array(
                'title' => $this->l('Values'),
                'orderby' => false,
                'search' => false,
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'position' => array(
                'title' => $this->l('Position'),
                'filter_key' => 'a!position',
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'position' => 'position'
            )
        );

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
        parent::__construct();
    }

    public function initFormFeatureValue()
    {
        $this->setTypeValue();

        $obj = $this->loadObject(true);

        $image = _PS_FEATU_IMG_DIR_.$obj->id.'.jpg';
        echo $image;
        $image_url = ImageManager::thumbnail($image, $this->table.'_'.(int)$obj->id.'.'.$this->imageType, 350,
            $this->imageType, true, true);
        $image_size = file_exists($image) ? filesize($image) / 1000 : false;


        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Feature value'),
                'icon' => 'icon-info-sign'
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Feature'),
                    'name' => 'id_feature',
                    'options' => array(
                        'query' => Feature::getFeatures($this->context->language->id),
                        'id' => 'id_feature',
                        'name' => 'name'
                    ),
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Value'),
                    'name' => 'value',
                    'lang' => true,
                    'size' => 33,
                    'hint' => $this->l('Invalid characters:').' <>;=#{}',
                    'required' => true
                ),
                  array(
                    'type' => 'file',
                    'label' => $this->l('Logo'),
                    'name' => 'logo',
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'display_image' => true,
                    'col' => 6,
                    'hint' => $this->l('Upload a manufacturer logo from your computer.')
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
            ),
            'buttons' => array(
                'save-and-stay' => array(
                    'title' => $this->l('Save then add another value'),
                    'name' => 'submitAdd'.$this->table.'AndStay',
                    'type' => 'submit',
                    'class' => 'btn btn-default pull-right',
                    'icon' => 'process-icon-save'
                )
            )
        );

        $this->fields_value['id_feature'] = (int)Tools::getValue('id_feature');

        $feature_value = new FeatureValue(Tools::getValue('id_feature_value'));

        $this->tpl_vars = array(
            'feature_value' => $feature_value,
        );

       

        $this->getlanguages();
        $helper = new HelperForm();
        $helper->show_cancel_button = true;

        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (empty($back)) {
            $back = self::$currentIndex.'&token='.$this->token;
        }
        if (!Validate::isCleanHtml($back)) {
            die(Tools::displayError());
        }

        $helper->back_url = $back;
        $helper->currentIndex = self::$currentIndex;
        $helper->token = $this->token;
        $helper->table = $this->table;
        $helper->identifier = $this->identifier;
        $helper->override_folder = 'feature_value/';
        $helper->id = $feature_value->id;
        $helper->toolbar_scroll = false;
        $helper->tpl_vars = $this->tpl_vars;
        $helper->languages = $this->_languages;
        $helper->default_form_language = $this->default_form_language;
        $helper->allow_employee_form_lang = $this->allow_employee_form_lang;
        $helper->fields_value = $this->getFieldsValue($feature_value);
        $helper->toolbar_btn = $this->toolbar_btn;
        $helper->title = $this->l('Add a new feature value');
        $this->content .= $helper->generateForm($this->fields_form);
    }


    public function renderView()
    {
        if (($id = Tools::getValue('id_feature'))) {
            $this->setTypeValue();
            $this->list_id = 'feature_value';
            $this->lang = true;

            $this->addRowAction('edit');
            $this->addRowAction('delete');

            if (!Validate::isLoadedObject($obj = new Feature((int)$id))) {
                $this->errors[] = Tools::displayError('An error occurred while updating the status for an object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
                return;
            }

            $this->feature_name = $obj->name;
            $this->toolbar_title = $this->feature_name[$this->context->employee->id_lang];
            $this->fields_list = array(
                'id_feature_value' => array(
                    'title' => $this->l('ID'),
                    'align' => 'center',
                    'class' => 'fixed-width-xs'
                ),
                'logo' => array(
                    'title' => $this->l('Image'),
                    'align' => 'center',
                    'image' => 'f',
                    'orderby' => false,
                    'filter' => false,
                    'search' => false
                ),
                'value' => array(
                    'title' => $this->l('Value')
                )
            );

            $this->_where = sprintf('AND `id_feature` = %d', (int)$id);
            self::$currentIndex = self::$currentIndex.'&id_feature='.(int)$id.'&viewfeature';
            $this->processFilter();
            return parent::renderList();
        }
    }


     protected function afterImageUpload()
    {
        $res = true;
        $generate_hight_dpi_images = (bool)Configuration::get('PS_HIGHT_DPI');

        if (($id_feature_value = (int)Tools::getValue('id_feature_value')) &&
            isset($_FILES) &&
            count($_FILES) &&
            file_exists(_PS_FEATU_IMG_DIR_.$id_feature_value.'.jpg')) {
            $images_types = ImageType::getImagesTypes('manufacturers');
            foreach ($images_types as $k => $image_type) {
                $res &= ImageManager::resize(
                    _PS_FEATU_IMG_DIR_.$id_feature_value.'.jpg',
                    _PS_FEATU_IMG_DIR_.$id_feature_value.'-'.stripslashes($image_type['name']).'.jpg',
                    (int)$image_type['width'],
                    (int)$image_type['height']
                );

                if ($generate_hight_dpi_images) {
                    $res &= ImageManager::resize(
                        _PS_FEATU_IMG_DIR_.$id_feature_value.'.jpg',
                        _PS_FEATU_IMG_DIR_.$id_feature_value.'-'.stripslashes($image_type['name']).'2x.jpg',
                        (int)$image_type['width']*2,
                        (int)$image_type['height']*2
                    );
                }
            }

            $current_logo_file = _PS_TMP_IMG_DIR_.'feature_value_mini_'.$id_feature_value.'_'.$this->context->shop->id.'.jpg';

            if ($res && file_exists($current_logo_file)) {
                unlink($current_logo_file);
            }
        }

        if (!$res) {
            $this->errors[] = Tools::displayError('Unable to resize one or more of your pictures.');
        }

        return $res;
    }



    public function postProcess()
    {
        if (!Feature::isFeatureActive()) {
            return;
        }

        if ($this->table == 'feature_value' && ($this->action == 'save' || $this->action == 'delete' || $this->action == 'bulkDelete')) {


        if($_POST['id_feature'] == 6 && $this->action == 'save' && !isset($_POST['id_feature_value'])){

            $feature_value2 = new FeatureValue();
            $feature_value2->id_feature = 1;
            $feature_value2->custom = false;
            $feature_value2->value = array_fill_keys(Language::getIDs(false),$_POST['value_1']);
            $feature_value2->add();

            $feature_value3 = new FeatureValue();
            $feature_value3->id_feature = 2;
            $feature_value3->custom = false;
            $feature_value3->value = array_fill_keys(Language::getIDs(false),$_POST['value_1']);
            $feature_value3->add();

                        
            $path = _PS_FEATU_IMG_DIR_;

            $id_feature_value2 = $feature_value2->id;
            
            $id_feature_value3 = $feature_value3->id;


            $image_format = 'jpg';
            $tempName = tempnam(_PS_IMG_DIR_."/tmp/", 'PS');
            copy($_FILES['logo']['tmp_name'], $tempName);
            
            $res = true;
            $generate_hight_dpi_images = (bool)Configuration::get('PS_HIGHT_DPI');
          
            
            ImageManager::resize($tempName, $path.$id_feature_value2.'.'.$image_format);
            ImageManager::resize($tempName, $path.$id_feature_value3.'.'.$image_format);
        
            $images_types = ImageType::getImagesTypes('manufacturers');
            foreach ($images_types as $k => $image_type) {
                $res &= ImageManager::resize(
                    _PS_FEATU_IMG_DIR_.$id_feature_value2.'.jpg',
                    _PS_FEATU_IMG_DIR_.$id_feature_value2.'-'.stripslashes($image_type['name']).'.jpg',
                    (int)$image_type['width'],
                    (int)$image_type['height']
                );

                $res &= ImageManager::resize(
                    _PS_FEATU_IMG_DIR_.$id_feature_value3.'.jpg',
                    _PS_FEATU_IMG_DIR_.$id_feature_value3.'-'.stripslashes($image_type['name']).'.jpg',
                    (int)$image_type['width'],
                    (int)$image_type['height']
                );


                if ($generate_hight_dpi_images) {
                    $res &= ImageManager::resize(
                        _PS_FEATU_IMG_DIR_.$id_feature_value2.'.jpg',
                        _PS_FEATU_IMG_DIR_.$id_feature_value2.'-'.stripslashes($image_type['name']).'2x.jpg',
                        (int)$image_type['width']*2,
                        (int)$image_type['height']*2
                    );

                    $res &= ImageManager::resize(
                        _PS_FEATU_IMG_DIR_.$id_feature_value3.'.jpg',
                        _PS_FEATU_IMG_DIR_.$id_feature_value3.'-'.stripslashes($image_type['name']).'2x.jpg',
                        (int)$image_type['width']*2,
                        (int)$image_type['height']*2
                    );
                }
            } 


        }


            Hook::exec('displayFeatureValuePostProcess',
                array('errors' => &$this->errors));
        } // send errors as reference to allow displayFeatureValuePostProcess to stop saving process
        else {
            Hook::exec('displayFeaturePostProcess',
                array('errors' => &$this->errors));
        } // send errors as reference to allow displayFeaturePostProcess to stop saving process




        parent::postProcess();

        if ($this->table == 'feature_value' && ($this->display == 'edit' || $this->display == 'add')) {
            $this->display = 'editFeatureValue';
        }
    }



}
