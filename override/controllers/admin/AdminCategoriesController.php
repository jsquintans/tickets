<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @property Category $object
 */
class AdminCategoriesController extends AdminCategoriesControllerCore
{


    public function postProcess()
    {
        if (!in_array($this->display, array('edit', 'add'))) {
            $this->multishop_context_group = false;
        }


        if(!empty($_FILES) && !empty($_POST)){
            //print_r($_FILES);

            $nombre =  $_POST['name_1'];

            $sql = "SELECT COUNT(*) FROM ps_feature_value_lang  WHERE value = '".$nombre."' AND id_lang = 1";

            $res_sql = Db::getInstance()->getValue($sql);

            if($res_sql == 0){
            //print_r($_POST);
            //
            

            if($_POST['id_parent'] == 14){

                $feature_value1 = new FeatureValue();
                $feature_value1->id_feature = 3;
                $feature_value1->custom = false;
                $feature_value1->value = array_fill_keys(Language::getIDs(false),$_POST['name_1']);
                $feature_value1->add();

                $path = _PS_FEATU_IMG_DIR_;

                 //Cogemos el id del Team
                $id_feature_value1 = $feature_value1->id;



                $image_format = 'png';
                $tempName = tempnam(_PS_IMG_DIR_."/tmp/", 'PS');
                copy($_FILES['thumb']['tmp_name'], $tempName);
                
                $res = true;
                $generate_hight_dpi_images = (bool)Configuration::get('PS_HIGHT_DPI');
              
                
                //Creamos la imagen tanto de vistante como de local
                ImageManager::resize($tempName, $path.$id_feature_value1.'.'.$image_format);
          
                //Redimensionamos
                $images_types = ImageType::getImagesTypes('manufacturers');
                foreach ($images_types as $k => $image_type) {
                   
                      $res &= ImageManager::resize(
                        _PS_FEATU_IMG_DIR_.$id_feature_value1.'.png',
                        _PS_FEATU_IMG_DIR_.$id_feature_value1.'-'.stripslashes($image_type['name']).'.png',
                        (int)$image_type['width'],
                        (int)$image_type['height']
                    );


                    if ($generate_hight_dpi_images) {
                    
                         $res &= ImageManager::resize(
                            _PS_FEATU_IMG_DIR_.$id_feature_value1.'.png',
                            _PS_FEATU_IMG_DIR_.$id_feature_value1.'-'.stripslashes($image_type['name']).'2x.png',
                            (int)$image_type['width']*2,
                            (int)$image_type['height']*2
                        );
                    }


                } 



            }


            if($_POST['id_parent'] == 15)
            {


                // Creamos el valor para Team 
                $feature_value1 = new FeatureValue();
                $feature_value1->id_feature = 6;
                $feature_value1->custom = false;
                $feature_value1->value = array_fill_keys(Language::getIDs(false),$_POST['name_3']);
                $feature_value1->add();


                // Creamos el valor para Local 
                $feature_value2 = new FeatureValue();
                $feature_value2->id_feature = 1;
                $feature_value2->custom = false;
                $feature_value2->value = array_fill_keys(Language::getIDs(false),$_POST['name_3']);
                $feature_value2->add();

                // Creamos el valor para visitantes
                $feature_value3 = new FeatureValue();
                $feature_value3->id_feature = 2;
                $feature_value3->custom = false;
                $feature_value3->value = array_fill_keys(Language::getIDs(false),$_POST['name_3']);
                $feature_value3->add();
                
                 // Lo necesario para subir la imagen 
                        
                $path = _PS_FEATU_IMG_DIR_;

                 //Cogemos el id del Team
                $id_feature_value1 = $feature_value1->id;

                //Cogemos el id del local
                $id_feature_value2 = $feature_value2->id;
                
                //Cogemos el id del visitante 
                $id_feature_value3 = $feature_value3->id;



                $image_format = 'png';
                $tempName = tempnam(_PS_IMG_DIR_."/tmp/", 'PS');
                copy($_FILES['thumb']['tmp_name'], $tempName);
                
                $res = true;
                $generate_hight_dpi_images = (bool)Configuration::get('PS_HIGHT_DPI');
              
                
                //Creamos la imagen tanto de vistante como de local
                ImageManager::resize($tempName, $path.$id_feature_value1.'.'.$image_format);
                ImageManager::resize($tempName, $path.$id_feature_value2.'.'.$image_format);
                ImageManager::resize($tempName, $path.$id_feature_value3.'.'.$image_format);
            
                //Redimensionamos
                $images_types = ImageType::getImagesTypes('manufacturers');
                foreach ($images_types as $k => $image_type) {
                    $res &= ImageManager::resize(
                        _PS_FEATU_IMG_DIR_.$id_feature_value2.'.png',
                        _PS_FEATU_IMG_DIR_.$id_feature_value2.'-'.stripslashes($image_type['name']).'.png',
                        (int)$image_type['width'],
                        (int)$image_type['height']
                    );

                    $res &= ImageManager::resize(
                        _PS_FEATU_IMG_DIR_.$id_feature_value3.'.png',
                        _PS_FEATU_IMG_DIR_.$id_feature_value3.'-'.stripslashes($image_type['name']).'.png',
                        (int)$image_type['width'],
                        (int)$image_type['height']
                    );

                      $res &= ImageManager::resize(
                        _PS_FEATU_IMG_DIR_.$id_feature_value1.'.png',
                        _PS_FEATU_IMG_DIR_.$id_feature_value1.'-'.stripslashes($image_type['name']).'.png',
                        (int)$image_type['width'],
                        (int)$image_type['height']
                    );


                    if ($generate_hight_dpi_images) {
                        $res &= ImageManager::resize(
                            _PS_FEATU_IMG_DIR_.$id_feature_value2.'.png',
                            _PS_FEATU_IMG_DIR_.$id_feature_value2.'-'.stripslashes($image_type['name']).'2x.png',
                            (int)$image_type['width']*2,
                            (int)$image_type['height']*2
                        );

                        $res &= ImageManager::resize(
                            _PS_FEATU_IMG_DIR_.$id_feature_value3.'.png',
                            _PS_FEATU_IMG_DIR_.$id_feature_value3.'-'.stripslashes($image_type['name']).'2x.png',
                            (int)$image_type['width']*2,
                            (int)$image_type['height']*2
                        );

                         $res &= ImageManager::resize(
                            _PS_FEATU_IMG_DIR_.$id_feature_value1.'.png',
                            _PS_FEATU_IMG_DIR_.$id_feature_value1.'-'.stripslashes($image_type['name']).'2x.png',
                            (int)$image_type['width']*2,
                            (int)$image_type['height']*2
                        );
                    }


                } 

            }




            }

            //exit;
            
        
        }

      //  print_r($this->display);

       

        if (Tools::isSubmit('forcedeleteImage') || (isset($_FILES['image']) && $_FILES['image']['size'] > 0) || Tools::getValue('deleteImage')) {
            $this->processForceDeleteImage();
            $this->processForceDeleteThumb();
            if (Tools::isSubmit('forcedeleteImage')) {
                Tools::redirectAdmin(self::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminCategories').'&conf=7');
            }
        }
        return parent::postProcess();
    }


    public function processAdd()
    {
        $id_category = (int)Tools::getValue('id_category');
        $id_parent = (int)Tools::getValue('id_parent');

        // if true, we are in a root category creation
        if (!$id_parent) {
            $_POST['is_root_category'] = $_POST['level_depth'] = 1;
            $_POST['id_parent'] = $id_parent = (int)Configuration::get('PS_ROOT_CATEGORY');
        }

        if ($id_category) {
            if ($id_category != $id_parent) {
                if (!Category::checkBeforeMove($id_category, $id_parent)) {
                    $this->errors[] = Tools::displayError('The category cannot be moved here.');
                }
            } else {
                $this->errors[] = Tools::displayError('The category cannot be a parent of itself.');
            }
        }
        $object = parent::processAdd();

        //if we create a you root category you have to associate to a shop before to add sub categories in. So we redirect to AdminCategories listing
        if ($object && Tools::getValue('is_root_category')) {
            Tools::redirectAdmin(self::$currentIndex.'&id_category='.(int)Configuration::get('PS_ROOT_CATEGORY').'&token='.Tools::getAdminTokenLite('AdminCategories').'&conf=3');
        }
        return $object;
    }



    public function processDelete()
    {
        if ($this->tabAccess['delete'] === '1') {
            /** @var Category $category */
            $category = $this->loadObject();
            if ($category->isRootCategoryForAShop()) {
                $this->errors[] = Tools::displayError('You cannot remove this category because one of your shops uses it as a root category.');
            } elseif (parent::processDelete()) {
                $this->setDeleteMode();
                $this->processFatherlessProducts((int)$category->id_parent);


              //  print_r($category);
              //  exit;

                $nombre_categoria = $category->name[1];
                $id_pariente = $category->id_parent;
                if($id_pariente == 15){
                   // $feature_value1 = new FeatureValue();

                    //Eliminamos de Teams
                     $id_feature_value1 = Db::getInstance()->getValue("SELECT pfv.`id_feature_value` FROM ps_feature_value_lang as pfvl
                             LEFT JOIN `ps_feature_value` as pfv ON pfvl.`id_feature_value` = pfv.`id_feature_value`   WHERE pfvl.`value` = '".$nombre_categoria."' AND pfvl.`id_lang` = 1 AND pfv.`id_feature` = 6");

                     $feature_value1 = new FeatureValue($id_feature_value1);
                     $feature_value1->delete();


                     //Eliminamos de Visitantes
                     $id_feature_value2 = Db::getInstance()->getValue("SELECT pfv.`id_feature_value` FROM ps_feature_value_lang as pfvl
                             LEFT JOIN `ps_feature_value` as pfv ON pfvl.`id_feature_value` = pfv.`id_feature_value`   WHERE pfvl.`value` = '".$nombre_categoria."' AND pfvl.`id_lang` = 1 AND pfv.`id_feature` = 1");

                     $feature_value2 = new FeatureValue($id_feature_value2);
                     $feature_value2->delete();


                     //Eliminamos de Local   
                     $id_feature_value3 = Db::getInstance()->getValue("SELECT pfv.`id_feature_value` FROM ps_feature_value_lang as pfvl
                             LEFT JOIN `ps_feature_value` as pfv ON pfvl.`id_feature_value` = pfv.`id_feature_value`   WHERE pfvl.`value` = '".$nombre_categoria."' AND pfvl.`id_lang` = 1 AND pfv.`id_feature` = 2");

                     $feature_value3 = new FeatureValue($id_feature_value3);
                     $feature_value3->delete();
                }

                if($id_pariente == 14){

                }

                return true;
            }
        } else {
            $this->errors[] = Tools::displayError('You do not have permission to delete this.');
        }
        return false;
    }

 

    protected function postImage($id)
    {
        $ret = parent::postImage($id);
        if (($id_category = (int)Tools::getValue('id_category')) && isset($_FILES) && count($_FILES)) {
            $name = 'image';
            if ($_FILES[$name]['name'] != null && file_exists(_PS_CAT_IMG_DIR_.$id_category.'.'.$this->imageType)) {
                $images_types = ImageType::getImagesTypes('categories');
                foreach ($images_types as $k => $image_type) {
                    if (!ImageManager::resize(
                        _PS_CAT_IMG_DIR_.$id_category.'.'.$this->imageType,
                        _PS_CAT_IMG_DIR_.$id_category.'-'.stripslashes($image_type['name']).'.'.$this->imageType,
                        (int)$image_type['width'],
                        (int)$image_type['height']
                    )) {
                        $this->errors = Tools::displayError('An error occurred while uploading category image.');
                    }
                }
            }

            $name = 'thumb';
            if ($_FILES[$name]['name'] != null) {
                if (!isset($images_types)) {
                    $images_types = ImageType::getImagesTypes('categories');
                }
                $formated_medium = ImageType::getFormatedName('medium');
                foreach ($images_types as $k => $image_type) {
                    if ($formated_medium == $image_type['name']) {
                        if ($error = ImageManager::validateUpload($_FILES[$name], Tools::getMaxUploadSize())) {
                            $this->errors[] = $error;
                        } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES[$name]['tmp_name'], $tmpName)) {
                            $ret = false;
                        } else {
                            if (!ImageManager::resize(
                                $tmpName,
                                _PS_CAT_IMG_DIR_.$id_category.'-'.stripslashes($image_type['name']).'.'.$this->imageType,
                                (int)$image_type['width'],
                                (int)$image_type['height']
                            )) {
                                $this->errors = Tools::displayError('An error occurred while uploading thumbnail image.');
                            } elseif (($infos = getimagesize($tmpName)) && is_array($infos)) {
                                ImageManager::resize(
                                    $tmpName,
                                    _PS_CAT_IMG_DIR_.$id_category.'_'.$name.'.'.$this->imageType,
                                    (int)$infos[0],
                                    (int)$infos[1]
                                );
                            }
                            if (count($this->errors)) {
                                $ret = false;
                            }
                            unlink($tmpName);
                            $ret = true;
                        }
                    }
                }
            }
        }
        return $ret;
    }

}
