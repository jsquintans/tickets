<?php /* Smarty version Smarty-3.1.19, created on 2017-01-16 20:29:27
         compiled from "/var/www/vhosts/vl52555.dns-privadas.es/httpdocs/tickets/themes/leka/modules/blockuserinfo/nav.tpl" */ ?>
<?php /*%%SmartyHeaderCode:655162979587d1f17d5f106-36016811%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd3e5972c5c87d073e04b8c8219f64876139d8592' => 
    array (
      0 => '/var/www/vhosts/vl52555.dns-privadas.es/httpdocs/tickets/themes/leka/modules/blockuserinfo/nav.tpl',
      1 => 1479898060,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '655162979587d1f17d5f106-36016811',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'option_tpl' => 0,
    'is_logged' => 0,
    'link' => 0,
    'cookie' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_587d1f17d6e9a7_40892523',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_587d1f17d6e9a7_40892523')) {function content_587d1f17d6e9a7_40892523($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['option_tpl'] = new Smarty_variable(OvicLayoutControl::getTemplateFile('nav.tpl','blockuserinfo'), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['option_tpl']->value!==null) {?>
    <?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['option_tpl']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php } else { ?>
<!-- Block user information module NAV  -->

    <li>
    	<?php if ($_smarty_tpl->tpl_vars['is_logged']->value) {?> 
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" class="account" rel="nofollow">
                <i class="fa fa-user"></i>&nbsp;
                <?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_firstname;?>
 <?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_lastname;?>

            </a>
    	<?php } else { ?>
    		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Log in to your customer account','mod'=>'blockuserinfo'),$_smarty_tpl);?>
">
    			<i class="fa fa-key"></i>
                <?php echo smartyTranslate(array('s'=>' LOGIN','mod'=>'blockuserinfo'),$_smarty_tpl);?>

    		</a>
    	<?php }?>
    </li>
    <li>
        <?php if ($_smarty_tpl->tpl_vars['is_logged']->value) {?>
            <i class="fa fa-key"></i>
            <a class="logout" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('index',true,null,"mylogout"), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Log me out','mod'=>'blockuserinfo'),$_smarty_tpl);?>
">
    			<?php echo smartyTranslate(array('s'=>' LOGOUT','mod'=>'blockuserinfo'),$_smarty_tpl);?>

    		</a>
        <?php } else { ?>
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
">
                <i class="fa fa-user"></i>
                <?php echo smartyTranslate(array('s'=>' REGISTER','mod'=>'blockuserinfo'),$_smarty_tpl);?>

            </a>    
        <?php }?>
        
    </li>

<!-- /Block usmodule NAV -->
<?php }?><?php }} ?>
