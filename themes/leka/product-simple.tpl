{if isset($products) && $products}
	{foreach from=$products item=product name=products}
        <div class="section-feture-product{if $smarty.foreach.products.iteration%2 == 0}2{else}1{/if}">
        	<div class="container">   
        		<div class="product-feture {if $smarty.foreach.products.iteration%2 == 0}position-right{/if}">
        			<div class="row">
        				<div class="col-sm-6 product-image">
        					<a href="{$product.link|escape:'html':'UTF-8'}"> 
                                <img class="img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" />
        					</a>
        				</div>
        				<div class="col-sm-6 product-info">
        					<h2><a href="{$product.link|escape:'html':'UTF-8'}">{$product.name|escape:'html':'UTF-8'}</a></h2>
        					{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
                                {hook h="displayProductPriceBlock" product=$product type='before_price'}
                                <span class="price-product">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span>
                                {hook h="displayProductPriceBlock" product=$product type="price"}
    							{hook h="displayProductPriceBlock" product=$product type="unit_price"}
    							{hook h="displayProductPriceBlock" product=$product type='after_price'}
                            {/if}
                            {capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
        					{if $smarty.capture.displayProductListReviews} 
        						{hook h='displayProductListReviews' product=$product}
        						{*}
                                <div class="star-rating">
            						<span style="width:100%">
            							<span class="rating">5</span>
            						</span>
            					</div>
                                {*}
        					{/if} 
        					<div class="desc-product">
                                <p>{$product.description_short|strip_tags:'UTF-8'|truncate:360:'...'}</p>
                            </div> 
							{if $product.allow_oosp || $product.quantity > 0}
								{capture}add=1&amp;id_product={$product.id_product|intval}{if isset($product.id_product_attribute) && $product.id_product_attribute}&amp;ipa={$product.id_product_attribute|intval}{/if}{if isset($static_token)}&amp;token={$static_token}{/if}{/capture}
								<a class="ajax_add_to_cart_button leka-button button-add-to-cart" href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" data-rel="nofollow" title="{l s='Add to cart'}" data-id-product-attribute="{$product.id_product_attribute|intval}" data-id-product="{$product.id_product|intval}" data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity >= 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">
									{l s='ADD TO CART'}
								</a>
							{else}
								<a href="javascript:void(0)" class="leka-button button-add-to-cart ajax_add_to_cart_button disabled">
									{l s='ADD TO CART'}
								</a>
							{/if}
        					<div class="product-thumbnails">
                                {foreach from=$product.images item=image name=images} 
	                                <a {if $smarty.foreach.images.iteration%1 == 0}class="selected"{/if} href="{$link->getImageLink($product.link_rewrite, $image, 'large_default')|escape:'html':'UTF-8'}" data-standard="{$link->getImageLink($product.link_rewrite, $image, 'large_default')|escape:'html':'UTF-8'}">
                                        <img src="{$link->getImageLink($product.link_rewrite, $image, 'small_default')|escape:'html':'UTF-8'}" alt="" />
                                    </a>
                                {/foreach}
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
	{/foreach} 
{/if}