{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*} 
{if isset($products) && $products}
	{foreach from=$products item=product name=products} 

	<!--	<div class="product">
			<div class="product-thumb"> 
				<a class="product_img_link" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" >
					<img class="img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} />
				</a>
                <div class="product-button">  
                    {if isset($comparator_max_item) && $comparator_max_item} 
						<a class="button-compare add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}" title="{l s='Add to compare'}">{l s='Compare'}</a>
					{/if}
					{hook h='displayProductListFunctionalButtons' product=$product}
					{*if isset($quick_view) && $quick_view*}
                        <a href="{$product.link|escape:'html':'UTF-8'}" data-rel="{$product.link|escape:'html':'UTF-8'}" class="quick-view button-quickview" title="{l s='Quick view'}">{l s='Quick view'}</a>
                    {*/if*}
				</div>  
                {if isset($product.new) && $product.new == 1}
					{*}<a class="newbox" href="{$product.link|escape:'html':'UTF-8'}">{*}
						<span class="new">{l s='New'}</span>
					{*}</a>{*}
				{/if}
				{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
					{*}<a class="onsalebox" href="{$product.link|escape:'html':'UTF-8'}">{*}
					   <span class="onsale">{l s='Sale!'}</span>
					{*}</a>{*}
				{/if} 
			</div>
			<div class="product-info">
				<h3 >
					{if isset($product.pack_quantity) && $product.pack_quantity}{$product.pack_quantity|intval|cat:' x '}{/if}
					<a href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" >
						{$product.name|truncate:45:'...'|escape:'html':'UTF-8'}
					</a>
				</h3>
				{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
				{if $smarty.capture.displayProductListReviews}
					<div class="hook-reviews">
					{hook h='displayProductListReviews' product=$product}
					</div>
				{/if}
				<p class="product-desc hidden">
					{$product.description_short|strip_tags:'UTF-8'|truncate:360:'...'}
				</p>
				{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
				<div class="content_price">
					{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
						{hook h="displayProductPriceBlock" product=$product type='before_price'}
						<span class="price product-price">
							{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
						</span>
						{if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
							{hook h="displayProductPriceBlock" product=$product type="old_price"}
							<span class="old-price product-price">
								{displayWtPrice p=$product.price_without_reduction}
							</span>
							{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
							{if $product.specific_prices.reduction_type == 'percentage'}
								<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
							{/if}
						{/if}
						{hook h="displayProductPriceBlock" product=$product type="price"}
						{hook h="displayProductPriceBlock" product=$product type="unit_price"}
						{hook h="displayProductPriceBlock" product=$product type='after_price'}
					{/if}
				</div>
				{/if}
				<div class="button-container">
					{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.customizable != 2 && !$PS_CATALOG_MODE}
						{if (!isset($product.customization_required) || !$product.customization_required) && ($product.allow_oosp || $product.quantity > 0)}
							{capture}add=1&amp;id_product={$product.id_product|intval}{if isset($product.id_product_attribute) && $product.id_product_attribute}&amp;ipa={$product.id_product_attribute|intval}{/if}{if isset($static_token)}&amp;token={$static_token}{/if}{/capture}
							<a class="button-add-to-cart ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product-attribute="{$product.id_product_attribute|intval}" data-id-product="{$product.id_product|intval}" data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity >= 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">
								{l s='Add to cart'}
							</a>
						{else}
							<a href="javascript:void(0)" class="button-add-to-cart ajax_add_to_cart_button disabled">
								{l s='Add to cart'}
							</a>
						{/if}
					{/if} 
				</div>
				{if isset($product.color_list)}
					<div class="color-list-container hidden">{$product.color_list}</div>
				{/if}
				<div class="product-flags hidden">
					{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
						{if isset($product.online_only) && $product.online_only}
							<span class="online_only">{l s='Online only'}</span>
						{/if}
					{/if}
					{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
						{elseif isset($product.reduction) && $product.reduction && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
							<span class="discount">{l s='Reduced price!'}</span>
						{/if}
				</div>
				{if (!$PS_CATALOG_MODE && $PS_STOCK_MANAGEMENT && ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
					{if isset($product.available_for_order) && $product.available_for_order && !isset($restricted_country_mode)}
						<span class="availability hidden" >
							{if ($product.allow_oosp || $product.quantity > 0)}
								<span class="{if $product.quantity <= 0 && isset($product.allow_oosp) && !$product.allow_oosp} label-danger{elseif $product.quantity <= 0} label-warning{else} label-success{/if}">
									{if $product.quantity <= 0}{if $product.allow_oosp}{if isset($product.available_later) && $product.available_later}{$product.available_later}{else}{l s='In Stock'}{/if}{else}{l s='Out of stock'}{/if}{else}{if isset($product.available_now) && $product.available_now}{$product.available_now}{else}{l s='In Stock'}{/if}{/if}
								</span>
							{elseif (isset($product.quantity_all_versions) && $product.quantity_all_versions > 0)}
								<span class="label-warning">
									{l s='Product available with different options'}
								</span>
							{else}
								<span class="label-danger">
									{l s='Out of stock'}
								</span>
							{/if}
						</span>
					{/if}
				{/if}
			</div>
		</div> -->

		<div class="product-container" itemscope itemtype="https://schema.org/Product">
		 	<div class='row contenido_evento'>
                <div class='col-md-2 col-sm-2 imgs_equipos'>
                	{if isset($product.id_equipo_local)}
                    <img src="/tickets/img/f/{$product.id_equipo_local}-medium_default.png"   height="45px" /> <span class='span_versus_lista_home'>vs.</span> <img src="/tickets/img/f/{$product.id_equipo_visitante}-medium_default.png" height="45px" />
                    {else}
						{if isset($product.features)}
							{foreach $product.features item=feature}
								{if $feature.name == 'Local'}
									{assign var='local' value=$feature.id_feature_value}
								{/if}
								{if $feature.name == 'Visitante'}
									{assign var='visitante' value=$feature.id_feature_value}
								{/if}

								{if $feature.name == 'Tournament'}
									{assign var='torneo' value=$feature.value}
								{/if}
							{/foreach}
							<img src="/tickets/img/f/{$local}-medium_default.png"   height="45px" /> <span class='span_versus_lista_home'>vs.</span> <img src="/tickets/img/f/{$visitante}-medium_default.png" height="45px" />
						{else}
							No hay features
						{/if}
                    {/if}
                </div>
                <div class='col-md-5 col-sm-5'>
               
                     <h5><a href="{$product.link}">{$product.name}</a></h5> 
                     <span class='nombre_torneo_lista_home'>{if isset($product.torneo_del_encuentro)}
                     											{$product.torneo_del_encuentro}
                     										{else}
                     											{if isset($torneo)}
                     												{$torneo}
                     											{/if}
                     											
                     										{/if} </span> <span class='hora_encuentro_lista_home'> {$product.fecha_encuentro|date_format:"%e/%m/%Y, %a %R"}</span>
                 </div>
                <div class='col-md-3 col-sm-3'>
                    <p class='nombre_estadio_lista_home'>{$product.nombre_estadio}</p>
                    <p class='localiz_estadio_lista_home'><span class='ciudad_estadio_lista_home'>{if isset($product.ciudad)}{$product.ciudad},{/if}</span><span class='pais_estadio_lista_home'> {if isset($product.pais)}{$product.pais}{/if}</span></p>
                </div>
                <div class='col-md-2 col-sm-2'>
                    <a href='{$product.link}' class='btn_comprar_grid'>BUY</a>
                </div>
            </div>
           </div>
	

    {/foreach}
{addJsDefL name=min_item}{l s='Please select at least one product' js=1}{/addJsDefL}
{addJsDefL name=max_item}{l s='You cannot add more than %d product(s) to the product comparison' sprintf=$comparator_max_item js=1}{/addJsDefL}
{addJsDef comparator_max_item=$comparator_max_item}
{addJsDef comparedProductsIds=$compared_products}
{/if}
