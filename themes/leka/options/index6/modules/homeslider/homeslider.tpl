{if $page_name =='index'}
    <!-- Module HomeSlider -->
     
        {if isset($homeslider_slides)}
    		<div class="section-slide">
                <div class="slide-home owl-carousel" data-margin="0" data-items="1" data-nav="true" data-autoplay="true" data-loop="true">
    				{foreach from=$homeslider_slides item=slide}
    					{if $slide.active}
    						<div class="item-slide">
    							<figure>
                                    <a href="{$slide.url|escape:'html':'UTF-8'}" title="{$slide.legend|escape:'html':'UTF-8'}">
        								<img src="{$link->getImageLink($slide.link_rewrite_product, $slide.id_image_product, 'category_default')|escape:'html':'UTF-8'} "{if isset($slide.size) && $slide.size} {$slide.size}{else} width="100%" height="100%"{/if} alt="{$slide.legend|escape:'htmlall':'UTF-8'}" />
        							</a>
                                </figure>				
                                <div class="content-slide"> 
                                        <div class="row">
                                <div class="col-md-3 col-sm-4 lado_datos_slide">
                                    <div class="slide_equipos">
                                        <img src="/tickets/img/f/{$slide.id_local}-medium_default.png" class="equipo_local" alt="celta copy.png" height="105px" /><span class="span_vs">vs.</span><img src="/tickets/img/f/{$slide.id_visitante}-medium_default.png" class="equipo_visitante" alt="liege copy.png"  height="105px"/>
                                    </div>
                                    <div class="texto_slide_equipos">
                                        <span class="vs_slide_texto_equipos">{$slide.name}</span>
                                    </div>
                                    <div class="datos_secudarios">
                                        <span>{$slide.torneo}<br />{$slide.fecha_encuentro}</span>
                                    </div>
                                    <a href="{$link->getProductLink($slide.id_producto)}" class="btn_comprar_slider">BUY</a>
                                </div>
                                    <div class="col-md-9 col-sm-8"></div>
                                    </div>
                                    </div>

    						
    						</div>
    					{/if}
    				{/foreach} 
                </div>
    		</div>
    	{/if}
    <!-- /Module HomeSlider -->
    {/if}