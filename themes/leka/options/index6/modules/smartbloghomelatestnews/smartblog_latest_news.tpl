<div class="section-blog6">
    <h5 class="title-section-bg">{l s='FASHION BLOG' mod='smartbloghomelatestnews'}</h5>
    <div class="latest-slide owl-carousel owl-dots-style1" data-autoplay="true" data-loop="true" data-margin="0" data-items="1" data-nav="false">
        {if isset($view_data) AND !empty($view_data)}
            {assign var='i' value=1}
            {foreach from=$view_data item=post}
                {assign var="options" value=null}
                {$options.id_post = $post.id}
                {$options.slug = $post.link_rewrite}
                <article class="blog-item">
        			<div class="post-format"><figure><img alt="{$post.title}" src="{$modules_dir}smartblog/images/{$post.post_img}-home-default.jpg" /></figure></div>
        			<h3>
                        <a href="{smartblog::GetSmartBlogLink('smartblog')}">{$post.title}</a>
                    </h3>
        			<div class="content-post">{$post.short_description|escape:'htmlall':'UTF-8'}</div>
        			<div class="meta-post">
        				<div class="date-post">{$post.date_added|date_format}</div>
        				<div class="like-post"><a href="#."><i class="fa fa-heart-o"></i>95</a></div>
        				<div class="comment-post"><a href="javascript:void(0)"><i class="fa fa-eye"></i>{$post.viewed}</a></div>
        			</div>
        		</article> 
                {$i=$i+1}
            {/foreach}
        {/if}
    </div>
</div>