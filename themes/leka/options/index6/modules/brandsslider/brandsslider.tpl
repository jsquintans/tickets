{if $manufacturers}  
    <!-- Brands slider module --> 
    <div class="section-shopbrands6">  
    	<div class="title-section style2 text-center">
    		<h2 class="title">{$brand_title}{*{l s='SHOP BY BRANDS' mod='brandsslider'}*}</h2>
        </div>
        <div class="list-brand owl-carousel" data-nav="true" data-dots="false" data-margin="130" data-responsive='{ldelim}"0":{ldelim}"items":3,"margin":20{rdelim},"600":{ldelim}"items":3,"margin":50{rdelim},"1000":{ldelim}"items":4{rdelim}{rdelim}'>
        {foreach from=$manufacturers item=manufacturer name=manufacturer_list}
            <div class="item-brand">
				<a href="{$link->getmanufacturerLink($manufacturer.id_manufacturer, $manufacturer.link_rewrite)|escape:'html'}">
					<div class="logo-brand">
                         <img class="img-responsive" src="{$img_manu_dir}{$manufacturer.image}" alt="{$manufacturer.name}"/>
                    </div>
					<div class="logo-brand-color"><img class="img-responsive" src="{$img_manu_dir}{$manufacturer.image}" alt="{$manufacturer.name}"/></div>
				</a>
			</div>
        {/foreach}
        </div>    
    </div>
    <!-- /Brands slider module -->
{/if}