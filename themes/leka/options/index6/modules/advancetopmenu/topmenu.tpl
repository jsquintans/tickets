{if isset($MENU)}

<div class="col-xs-12 col-sm-10 col-md-10 col-lg-7 main-menu-wapper">
    <a href="#" class="mobile-navigation"><i class="fa fa-bars"></i></a> 
        <nav id="nav_topmenu"> 
            <div id="topmenu" class="main-menu-top">
                <ul class="nav navbar-nav navigation">
                {foreach $MENU item=mainitem name=mainmenu}
                    <li class="level-1{if $mainitem.active == 1 } active{/if}{if isset($mainitem.submenu)} menu-item-has-children dropdown{/if}{if $mainitem.class|count_characters>0} {$mainitem.class}{/if}">
                        <a href="{$mainitem.link}" {if isset($mainitem.submenu)} class="dropdown-toggle" data-toggle="dropdown"{/if}>{if $mainitem.icon|count_characters>0}<i class="{$mainitem.icon}"></i>{/if}{$mainitem.title}</a>
                        {if isset($mainitem.submenu)}
                            {assign var=sub value=$mainitem.submenu}
                                <ul class="{*}sub-menu-top{*} dropdown-menu {if $sub.class|count_characters>0}{$sub.class} {/if}" {if $sub.width}style="width:{$sub.width}px"{/if}>
                                {if isset($sub.blocks) && count($sub.blocks)>0}
                                    {foreach $sub.blocks item=block name=blocks}
                                        {if isset($block.items)}
                                            <li class="block-container col-sm-{$block.width}{if $block.class|count_characters>0} {$block.class}{/if}">
                                                <ul class="block">
                                                {foreach $block.items item=item name=items}
                                                    <li class="level-2 {$item.type}_container {$item.class}">
                                                        {if $item.type=='link'}
                                                            <a href="{$item.link}">{if $item.icon|count_characters>0}<i class="{$item.icon}"></i>{/if}{$item.title}</a>
                                                        {elseif $item.type=='img' && $item.icon|count_characters>0}
                                                            <a class="{$item.class}" href="{$item.link}">
                                                                <img alt="" src="{$absoluteUrl}img/{$item.icon}" class="img-responsive" />
                                                            </a>
                                                        {elseif $item.type=='html'}
                                                            {$item.text}
                                                        {/if}
                                                    </li>
                                                {/foreach}
                                                </ul>
                                            </li>
                                        {/if}
                                    {/foreach}
                                {/if}
                                </ul>
                        {/if}
                    </li>
                {/foreach}
                </ul>
            </div>
        </nav>
    </div>
{/if}