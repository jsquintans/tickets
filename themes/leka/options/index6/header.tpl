<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<html{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}>
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
		{if isset($meta_description) AND $meta_description}
			<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
		{/if}
		{if isset($meta_keywords) AND $meta_keywords}
			<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
		{/if}
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		{if isset($css_files)}
			{foreach from=$css_files key=css_uri item=media}
				<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
			{/foreach}
		{/if}
		{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
			{$js_def}
			{foreach from=$js_files item=js_uri}
			<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
			{/foreach}
		{/if}
        {* Get CSS/JS option *}
        {if isset($current_css)} 
            <link rel="stylesheet" type="text/css" href="{$current_css}" />
        {/if}
        {if isset($current_js)}
            <script type="text/javascript" src="{$current_js}"></script>
        {/if}
		{$HOOK_HEADER}
		<link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Open+Sans:300,600&amp;subset=latin,latin-ext" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" href="http://vl52555.dns-privadas.es/tickets/themes/leka/css/custom.css">
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{$current_dir} {if isset($page_name) && $page_name == 'index'}home {/if}{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{else} show-left-column{/if}{if $hide_right_column} hide-right-column{else} hide-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}">
	
    {if $page_name == 'index' || $page_name == 'product'}{addJsDefL name=min_item}{l s='Please select at least one product' js=1}{/addJsDefL}{/if}
    {if ($page_name == 'index' || $page_name == 'product') and isset($comparator_max_item)}
    {addJsDefL name=max_item}{l s='You cannot add more than %d product(s) to the product comparison' sprintf=$comparator_max_item js=1}{/addJsDefL}
    {addJsDef comparator_max_item=$comparator_max_item}
    {/if}
    {if ($page_name != 'category' && $page_name !='best-sales' && $page_name != 'search' && $page_name != 'manufacturer' && $page_name != 'supplier' && $page_name != 'prices-drop' && $page_name != 'new-products') and isset($compared_products)}{addJsDef comparedProductsIds=$compared_products}{/if}
    
    {if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
			</div>
		{/if}
        
		<div id="page"> 
        
            <header class="header header-style2"> 
            {*}
                <div class="banner">
    				<div class="container">
    					<div class="row">
    						{hook h="displayBanner"}
    					</div>
    				</div>
    			</div>
            {*}
                <div class="top-header">
                    <div class="container">
                        <div class="top-header-menu">
                            {if isset($HOOK_NAV_TOP) && $HOOK_NAV_TOP|trim} 
				                {$HOOK_NAV_TOP} 
                            {/if} 
                        </div>
                        <div class="top-header-right">
                            <ul>
                                {hook h="displayNav"}
                                <li>
                                	<a href="#" class="facebook-link-footer"><i class="fa fa-facebook"></i></a><a href="#" class="twitter-link-footer"><i class="fa fa-twitter"></i></a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="main-header">
                    <div class="container main-header-inner"> 
                        {if isset($BEFORE_LOGO) && $BEFORE_LOGO|trim}
						    {$BEFORE_LOGO} 
                        {/if}
                        <div class="row sticky_content">
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-logo-home"> 
                                <div class="logo" style="opacity: 1;">
                                    <a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
        								<img class="img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
        							</a>
                                </div>
                            </div> 
                            {if isset($HOOK_TOP)}{$HOOK_TOP}{/if} 
                        </div>
<div id="menu_sticky">
							<div class="clearfix">
								<div class="container">
									<div class="sticky_container clearfix">

									</div>
									<div class="vertical-wrap">
									<span class="vertical-navigation"><i class="fa fa-bars"></i></span>
									{if $page_name =='index'}{hook h="displayTopColumn"}{/if}
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>

            </header> 
            <div class='container display_home_1'>
            {if $page_name =='index'}
                {if isset($HOOK_HOME_TOP_COLUMN) && $HOOK_HOME_TOP_COLUMN|trim}
                <!-- Home top column -->
				      {$HOOK_HOME_TOP_COLUMN}
                <!-- End / Home top column -->                          
                {/if}
			{/if}
			</div>
			<div class="columns-container"> 
                {hook h='displayParalaxPage'}
                {if $page_name !='index' && $page_name !='pagenotfound'}
                   {include file="$tpl_dir./breadcrumb.tpl"}
                {/if}
				<div id="columns" class="container"> 	 
					<div class="row {if $page_name !='index'}maincontainer{/if} {if $page_name == 'product'} item-detail-page{/if} {if $page_name == 'contact'} page-contact{/if}">


						{* AÑADIMOS LA  MODFICIACIÓN PARA QUE LA CABECERA DE LA CATEGORÍA SEA A FULL WIDTH *}
						{if isset($category) && $page_name !='product' }
							 {if $scenes || $category->description || $category->id_image}
								{if (isset($display_subcategories) && $display_subcategories eq 1) || !isset($display_subcategories) }
					            <div class="content_scene_cat">

					            	 {if $scenes}
					                 	<div class="content_scene">
					                        <!-- Scenes -->
					                        {include file="$tpl_dir./scenes.tpl" scenes=$scenes}
					                        {if $category->description}
					                            <div class="cat_desc rte">
					                            {if Tools::strlen($category->description) > 350}
					                                <div id="category_description_short">{$description_short}</div>
					                                <div id="category_description_full" class="unvisible">{$category->description}</div>
					                                <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
					                            {else}
					                                <div>{$category->description}</div>
					                            {/if}
					                            </div>
					                        {/if}
					                    </div>
									{else}
										
										<div class='row row-datos-cat'>
						                    <!-- Category image -->
						                    <div class='col-md-3 col-info-categoria'>
						                        <div class='bloque_img_logo_producto'>
						                            <img src='{$link->getCatImageLink($category->link_rewrite, $category->id_image, 'medium_default')|escape:'html':'UTF-8'}'  class='img-responsive' />
						                        </div>
						                        <div class='datos_bloque_categoria'>
						                        	<span>¡Buy your tickets!</span>
						                        	<h3 class='nombre_categoría'>{$category->name|escape:'html':'UTF-8'}</h3>
						                        </div>
						                    </div>
						                    <div class=" col-md-9 content_scene_cat_bg"{if $category->id_image} style="background:url({$link->getCatImageLink($category->link_rewrite, $category->id_image, 'category_default')|escape:'html':'UTF-8'}) right center no-repeat; background-size:cover; min-height:{$categorySize.height}px;"{/if}>
						                        {if $category->description}
						                            <div class="cat_desc">
						                            <span class="category-name">
						                                {strip}
						                                    {$category->name|escape:'html':'UTF-8'}
						                                    {if isset($categoryNameComplement)}
						                                        {$categoryNameComplement|escape:'html':'UTF-8'}
						                                    {/if}
						                                {/strip}
						                            </span>
						                            {if Tools::strlen($category->description) > 350}
						                                <div id="category_description_short" class="rte">{$description_short}</div>
						                                <div id="category_description_full" class="unvisible rte">{$category->description}</div>
						                                <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
						                            {else}
						                                <div class="rte">{$category->description}</div>
						                            {/if}
						                            </div>
						                        {/if}
						                     </div>

						                     
					                     </div>
					                     <div class="row row_sub_slider">
												<div class="toc_1"><img src="./img/Layer 55.png" class="img-toc"><span>Warranty 100%</span></div>
												<div class="toc_2"><img src="./img/Layer 56.png" class="img-toc"><span>Competitive Prices</span></div>
												<div class="toc_3"><img src="./img/Layer 57.png" class="img-toc"><span>Secure Transactions</span></div>
												<div class="toc_4"><img src="./img/Layer 58.png" class="img-toc"><span>Always deliver on time</span></div>
												<div class="toc_5"><img src="./img/Layer 59.png" class="img-toc"><span>Customer Support</span></div>
											 </div>
					                  {/if}
					            </div> 
					            {/if}
					        {/if}
						{/if}

						{* AÑADIMOS LA CABECERA PARA LOS PRODUCTOS *}

						{if  $page_name =='product' }
						<div class="content_scene_cat">
							<div class='row row-datos-cat'>
			                    <!-- Category image -->
			                    <div class='col-sm-3 col-md-3 col-lg-3 col-info-categoria'>
			                        <div class='bloque_img_logo_producto'>
			                        	<div  class='sub_contenedor'>
			                          {*$features|@print_r*}
			                          {foreach from=$features item=feature  }
									  		{if $feature.name == 'Local'}
												<img src="./../img/f/{$feature.id_feature_value}-medium_default.png" height="105px" />
									  		{/if}
									  {/foreach}
									  <span class='span_versus'>vs.</span>

									  {*$features|@print_r*}
			                          {foreach from=$features item=feature  }
									  		{if $feature.name == 'Visitante'}
												<img src="./../img/f/{$feature.id_feature_value}-medium_default.png" height="105px" />
									  		{/if}
									  {/foreach}
										</div>
			                        </div>
			                        <div class='datos_bloque_evento_producto'>
			                        	<h3 class='nombre_categoría'>{$product->name|escape:'html':'UTF-8'}</h3>
			                        	<p class='nombre_competicion'>  
			                        	{foreach from=$features item=feature  }
									  		{if $feature.name == 'Tournament'}
												{$feature.value}
									  		{/if}
									  	{/foreach}
									  </p>
			                        	<p class='fecha_evento_pagina_evento'>{$product->fecha_encuentro|date_format:"%e-%m-%Y  %a %R"}</p>
			                        </div>
			                    </div>
			                    {*$product|@print_r*}
			                    {$image_cover = Product::getCover($product->id)}
			                    <div class="col-sm-9 col-md-9 col-lg-9 content_scene_cat_bg" style="background:url({$link->getImageLink($product->link_rewrite, $image_cover['id_image'], 'category_default')|escape:'html':'UTF-8'}) right center no-repeat; background-size:cover; min-height:320px;">
			                        <div class='datos_estadio'>
			                        	<p class='nombre_estadio'>{$product->nombre_estadio}</p>
			                        	<p class='capacidad_estadio'><span>Capacidad:</span>{$product->capacidad}</p>
			                        </div>
			                     </div>

			                     
		                     </div>
							</div>


						{/if}
				

						{if isset($left_column_size) && !empty($left_column_size)}
						<div id="left_column" class="side-bar column col-xs-12 col-sm-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>
						{/if}
						{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
						<div id="center_column" class="center_column col-xs-12 col-sm-{$cols|intval}">
	{/if}

	{literal}

<script type="text/javascript">

$( function() {
    var dateFormat = "dd-mm-yy",
      from = $( ".fecha_desde_home" )
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          changeYear: false,
          numberOfMonths: 1,
          dateFormat : 'dd-mm-yy',
          minDate : 1
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( ".fecha_hasta_home" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat : 'dd-mm-yy'
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
	
	jQuery(document).ready(function($) {



		var query = $.ajax({
		  type: 'POST',
		  url: baseDir + '/modules/advancedfeaturesvalues/feature_country_city-ajax.php',
		  data: 'valor_feaute=7',
		  dataType: 'json',
		  success: function(json) {
		    $.each(json, function(index, val) {
		    	$('.contenedor-select-pais-home select').append('<option value="'+val.id_feature_value+'">'+val.name+'</option>');
		    });
		  }
		});

		var query = $.ajax({
		  type: 'POST',
		  url: baseDir + '/modules/advancedfeaturesvalues/feature_country_city-ajax.php',
		  data: 'valor_feaute=8',
		  dataType: 'json',
		  success: function(json) {
		    
		    $.each(json, function(index, val) {
		    	$('.contenedor-select-ciudad-home select').append('<option value="'+val.id_feature_value+'">'+val.name+'</option>');
		    }); 
		  }
		});

		$(".contenedor-select-pais-home select").change(function(event) {
			$sseleccion =$(".contenedor-select-pais-home select option:selected").val();
			if($sseleccion != ''){
				var query = $.ajax({
				  type: 'POST',
				  url: baseDir + '/modules/advancedfeaturesvalues/feature_country_city-ajax.php',
				  data: 'sincronizar_ciudades=yes&id_feature_update='+$sseleccion,
				  dataType: 'json',
				  success: function(json) {
				    $('.contenedor-select-ciudad-home select').html('');
				     $.each(json, function(index, val) {

				     	$('.contenedor-select-ciudad-home select').append('<option value="'+val.id_feature_value+'">'+val.value+'</option>');
				     });
				  }
				});
			}else{
				var query = $.ajax({
				  type: 'POST',
				  url: baseDir + '/modules/advancedfeaturesvalues/feature_country_city-ajax.php',
				  data: 'valor_feaute=8',
				  dataType: 'json',
				  success: function(json) {   
				    $.each(json, function(index, val) {
				    	$('.contenedor-select-ciudad-home select').append('<option value="'+val.id_feature_value+'">'+val.name+'</option>');
				    }); 
				  }
				});
			}
		});

		
		$(".blockhtml_CustomHtml6 .filtro input[type='button']").click(function(event) {
			var fecha_desde_home = $('.fecha_desde_home').datepicker().val();
			var fecha_hasta_home = $('.fecha_hasta_home').datepicker().val();

			var country = $('.contenedor-select-pais-home select').val();
			var city = $('.contenedor-select-ciudad-home select').val();
			


			if(!fecha_desde_home  && !fecha_hasta_home && !country && !city){
				console.log("todos los valores vacios");

				// A partir de aquí añadimos los valores 
			}else{
				console.log("alguno de los valores está ok");
				/*
				if(fecha_desde_home || fecha_hasta_home){
					if(fecha_desde_home && fecha_hasta_home){
						
						//aquí añadiria el filtro por fecha //
					}
				}*/
				$(".form_home").submit();
			}
		});

	});





</script>

	{/literal}
	{if (isset($smarty.post['peticion_busqueda_home'])  && ($page_name =='category'))}
		
		{literal}
		<script type="text/javascript">
			
			jQuery(document).ready(function($) {
				console.log("hola");
				if (!ajaxLoaderOn)
				{
					$('.product_list').prepend($('#layered_ajax_loader').html());
					$('.product_list').css('opacity', '0.7');
					ajaxLoaderOn = 1;
				}

				data = $('#layered_form').serialize();
				$('.layered_slider').each( function () {
					var sliderStart = $(this).slider('values', 0);
					var sliderStop = $(this).slider('values', 1);
					if (typeof(sliderStart) == 'number' && typeof(sliderStop) == 'number')
						data += '&'+$(this).attr('id')+'='+sliderStart+'_'+sliderStop;
				});

				$(['price', 'weight']).each(function(it, sliderType)
				{
					if ($('#layered_'+sliderType+'_range_min').length)
					{
						data += '&layered_'+sliderType+'_slider='+$('#layered_'+sliderType+'_range_min').val()+'_'+$('#layered_'+sliderType+'_range_max').val();
					}
				});

				

				if ($('.selectProductSort').length && $('.selectProductSort').val())
				{
					if ($('.selectProductSort').val().search(/orderby=/) > 0)
					{
						// Old ordering working
						var splitData = [
							$('.selectProductSort').val().match(/orderby=(\w*)/)[1],
							$('.selectProductSort').val().match(/orderway=(\w*)/)[1]
						];
					}
					else
					{
						// New working for default theme 1.4 and theme 1.5
						var splitData = $('.selectProductSort').val().split(':');
					}
					data += '&orderby='+splitData[0]+'&orderway='+splitData[1];
				}


				

				// Get nb items per page
				var n = '';
				

						{/literal}
							{if (isset($smarty.post['fechadesde_home']) && $smarty.post['fechadesde_home'] !=''  )&&(  isset($smarty.post['fechahasta_home']) && $smarty.post['fechadesde_home'] != '' )}
						{literal}
							data += '&layered_fechadesde_fecha='+"{/literal}{$smarty.post['fechadesde_home']}{literal}";
							data += '&layered_fechahasta_fecha='+"{/literal}{$smarty.post['fechahasta_home']}{literal}";
						{/literal}
							{/if}
						{literal}

						console.log("he pasado por aquí");

						{/literal}
							{if (isset($smarty.post['pais_home']) && $smarty.post['pais_home'] !=''  )}
						{literal}
									data += '&layered_id_feature_7={/literal}{$smarty.post["pais_home"]}{literal}_7';
						{/literal}
							{/if}
						{literal}
								
						{/literal}
							{if (isset($smarty.post['ciudad_home']) && $smarty.post['ciudad_home'] !=''  )}
						{literal}
								data += '&layered_id_feature_8={/literal}{$smarty.post["ciudad_home"]}{literal}_8';
						{/literal}
							{/if}
						{literal}


				//console.log("hola");
				ajaxQuery = $.ajax(
				{
					type: 'GET',
					url: baseDir + 'modules/advancedfeaturesvalues/blocklayered-ajax.php',
					data: data+"&"+n,
					dataType: 'json',
					cache: false, // @todo see a way to use cache and to add a timestamps parameter to refresh cache each 10 minutes for example
					success: function(result)
					{
						if (result.meta_description != '')
							$('meta[name="description"]').attr('content', result.meta_description);

						if (result.meta_keywords != '')
							$('meta[name="keywords"]').attr('content', result.meta_keywords);

						if (result.meta_title != '')
							$('title').html(result.meta_title);

						if (result.heading != '')
							$('h1.page-heading .cat-name').html(result.heading);

						$('#layered_block_left').replaceWith(utf8_decode(result.filtersBlock));
						$('.category-product-count, .heading-counter').html(result.categoryCount);

						if (result.nbRenderedProducts == result.nbAskedProducts)
							$('div.clearfix.selector1').hide();

						if (result.productList)
							$('.product_list').replaceWith(utf8_decode(result.productList));
						else
							$('.product_list').html('');

						$('.product_list').css('opacity', '1');
						if ($.browser.msie) // Fix bug with IE8 and aliasing
							$('.product_list').css('filter', '');

						if (result.pagination.search(/[^\s]/) >= 0) {
							var pagination = $('<div/>').html(result.pagination)
							var pagination_bottom = $('<div/>').html(result.pagination_bottom);

							if ($('<div/>').html(pagination).find('#pagination').length)
							{
								$('#pagination').show();
								$('#pagination').replaceWith(pagination.find('#pagination'));
							}
							else
							{
								$('#pagination').hide();
							}

							if ($('<div/>').html(pagination_bottom).find('#pagination_bottom').length)
							{
								$('#pagination_bottom').show();
								$('#pagination_bottom').replaceWith(pagination_bottom.find('#pagination_bottom'));
							}
							else
							{
								$('#pagination_bottom').hide();
							}
						}
						else
						{
							$('#pagination').hide();
							$('#pagination_bottom').hide();
						}

						paginationButton(result.nbRenderedProducts, result.nbAskedProducts);
						ajaxLoaderOn = 0;

						// On submiting nb items form, relaod with the good nb of items
						$('div.pagination form').on('submit', function(e)
						{
							e.preventDefault();
							val = $('div.pagination select[name=n]').val();
						
							$('div.pagination select[name=n]').children().each(function(it, option) {
								if (option.value == val)
									$(option).attr('selected', true);
								else
									$(option).removeAttr('selected');
							});

							// Reload products and pagination
							reloadContent();
						});
						if (typeof(ajaxCart) != "undefined")
							ajaxCart.overrideButtonsInThePage();

						if (typeof(reloadProductComparison) == 'function')
							reloadProductComparison();

						filters = result.filters;
						initFilters();
						initSliders();

						current_friendly_url = result.current_friendly_url;

						// Currente page url
						if (typeof(current_friendly_url) === 'undefined')
							current_friendly_url = '#';

						// Get all sliders value
						$(['price', 'weight']).each(function(it, sliderType)
						{
							if ($('#layered_'+sliderType+'_slider').length)
							{
								// Check if slider is enable & if slider is used
								if(typeof($('#layered_'+sliderType+'_slider').slider('values', 0)) != 'object')
								{
									if ($('#layered_'+sliderType+'_slider').slider('values', 0) != $('#layered_'+sliderType+'_slider').slider('option' , 'min')
									|| $('#layered_'+sliderType+'_slider').slider('values', 1) != $('#layered_'+sliderType+'_slider').slider('option' , 'max'))
										current_friendly_url += '/'+blocklayeredSliderName[sliderType]+'-'+$('#layered_'+sliderType+'_slider').slider('values', 0)+'-'+$('#layered_'+sliderType+'_slider').slider('values', 1)
								}
							}
							else if ($('#layered_'+sliderType+'_range_min').length)
							{
								current_friendly_url += '/'+blocklayeredSliderName[sliderType]+'-'+$('#layered_'+sliderType+'_range_min').val()+'-'+$('#layered_'+sliderType+'_range_max').val();
							}
						});

						window.location.href = current_friendly_url;

						if (current_friendly_url != '#/show-all')
							$('div.clearfix.selector1').show();
						
						lockLocationChecking = true;

						if(slideUp)
							$.scrollTo('.product_list', 400);
						updateProductUrl();

						$('.hide-action').each(function() {
							hideFilterValueAction(this);
						});

						if (display instanceof Function) {
							var view = $.totalStorage('display');

							if (view && view != 'grid')
								display(view);
						}
					}
				});
				ajaxQueries.push(ajaxQuery);


			});
		


		</script>
		{/literal}
	{else}
		{literal}
			<script type="text/javascript">
				//alert("hola");
			</script>
		{/literal}
	{/if}