{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="side-bar col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
                    {if $page_name =='index'}
                        {if isset($HOME_BOTTOM_COLUMN) && $HOME_BOTTOM_COLUMN|trim}
        				      <div id="home_bottom_column" class="home_bottom_column">{$HOME_BOTTOM_COLUMN}</div>
                        {/if}
        			{/if}
				</div><!-- #columns --> 
			</div><!-- .columns-container -->
        </div><!-- .main-container -->            
            
            {if isset($BOTTOM_COLUMN) && $BOTTOM_COLUMN|trim}
			      <div id="bottom_column" class="bottom_column">{$BOTTOM_COLUMN}</div>
            {/if}
			{*}
            {if isset($HOOK_FOOTER)} 
				<footer class="footer">
                    <div class="container"> {$HOOK_FOOTER}</div>
				</footer>  
			{/if}
            {*}
		</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
	</body>
</html>
