    <!-- Block currencies module -->
    {if count($currencies) > 1}  
        <div id="block-currencies" class="dropdown navbox-index4"> 
        	<form id="setCurrency" action="{$request_uri}" method="post">
        		<div class="current hidden">
        			<input type="hidden" name="id_currency" id="id_currency" value=""/>
        			<input type="hidden" name="SubmitCurrency" value="" /> 
        		</div>
                {foreach from=$currencies key=k item=f_currency}
    				{if $cookie->id_currency == $f_currency.id_currency}
                        <a class="current_currency" href="javascript:void(0)" data-toggle="dropdown">{$f_currency.iso_code}</a>
                    {/if}
    			{/foreach}
        		<ul id="first-currencies" class="dropdown-menu top-header-dropdown">
        			{foreach from=$currencies key=k item=f_currency}
        				{if strpos($f_currency.name, '('|cat:$f_currency.iso_code:')') === false}
        					{assign var="currency_name" value={l s='%s (%s)' sprintf=[$f_currency.name, $f_currency.iso_code]}}
        				{else}
        					{assign var="currency_name" value=$f_currency.name}
        				{/if}
        				<li >
        					<a href="javascript:setCurrency({$f_currency.id_currency});" {if $cookie->id_currency == $f_currency.id_currency}class="current"{/if} rel="nofollow" title="{$currency_name}"> 
                                {$f_currency.iso_code}
        					</a>
        				</li>
        			{/foreach}
        		</ul>
        	</form>  
        </div> 
    {/if} 