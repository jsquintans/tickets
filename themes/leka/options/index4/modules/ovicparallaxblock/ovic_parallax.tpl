<div class="parallax-container">
    <div class="parallax parallax_index4" style="background-image: url({$link->getMediaLink("`$smarty.const._MODULE_DIR_`ovicparallaxblock/img/`$background|escape:'htmlall':'UTF-8'`")})" data-stellar-background-ratio="{$ratio}">
        {$content|html_entity_decode}
    </div>
</div>