<div class="section-tabslide simplecategory-tabs {$simplecategory_item.custom_class}">
    {if $simplecategory_item.display_name == 1}
        <div class="title-section text-center">
          <h2 class="title">{$simplecategory_item.name}</h2>
          {if isset($simplecategory_item.description) && $simplecategory_item.description}
             {$simplecategory_item.description|html_entity_decode}
          {/if} 
        </div>
    {/if} 
	<div class="tab-slide-category">
		<ul class="products-tab nav nav-tabs" role="tablist">
			{foreach from=$simplecategory_item.groups item=group name=groups}
				{if $smarty.foreach.groups.first}
					<li role="presentation" class="active"><a href="#tab_{$group.category_id}_{$group.id}" role="tab" data-toggle="tab">{$group.name}</a></li>
				{else}
					<li role="presentation"><a href="#tab_{$group.category_id}_{$group.id}" role="tab" data-toggle="tab">{$group.name}</a></li>
				{/if}
			{/foreach}
		</ul>
		<div class="tab-content">
            {foreach from=$simplecategory_item.groups item=group name=groups}
                <div role="tabpanel" class="tab-pane fade" id="tab_{$group.category_id}_{$group.id}"> 
					{if isset($group.products) && $group.products|@count} 
                        <div class="product-slide owl-carousel product-slide-style2 owl-dots-style1" data-responsive='{ldelim}"0":{ldelim}"items":1{rdelim},"600":{ldelim}"items":3{rdelim},"1000":{ldelim}"items":4{rdelim},"1366":{ldelim}"items":5{rdelim}{rdelim}' data-margin="0" data-items="5" data-nav="false" data-Dots="true">
                            {foreach from=$group.products item=product name=products}
                            <div class="product">
    							<div  class="product-thumb">
    								<a class="product_img_link" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" >
                    					<img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} />
                    				</a>
    								<div class="product-hover">
    									<div class="product-info">
    										<h3 >
                            					{if isset($product.pack_quantity) && $product.pack_quantity}{$product.pack_quantity|intval|cat:' x '}{/if}
                            					<a href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" >
                            						{$product.name|truncate:45:'...'|escape:'html':'UTF-8'}
                            					</a>
                            				</h3> 
                                            {if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
                                            <div class="content_price">
                            					{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
                            						{hook h="displayProductPriceBlock" product=$product type='before_price'}
                            						<span class="price product-price">
                            							{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
                            						</span>
                            						{if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
                            							{hook h="displayProductPriceBlock" product=$product type="old_price"}
                            							<span class="old-price product-price">
                            								{displayWtPrice p=$product.price_without_reduction}
                            							</span>
                            							{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
                            							{if $product.specific_prices.reduction_type == 'percentage'}
                            								<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
                            							{/if}
                            						{/if}
                            						{hook h="displayProductPriceBlock" product=$product type="price"}
                            						{hook h="displayProductPriceBlock" product=$product type="unit_price"}
                            						{hook h="displayProductPriceBlock" product=$product type='after_price'}
                            					{/if}
                            				</div>
                                            {/if}
    									</div>
    									<div class="product-button">  
                                            {if isset($comparator_max_item) && $comparator_max_item} 
                        						<a class="button-compare add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}" title="{l s='Add to compare' mod='simplecategory'}">{l s='Compare' mod='simplecategory'}</a>
                        					{/if}
                        					{hook h='displayProductListFunctionalButtons' product=$product}
                        					{if isset($quick_view) && $quick_view}
                                                <a href="{$product.link|escape:'html':'UTF-8'}" data-rel="{$product.link|escape:'html':'UTF-8'}" class="quick-view button-quickview" title="{l s='Quick view' mod='simplecategory'}">{l s='Quick view' mod='simplecategory'}</a>
                                            {/if}
                        				</div> 
    									<div class="button-container">
                        					{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.customizable != 2 && !$PS_CATALOG_MODE}
                        						{if (!isset($product.customization_required) || !$product.customization_required) && ($product.allow_oosp || $product.quantity > 0)}
                        							{capture}add=1&amp;id_product={$product.id_product|intval}{if isset($product.id_product_attribute) && $product.id_product_attribute}&amp;ipa={$product.id_product_attribute|intval}{/if}{if isset($static_token)}&amp;token={$static_token}{/if}{/capture}
                        							<a class="button-add-to-cart ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product-attribute="{$product.id_product_attribute|intval}" data-id-product="{$product.id_product|intval}" data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity >= 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">
                        								{l s='Add to cart'}
                        							</a>
                        						{else}
                        							<a href="javascript:void(0)" class="button-add-to-cart ajax_add_to_cart_button disabled">
                        								{l s='Add to cart'}
                        							</a>
                        						{/if}
                        					{/if} 
                        				</div>
    								</div>
    							</div>
    						</div>
                            {/foreach}
                        </div>
					{else}
						<div>{l s='Sorry! There are no products' mod='simplecategory'}</div>
					{/if} 
				</div>
			{/foreach} 
		</div>
	</div>
</div>