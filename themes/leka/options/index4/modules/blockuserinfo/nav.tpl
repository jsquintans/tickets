<div class="navbox-index4 blockuserinfo">
	{if $is_logged} 
        <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow">
            <i class="fa fa-user"></i>&nbsp;
            {$cookie->customer_firstname} {$cookie->customer_lastname}
        </a>
	{else}
		<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
			<i class="fa fa-key"></i>
            {l s=' LOGIN' mod='blockuserinfo'}
		</a>
	{/if}
</div> 
