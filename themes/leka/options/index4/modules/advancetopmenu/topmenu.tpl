{if isset($MENU)}
    <div class="main-menu-wapper">
    	<nav class="main-menu-top" id="main-menu"> 
            <ul> 
            {foreach $MENU item=mainitem name=mainmenu} 
                <li class="level-1{if $mainitem.active == 1 } active{/if}{if isset($mainitem.submenu)} dropdown{/if}{if $mainitem.class|count_characters>0} {$mainitem.class}{/if}">
                    <a href="{$mainitem.link}">{$mainitem.title}</a> 
                    {if isset($mainitem.submenu)}
                        {assign var=sub value=$mainitem.submenu}
                        <ul class="dropdown-menu {if $sub.class|count_characters>0}{$sub.class} {/if}">
                        {if isset($sub.blocks) && count($sub.blocks)>0}
                            {foreach $sub.blocks item=block name=blocks} 
                                {if isset($block.items)}  
                                    {foreach $block.items item=item name=items}
                                        <li class="level-2 {$item.type}_container {$item.class}"> 
                                            {if $item.type=='link'}
                                                <a href="{$item.link}">{if $item.icon|count_characters>0}<i class="{$item.icon}"></i>{/if}{$item.title}</a>
                                            {/if} 
                                        </li>
                                    {/foreach} 
                                {/if}
                            {/foreach}
                        {/if}
                        </ul>
                    {/if}
                </li> 
            {/foreach} 
            </ul> 
        </nav>
    </div>
{/if}