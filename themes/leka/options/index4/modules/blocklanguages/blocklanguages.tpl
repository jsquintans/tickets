<!-- Block languages module -->
{if count($languages) > 1} 
   <div id="languages-block-top" class="dropdown language navbox-index4">
    	{foreach from=$languages key=k item=language name="languages"}
			{if $language.iso_code == $lang_iso} 
				<a  data-toggle="dropdown" href="javascript:void(0)">
                    <img src="{$img_lang_dir}{$language.id_lang}.jpg" alt="" />{$language.name|regex_replace:"/\s\(.*\)$/":""}</a>
			{/if}
		{/foreach}
		<ul id="first-languages" class="dropdown-menu top-header-dropdown">
			{foreach from=$languages key=k item=language name="languages"}
				<li>
				{if $language.iso_code != $lang_iso}
					{assign var=indice_lang value=$language.id_lang}
					{if isset($lang_rewrite_urls.$indice_lang)}
						<a class="" href="{$lang_rewrite_urls.$indice_lang|escape:'html':'UTF-8'}" title="{$language.name|escape:'html':'UTF-8'}" rel="alternate" hreflang="{$language.iso_code|escape:'html':'UTF-8'}">
					{else}
						<a href="{$link->getLanguageLink($language.id_lang)|escape:'html':'UTF-8'}" title="{$language.name|escape:'html':'UTF-8'}" rel="alternate" hreflang="{$language.iso_code|escape:'html':'UTF-8'}">
					{/if} 
                    {$language.name|regex_replace:"/\s\(.*\)$/":""}<img src="{$img_lang_dir}{$language.id_lang}.jpg" alt="" /></a>
                {else}  
                    <a {if $language.iso_code == $lang_iso}class="current"{/if} href="javascript:void(0)">{$language.name|regex_replace:"/\s\(.*\)$/":""}</a>
				{/if} 
					
				</li>
			{/foreach}
		</ul>
    </div> 
{/if}
<!-- /Block languages module -->