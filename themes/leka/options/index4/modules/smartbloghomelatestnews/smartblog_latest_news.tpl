<div class="section-latestblog2"> 
	<div class="title-section text-center">
		<h2 class="title">{l s='LATEST BLOG' mod='smartbloghomelatestnews'}</h2>
	</div> 
    <div class="row latest">
        {if isset($view_data) AND !empty($view_data)}
            {assign var='i' value=1}
            {foreach from=$view_data item=post}
                {assign var="options" value=null}
                {$options.id_post = $post.id}
                {$options.slug = $post.link_rewrite}
                <article class="col-sm-4 blog-item">
                    <div class="post-format">
                        <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}"><figure><img alt="{$post.title}" src="{$modules_dir}smartblog/images/{$post.post_img}-home-default.jpg" /></figure></a>
                    </div> 
                    <h3><a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}">{$post.title}</a></h3>
                    <span></span>
                    <div class="content-post">{$post.short_description|escape:'htmlall':'UTF-8'|truncate:130:'...'|escape:'html':'UTF-8'}</div>
                    <div class="meta-post">
						<div class="date-post">{$post.date_added|date_format}</div>
						<div class="like-post"><i class="fa fa-heart-o"></i></div>
						<div class="comment-post"><a href="javascript:void(0)"><i class="fa fa-eye"></i>{$post.viewed}</a></div>
					</div>
                    
                </article>
                {$i=$i+1}
            {/foreach}
        {/if}
     </div> 
</div> 