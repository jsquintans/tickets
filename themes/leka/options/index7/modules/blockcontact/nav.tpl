<div class="col-sm-12 col-md-7">
    <div class="footer-menu">
        <a href="javascript:void(0)"><i class="fa fa-phone"></i>{if $telnumber}{$telnumber}{/if}</a> 
        <a href="javascript:void(0)"><i class="fa fa-clock-o"></i> {l s='MON - SAT: 08 am - 17 pm' mod='blockcontact'}</a>
        <a href="{if $email != ''}mailto:{$email}{else}#{/if}"><i class="fa fa-envelope-o"></i> {if $email != ''}{$email}{/if}</a>
    </div>
</div>