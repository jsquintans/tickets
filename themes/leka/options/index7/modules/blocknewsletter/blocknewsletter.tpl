<div class="col-sm-12 col-md-4" style="clear:left;">
    <div id="newsletter_block" class="widget widget-newaletter"> 
    	<form action="{$link->getPageLink('index', null, null, null, false, null, true)|escape:'html':'UTF-8'}" method="post">
    		<div class="form-group{if isset($msg) && $msg } {if $nw_error}form-error{else}form-ok{/if}{/if}" >
    			{*}<input class="inputNew form-control newaletter-email newsletter-input" id="newsletter-input" placeholder="{l s='EMAIL FOR NEWSLETTER' mod='blocknewsletter'}" type="text" name="email" size="18" value="{if isset($msg) && $msg}{$msg}{elseif isset($value) && $value}{$value}{else}{l s='EMAIL FOR NEWSLETTER' mod='blocknewsletter'}{/if}" />{*}
                <input class="inputNew form-control newaletter-email newsletter-input" id="newsletter-input" placeholder="{l s='EMAIL FOR NEWSLETTER' mod='blocknewsletter'}" type="text" name="email" size="18" value="" />
                <span>
                    <button type="submit" name="submitNewsletter" class="newaletter-button">
                        <i class="fa fa-envelope-o"></i>
                    </button>
                </span>
    			<input type="hidden" name="action" value="0" />
    		</div>
    	</form> 
        {hook h="displayBlockNewsletterBottom" from='blocknewsletter'}
    </div>
</div>