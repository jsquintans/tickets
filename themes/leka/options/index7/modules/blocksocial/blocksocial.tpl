<div class="col-sm-12 col-md-5">
    <div class="widget widget-social">
        <div id="social_block" class="list-social"> 
        	<ul>
        		{if isset($facebook_url) && $facebook_url != ''}
        			<li class="facebook">
        				<a class="_blank" href="{$facebook_url|escape:html:'UTF-8'}">
        					<i class="fa fa-facebook"></i>
        				</a>
        			</li>
        		{/if}
        		{if isset($twitter_url) && $twitter_url != ''}
        			<li class="twitter">
        				<a class="_blank" href="{$twitter_url|escape:html:'UTF-8'}">
        					<i class="fa fa-twitter"></i>
        				</a>
        			</li>
        		{/if}
        		{if isset($rss_url) && $rss_url != ''}
        			<li class="rss">
        				<a class="_blank" href="{$rss_url|escape:html:'UTF-8'}">
        					<i class="fa fa-rss"></i>
        				</a>
        			</li>
        		{/if}
                {if isset($youtube_url) && $youtube_url != ''}
                	<li class="youtube">
                		<a class="_blank" href="{$youtube_url|escape:html:'UTF-8'}">
                			<i class="fa fa-youtube"></i>
                		</a>
                	</li>
                {/if}
                {if isset($google_plus_url) && $google_plus_url != ''}
                	<li class="google-plus">
                		<a class="_blank" href="{$google_plus_url|escape:html:'UTF-8'}">
                			<i class="fa fa-google-plus"></i>
                		</a>
                	</li>
                {/if}
                {if isset($pinterest_url) && $pinterest_url != ''}
                	<li class="pinterest">
                		<a class="_blank" href="{$pinterest_url|escape:html:'UTF-8'}">
                			<i class="fa fa-pinterest-p"></i>
                		</a>
                	</li>
                {/if}
                {if isset($vimeo_url) && $vimeo_url != ''}
                	<li class="vimeo">
                		<a class="_blank" href="{$vimeo_url|escape:html:'UTF-8'}">
                			<i class="fa fa-vimeo"></i>
                		</a>
                	</li>
                {/if}
                {if isset($instagram_url) && $instagram_url != ''}
                	<li class="instagram">
                		<a class="_blank" href="{$instagram_url|escape:html:'UTF-8'}">
                			<i class="fa fa-instagram"></i>
                		</a>
                	</li>
                {/if}
        	</ul> 
        </div>
    </div>
</div>