{if $manufacturers}  
<!-- Brands slider module --> 
<div class="section-shopbrand section-shopbrand3 bg-parallax div_full_width" style="background-position: 50% -553px;">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
				{$brand_desc}
				{*<div class="text-shopbrand">
					<p>{l s='Best Collections from'}</p>
					<h2>{l s='AROUND THE WORLD'}</h2>
					<p class="crimtext">{l s='Leka - The Amazing Online Store'}</p>
				</div> *}
                <div class="list-brand owl-carousel owl-theme owl-loaded" data-nav="true" data-dots="false" data-margin="130" data-responsive='{ldelim}"0":{ldelim}"items":3,"margin":20{rdelim},"600":{ldelim}"items":3,"margin":50{rdelim},"1000":{ldelim}"items":4{rdelim}{rdelim}'>
                {foreach from=$manufacturers item=manufacturer name=manufacturer_list}
                    <div class="item-brand">
    					<a href="{$link->getmanufacturerLink($manufacturer.id_manufacturer, $manufacturer.link_rewrite)|escape:'html'}">
    						<div class="logo-brand">
                                 <img class="img-responsive" src="{$img_manu_dir}{$manufacturer.image}" alt="{$manufacturer.name}"/>
                            </div>
    						<div class="logo-brand-color"><img class="img-responsive" src="{$img_manu_dir}{$manufacturer.image}" alt="{$manufacturer.name}"/></div>
    					</a>
    				</div>
                {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Brands slider module -->
{/if}