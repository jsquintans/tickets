<!-- Blog -->
<section class="section-fashion-blog">
	<div class="container">
		<div class="title-section text-center">
			<h2 class="title"><a href="{smartblog::GetSmartBlogLink('smartblog')}">{l s='FASHION BLOG' mod='smartbloghomelatestnews'}</a></h2>
		</div>
		<div class="blog-style2 owl-carousel owl-dots-style1" data-responsive='{ldelim}"0":{ldelim}"items":1{rdelim},"767":{ldelim}"items":1{rdelim},"1000":{ldelim}"items":2{rdelim}{rdelim}' data-auto="true" data-loop="true" data-items="2" data-margin="30">
        {if isset($view_data) AND !empty($view_data)}
            {assign var='i' value=1}
            {foreach from=$view_data item=post}
                {assign var="options" value=null}
                {$options.id_post = $post.id}
                {$options.slug = $post.link_rewrite}
        			<article class="post-item">
        				<div class="post-thumb">
        					<a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}"><img alt="{$post.title}" src="{$modules_dir}smartblog/images/{$post.post_img}-home-default.jpg" /></a>
        					<span class="post-date">
        						<span class="month-day">{$post.date_added|date_format:"%B %e"}</span>
        						<span class="year">{$post.date_added|date_format:"%Y"}</span>
        					</span>
        				</div>
        				<div class="post-info">
        					<h5><a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}">{$post.title}</a></h5>
        					<div class="post-excerpt">
        						<p>{$post.short_description|escape:'htmlall':'UTF-8'|truncate:100:'...'}</p> 
        					</div>
        					<div class="post-meta">
        						<span class="comment-count"><i class="fa fa-eye"></i>{$post.viewed}</span>
        						<span class="like"><i class="fa fa-heart-o"></i> 5</span>
        					</div>
        				</div>
        			</article> 
                    {$i=$i+1}
                {/foreach}
            {/if}
		</div>
	</div>
</section>
<!-- ./Blog -->