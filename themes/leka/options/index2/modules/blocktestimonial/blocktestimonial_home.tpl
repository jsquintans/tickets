{if $testimonials|@count > 0}
<!-- MODULE Block Testimonial -->
<section class="section-testimonial">
	<div class="container">
		<div class="title-section text-center">
            {if isset($TESTIMONIAL_TITLE) && $TESTIMONIAL_TITLE != ''}
                <h2 class="title">{$TESTIMONIAL_TITLE}</h2>
            {else}
                <h2>{l s='TESTIMONIAL' mod='blocktestimonial'}</h2>
            {/if}
		</div>
		<div class="row">
            {foreach from=$testimonials item=info}
    			<div class="col-sm-6">
    				<div class="testimonial">
    					<div class="client-avatar">
    						<img src="{$modules_dir}blocktestimonial/img/{$info.file_name}" alt="" title="" />
    					</div>
    					<div class="info-testimonial">
    						<div class="client-quote">
    							<p>“{$info.text|escape:html:'UTF-8'|truncate:150:'...'}”</p>
    						</div>
    						<div class="client-info">
    							<span class="client-name">{$info.name|escape:html:'UTF-8'}</span>
    							<span class="client-position">{$info.company|escape:html:'UTF-8'}</span>
    						</div>
    					</div>
    				</div>
    			</div>
            {/foreach}
		</div>
	</div>
</section>
 <!-- /MODULE Block Testimonial -->
{/if}