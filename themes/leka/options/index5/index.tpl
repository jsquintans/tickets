{if isset($HOME_TOP_CONTENT) && $HOME_TOP_CONTENT|trim}
    <div id="HOME_TOP_CONTENT">{$HOME_TOP_CONTENT}</div>
{/if}
{if isset($HOOK_HOME_TAB_CONTENT) && $HOOK_HOME_TAB_CONTENT|trim}
    {if isset($HOOK_HOME_TAB) && $HOOK_HOME_TAB|trim}
        <ul id="home-page-tabs" class="nav nav-tabs clearfix">
			{$HOOK_HOME_TAB}
		</ul>
	{/if}
	<div class="tab-content">{$HOOK_HOME_TAB_CONTENT}</div>
{/if}
{if isset($HOOK_HOME) && $HOOK_HOME|trim}
	<div id="HOME_CONTENT" class="clearfix">{$HOOK_HOME}</div>
{/if}    
{if isset($HOME_BOTTOM_CONTENT) && $HOME_BOTTOM_CONTENT|trim}
    <div id="HOME_BOTTOM_CONTENT" class="div_full_width section-shopbrand2">
        <div class="row">
            {$HOME_BOTTOM_CONTENT}
        </div>
    </div>
{/if}