<div class="col-md-6">
    <div class="shopbrand2">
        <div class="title-section">
			<h3 class="title">{$brand_title}{*{l s='SHOP BRANDS' mod='brandsslider'}*}</h3>
		</div>
        {$brand_desc}
        {*<p>
            {l s='Contrary to popular belief, Lorem Ipsum is not simply random text. ' mod='brandsslider'}
            {l s='It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. '}
            {l s='Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia ' mod='brandsslider'}
        </p>*}
        <div class="list-brand owl-carousel" 
            data-nav="true" 
            data-dots="false" 
            data-margin="130" 
            data-responsive='{ldelim}"0":{ldelim}"items":3,"margin":20{rdelim},"600":{ldelim}"items":3,"margin":50{rdelim},"1000":{ldelim}"items":3,"margin":100{rdelim},"1500":{ldelim}"items":4{rdelim}{rdelim}'>
            {foreach from=$manufacturers item=manufacturer name=manufacturer_list}
                <div class="item-brand">
					<a href="{$link->getmanufacturerLink($manufacturer.id_manufacturer, $manufacturer.link_rewrite)|escape:'html'}">
						<div class="logo-brand">
                             <img class="img-responsive" src="{$img_manu_dir}{$manufacturer.image}" alt="{$manufacturer.name}"/>
                        </div>
						<div class="logo-brand-color"><img class="img-responsive" src="{$img_manu_dir}{$manufacturer.image}" alt="{$manufacturer.name}"/></div>
					</a>
				</div>
            {/foreach}
		</div>
    </div>
</div>