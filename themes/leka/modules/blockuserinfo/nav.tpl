{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('nav.tpl','blockuserinfo')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
<!-- Block user information module NAV  -->
{*}<ul>{*}
    <li>
    	{if $is_logged} 
            <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow">
                <i class="fa fa-user"></i>&nbsp;
                {$cookie->customer_firstname} {$cookie->customer_lastname}
            </a>
    	{else}
    		<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
    			<i class="fa fa-key"></i>
                {l s=' LOGIN' mod='blockuserinfo'}
    		</a>
    	{/if}
    </li>
    <li>
        {if $is_logged}
            <i class="fa fa-key"></i>
            <a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
    			{l s=' LOGOUT' mod='blockuserinfo'}
    		</a>
        {else}
            <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
                <i class="fa fa-user"></i>
                {l s=' REGISTER' mod='blockuserinfo'}
            </a>    
        {/if}
        
    </li>
{*</ul>{*}
<!-- /Block usmodule NAV -->
{/if}