{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('blocksaleproducts_left.tpl', 'ovicsaleproducts')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <div id="sale-products_block_left" class="block products_block">
        <div class="block-title">
          <strong>{l s='Sale products' mod='ovicsaleproducts'}</strong>
        </div>
    	<div class="block_content products_content products-slider-sidebar ">
    	{if $sale_products !== false}
    		<ul class="product-item">
                {foreach from=$sale_products item=saleproduct name=myLoop}
                    <li itemscope itemtype="http://schema.org/Product" class="clearfix">
                        <div class="row">
                            <div class="col-xs-4 col-side-product">
                              <div class="product-image-wrapper">
                                 <div class="product-image">
                                    <a href="{$saleproduct.link|escape:'html'}" title="{if !empty($saleproduct.legend)}{$saleproduct.legend|escape:'html':'UTF-8'}{else}{$saleproduct.name|escape:'html':'UTF-8'}{/if}">
                                    <img src="{$link->getImageLink($saleproduct.link_rewrite, $saleproduct.id_image, 'home_default')|escape:'html'}" alt="{$saleproduct.name|escape:html:'UTF-8'}"/>
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <div class="product-description col-xs-8">
                              <h4 class="product-name"><a href="{$saleproduct.link|escape:'html'}" title="{$saleproduct.name|escape:html:'UTF-8'}">{$saleproduct.name|strip_tags|escape:html:'UTF-8'}</a></h4>
                              {hook h='displayProductListReviews' product=$saleproduct}
                              {if (!$PS_CATALOG_MODE AND ((isset($saleproduct.show_price) && $saleproduct.show_price) || (isset($saleproduct.available_for_order) && $saleproduct.available_for_order)))}
                                	{if isset($saleproduct.show_price) && $saleproduct.show_price && !isset($restricted_country_mode)}
                                      <div class="price-box">
                                         <span class="regular-price">
                                            <span class="price">{if !$priceDisplay}{convertPrice price=$saleproduct.price}{else}{convertPrice price=$saleproduct.price_tax_exc}{/if}</span>                                   
                                         </span>
                                      </div>
                                    {/if}
                              {/if}
                           </div>
                        </div>
                    </li>
                {/foreach}
            </ul>
    	{else}
    		<p>&raquo; {l s='Do not allow sale products at this time.' mod='ovicsaleproducts'}</p>
    	{/if}
    	</div>
    </div>  
{/if}