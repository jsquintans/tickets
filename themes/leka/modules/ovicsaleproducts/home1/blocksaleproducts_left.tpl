<div id="sale-products-left" class="col-sm-3 col-xs-12 product-block-bottom">
    <h3 class="title_block">
        <span>{l s='Sale products' mod='ovicsaleproducts'}</span>
    </h3>
	<div class="block_content products-block ">
	{if $sale_products !== false}
		<ul class="products">
            {foreach from=$sale_products item=saleproduct name=myLoop}
                <li itemscope itemtype="http://schema.org/Product" class="clearfix">
                    <a class="products-block-image" href="{$saleproduct.link|escape:'html'}" title="{if !empty($saleproduct.legend)}{$saleproduct.legend|escape:'html':'UTF-8'}{else}{$saleproduct.name|escape:'html':'UTF-8'}{/if}"><img src="{$link->getImageLink($saleproduct.link_rewrite, $saleproduct.id_image, 'small_default')|escape:'html'}" alt="{$saleproduct.name|escape:html:'UTF-8'}"/></a>
                    <div class="product-content">
                          <h5 class="product-name"><a href="{$saleproduct.link|escape:'html'}" title="{$saleproduct.name|escape:html:'UTF-8'}">{$saleproduct.name|strip_tags|escape:html:'UTF-8'}</a></h5>
                          {hook h='displayProductListReviews' product=$saleproduct}
                          {if (!$PS_CATALOG_MODE AND ((isset($saleproduct.show_price) && $saleproduct.show_price) || (isset($saleproduct.available_for_order) && $saleproduct.available_for_order)))}
                        	{if isset($saleproduct.show_price) && $saleproduct.show_price && !isset($restricted_country_mode)}
                                <span class="price">{if !$priceDisplay}{convertPrice price=$saleproduct.price}{else}{convertPrice price=$saleproduct.price_tax_exc}{/if}</span>                                   
                            {/if}
                          {/if}
                    </div>
                </li>
            {/foreach}
        </ul>
	{else}
		<p>&raquo; {l s='Do not allow sale products at this time.' mod='ovicsaleproducts'}</p>
	{/if}
	</div>
</div>  