{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('blocktestimonial_home.tpl','blocktestimonial')}
{if  $option_tpl!== null} 
    {include file=$option_tpl}
{else} 
    {if isset($testimonials) && $testimonials && $testimonials|@count > 0}
        <div class="section-testmonial6">
            <h5 class="title-section-bg">{$TESTIMONIAL_TITLE}</h5>
            <div class="testimonial leka-testimonial-3">
				<div class="client-carousel owl-carousel" data-autoplay="true" data-loop="true" data-dots="true" data-margin="0" data-items="1" data-nav="false">
					{foreach from=$testimonials item=info}
                    <div class="item-client">
						<div class="client-avatar">
							<img src="{$module_dir}img/{$info.file_name}" alt="{$info.text|escape:html:'UTF-8'}" />
						</div>
						<div class="info-testimonial">
							<div class="client-quote">"{$info.text|escape:html:'UTF-8'}"</div>
							<div class="hr-testimonial"><span class="cicrle"></span></div>
							<div class="client-info">
								<div class="client-name">{$info.name|escape:html:'UTF-8'}</div>
								<div class="client-position">{$info.company|escape:html:'UTF-8'}</div>
							</div>
						</div>
					</div> 
                    {/foreach}
				</div>
			</div>
        </div>
    {/if}
{/if}