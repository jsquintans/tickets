{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('blocktestimonial_home.tpl','blocktestimonial')}
{if  $option_tpl!== null} 
    {include file=$option_tpl}
{else} 
    {if $testimonials|@count > 0}
    <!-- MODULE Block Testimonial -->
    <div class="section-clientsay"> 
        <div class="container">
            <div class="title-section text-center">
              {if isset($TESTIMONIAL_TITLE) && $TESTIMONIAL_TITLE != ''}
                <h2 class="title">{$TESTIMONIAL_TITLE}</h2>
              {else}
                <h2>{l s='Client say' mod='blocktestimonial'}</h2>
              {/if}
            </div> 
            <div class="row">
				<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
					<div class="testimonial leka-testimonial-2"> 
                        <div class="leka-client-quote"></div>
						<div class="client-carousel">
                            {foreach from=$testimonials item=info}
							<div class="item-client">
								<div class="client-avatar"><img src="{$modules_dir}blocktestimonial/img/{$info.file_name}" alt="" title="" /></div>
								<div class="info-testimonial">
									<div class="client-info">
										<span class="client-name">{$info.name|escape:html:'UTF-8'}</span>
										<span class="client-position">{$info.company|escape:html:'UTF-8'}</span>
									</div>
									<div class="client-quote">“{$info.text|escape:html:'UTF-8'}”</div>
								</div>  
							</div> 
                            {/foreach}
						</div> 
                      </div>
                </div>
            </div>
        </div>
      </div>  
    <!-- /MODULE Block Testimonial -->
    {/if}
{/if}
