{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('displayfooter.tpl', 'blockhtml')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    {if isset($item) && !empty($item)}
        <div class="blockhtml_{$hook_position}">
            {if isset($item.content)}
                {$item.content|html_entity_decode}
            {/if}
        </div>
    {/if}
{/if}