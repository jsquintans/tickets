{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('topmenu.tpl', 'advancetopmenu')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}

    {if isset($MENU)}
    <div class="col-sm-10 main-menu-wapper">  
        <a href="#" class="mobile-navigation"><i class="fa fa-bars"></i></a> 
        <nav id="nav_topmenu"> 
            <div id="topmenu" class="main-menu-top">
                <ul class="nav navbar-nav navigation">
                {foreach $MENU item=mainitem name=mainmenu}
                    <li class="level-1{if $mainitem.active == 1 } active{/if}{if isset($mainitem.submenu)} menu-item-has-children dropdown{/if}{if $mainitem.class|count_characters>0} {$mainitem.class}{/if}">
                        <a href="{$mainitem.link}" {if isset($mainitem.submenu)} class="dropdown-toggle" data-toggle="dropdown"{/if}>{if $mainitem.icon|count_characters>0}<i class="{$mainitem.icon}"></i>{/if}{$mainitem.title}</a>
                        {if isset($mainitem.submenu)}
                            {assign var=sub value=$mainitem.submenu}
                                <ul class="{*}sub-menu-top{*} dropdown-menu {if $sub.class|count_characters>0}{$sub.class} {/if}" {if $sub.width}style="width:{$sub.width}px"{/if}>
                                {if isset($sub.blocks) && count($sub.blocks)>0}
                                    {foreach $sub.blocks item=block name=blocks}
                                        {if isset($block.items)}
                                            <li class="block-container col-sm-{$block.width}{if $block.class|count_characters>0} {$block.class}{/if}">
                                                <ul class="block">
                                                {foreach $block.items item=item name=items}
                                                    <li class="level-2 {$item.type}_container {$item.class}">
                                                        {if $item.type=='link'}
                                                            <a href="{$item.link}">{if $item.icon|count_characters>0}<i class="{$item.icon}"></i>{/if}{$item.title}</a>
                                                        {elseif $item.type=='img' && $item.icon|count_characters>0}
                                                            <a class="{$item.class}" href="{$item.link}">
                                                                <img alt="" src="{$absoluteUrl}img/{$item.icon}" class="img-responsive" />
                                                            </a>
                                                        {elseif $item.type=='html'}
                                                            {$item.text}
                                                        {/if}
                                                    </li>
                                                {/foreach}
                                                </ul>
                                            </li>
                                        {/if}
                                    {/foreach}
                                {/if}
                                </ul>
                        {/if}
                    </li>
                {/foreach}
                </ul>
            </div>
        </nav>
    </div>
    <!--/ Menu -->
    {/if}
{/if}
{*}
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="http://abc.com">Link <span class="sr-only">(current)</span></a></li>
        <li class="dropdown">
                      <a href="http://test.com" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="http://abcv.com">Action</a></li>
                        <li><a href="ttp://bcs.com">Another action</a></li>
                      </ul>
                    </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
{*}