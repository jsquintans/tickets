{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('simplecategory.grid.module.tpl','simplecategory')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <div class="col-sm-12">
        <div class="leka-product-grid">
        	{if $simplecategory_item.display_name == 1} 
              <h2>{$simplecategory_item.name}</h2>
              {if isset($simplecategory_item.description) && $simplecategory_item.description}
                 {$simplecategory_item.description|html_entity_decode}
              {/if} 
            {else}
                <h2 class="hidden" style="padding:0;">{l s='Title'}</h2>
            {/if}
            <div class="row"> 
                <h2>Next Matchs</h2>
                {if isset($simplecategory_item.products) && $simplecategory_item.products|@count >0}  
                    {foreach from=$simplecategory_item.products item=product}
    					<div class="col-md-12 col-sm-12 col-xs-12 product"> 
                            <div class='row contenido_evento'>
                                <div class='col-md-2 col-sm-2 imgs_equipos'>
                                    <img src="./img/f/{$product.id_equipo_local}-medium_default.png"   height="45px" /> <span class='span_versus_lista_home'>vs.</span> <img src="./img/f/{$product.id_equipo_visitante}-medium_default.png" height="45px" />
                                </div>
                                <div class='col-md-5 col-sm-5'>
                               
                                     <h5><a href="{$product.link}">{$product.name}</a></h5> 
                                     <span class='nombre_torneo_lista_home'>{$product.torneo_del_encuentro} </span> <span class='hora_encuentro_lista_home'> {$product.fecha_encuentro|date_format:"%e/%m/%Y, %a %R"}</span>
                                 </div>
                                <div class='col-md-3 col-sm-3'>
                                    <p class='nombre_estadio_lista_home'>{$product.nombre_estadio}</p>
                                   
                                    <p class='localiz_estadio_lista_home'><span class='ciudad_estadio_lista_home'>{if isset($product.ciudad)}{$product.ciudad}{/if},</span><span class='pais_estadio_lista_home'> {if isset($product.pais)}{$product.pais}{/if}</span></p>
                                </div>
                                <div class='col-md-2 col-sm-2'>
                                    <a href='{$product.link}' class='btn_comprar_grid'>BUY</a>
                                </div>
                            </div>
                         
    					</div> 
                    {/foreach}
                {/if}
            </div> 
        </div>
    </div>
{/if}


