{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('simplecategory.classic.module.tpl','simplecategory')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <section class="block-buttom-products">
        {if $simplecategory_item.display_name == 1} 
              <h2 class="title">{$simplecategory_item.name}</h2> {* title *}
              {if isset($simplecategory_item.description) && $simplecategory_item.description}
                 {$simplecategory_item.description|html_entity_decode}
              {/if}  
        {else}
            <h2 class="hidden">{l s='Title'}</h2>
        {/if}
        
    	<div class="container">{* rows *}
    		<div class="row"> {* row *}
                {if isset($simplecategory_item.groups) && $simplecategory_item.groups|@count >0} {* groups *}
                    {foreach from=$simplecategory_item.groups item=group name=groups} 
        			<div class="col-sm-12 col-md-4"> {* group *}
        				<div class="block-products">
        					<div class="title-section text-left">
        						<h3 class="title">{$group.name}</h3>
        					</div>
                            
        					<ul class="prodcut-list">
                                {foreach from=$group.products item=product}
            						<li>
                                    
            							<div class="product-img">
            								<a href="{$product.link}"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="" /></a>
            							</div>
            							<div class="product-info">
            								<h6><a href="{$product.link}">{$product.name}</a></h6>  
                                            {if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
                                            <div class="content_price">
                        						{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
                        							{hook h="displayProductPriceBlock" product=$product type='before_price'}
                        							<span class="price product-price">
                        								{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
                        							</span>
                        							{if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
                        								{hook h="displayProductPriceBlock" product=$product type="old_price"}
                        								<span class="old-price product-price">
                        									{displayWtPrice p=$product.price_without_reduction}
                        								</span>
                        								{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
                        								{if $product.specific_prices.reduction_type == 'percentage'}
                        									<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
                        								{/if}
                        							{/if}
                        							{hook h="displayProductPriceBlock" product=$product type="price"}
                        							{hook h="displayProductPriceBlock" product=$product type="unit_price"}
                        							{hook h="displayProductPriceBlock" product=$product type='after_price'}
                        						{/if}
                        					</div> 
                                            <div class="box-button">
                        						{hook h='displayProductListFunctionalButtons' product=$product}
                                                {if isset($comparator_max_item) && $comparator_max_item} 
                        							<a class="button-compare compare add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}" title="{l s='Add to compare' mod='simplecategory'}">{l s='Compare'  mod='simplecategory'}</a>
                        						{/if}
                                                {if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.customizable != 2 && !$PS_CATALOG_MODE}
                        							{if (!isset($product.customization_required) || !$product.customization_required) && ($product.allow_oosp || $product.quantity > 0)}
                        								{capture}add=1&amp;id_product={$product.id_product|intval}{if isset($product.id_product_attribute) && $product.id_product_attribute}&amp;ipa={$product.id_product_attribute|intval}{/if}{if isset($static_token)}&amp;token={$static_token}{/if}{/capture}
                        								<a class=" add-to-cart ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product-attribute="{$product.id_product_attribute|intval}" data-id-product="{$product.id_product|intval}" data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity >= 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">
                        									{l s='Add to cart'}
                        								</a>
                        							{else}
                        								<a href="javascript:void(0)" class="add-to-cart ajax_add_to_cart_button disabled">
                        									{l s='Add to cart'}
                        								</a>
                        							{/if}
                        						{/if} 
            								</div> 
                                            {/if}
            							</div>
            						</li>
                                    
                                {/foreach}
        					</ul>
        				</div>
        			</div> 
                    {/foreach}
                {/if}
    		</div>
    	</div>
    </section>
{/if}