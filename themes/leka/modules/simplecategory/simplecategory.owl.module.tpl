{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('simplecategory.owl.tpl','simplecategory')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <section class="simplecategory-owl {if isset($custom_class) && $custom_class}{$custom_class}{/if}" >
        <div class="tittle tittle-2">
        {if isset($display_name) && $display_name}
            <h5>{$module_name}</h5>
        {/if}
        {if isset($module_description) && $module_description}
            {$module_description|html_entity_decode}
        {/if}            
        </div>	
        {if isset($module_products)}    
            {if $module_products|@count > 0}    
            	<div class="popurlar_product client-slide">
                    {include file="$tpl_dir./product-list-owl-index3.tpl" products=$module_products} 
            	</div>
            {/if}
        {else}
    		<div>{l s='Sorry! There are no products' mod='simplecategory'}</div>
    	{/if}
    </section>  
{/if}