{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('simplecategory.slider.tpl','simplecategory')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <section class="simplecategory-slider bnr-items {if isset($custom_class) &&  $custom_class}{$custom_class}{/if}" >
        <h3 class="hidden" style="padding:0;">Title</h3>
    	{if isset($module_products)}   
    	<div class="bnr-items-slider">   
    		{if $module_products|@count > 0}                
                {*}{include file="$tpl_dir./product-list-slider.tpl" products=$module_products}{*}
                {foreach from=$module_products item=product name=products}
                    <div class="items-pro">   
                        <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} />
                        <!-- Hover Details -->
                        <div class="item-hover">
                          <div class="row"> <span>{$product.category|replace:'-':' '|upper|escape:'html':'UTF-8'}</span> </div> 
                          <a class="head" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" >{$product.name|truncate:20:'...'|escape:'html':'UTF-8'}</a>
                          <p>{$product.description_short|strip_tags}</p>
                          <a href="{$product.link|escape:'html':'UTF-8'}" class="btn btn-1">{l s='DISCOVER' mod='simplecategory'}</a> 
                        </div>
                    </div>
                {/foreach}
    		{else}
    			<div>{l s='Sorry! There are no products' mod='simplecategory'}</div>
    		{/if}	
    	</div>  
    	{/if}
    </section>
{/if}