{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('simplecategory.tabsim.module.tpl','simplecategory')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else} 
    <div class="section-tab-trending simplecategory-tabs-simple {$simplecategory_item.custom_class}">
    	{if $simplecategory_item.display_name == 1}
            <div class="title-section style2 text-center">
              <h2 class="title">{$simplecategory_item.name}</h2>
              {if isset($simplecategory_item.description) && $simplecategory_item.description}
                 {$simplecategory_item.description|html_entity_decode}
              {/if} 
            </div>
        {else}
            <h2 class="hidden" style="padding:0;">Title</h2>
        {/if}	
    	{if isset($simplecategory_item.groups) && $simplecategory_item.groups|@count >0}
            <div class="tab-slide-category">
        		<ul class="products-tab nav nav-tabs" role="tablist">
        			{foreach from=$simplecategory_item.groups item=group name=groups}
        				{if $smarty.foreach.groups.first}
        					<li role="presentation" class="active"><a href="#tabproduct-{$group.id}"  role="tab" data-toggle="tab">{$group.name}</a></li>
        				{else}
        					<li role="presentation"><a href="#tabproduct-{$group.id}"  role="tab" data-toggle="tab">{$group.name}</a></li>
        				{/if}
        			{/foreach}
        		</ul>
        		<div class="tab-content">
        			{foreach from=$simplecategory_item.groups item=group name=groups}
                        <div role="tabpanel" class="tab-pane fade" id="tabproduct-{$group.id}"> 
        					{if isset($group.products) && $group.products|@count} 
                                <div class="product-slide owl-carousel"  data-responsive='{ldelim}"0":{ldelim}"items":1{rdelim},"600":{ldelim}"items":2{rdelim},"1000":{ldelim}"items":3{rdelim}{rdelim}' data-margin="30" data-items="3" data-nav="false" data-dots="false">
                                    {include file="$tpl_dir./product-list-owl.tpl" products=$group.products}
                                </div>
        					{else}
        						<div>{l s='Sorry! There are no products' mod='simplecategory'}</div>
        					{/if} 
        				</div>
        			{/foreach}
        		</div>
            </div>
    	{/if}
    </div> 
{/if}