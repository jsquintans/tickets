{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('simplecategory.simple.module.tpl','simplecategory')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <div class="simplecategory-simple {$simplecategory_item.custom_class}" >
        {if $simplecategory_item.display_name == 1}<h5>{$simplecategory_item.name}</h5>{/if}
        {if isset($simplecategory_item.description) && $simplecategory_item.description} 
            {$simplecategory_item.description|html_entity_decode} 
        {/if}
    	{if isset($simplecategory_item.products)}          
    	<div class="section-feture-product div_full_width">  
			{if $simplecategory_item.products|@count > 0}                
                {include file="$tpl_dir./product-simple.tpl" products=$simplecategory_item.products}
			{else}
				<div>{l s='Sorry! There are no products' mod='simplecategory'}</div>
			{/if}	
    	</div>
    	{/if}
    </div>
{/if}