{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('simplecategory.multiowl.tpl','simplecategory')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
<section class="section-p-30px popurlar_product">
    <div class="row">       
    	{if isset($groupsp) && $groupsp|@count >0}
            {assign var='nbGroup' value=$groupsp|@count}
            {assign var='colValue' value=(12/$nbGroup)}
			
            {foreach from=$groupsp item=group name=groups}
                <div class="group-multiowl col-xs-12 col-sm-{$colValue}">
                <div class="tittle">
                  <h5>{$group.name}</h5>
                  <p>Treding fashion</p>
                </div>
					{if isset($group.products) && $group.products|@count}
                        {include file="$tpl_dir./product-list-owl.tpl" products=$group.products}
					{else}
						<div>{l s='Sorry! There are no products' mod='simplecategory'}</div>
					{/if}	
				</div>
			{/foreach}
    	{/if}
    </div>        
</section>
{/if}