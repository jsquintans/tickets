{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('simplecategory.leftowl.module.tpl','simplecategory')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <div class="simplecategory-left-owl {$simplecategory_item.custom_class}" >
        {if $simplecategory_item.display_name == 1}<h5 class="title-section-bg">{$simplecategory_item.name}</h5>{/if}
        {if isset($simplecategory_item.description) && $simplecategory_item.description} 
            {$simplecategory_item.description|html_entity_decode} 
        {/if}
    	{if isset($simplecategory_item.products)}          
    	<div class="product-slide owl-carousel product-slide-style2 owl-nav-style2" data-autoplay="true" data-nav="true" data-items="1" data-margin="0" data-Dots="false">  
			{if $simplecategory_item.products|@count > 0}                
               {foreach from=$simplecategory_item.products item=product name=products}
                    <div class="product">
						<div  class="product-thumb">
							<a href="{$product.link|escape:'html':'UTF-8'}"> 
                                <img class="img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" />
        					</a>
							<div class="product-hover">
								<div class="product-info">
									<h3><a href="{$product.link|escape:'html':'UTF-8'}">{$product.name|escape:'html':'UTF-8'}</a></h3>
									{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
                                        {hook h="displayProductPriceBlock" product=$product type='before_price'}
                                        <span class="price-product">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span>
                                        {hook h="displayProductPriceBlock" product=$product type="price"}
            							{hook h="displayProductPriceBlock" product=$product type="unit_price"}
            							{hook h="displayProductPriceBlock" product=$product type='after_price'}
                                    {/if}
								</div>
								<div class="product-button">  
                                    {if isset($comparator_max_item) && $comparator_max_item} 
            							<a class="button-compare add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}" title="{l s='Add to compare' mod='simplecategory'}">{l s='Compare' mod='simplecategory'}</a>
            						{/if}
            						{hook h='displayProductListFunctionalButtons' product=$product}
            						{if isset($quick_view) && $quick_view}
                                        <a href="{$product.link|escape:'html':'UTF-8'}" data-rel="{$product.link|escape:'html':'UTF-8'}" class="quick-view button-quickview" title="{l s='Quick view' mod='simplecategory'}">{l s='Quick view' mod='simplecategory'}</a>
                                    {/if}
            					</div>
								{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
								{if $product.allow_oosp || $product.quantity > 0}
    								{capture}add=1&amp;id_product={$product.id_product|intval}{if isset($product.id_product_attribute) && $product.id_product_attribute}&amp;ipa={$product.id_product_attribute|intval}{/if}{if isset($static_token)}&amp;token={$static_token}{/if}{/capture}
    								<a class="ajax_add_to_cart_button leka-button button-add-to-cart" href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product-attribute="{$product.id_product_attribute|intval}" data-id-product="{$product.id_product|intval}" data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity >= 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">
    									{l s='ADD TO CART'}
    								</a>
    							{else}
    								<a href="javascript:void(0)" class="leka-button button-add-to-cart ajax_add_to_cart_button disabled">
    									{l s='ADD TO CART'}
    								</a>
    							{/if}
                                {/if}
							</div>
						</div>
					</div>
               {/foreach}
			{/if}	
    	</div>
    	{/if}
    </div>
{/if}