{if $PS_SC_TWITTER || $PS_SC_FACEBOOK || $PS_SC_GOOGLE || $PS_SC_PINTEREST}

	<p class="product-share">
        <strong>{l s='Share:' mod='socialsharing'}</strong>
		{if $PS_SC_TWITTER}
			<button data-type="twitter" type="button" class="btn-twitter social-sharing">
				<i class="fa fa-twitter"></i>
			</button>
		{/if}
		{if $PS_SC_FACEBOOK}
			<button data-type="facebook" type="button" class="btn-facebook social-sharing">
				<i class="fa fa-facebook"></i> 
			</button>
		{/if}
		{if $PS_SC_GOOGLE}
			<button data-type="google-plus" type="button" class="btn-google-plus social-sharing">
				<i class="fa fa-tencent-weibo"></i> 
			</button>
		{/if}
		{if $PS_SC_PINTEREST}
			<button data-type="pinterest" type="button" class="btn-pinterest social-sharing">
				<i class="fa fa-vk"></i>
			</button>
		{/if}
	</p>
{/if}