{if isset($megamenus_menus) && $megamenus_menus|@count >0}
<div class="container header-vertical-menu">
{$before}
<nav id="vertical-menu" class="vertical-menu megamenus nav-vertical-left-megamenu widget active {$custom_class}" role="navigation">
	{if $display_name == '1'}
		<h4 class="vertical-title {$root_color} {$root_background} clearfix">
			<span class="title">{$module_name}</span><span class="ti-menu pull-right vertical-none-toggle-menu"></span> 
			<span class="vertical-navigation ti-menu pull-right vertical-toggle-menu"><i class="fa fa-bars"></i></span> 
		</h4>
	{/if}
	<div class="responsive-nav">		
		<a class="responsive-btn {$root_color} {$root_background}" href="#">
			<span class="responsive-btn-icon ti-menu pull-left"></span>
			<span class="responsive-btn-text">{$module_name}</span>			
		</a>
		<div class="responsive-menu-container"></div>
	</div>
	<ul class="vertical-content desktop-nav vertical-left-megamenu menu clearfix">
		{foreach from=$megamenus_menus item=menu name=menus}
			{if !isset($menu.rows) || $menu.rows == ''}
				{if isset($menu.subs) && $menu.subs}
					<li  {if $smarty.foreach.menus.iteration > $show_count} class="simplemenu-container {$menu.custom_class} menu-parent cat-link-orther" style="display:none" {else} class="simplemenu-container {$menu.custom_class} menu-parent" {/if}>
						<a class="{$root_hover_color}" data-rel="{$menu.link}" href="{$menu.link}" title="{$menu.name}">{$menu.name}</a>
						{$menu.subs}						
					</li>
				{else}
					<li {if $smarty.foreach.menus.iteration > $show_count} class="{$menu.custom_class} cat-link-orther" style="display:none" {else}class="{$menu.custom_class} "{/if}  >
						<a class="{$root_hover_color}" href="{$menu.link}" title="{$menu.name}">{$menu.name}</a>
					</li>
				{/if}
			{else}
			<li {if $smarty.foreach.menus.iteration > $show_count}class="megamenu-container megamenu-rows {$menu.custom_class} menu-parent cat-link-orther" style="display:none" {else}class="megamenu-container megamenu-rows {$menu.custom_class} menu-parent"{/if} >
				<a class="{$root_hover_color}" href="{$menu.link}" data-rel="{$menu.link}" title="{$menu.name}">{$menu.name}</a>
				{$menu.rows}
			</li>
			{/if}				
		{/foreach}
		{if $megamenus_menus|@count > $show_count}
			<li class="all-category">
				<a href="javascript:void(0)" class="open-cate">{l s='All Categories' mod='megamenus'}</a>
			</li>
		{/if}
	</ul>     
</nav>
{$after}
</div>
{/if}

