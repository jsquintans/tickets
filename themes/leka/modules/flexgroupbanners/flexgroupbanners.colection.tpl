<!-- Banner clection -->
{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('flexgroupbanners.colection.tpl','flexgroupbanners')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
     <div class="flexgroupbanners section-banner-clection6 {$custom_class}">
    	{if $display_name == '1'}<h3 class="module-name">{$module_name}</h3>{/if}	
    	{if isset($rowContents) && $rowContents|@count >0} 
    		{foreach from=$rowContents item=row name=rows}
    			<div class="flexgroupbanners-row {$row.custom_class} {if $row.width >0}col-sm-{$row.width}{/if} clearfix">
    				{if isset($row.groups) && $row.groups|@count >0} 				
    					{foreach from=$row.groups item=group name=groups}						
    			            <!-- group banners -->                                                        
                            <div class="flexgroupbanners-group banner-colection grid-mansory {$group.custom_class} {if $group.width >0} col-sm-{$group.width} {else} clearfix {/if}"
                                data-layoutmode="packery" data-gutter="30">                            
                                {if isset($group.items) && $group.items|@count >0}
                                    <!-- banner items --> 
                                    {foreach from=$group.items item=banneritem name=banneritems} 
    										{if $banneritem.full_path}
    							                <a class="grid banner-opacity {$banneritem.custom_class}" href="{$banneritem.link}" target="_blank" title="{$banneritem.alt}">
    							                	<img src="{$banneritem.full_path}" alt="{$banneritem.alt}" />
    							                </a>
    						                {/if}
    						                {if $banneritem.description}
    						                {$banneritem.description|html_entity_decode}
    						                {/if} 
                                    {/foreach} 
                                    <!-- end banner item -->
                                {/if}  
                            </div>
                            <!-- end group banners -->
    					{/foreach}	 
    				{/if}			
    			</div>	
    		{/foreach} 	
    	{/if} 
    </div>  
{/if}
<!-- ./ Banner colection -->