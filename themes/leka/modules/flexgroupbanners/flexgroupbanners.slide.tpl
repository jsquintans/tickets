<div class="flexgroupbanners-slide {$custom_class}">
	{if $display_name == '1'}<h3 class="module-name">{$module_name}</h3>{/if}	 
	{if isset($rowContents) && $rowContents|@count >0}
		<div class="flexgroupbanners-rows">	
		{foreach from=$rowContents item=row name=rows}
			<div class="flexgroupbanners-row {$row.custom_class} {if $row.width >0}col-sm-{$row.width}{/if} clearfix">
				{if $row.display_title == "1"}<h3 class="row-title"><span>{$row.name}</span></h3>{/if}
				{if isset($row.groups) && $row.groups|@count >0}
				{assign var='totalwidth' value=0}
				<div class="flexgroupbanners-groups clearfix">					
					{foreach from=$row.groups item=group name=groups}
						{if $group.width == 0} 
							{assign var='totalwidth' value=0}
						{else}
							{assign var='totalwidth' value=$totalwidth+$group.width}            
				            {if $totalwidth>12 && !$smarty.foreach.groups.last}
				                <div class="clearfix"></div>                
				                {assign var='totalwidth' value=0}            
				            {/if}
						{/if}						
			            <!-- group banners -->                                                        
                        <div class="leka-list-cat owl-carousel {$group.custom_class} {if $group.width >0} col-sm-{$group.width} {else} clearfix {/if}"
                            data-responsive='{ldelim}"0":{ldelim}"items":1{rdelim},"600":{ldelim}"items":3{rdelim},"1000":{ldelim}"items":4{rdelim},"1366":{ldelim}"items":5{rdelim}{rdelim}' 
                            data-margin="0" data-items="5" data-nav="false" data-Dots="false">                         
                            {if isset($group.items) && $group.items|@count >0}
                                <!-- banner items --> 
                                {foreach from=$group.items item=banneritem name=banneritems}
									<div class="item-cat" {if $banneritem.full_path}data-background="{$banneritem.full_path}"{/if}>
                                		<div class="cat-info">
                                			{if $banneritem.description}
    						                  <div class="flex-banner-description">{$banneritem.description|html_entity_decode}</div>
    						                {/if}
                                		</div>
                                	</div> 
                                {/foreach} 
                                <!-- end banner item -->
                            {/if}  
                        </div>
                        <!-- end group banners -->
					{/foreach}								
				</div>
				{/if}			
			</div>	
		{/foreach}
		</div>	
	{/if}
	
</div>