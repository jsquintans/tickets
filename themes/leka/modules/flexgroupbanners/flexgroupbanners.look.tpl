{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('flexgroupbanners.look.tpl','flexgroupbanners')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <div class="section-thelook {$custom_class}"> 
        <div class="title-section text-center">
			{if isset($module_name) && !empty($module_name)} 
                <h2 class="title">{$module_name}</h2>
            {else}
                <h2 class="hidden" style="padding:0;">Title</h2>
            {/if}
            
		</div>
    	{if isset($rowContents) && $rowContents|@count >0}
    		<div class="flexgroupbanners-rows">	 
    		{foreach from=$rowContents item=row name=rows} 
    			<div data-layoutmode="packery" class="flexgroupbanners-row {$row.custom_class} {if $row.width >0}col-sm-{$row.width}{/if} clearfix">
    				{if isset($row.groups) && $row.groups|@count >0}
    				{assign var='totalwidth' value=0}
    					{foreach from=$row.groups item=group name=groups}					
    			            <!-- group banners -->                                                        
                            <div class="{$group.custom_class} {if $group.width >0} col-sm-{$group.width}{/if}">                            
                                {if isset($group.items) && $group.items|@count >0}
                                    <!-- banner items -->
                                    <div class="flexgroupbanners-banners">
                                        {foreach from=$group.items item=banneritem name=banneritems}
    										<div class="banner-item {$banneritem.custom_class}">
    											{if $banneritem.full_path}
    								                <a href="{$banneritem.link}" target="_blank" title="{$banneritem.alt}">
    								                	<img class="img-responsive" src="{$banneritem.full_path}" alt="{$banneritem.alt}" />
                                                        {if $banneritem.description}
            							                <span class="banner-description">{$banneritem.description|html_entity_decode}</span>
            							                {/if}
    								                </a>
    							                {/if}
    							                
    							            </div> 
                                        {/foreach}
                                    </div>
                                    <!-- end banner item -->
                                {/if}  
                            </div>
                            <!-- end group banners -->
    					{/foreach}	
    				{/if}			
    			</div>	
    		{/foreach}
    		</div>	
    	{/if}
    </div>
{/if}