{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('flexgroupbanners.sliderlarge.tpl','flexgroupbanners')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <div class="section-collection {$custom_class}">
        {if isset($rowContents) && $rowContents|@count >0}
        {* flexgroupbanners-rows *}
            {foreach from=$rowContents item=row name=rows}
                {* Row item *} 
                <div class="slide-collection owl-dots-style1 {$row.custom_class} {if $row.width >0}col-sm-{$row.width}{/if}">
                    {if isset($row.groups) && $row.groups|@count >0}
    				    {assign var='totalwidth' value=0}
    					{foreach from=$row.groups item=group name=groups}
                        {* Group item *} 
                            <div class="item-collect {$group.custom_class} {if $group.width >0} col-sm-{$group.width}{/if}">                            
                                {if isset($group.items) && $group.items|@count >0}
                                    {* Banner items *} 
                                    {foreach from=$group.items item=banneritem name=banneritems} 
                                        <div class="{$banneritem.custom_class}">
											{if $banneritem.full_path}
                                                <a href="{$banneritem.link}" target="_blank" title="{$banneritem.alt}">
								                	<figure>
                                                        <img src="{$banneritem.full_path}" alt="{$banneritem.alt}" />
                                                    </figure>
								                </a> 
							                {/if} 
                                            {if $banneritem.description}
    							                <div class="text-colect">
                                                    {$banneritem.description|html_entity_decode} 
                                                </div>
							                {/if}
							            </div> 
                                    {/foreach} 
                                    {* END - Banner items *}
                                {/if}
                            </div>
                        {* END - Group item *} 
                        {/foreach}
                    {/if}
                </div>
                {* END - Row item *}
            {/foreach}
        {* END - flexgroupbanners-rows *}
        {/if}
    </div>
{/if}