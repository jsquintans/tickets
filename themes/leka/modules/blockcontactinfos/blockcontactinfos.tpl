{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- MODULE Block contact infos -->
<div class="col-md-4">
    <div class="boxes-in">
        <h5>{l s='SHOP LOCATION' mod='blockcontactinfos'}</h5>
        <hr />
        <ul class="location">
            {if $blockcontactinfos_company != ''}
                <li> <i class="fa fa-location-arrow"></i>
                    <p>{$blockcontactinfos_company|escape:'html':'UTF-8'}{if $blockcontactinfos_address != ''}, {$blockcontactinfos_address|escape:'html':'UTF-8'}{/if}</p>
                </li>
            {/if}
            {if $blockcontactinfos_phone != ''}                
                <li> <i class="fa fa-phone"></i> 
                    <p>{l s='Phone: ' mod='blockcontactinfos'} {$blockcontactinfos_phone|escape:'html':'UTF-8'}</p>
                </li>
            {/if}    
            {if $blockcontactinfos_email != ''}            
                <li> <i class="fa fa-envelope"></i>
                    <p>{mailto address=$blockcontactinfos_email|escape:'html':'UTF-8' encode="hex"}</p>
                </li>
            {/if}
            <li> <i class="fa fa-clock-o"></i>
                <p>{l s='OPEN: 9AM - 8PM' mod='blockcontactinfos'}</p>
            </li>
        </ul>
    </div>
</div>

<!-- /MODULE Block contact infos -->
