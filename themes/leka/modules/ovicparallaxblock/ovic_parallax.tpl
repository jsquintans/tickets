{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('ovic_parallax.tpl','ovicparallaxblock')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <div class="parallax-container">
        <div class="parallax div_full_width" style="background-image: url({$link->getMediaLink("`$smarty.const._MODULE_DIR_`ovicparallaxblock/img/`$background|escape:'htmlall':'UTF-8'`")})" data-stellar-background-ratio="{$ratio}">
            {$content|html_entity_decode}
        </div>
    </div>
{/if}