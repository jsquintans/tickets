 {*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if (!$content_only && (($nbComments == 0 && $too_early == false && ($is_logged || $allow_guests)) || ($nbComments != 0)))}
<div id="product_comments_block_extra" class="no-print" itemscope itemtype="http://schema.org/AggregateRating">
    {assign var="nbComments" value=ProductComment::getCommentNumber($id_product_comment_form)}
    {assign var="ratings" value=ProductComment::getRatings($id_product_comment_form)}
    {assign var="average" value=ProductComment::getAverageGrade($id_product_comment_form)}
    {assign var="averageTotal" value=$average.grade|round}
	{if $nbComments != 0}
		<div class="comments_note">
			<div class="star_content stars product-star">
				{section name="i" start=0 loop=5 step=1}
					{if $averageTotal le $smarty.section.i.index}
						<i class="fa fa-star-o"></i>
					{else}
						<i class="fa fa-star"></i>
					{/if}
				{/section}
				<meta itemprop="worstRating" content = "0" />
				<meta itemprop="ratingValue" content = "{if isset($ratings.avg)}{$ratings.avg|round:1|escape:'html':'UTF-8'}{else}{$averageTotal|round:1|escape:'html':'UTF-8'}{/if}" />
				<meta itemprop="bestRating" content = "5" />
			</div>
		</div> <!-- .comments_note -->
    {else}
        <div class="product-star face">
            <i class="fa fa-star-o"></i>
            <i class="fa fa-star-o"></i>
            <i class="fa fa-star-o"></i>
            <i class="fa fa-star-o"></i>
            <i class="fa fa-star-o"></i>
        </div>
        <a href="javascript:void(0);" class="review-link">{l s='0 Review' mod='productcomments'}&nbsp;|</a>
        {if ($too_early == false AND ($is_logged OR $allow_guests))}
            <a class="open-comment-form" href="#new_comment_form">
    			{l s='Add Your Review' mod='productcomments'}
    		</a>
        {/if}
	{/if}
    
	<ul class="comments_advices">
		{if $nbComments != 0}
			<li>
				<a href="#idTab5" class="reviews">
			     {$nbComments}{l s=' Review(s)' mod='productcomments'}&nbsp;|				</a>
			</li>
		{/if}
		{*}
        {if ($too_early == false AND ($is_logged OR $allow_guests))}
			<li>
				<a class="open-comment-form" href="#new_comment_form">
					{l s='Add Your Review' mod='productcomments'}
				</a>
			</li>
		{/if}
        {*}
	</ul>
</div>
{/if}
<!--  /Module ProductComments -->
