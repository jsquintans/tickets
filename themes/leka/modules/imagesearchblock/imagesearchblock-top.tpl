{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('imagesearchblock-top.tpl','imagesearchblock')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <div id="form-search" class="form-search" >
    	{*}<a id="call_search_block" href="javascript:void(0);"><i class="fa fa-search"></i></a>{*}
    	<form method="get" action="{$link->getPageLink('search')}" id="searchbox">
    		<div class="search_block_top_form">			
    			<input type="hidden" name="controller" value="search" />
    			<input type="hidden" name="orderby" value="position" />
    			<input type="hidden" name="orderway" value="desc" />
    			<input type="text" id="search_query_top" name="search_query" value="" placeholder="{l s='Search your tickets' mod='imagesearchblock'}" />
    			<button type="submit" class="btn-search">SEARCH</button>
    		</div>
    	</form>
    </div>
{/if}
{include file="$self/imagesearchblock-instantsearch.tpl"}