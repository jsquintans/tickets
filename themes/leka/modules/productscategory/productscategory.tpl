{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if count($categoryProducts) > 0 && $categoryProducts !== false}
{* OWL Version *}

<div class="related-products"> 
    <div class="title-section text-center">
		<h2 class="title">{l s='RELATED PRODUCTS' mod='productscategory'}</h2>
	</div> 
    <div class="product-slide "  data-dots="false" data-nav ="true" data-margin ="30" data-responsive='{ldelim}"0":{ldelim}"items":1{rdelim},"600":{ldelim}"items":3{rdelim},"1000":{ldelim}"items":4{rdelim}{rdelim}'>
		{include file="$tpl_dir./product-list-owl.tpl" products=$categoryProducts}
	</div>
</div>

{/if}
