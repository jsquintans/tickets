{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('multistyle.tpl','oviclayoutcontrol')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    {if isset($font) && $font|count>0}
        {foreach $font as $f}
            {$f.linkfont|html_entity_decode}
        {/foreach}    
    {/if}
    {if isset($font['font1'].fontname) && $font['font1'].fontname }
        {assign var='fontname' value=$font['font1'].fontname}
    {else}
        {assign var='fontname' value="<link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>"}
    {/if}
    
    {if isset($color['main']) && $color['main'] }
        {assign var='maincolor' value=$color['main']}
    {else}
        {assign var='maincolor' value="#7e883a"}
    {/if}
    
    <style type="text/css">
        /***  Font default ***/
        body {ldelim}
            font-family: {$fontname};
        {rdelim}
    
        a,
        .testimonial .info-testimonial  .client-position,
        .blog-style2 .post-item .post-info h5 a:hover,
        .blog-style2 .post-item .post-meta .fa,
        .block-products .prodcut-list h6 a:hover,
        .block-products .product-price,
        .block-products .box-button a:hover,
        .header .top-header a:hover,
        .header .top-header a .fa,
        .header .top-header-right .dropdown-menu>li>a:focus, 
        .header .top-header-right .dropdown-menu>li>a:hover,
        .header .top-header-right .dropdown-menu>li>a.current,
        .header.header-style3 .top-header a:hover,
        .header.header-style3 .top-header a:focus,
        .header.header-style3 .top-header-right .dropdown-menu>li>a:hover,
        .header.header-style3 .top-header-right .dropdown-menu>li>a:focus,
        .header.header-style3 .main-header .main-menu>ul>li>a:hover,
        .header.header-style3 .main-header .main-menu>ul>li>a:focus,
        .main-menu a:hover,
        .main-menu .sub-menu>li>a:focus, 
        .main-menu .sub-menu>li>a:hover,
        .main-menu .sub-menu.mega-menu.style2 .widget a:hover,
        .main-menu .sub-menu.mega-menu.style2 .widget a:focus,
        .mini-cart .list-cart-product .product-info .price,
        .mini-cart .sub-toal,
        .footer a:hover,
        .footer .widget ul li a:hover,
        .footer .widget .widget-title,
        .footer .widget-newaletter .newaletter-button,
        .footer.footer-style2 .footer-menu a:hover,
        .footer.footer-style2 .footer-menu a .fa,
        .footer.footer-style2 .widget-social a:hover,
        .product .product-info h3 a:hover,
        .product .product-info .product-price,
        .product-feture .product-info h2 a:hover,
        .sortBar .display-product-option a.selected,
        .sortBar .display-product-option a:hover,
        .sortBar .display-product-option a:focus,
        .summary .price,
        .summary .product-share a:hover,
        .summary .product-share a:focus,
        .product-tabs .nav-tab li a:hover,
        .product-tabs .nav-tab li.active a,
        .product-tabs .nav-tab li:before,
        .blog-item .meta-post a:hover,
        .blog-item .meta-post .fa,
        .groupshare ul li a:hover,
        .widget ul li a:hover,
        .tagcloud a:hover,
        .widget .recent_posts_list li .post-cat a,
        .team-item .team-name a:hover,
        .team-item .team-name a:focus,
        .team-item .team-postion,
        .cart_totals  .order-total .amount,
        .checkout-page .amount,
        .block-info-contact .social-network a:hover,
        .block-info-contact .social-network a:focus,
        .leka-accordion .ui-state-active,
        .leka-accordion .ui-state-hover,
        .leka-accordion .ui-state-focus,
        .portfolio-nav a:hover,
        .portfolio-nav a:focus,
        .portfolio-nav a.active,
        .popup-add-to-cart .message .fa,
        .slide-home .leka-button:hover,
        .leka-button.button-style2:hover,
        .bottom-pagination-content .compare-form .bt_compare_bottom,
        /*.pagination ul li a, .pagination ul li a span, .pagination ul li span ,*/
        .chart-page .payment_steps .current h6 span ,
        .cart_delete a.cart_quantity_delete, a.price_discount_delete,
        .slide-home .owl-nav > div.owl-prev:hover,
        .slide-home .owl-nav > div.owl-next:hover        
        {ldelim}
            color:{$maincolor};
        {rdelim}
        
        .mini-cart .icon .count,
        .vertical-menu .mega-menu .widget .widgettitle,
        .product-tabs .review_form .submit:hover,
        .product-tabs .review_form .submit:focus,
        .mobile-sidebar .line-menubar,
        .team-item  .social-network a:hover,
        .team-item  .social-network a:focus,
        .leka-popup .form-subscribe .button:hover,
        .leka-popup .form-subscribe .button:focus,
        .leka-button.button-style2,
        .title-section .title:after,
        .leka-button:hover, button:hover, .button:hover, input[type="submit"]:hover,
        .bottom-pagination-content .compare-form:hover .bt_compare_bottom,
        .product .onsale,
        .summary .single_variation_wrap .single_add_to_cart_button,
        .summary .single_variation_wrap .buttom-compare:hover, .summary .single_variation_wrap .buttom-wishlist:hover,
        div#layered_price_slider .ui-state-default, div#layered_price_slider .ui-widget-content .ui-state-default, div#layered_price_slider .ui-widget-header .ui-state-default,
        div#layered_price_slider,
        div#layered_price_slider .ui-state-default:focus, div#layered_price_slider .ui-state-default:hover,
        {ldelim}
            background-color:{$maincolor};
        {rdelim}
        
        .owl-dots-style1 .owl-dots .owl-dot.active,
        .pagination ul li a:hover,
        .pagination ul li.active a,
        .pagination ul li a:focus,
        .products-list-view .product .product-button > a:hover,
        .products-list-view .product .product-button > a:focus,
        .section-redirectshop .leka-button:hover,
        .leka-button.button-style2,
        .leka-popup .form-subscribe .button:focus,
        .portfolio-share a:hover,
        .portfolio-share a:focus,
        .form-contact .button:hover,
        .form-contact .button:focus,
        .block-info-contact .infomation>span:hover .icon,
        .block-info-contact .social-network a:hover,
        .block-info-contact .social-network a:focus,
        .product-thumbnails a.selected,
        .bottom-pagination-content .compare-form:hover .bt_compare_bottom,
        .button.button-small span:hover,
        .button.button-small span:hover,
        .chart-page .payment_steps .current h6 span ,
        div#layered_price_slider .ui-state-default, 
        div#layered_price_slider .ui-widget-content .ui-state-default, 
        div#layered_price_slider .ui-widget-header .ui-state-default,
        .slide-home .owl-nav > div.owl-prev:hover,
        .slide-home .owl-nav > div.owl-next:hover
        {ldelim}
            border-color:{$maincolor};
        {rdelim}
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, 
        .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover,
        #productscomparison .button.ajax_add_to_cart_button span,
        #productscomparison #product_comparison .button.lnk_view ,
        .products-list-view .product .product-button > a.button-compare.checked:after,
        .popup-add-to-cart .button-continue-shop:hover,
        .popup-add-to-cart .button-view-cart:hover  
        {ldelim}
            border-color:{$maincolor}!important;
            background-color:{$maincolor}!important;
        {rdelim}      
    
        /* Title block font */
        h1, h2, h3, h4, h5, h6,
        a,
       {ldelim}
            font-family: {$fontname}, sans-serif;
        {rdelim}
    
    </style>
{/if}