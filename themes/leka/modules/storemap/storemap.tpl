
{if $page_name == 'index'}
    <div class="section-p-60px conact-us">
        <div class="tittle">
          <h5>{l s='CONTACT US' mod='storemap'}</h5>
          <p>{l s='Contact us if you have any question.' mod='storemap'}</p>
        </div>
        <div id="map_canvas" class="div_full_width"></div>        
    </div>
{else}
    <div id="map_canvas"></div>  
{/if}
{strip}
{addJsDef defaultLat=$defaultLat}
{addJsDef defaultLong=$defaultLong}
{addJsDef storeName=$storeName}
{/strip}