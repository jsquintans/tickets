{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('nav.tpl', 'blockcontact')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    <a href="javascript:void(0)"><i class="fa fa-phone"></i>{if $telnumber}{$telnumber}{/if}</a> 
    <!--<a href="javascript:void(0)"><i class="fa fa-clock-o"></i> {l s='MON - SAT: 08 am - 17 pm' mod='blockcontact'}</a> -->
    <a href="{if $email != ''}mailto:{$email}{else}#{/if}"><i class="fa fa-envelope-o"></i> {if $email != ''}{$email}{/if}</a>
{/if}