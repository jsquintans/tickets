{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('brandsslider.tpl','brandsslider')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    {if $manufacturers}  
    <!-- Brands slider module --> 
    <div class="section-shopbrand parallax bg-parallax div_full_width">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="title-section text-left">
						{*<h2 class="title">{l s='SHOP BRANDS' mod='brandsslider'}</h2>*}
                        <h2 class="title">{$brand_title}</h2>
					</div>
                    {$brand_desc}
					{*<p>
                        {l s='Contrary to popular belief, Lorem Ipsum is not simply random text. ' mod='brandsslider'}
                        {l s='It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. ' mod='brandsslider'}
                        {l s='Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia ' mod='brandsslider'}
                    </p>*}
				</div>
				<div class="col-sm-8">
                    <div class="list-brand owl-carousel owl-theme owl-loaded" data-nav="true" data-dots="false" data-margin="130" data-responsive='{ldelim}"0":{ldelim}"items":3,"margin":20{rdelim},"600":{ldelim}"items":3,"margin":50{rdelim},"1000":{ldelim}"items":4{rdelim}{rdelim}'>
                    {foreach from=$manufacturers item=manufacturer name=manufacturer_list}
                        <div class="item-brand">
							<a href="{$link->getmanufacturerLink($manufacturer.id_manufacturer, $manufacturer.link_rewrite)|escape:'html'}">
								<div class="logo-brand">
                                     <img class="img-responsive" src="{$img_manu_dir}{$manufacturer.image}" alt="{$manufacturer.name}"/>
                                </div>
								<div class="logo-brand-color"><img class="img-responsive" src="{$img_manu_dir}{$manufacturer.image}" alt="{$manufacturer.name}"/></div>
							</a>
						</div>
                    {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Brands slider module -->
    {/if}
{/if}