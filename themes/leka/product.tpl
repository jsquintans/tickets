{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{include file="$tpl_dir./errors.tpl"}
{if $errors|@count == 0}
	{if !isset($priceDisplayPrecision)}
		{assign var='priceDisplayPrecision' value=2}
	{/if}
	{if !$priceDisplay || $priceDisplay == 2}
		{assign var='productPrice' value=$product->getPrice(true, $smarty.const.NULL, 6)}
		{assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(false, $smarty.const.NULL)}
	{elseif $priceDisplay == 1}
		{assign var='productPrice' value=$product->getPrice(false, $smarty.const.NULL, 6)}
		{assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(true, $smarty.const.NULL)}
	{/if}
<div itemscope itemtype="http://schema.org/Product" class="item-detail-page"> 
	<meta itemprop="url" content="{$link->getProductLink($product)}">
	<div class="primary_block row">
		{if isset($adminActionDisplay) && $adminActionDisplay}
			<div id="admin-action" class="container">
				<p class="alert alert-info">{l s='This product is not visible to your customers.'}
					<input type="hidden" id="admin-action-product-id" value="{$product->id}" />
					<a id="publish_button" class="btn btn-default button button-small" href="#">
						<span>{l s='Publish'}</span>
					</a>
					<a id="lnk_view" class="btn btn-default button button-small" href="#">
						<span>{l s='Back'}</span>
					</a>
				</p>
				<p id="admin-action-result"></p>
			</div>
		{/if}
		{if isset($confirmation) && $confirmation}
			<p class="confirmation">
				{$confirmation}
			</p>
		{/if}
		<!-- left infos--> 
        <div class="col-sm-4 {if $jqZoomEnabled}single-images-wrap{/if}">
            <div class="single-images">
    			<!-- product img-->
    			<div id="image-block">
    				{if $product->new}
    					<span class="new-box hidden">
    						<span class="new-label">{l s='New'}</span>
    					</span>
    				{/if}
    				{if $product->on_sale}
    					<span class="sale-box no-print hidden">
    						<span class="sale-label">{l s='Sale!'}</span>
    					</span>
    				{elseif $product->specificPrice && $product->specificPrice.reduction && $productPriceWithoutReduction > $productPrice}
    					<span class="discount hidden">{l s='Reduced price!'}</span>
    				{/if}
    				{if $have_image}
    					<span id="view_full_size">
    						{if $jqZoomEnabled && $have_image && !$content_only}
    							<a class="jqzoom" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" rel="gal1" href="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'thickbox_default')|escape:'html':'UTF-8'}">
    								<img class="main-image" itemprop="image" src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" alt="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}"/>
    							</a>
    						{else}
    							<img id="bigpic" itemprop="image" src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" alt="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" width="{$largeSize.width}" height="{$largeSize.height}"/>
    							{if !$content_only}
    								<span class="span_link no-print hidden">{l s='View larger'}</span>
    							{/if}
    						{/if}
    					</span>
    				{else}
    					<span id="view_full_size">
    						<img itemprop="image" src="{$img_prod_dir}{$lang_iso}-default-large_default.png" id="bigpic" alt="" title="{$product->name|escape:'html':'UTF-8'}" width="{$largeSize.width}" height="{$largeSize.height}"/>
    						{if !$content_only}
    							<span class="span_link hidden">
    								{l s='View larger'}
    							</span>
    						{/if}
    					</span>
    				{/if}
    			</div> <!-- end image-block -->
    			{if isset($images) && count($images) > 0}
    				<!-- thumbnails -->
    				<div class="single-product-thumbnails">
                        <div id="views_block" class="{if isset($images) && count($images) < 2}hidden{/if}">
        					{if isset($images) && count($images) > 2}
        						<div class="view_scroll_spacer">
        							<a id="view_scroll_left" class="" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
        								{l s='Previous'}
        							</a>
        						</div>
        					{/if}
        					<div id="thumbs_list">
        						<ul id="thumbs_list_frame">
        						{if isset($images)}
        							{foreach from=$images item=image name=thumbnails}
        								{assign var=imageIds value="`$product->id`-`$image.id_image`"}
        								{if !empty($image.legend)}
        									{assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
        								{else}
        									{assign var=imageTitle value=$product->name|escape:'html':'UTF-8'}
        								{/if}
        								<!--<li id="thumbnail_{$image.id_image}"{if $smarty.foreach.thumbnails.last} class="last"{/if}>
        									<a{if $jqZoomEnabled && $have_image && !$content_only} href="javascript:void(0);" rel="{literal}{{/literal}gallery: 'gal1', smallimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'large_default')|escape:'html':'UTF-8'}',largeimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}'{literal}}{/literal}"{else} href="{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}"	data-fancybox-group="other-views" class="fancybox{if $image.id_image == $cover.id_image} shown{/if}"{/if} title="{$imageTitle}">
        										<img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'cart_default')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}"{if isset($cartSize)} height="{$cartSize.height}" width="{$cartSize.width}"{/if} itemprop="image" />
        									</a>
        								</li> -->
        							{/foreach}
        						{/if}
        						</ul>
        					</div> <!-- end thumbs_list -->
        					{if isset($images) && count($images) > 2}
        						<a id="view_scroll_right" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
        							{l s='Next'}
        						</a>
        					{/if}
        				</div> <!-- end views-block -->
                    </div>
    				<!-- end thumbnails -->
    			{/if}
    			{if isset($images) && count($images) > 1}
    				<p class="resetimg clear no-print">
    					<span id="wrapResetImages" style="display: none;">
    						<a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" data-id="resetImages">
    							<i class="icon-repeat"></i>
    							{l s='Display all pictures'}
    						</a>
    					</span>
    				</p>
    			{/if}
            </div>
            <div class='bloque_mapa'>
            	<div id='map_campo' style='height:150px;'></div>
            </div>
			{*$product|@print_r*}
			{literal}

			<!--<script async defer
		      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvtzV3dKDa1J2RXMdgIzFnLG6M2s66u20">
		    </script> -->
			<script type="text/javascript" charset="utf-8" >
			
				jQuery(document).ready(function($) {
					
					map = new google.maps.Map(document.getElementById('map_campo'), {
					    center: {lat: {/literal}{$product->lat}{literal}, lng: {/literal}{$product->lng}{literal}},
					    zoom: 15
					  });

					var marker = new google.maps.Marker({
					    position: {lat: {/literal}{$product->lat}{literal}, lng: {/literal}{$product->lng}{literal}},
					    map: map,
					    title: 'Hello World!'
					  });
				});

			</script>
			{/literal}

		
			{if isset($combinations)}
            <div class='row_slide_precios'>
            	<h6>Price Filter</h6>
            	<div class='contenedor_slider_precio'>
            	<span id="layered_price_range" class="amount-range-price"></span>
				<div class="layered_slider_container">
					<div class="layered_slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" id="layered_price_slider" data-type="price" data-format="2" data-unit="€" aria-disabled="false"></div>
				</div>
				<div class="precio_minimo"></div><div class="precio_maximo"></div>
				</div>
            </div>
            {/if}
		</div> <!-- end pb-left-column -->
			{assign var="maxPrice" value=0 }
			{assign var="minPrice" value=1000 }
			{if isset($combinations)}
			{foreach from=$combinations key=id_combination item=combination}
				
				{if ($combination.price < $minPrice) }
					{assign var="minPrice" value=$combination.price }
				{/if}
				

				{if ($combination.price > $maxPrice) }
					{assign var="maxPrice" value=$combination.price }
				{/if}

			{/foreach}
			{/if}

		{literal}
		 <script type="text/javascript">
		 	
		  $( function() {
		    $( ".layered_slider" ).slider(
		    	{
		    		range: true,
		    		max: {/literal}{$maxPrice}{literal},
		    		min: {/literal}{$minPrice}{literal},
		    		values: [{/literal}{$minPrice}{literal},{/literal}{$maxPrice}{literal}],
		    		slide: function( event, ui ) {
				      //  console.log("cambio");
				        $(".precio_minimo").html(ui.values[ 0 ]+" €");
						$(".precio_maximo").html(ui.values[ 1 ]+" €");

						
								
						$('#lista_combinaciones').each(function() {
						    $("li", this).each(function(i) {
						       //console.log($(this).attr('precio-item'));
						       //console.log('El valor minimo es '+ui.values[0]); 
						       //console.log('El valor maximo es '+ui.values[1]);
						       if($(this).attr('precio-item')>= ui.values[0] && $(this).attr('precio-item') <= ui.values[1]){
						       		$(this).show();
						       		//console.log('EL precio es mayor al minimo y menor al máximo');
						       }else{
						       		$(this).hide('slow', function() {
						       			
						       		});
						       		//console.log('El precio es mayor al maximo ');
						       }
						    });
						});

				      }
				    
		    	});
				//$( "#layered_price_range" ).html(  + $( ".layered_slider" ).slider( "values", 1 ) + " €" );
				$(".precio_minimo").html($( ".layered_slider" ).slider( "values", 0 ) + " €");
				$(".precio_maximo").html($( ".layered_slider" ).slider( "values", 1 ) + " €");
		  } );


		  jQuery(document).ready(function($) {
		  		$(".single_add_to_cart_button").click(function(event) {
					/* Act on the event */

					//console.log(this);
					id_combi = $(this).attr('id_combi_btn');
					cantidad_para_combi = $("#qty_"+id_combi).val();
					//console.log(id_combi);
					//console.log(cantidad_para_combi);
					$('#idCombination').val(id_combi);
					$('#quantity_wanted').val(cantidad_para_combi);
					ajaxCart.add($('#product_page_product_id').val(), $('#idCombination').val(), true, null, $('#quantity_wanted').val(), null);
				});
		  });
				


		  </script>
		{/literal}



		<script type="text/javascript" >
		//console.log('hola');

			jQuery(document).ready(function($) {
				$(document).on('mouseover', '#attributes li', function(){
					//displayImage($(this));
					var capa = $(this);
					//console.log(capa);
					//console.log('hola');
					if (typeof(no_animation) == 'undefined')
						no_animation = false;
					if (capa.attr('url-combio'))
					{
						var new_src = capa.attr('url-combio').replace('thickbox', 'large');
						var new_title = capa.attr('title');
						var new_href = capa.attr('url-combio');
						if ($('#bigpic').attr('src') != new_src)
						{
							$('#bigpic').attr({
								'src' : new_src,
								'alt' : new_title,
								'title' : new_title
							}).load(function(){
								if (typeof(jqZoomEnabled) != 'undefined' && jqZoomEnabled)
									$(this).attr('rel', new_href);
							});
						}
						$('#views_block li a').removeClass('shown');
					//	$(domAAroundImgThumb).addClass('shown');
					}
			});

			});
			
			
			//ajaxCart.add($('#product_page_product_id').val(), $('#idCombination').val(), true, null, $('#quantity_wanted').val(), null);
			
		</script>
		<!-- end left infos-->
		
        <div class="col-sm-8">
            <div class="summary entry-summary">  
				<!--<h1 class="product_title entry-title">{$product->name|escape:'html':'UTF-8'}</h1> -->
                {if isset($HOOK_EXTRA_RIGHT) && $HOOK_EXTRA_RIGHT}{$HOOK_EXTRA_RIGHT}{/if}
                
                {if $product->description_short}
					<div class="short_description_block description">
						{$product->description_short} 
					</div> <!-- end short_description_block -->
				{/if}
        		 
				<span class="code hidden" itemprop="sku"{if !empty($product->reference) && $product->reference} content="{$product->reference}{/if}">PRODUCT CODE: SKU: {$product->reference|escape:'html':'UTF-8'}</span>
				
                
				<div class="variations_form ">
    				    {if isset($groups)}
    					<!-- attributes -->
    					<div id="attributes"> 
    						<div class="clearfix"></div>
    							{foreach from=$groups key=id_attribute_group item=group}
    								{if isset($group.attributes) && $group.attributes|@count} 


    									<fieldset class="attribute_fieldset item-select">
    										<p class="hidden" {if $group.group_type != 'color' && $group.group_type != 'radio'}for="group_{$id_attribute_group|intval}"{/if}>*{$group.name|escape:'html':'UTF-8'}&nbsp;</p>
    										{assign var="groupName" value="group_$id_attribute_group"}
    										<div class="attribute_list">
    											
    											{if ($group.group_type == 'radio')}
													
													<div class='row row-head-combis'>
														<div class='col-md-5'>Category</div>
														<div class='col-md-1'><span class='span-asientos'>Seats</span></div>
														<div class='col-md-2'>Quantity</div>
														<div class='col-md-2'>Price</div>
														<div class='col-md-2'></div>

													</div>


    												 {* BUCLE DE LAS COMBINACIONES DEL PRODUCTO *}
													
													{*$combinations|@print_r*}

    												 {if  !empty($combinations)}
    												<ul id='lista_combinaciones'>
													{foreach from=$combinations key=id_combination item=combination}
														<li data-combi='{$id_combination}' url-combio="{$link->getImageLink($product->link_rewrite, $combination.id_image, 'thickbox_default')|escape:'html':'UTF-8'}" precio-item='{$combination.price}'>
    															<div class='row fila-combi-asiento' style='width:100%;'>
																	<div class='col-xs-9 col-md-5'>
																		<input type="radio" style='display:none;' id="check_combi_{$combination.attributes[0]}" class="attribute_radio" name="{$groupName|escape:'html':'UTF-8'}" value="{$combination.attributes[0]}" {*if ($group.default == $id_attribute)*} checked="checked"{*/if*} />
    																	<span class='nombre_combi'>{$combination.attributes_values[1]|escape:'html':'UTF-8'}</span>
    																	<span class='descrip_combi'>{$combination.descripcion_corta}</span>
																	</div>	
																	<div class='col-xs-3  col-md-1'>
																		{if isset($combination.attributes_values[6])}
																				{if $combination.attributes_values[6] == "Single" }
																					<img src='/tickets/img/Layer 81.png' class='img-seat-mid' />
																				{else}
																					<img src='/tickets/img/Layer 81.png' class='img-seat' />
																					<img src='/tickets/img/Layer 81.png' class='img-seat img-seat-2' />
																				{/if}
																			{else}
																				<img src='/tickets/img/Layer 81.png' class='img-seat' />
																				<img src='/tickets/img/Layer 81.png' class='img-seat img-seat-2' />

																			{/if}
																	</div>
																	<div class='col-xs-6 col-xs-offset-1 col-md-2 col-lg-offset-0 col-cantidad-producto'>
																		<div class='contenedor-select-combinacion'>
																		{if $combination.quantity > 0}
																			<select class="form-control attribute_select" id='qty_{$id_combination}'>{for $foo=1 to $combination.quantity }<option value='{$foo}'>{$foo}</option>{/for}</select></div>
																		<div class='contenedor-cantidad-total-combinacion'> of {$combination.quantity}
																		{/if}
																		</div>
																	</div>
																	<div class='col-xs-3 col-xs-offset-2 col-md-2 col-lg-offset-0 col-precio-producto'>
																		<span class='precio_combi'>{convertPrice price=$combination.price|floatval }</span>
																	</div>
																	<div class='col-xs-12 col-md-2 col-btn-comprar-producto'>
																		{if $combination.quantity > 0}
																		<button type="button" name="add_cart" id='add_cart' class="exclusive single_add_to_cart_button" id_combi_btn="{$id_combination}"> 
								    										{if $content_only && (isset($product->customization_required) && $product->customization_required)}
								    											{l s='Customize'}
								    										{else}
								    											{l s='Add to cart'}
								    										{/if}
								    									</button>
								    									{else}
								    										<span class='agotado'>SOLD OUT</span>
								    									{/if}
																	</div>
    															
    															</div>
    														</li>
													{/foreach}
    												</ul>
													{else}
														<div class='alert'>
															No tickets for this match
														</div>
													{/if}
		
    											{/if}
    										</div> <!-- end attribute_list -->
    									</fieldset>  
    								{/if}
    							{/foreach}
    						</div> <!-- end attributes -->
    					{else}
							<div class='alert'>
								No tickets for this match
							</div>
    					{/if}  
    	                <div class="single_variation_wrap"> 
        					{if !$PS_CATALOG_MODE}
        					<div id="quantity_wanted_p" style='display:none;' class="box-qty" {if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
    							<a href="#" data-field-qty="qty" class="quantity-plus button-plus product_quantity_up">
    								<i class="fa fa-angle-up"></i>
    							</a>
                                <input type="text" {*min="1"*} name="qty" id="quantity_wanted" class="input-text qty text" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" />
    							<a href="#" data-field-qty="qty" class="quantity-minus button-minus product_quantity_down">
    								<i class="fa fa-angle-down"></i>
    							</a>
    						</div>
        					{/if}
        					{if ($product->show_price && !isset($restricted_country_mode)) || (isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS)}
        						<!-- add to cart form-->
        						<form id="buy_block"{if $PS_CATALOG_MODE && !isset($groups) && $product->quantity > 0} class="hidden"{/if} action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
        							<!-- hidden datas -->
        							<p class="hidden">
        								<input type="hidden" name="token" value="{$static_token}" />
        								<input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id" />
        								<input type="hidden" name="add" value="1" />
        								<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
        							</p> 
    								<div id="add_to_cart" {if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || (isset($restricted_country_mode) && $restricted_country_mode) || $PS_CATALOG_MODE} class="unvisible"{/if}>
    									<button type="submit" name="Submit" style='display:none;' class="exclusive single_add_to_cart_button"> 
    										{if $content_only && (isset($product->customization_required) && $product->customization_required)}
    											{l s='Customize'}
    										{else}
    											{l s='Add to cart'}
    										{/if}
    									</button>
    								</div>  
        							{if isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS}{$HOOK_PRODUCT_ACTIONS}{/if}
        							
        						</form>
        					{/if}
    	                </div>  
                </div>   
                {*}
                <div class="sigle-product-services">
                    <div class="services-item">
                        <div class="icon"><i class="fa fa-plane"></i></div>
                        <h5 class="service-name">FREE SHIPPING WORLD WIDE</h5>
                    </div>
                    <div class="services-item">
                        <div class="icon"><i class="fa fa-whatsapp"></i></div>
                        <h5 class="service-name">24/24 ONLINE SUPPORT CUSTOME</h5>
                    </div>
                    <div class="services-item">
                        <div class="icon"><i class="fa fa-usd"></i></div>
                        <h5 class="service-name">30 Days money back</h5>
                    </div>
                </div>
                {*}
                {hook h='CustomHtml8'}
                {if $HOOK_EXTRA_LEFT}{$HOOK_EXTRA_LEFT}{/if}
				
                <!-- Out of stock hook -->
				<div id="oosHook"{if $product->quantity > 0} style="display: none;"{/if}>
					{$HOOK_PRODUCT_OOS}
				</div>
    			</div>
            </div>
           {if isset($product) && $product->description}
            <div class='col-lg-12 col-sm-12 col-xs-12 row-descripcion-producto'>
            	<h2 class='titular-descripcion'>Information about {$product->name}</h2>
            	<div class='col-lg-12 col-sm-12 col-xs-12 pos-descripcion-producto'>
            		 {$product->description}
            	</div>
            </div>
            {/if}
       {if !$content_only}
              	 <!--======= PRODUCT DESCRIPTION =========-->
    	        <div class="product-tabs col-sm-12"> 

    	           <!-- <ul class="nav-tab" >
      					{if isset($product) && $product->description}
    					<li ><a href="#description" data-toggle="tab">{l s='Product Description'}</a></li>
    					{/if}
    					{if (isset($quantity_discounts) && count($quantity_discounts) > 0)}
    					<li ><a href="#quantityDiscount"  data-toggle="tab">{l s='Volume discounts'}</a></li>
    					{/if}
    					{if isset($features) && $features}
    					<li ><a href="#features"  data-toggle="tab">{l s='Data sheet'}</a></li>
    					{/if}
    
    					{if isset($packItems) && $packItems|@count > 0}
    					<li ><a href="#packItems"  data-toggle="tab">{l s='Pack content'}</a></li>
    					{/if}
    					{if isset($HOOK_PRODUCT_TAB)}
    					{$HOOK_PRODUCT_TAB}
    					{/if}
    
    					{if isset($accessories) && $accessories}
    					<li ><a href="#accessories"  data-toggle="tab">{l s='Accessories'}</a></li>
    					{/if}
    
    					{if (isset($product) && $product->description) || (isset($features) && $features) || (isset($accessories) && $accessories) || (isset($HOOK_PRODUCT_TAB) && $HOOK_PRODUCT_TAB) || (isset($attachments) && $attachments) || isset($product) && $product->customizable}
    					{if isset($attachments) && $attachments}
    					    <li ><a href="#attachments" data-toggle="tab">{l s='Download'}</a></li>
    					{/if}
    					{/if}
    					{if isset($product) && $product->customizable}
    					<li ><a href="#customizable" data-toggle="tab">{l s='Product customization'}</a></li>
    					{/if}
    	            </ul> -->
    	          <!-- Tab panes -->
    	         <!-- <div class="tab-content"> 
    	            {if isset($product) && $product->description}
    			        <div role="tabpanel" class="tab-pane" id="description">
    			            <div  class="rte">{$product->description}</div>
    			        </div>
    			    {/if}
    			    {if (isset($quantity_discounts) && count($quantity_discounts) > 0)}
    			        <div role="tabpanel" class="tab-pane" id="quantityDiscount">
    						<table class="std table-product-discounts">
    							<thead>
    								<tr>
    									<th>{l s='Quantity'}</th>
    									<th>{if $display_discount_price}{l s='Price'}{else}{l s='Discount'}{/if}</th>
    									<th>{l s='You Save'}</th>
    								</tr>
    							</thead>
    							<tbody>
    								{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
    								<tr id="quantityDiscount_{$quantity_discount.id_product_attribute}" class="quantityDiscount_{$quantity_discount.id_product_attribute}" data-discount-type="{$quantity_discount.reduction_type}" data-discount="{$quantity_discount.real_value|floatval}" data-discount-quantity="{$quantity_discount.quantity|intval}">
    									<td>
    										{$quantity_discount.quantity|intval}
    									</td>
    									<td>
    										{if $quantity_discount.price >= 0 || $quantity_discount.reduction_type == 'amount'}
    											{if $display_discount_price}
    												{if $quantity_discount.reduction_tax == 0 && !$quantity_discount.price}
    													{convertPrice price = $productPriceWithoutReduction|floatval-($productPriceWithoutReduction*$quantity_discount.reduction_with_tax)|floatval}
    												{else}
    													{convertPrice price=$productPriceWithoutReduction|floatval-$quantity_discount.real_value|floatval}
    												{/if}
    											{else}
    												{convertPrice price=$quantity_discount.real_value|floatval}
    											{/if}
    										{else}
    											{if $display_discount_price}
    												{if $quantity_discount.reduction_tax == 0}
    													{convertPrice price = $productPriceWithoutReduction|floatval-($productPriceWithoutReduction*$quantity_discount.reduction_with_tax)|floatval}
    												{else}
    													{convertPrice price = $productPriceWithoutReduction|floatval-($productPriceWithoutReduction*$quantity_discount.reduction)|floatval}
    												{/if}
    											{else}
    												{$quantity_discount.real_value|floatval}%
    											{/if}
    										{/if}
    									</td>
    									<td>
    										<span>{l s='Up to'}</span>
    										{if $quantity_discount.price >= 0 || $quantity_discount.reduction_type == 'amount'}
    											{$discountPrice=$productPriceWithoutReduction|floatval-$quantity_discount.real_value|floatval}
    										{else}
    											{$discountPrice=$productPriceWithoutReduction|floatval-($productPriceWithoutReduction*$quantity_discount.reduction)|floatval}
    										{/if}
    										{$discountPrice=$discountPrice * $quantity_discount.quantity}
    										{$qtyProductPrice=$productPriceWithoutReduction|floatval * $quantity_discount.quantity}
    										{convertPrice price=$qtyProductPrice - $discountPrice}
    									</td>
    								</tr>
    								{/foreach}
    							</tbody>
    						</table>
    			        </div>
    			    {/if}
    			    {if isset($features) && $features}
    			        <div role="tabpanel" class="tab-pane" id="features">
    			            <table class="table-data-sheet">
    							{foreach from=$features item=feature}
    							<tr class="{cycle values="odd,even"}">
    								{if isset($feature.value)}
    								<td>{$feature.name|escape:'html':'UTF-8'}</td>
    								<td>{$feature.value|escape:'html':'UTF-8'}</td>
    								{/if}
    							</tr>
    							{/foreach}
    						</table>
    			        </div>
    			    {/if}
    			 
    			    {if isset($packItems) && $packItems|@count > 0}
    			        <div role="tabpanel" class="tab-pane" id="packItems">
    			            {include file="$tpl_dir./product-list.tpl" products=$packItems}
    			        </div>
    			    {/if}
    			    {if isset($HOOK_PRODUCT_TAB_CONTENT) && $HOOK_PRODUCT_TAB_CONTENT}
    			        {$HOOK_PRODUCT_TAB_CONTENT}
    			    {/if}
    			    
    			    {if isset($accessories) && $accessories}
    			        <div role="tabpanel" class="tab-pane" id="accessories">
    			            <div class="block products_block accessories-block clearfix">
    								<div class="block_content">
    									<ul id="bxslider-accessories" class="clearfix">
    										{foreach from=$accessories item=accessory name=accessories_list}
    											{if ($accessory.allow_oosp || $accessory.quantity_all_versions > 0 || $accessory.quantity > 0) && $accessory.available_for_order && !isset($restricted_country_mode)}
    												{assign var='accessoryLink' value=$link->getProductLink($accessory.id_product, $accessory.link_rewrite, $accessory.category)}
    												<li class="item product-box ajax_block_product{if $smarty.foreach.accessories_list.first} first_item{elseif $smarty.foreach.accessories_list.last} last_item{else} item{/if} product_accessories_description">
    													<div class="product_desc">
    														<a href="{$accessoryLink|escape:'html':'UTF-8'}" title="{$accessory.legend|escape:'html':'UTF-8'}" class="product-image product_image">
    															<img class="lazyOwl" src="{$link->getImageLink($accessory.link_rewrite, $accessory.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$accessory.legend|escape:'html':'UTF-8'}" width="{$homeSize.width}" height="{$homeSize.height}"/>
    														</a>
    														<div class="block_description">
    															<a href="{$accessoryLink|escape:'html':'UTF-8'}" title="{l s='More'}" class="product_description">
    																{$accessory.description_short|strip_tags|truncate:25:'...'}
    															</a>
    														</div>
    													</div>
    													<div class="s_title_block">
    														<h5 itemprop="name" class="product-name">
    															<a href="{$accessoryLink|escape:'html':'UTF-8'}">
    																{$accessory.name|truncate:20:'...':true|escape:'html':'UTF-8'}
    															</a>
    														</h5>
    														{if $accessory.show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
    														<span class="price">
    															{if $priceDisplay != 1}
    															{displayWtPrice p=$accessory.price}{else}{displayWtPrice p=$accessory.price_tax_exc}
    															{/if}
    														</span>
    														{/if}
    													</div>
    													<div class="clearfix" style="margin-top:5px">
    														{if !$PS_CATALOG_MODE && ($accessory.allow_oosp || $accessory.quantity > 0)}
    															<div class="no-print">
    																<a class="exclusive button ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$accessory.id_product|intval}&amp;token={$static_token}&amp;add")|escape:'html':'UTF-8'}" data-id-product="{$accessory.id_product|intval}" title="{l s='Add to cart'}">
    																	<span>{l s='Add to cart'}</span>
    																</a>
    															</div>
    														{/if}
    													</div>
    												</li>
    											{/if}
    										{/foreach}
    									</ul>
    								</div>
    							</div>
    			        </div>
    			    {/if}
    			    {if (isset($product) && $product->description) || (isset($features) && $features) || (isset($accessories) && $accessories) || (isset($HOOK_PRODUCT_TAB) && $HOOK_PRODUCT_TAB) || (isset($attachments) && $attachments) || isset($product) && $product->customizable}
    			        {if isset($attachments) && $attachments}
    			            <div role="tabpanel" class="tab-pane" id="attachments">
    			                {foreach from=$attachments item=attachment name=attachements}
    								{if $smarty.foreach.attachements.iteration %3 == 1}<div class="row">{/if}
    									<div class="col-lg-4">
    										<h4><a href="{$link->getPageLink('attachment', true, NULL, "id_attachment={$attachment.id_attachment}")|escape:'html':'UTF-8'}">{$attachment.name|escape:'html':'UTF-8'}</a></h4>
    										<p class="text-muted">{$attachment.description|escape:'html':'UTF-8'}</p>
    										<a class="btn btn-default btn-block" href="{$link->getPageLink('attachment', true, NULL, "id_attachment={$attachment.id_attachment}")|escape:'html':'UTF-8'}">
    											<i class="icon-download"></i>
    											{l s="Download"} ({Tools::formatBytes($attachment.file_size, 2)})
    										</a>
    										<hr />
    									</div>
    								{if $smarty.foreach.attachements.iteration %3 == 0 || $smarty.foreach.attachements.last}</div>{/if}
    							{/foreach}
    			            </div>
    			        {/if}
    			        {if isset($product) && $product->customizable}
    			            <div role="tabpanel" class="tab-pane" id="customizable">
    			                <form method="post" action="{$customizationFormTarget}" enctype="multipart/form-data" id="customizationForm" class="clearfix">
    								<p class="infoCustomizable">
    									{l s='After saving your customized product, remember to add it to your cart.'}
    									{if $product->uploadable_files}
    									<br />
    									{l s='Allowed file formats are: GIF, JPG, PNG'}{/if}
    								</p>
    								{if $product->uploadable_files|intval}
    									<div class="customizableProductsFile">
    										<h5 class="product-heading-h5">{l s='Pictures'}</h5>
    										<ul id="uploadable_files" class="clearfix">
    											{counter start=0 assign='customizationField'}
    											{foreach from=$customizationFields item='field' name='customizationFields'}
    												{if $field.type == 0}
    													<li class="customizationUploadLine{if $field.required} required{/if}">{assign var='key' value='pictures_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
    														{if isset($pictures.$key)}
    															<div class="customizationUploadBrowse">
    																<img src="{$pic_dir}{$pictures.$key}_small" alt="" />
    																	<a href="{$link->getProductDeletePictureLink($product, $field.id_customization_field)|escape:'html':'UTF-8'}" title="{l s='Delete'}" >
    																		<img src="{$img_dir}icon/delete.gif" alt="{l s='Delete'}" class="customization_delete_icon" width="11" height="13" />
    																	</a>
    															</div>
    														{/if}
    														<div class="customizationUploadBrowse form-group">
    															<label class="customizationUploadBrowseDescription">
    																{if !empty($field.name)}
    																	{$field.name}
    																{else}
    																	{l s='Please select an image file from your computer'}
    																{/if}
    																{if $field.required}<sup>*</sup>{/if}
    															</label>
    															<input type="file" name="file{$field.id_customization_field}" id="img{$customizationField}" class="form-control customization_block_input {if isset($pictures.$key)}filled{/if}" />
    														</div>
    													</li>
    													{counter}
    												{/if}
    											{/foreach}
    										</ul>
    									</div>
    								{/if}
    								{if $product->text_fields|intval}
    									<div class="customizableProductsText">
    										<h5 class="product-heading-h5">{l s='Text'}</h5>
    										<ul id="text_fields">
    										{counter start=0 assign='customizationField'}
    										{foreach from=$customizationFields item='field' name='customizationFields'}
    											{if $field.type == 1}
    												<li class="customizationUploadLine{if $field.required} required{/if}">
    													<label for ="textField{$customizationField}">
    														{assign var='key' value='textFields_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
    														{if !empty($field.name)}
    															{$field.name}
    														{/if}
    														{if $field.required}<sup>*</sup>{/if}
    													</label>
    													<textarea name="textField{$field.id_customization_field}" class="form-control customization_block_input" id="textField{$customizationField}" rows="3" cols="20">{strip}
    														{if isset($textFields.$key)}
    															{$textFields.$key|stripslashes}
    														{/if}
    													{/strip}</textarea>
    												</li>
    												{counter}
    											{/if}
    										{/foreach}
    										</ul>
    									</div>
    								{/if}
    								<p id="customizedDatas">
    									<input type="hidden" name="quantityBackup" id="quantityBackup" value="" />
    									<input type="hidden" name="submitCustomizedDatas" value="1" />
    									<button class="button btn btn-default button button-small" name="saveCustomization">
    										<span>{l s='Save'}</span>
    									</button>
    									<span id="ajax-loader" class="unvisible">
    										<img src="{$img_ps_dir}loader.gif" alt="loader" />
    									</span>
    								</p>
    							</form>
    							<p class="clear required"><sup>*</sup> {l s='required fields'}</p>
    			            </div>
    			        {/if}
    			    {/if}
    	            
    	            
    	          </div> -->
    	{*}</div>{*}
        </div> 
		{/if}
	</div> <!-- end primary_block -->
	{if !$content_only}
		{if isset($HOOK_PRODUCT_FOOTER) && $HOOK_PRODUCT_FOOTER}{$HOOK_PRODUCT_FOOTER}{/if}
	{/if}
</div> <!-- itemscope product wrapper -->
{strip}
{if isset($smarty.get.ad) && $smarty.get.ad}
	{addJsDefL name=ad}{$base_dir|cat:$smarty.get.ad|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{if isset($smarty.get.adtoken) && $smarty.get.adtoken}
	{addJsDefL name=adtoken}{$smarty.get.adtoken|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{addJsDef allowBuyWhenOutOfStock=$allow_oosp|boolval}
{addJsDef availableNowValue=$product->available_now|escape:'quotes':'UTF-8'}
{addJsDef availableLaterValue=$product->available_later|escape:'quotes':'UTF-8'}
{addJsDef attribute_anchor_separator=$attribute_anchor_separator|escape:'quotes':'UTF-8'}
{addJsDef attributesCombinations=$attributesCombinations}
{addJsDef currentDate=$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}
{if isset($combinations) && $combinations}
	{addJsDef combinations=$combinations}
	{addJsDef combinationsFromController=$combinations}
	{addJsDef displayDiscountPrice=$display_discount_price}
	{addJsDefL name='upToTxt'}{l s='Up to' js=1}{/addJsDefL}
{/if}
{if isset($combinationImages) && $combinationImages}
	{addJsDef combinationImages=$combinationImages}
{/if}
{addJsDef customizationId=$id_customization}
{addJsDef customizationFields=$customizationFields}
{addJsDef default_eco_tax=$product->ecotax|floatval}
{addJsDef displayPrice=$priceDisplay|intval}
{addJsDef ecotaxTax_rate=$ecotaxTax_rate|floatval}
{if isset($cover.id_image_only)}
	{addJsDef idDefaultImage=$cover.id_image_only|intval}
{else}
	{addJsDef idDefaultImage=0}
{/if}
{addJsDef img_ps_dir=$img_ps_dir}
{addJsDef img_prod_dir=$img_prod_dir}
{addJsDef id_product=$product->id|intval}
{addJsDef jqZoomEnabled=$jqZoomEnabled|boolval}
{addJsDef maxQuantityToAllowDisplayOfLastQuantityMessage=$last_qties|intval}
{addJsDef minimalQuantity=$product->minimal_quantity|intval}
{addJsDef noTaxForThisProduct=$no_tax|boolval}
{if isset($customer_group_without_tax)}
	{addJsDef customerGroupWithoutTax=$customer_group_without_tax|boolval}
{else}
	{addJsDef customerGroupWithoutTax=false}
{/if}
{if isset($group_reduction)}
	{addJsDef groupReduction=$group_reduction|floatval}
{else}
	{addJsDef groupReduction=false}
{/if}
{addJsDef oosHookJsCodeFunctions=Array()}
{addJsDef productHasAttributes=isset($groups)|boolval}
{addJsDef productPriceTaxExcluded=($product->getPriceWithoutReduct(true)|default:'null' - $product->ecotax)|floatval}
{addJsDef productPriceTaxIncluded=($product->getPriceWithoutReduct(false)|default:'null' - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcluded=($product->getPrice(false, null, 6, null, false, false) - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcl=($product->getPrice(false, null, 6, null, false, false)|floatval)}
{addJsDef productBasePriceTaxIncl=($product->getPrice(true, null, 6, null, false, false)|floatval)}
{addJsDef productReference=$product->reference|escape:'html':'UTF-8'}
{addJsDef productAvailableForOrder=$product->available_for_order|boolval}
{addJsDef productPriceWithoutReduction=$productPriceWithoutReduction|floatval}
{addJsDef productPrice=$productPrice|floatval}
{addJsDef productUnitPriceRatio=$product->unit_price_ratio|floatval}
{addJsDef productShowPrice=(!$PS_CATALOG_MODE && $product->show_price)|boolval}
{addJsDef PS_CATALOG_MODE=$PS_CATALOG_MODE}
{if $product->specificPrice && $product->specificPrice|@count}
	{addJsDef product_specific_price=$product->specificPrice}
{else}
	{addJsDef product_specific_price=array()}
{/if}
{if $display_qties == 1 && $product->quantity}
	{addJsDef quantityAvailable=$product->quantity}
{else}
	{addJsDef quantityAvailable=0}
{/if}
{addJsDef quantitiesDisplayAllowed=$display_qties|boolval}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'percentage'}
	{addJsDef reduction_percent=$product->specificPrice.reduction*100|floatval}
{else}
	{addJsDef reduction_percent=0}
{/if}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'amount'}
	{addJsDef reduction_price=$product->specificPrice.reduction|floatval}
{else}
	{addJsDef reduction_price=0}
{/if}
{if $product->specificPrice && $product->specificPrice.price}
	{addJsDef specific_price=$product->specificPrice.price|floatval}
{else}
	{addJsDef specific_price=0}
{/if}
{addJsDef specific_currency=($product->specificPrice && $product->specificPrice.id_currency)|boolval} {* TODO: remove if always false *}
{addJsDef stock_management=$PS_STOCK_MANAGEMENT|intval}
{addJsDef taxRate=$tax_rate|floatval}
{addJsDefL name=doesntExist}{l s='This combination does not exist for this product. Please select another combination.' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMore}{l s='This product is no longer in stock' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMoreBut}{l s='with those attributes but is available with others.' js=1}{/addJsDefL}
{addJsDefL name=fieldRequired}{l s='Please fill in all the required fields before saving your customization.' js=1}{/addJsDefL}
{addJsDefL name=uploading_in_progress}{l s='Uploading in progress, please be patient.' js=1}{/addJsDefL}
{addJsDefL name='product_fileDefaultHtml'}{l s='No file selected' js=1}{/addJsDefL}
{addJsDefL name='product_fileButtonHtml'}{l s='Choose File' js=1}{/addJsDefL}
{/strip}
{/if}
