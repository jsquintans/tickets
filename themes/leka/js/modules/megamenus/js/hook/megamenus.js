
var is_mobile = false, headerClone = false, windowWidth = $(window).width();
jQuery(function($){		
    is_mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? !0 : !1;
    if(is_mobile == true){
    	$(".desktop-nav .menu-parent > a").attr("href", "#");
    }
    
    /*
    $(document).on('click', '.megamenus .active',function(e){
    	$(this).removeClass('active');
    	//e.preventDefault();
    });
    */ 
	$("ul.menu li").hoverIntent({		
        over: function() {
            $(this).addClass("active")
        },
        out: function() {
            $(this).removeClass("active")
        },
        selector: "li",
        timeout: 145,
        interval: 55
    });
    /*
    $(".widget").hoverIntent({		
        over: function() {
            $(this).addClass("active")
        },
        out: function() {
            $(this).removeClass("active")
        },
        
        timeout: 145,
        interval: 55
    }); 
    */   
   	responsiveHorizontalMenu();
  	responsiveVerticalMenu();
   	megamenusSetup();
   	var url = window.location.href;
	$('.megamenus a[href="'+url+'"]').parent().addClass('actived');   
	     
	
    stickyMenu();
    /*
    if($(".horizontal-top-megamenu").length > 0){
    	$(".sticky-header").addClass($(".horizontal-top-megamenu").data('background'));
    	$(".sticky-header").addClass($(".horizontal-top-megamenu").data('color'));
    }
	*/
    $(document).on('click','.vertical-toggle-menu',function(){
        $(this).closest('.megamenus').find('.menu').slideToggle("slow");
        return false;
    })
    

    //setMenuActived();
});
function setMenuActived() {
    var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);
    $(".megamenus a").each(function () {
        var href = $(this).attr('href');
        if (path.substring(0, href.length) === href) {
            $(this).closest('li').addClass('actived');
        }
    });
}

$.event.special.debouncedresize ? $(window).on("debouncedresize", function() {
    if($(window).width() != windowWidth){
		$(".megamenu").removeAttr('style');
		windowWidth = $(window).width();
		megamenusSetup();
	}
    destroyStickyHeader();
    if($(".option4 #mobile-search").length >0){
	    if (windowWidth > 991) {
	    	$('.option4 .desktop-search' ).appendTo($(".option4 .main-menus"));
	    }else{
	    	$('.option4 .desktop-search' ).appendTo($(".option4 #mobile-search"));
	    }	
    }
    
}): $(window).on("resize", function() {
	if($(window).width() != windowWidth){
		$(".megamenu").removeAttr('style');
		windowWidth = $(window).width();
		megamenusSetup();
	}
    destroyStickyHeader();    
    if (windowWidth > 991) {
    	if($('.option4 #mobile-search .desktop-search').length >0){
    		$('.option4 #mobile-search .desktop-search').appendTo($(".option4 .main-menus"));	
    	}	    	
    }else{
    	if($('.option4 .main-menus .desktop-search').length >0){
    		$('.option4 .main-menus .desktop-search').appendTo($(".option4 #mobile-search"));	
    	}
    }	
});
    
function responsiveHorizontalMenu(){
	var b = $(".nav-horizontal-top-megamenu ").find(".horizontal-top-megamenu").clone(!0).removeClass("menu clearfix desktop-nav").addClass("responsive-menu"), c = $(".nav-horizontal-top-megamenu ").find(".responsive-menu-container");
    c.append(b), c.find('.menu-group-module, .megamenus-group-products').addClass('megamenu'), c.find("li, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-6, .col-md-2, .col-md-3, .col-md-4, .col-md-7, .col-md-8, .col-md-9, .col-md-10").each(function() {
        var b = $(this);
        b.hasClass("megamenu-container") && b.removeClass("megamenu-container"), b.has("ul, .megamenu").prepend('<span class="menu-btn-wrapper"><span class="menu-btn"></span></span>')
    }), $(".nav-horizontal-top-megamenu .menu-btn-wrapper").on("click", function(b) {
        var c = $(this);
        c.hasClass("active") ? $(this).closest("li, div").removeClass("open").end().removeClass("active").siblings("ul, .megamenu").slideUp("800") : $(this).closest("li, div").addClass("open").end().addClass("active").siblings("ul, .megamenu").slideDown("800"), b.preventDefault()
    }), $(".nav-horizontal-top-megamenu  .responsive-btn").on("click", function(b) {
        var c = $(this), d = $(".nav-horizontal-top-megamenu .responsive-menu-container");
        c.hasClass("active") ? d.slideUp(300, function() {
            c.removeClass("active")
        }): d.slideDown(300, function() {
            c.addClass("active")
        }), b.preventDefault()
    })
}
function responsiveVerticalMenu(){
	var b = $(".nav-vertical-left-megamenu").find(".vertical-left-megamenu").clone(!0).removeClass("menu clearfix desktop-nav vertical-left-megamenu").addClass("responsive-menu"), c = $(".nav-vertical-left-megamenu").find(".responsive-menu-container");
    c.append(b), c.find('.menu-group-module, .megamenus-group-products').addClass('megamenu'), c.find("li, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-6, .col-md-2, .col-md-3, .col-md-4, .col-md-7, .col-md-8, .col-md-9, .col-md-10").each(function() {
        var b = $(this);
        b.hasClass("megamenu-container") && b.removeClass("megamenu-container"), b.has("ul, .megamenu").prepend('<span class="menu-btn-wrapper"><span class="menu-btn"></span></span>')
    }), $(".nav-vertical-left-megamenu .menu-btn-wrapper").on("click", function(b) {
        var c = $(this);
        c.hasClass("active") ? $(this).closest("li, div").removeClass("open").end().removeClass("active").siblings("ul, .megamenu").slideUp("800") : $(this).closest("li, div").addClass("open").end().addClass("active").siblings("ul, .megamenu").slideDown("800"), b.preventDefault()
    }), $(".nav-vertical-left-megamenu .responsive-btn").on("click", function(b) {
        var c = $(this), d = $(".nav-vertical-left-megamenu .responsive-menu-container");
        c.hasClass("active") ? d.slideUp(300, function() {
            c.removeClass("active")
        }): d.slideDown(300, function() {
            c.addClass("active")
        }), b.preventDefault()
    })
}
function containerWidth() {
    var $ = jQuery;
    var b = $(".container").width(), a=$(window).width();
    $('.container_width').each(function(){
        $(this).css({
            'left': - (a - b)/2,
            'width': b,
            'visibility': 'visible'
        });
    });
}
function fullWidth() {
    var $ = jQuery;
    var b = $(window).width();           
    $('.full_width').each(function(){
        $(this).css({
            'width': b,
            'visibility': 'visible'
        });
    });
}

function megamenusSetup(){
	// vertical setup
	if($(".nav-vertical-left-megamenu").length >0){
        $(".nav-vertical-left-megamenu").each(function(){		
            var a = $(".container").actual('width') , b = $(this).actual('width'), c = a-b;			
			if(windowWidth >= 992){
				var c = a-b;
				$(this).find('.megamenu').each(function (){
					$(this).css({'width': c});	
				});				
			}            
        });
    }
}
function stickyMenu() {	
    if (windowWidth > 991) {
        var b = this, c = $("#sticky-header");
        
        if (!headerClone) {
        	/*
        	alert("aaaa");
            var d = $('[data-clone="sticky"]').clone(!0);
            c.append(d).find("#responsive-nav").remove(), headerClone = !0
            */
        }
        $.fn.waypoint && $('[data-fixed="fixed"]').waypoint("sticky", {
            stuckClass: "fixed",
            handler: function(direction) {
            	if(direction == 'up'){
            		$(".sticky-item").each(function(index) {
            			var b = $(this).data('sticky');
            			var id = $(this).attr('id')
						$('#'+id+' > div, #'+id+' > ul, #'+id+' > nav' ).appendTo($("."+b));
					});	
            	}else{
            		$(".sticky-item").each(function(index) {
            			var b = $(this).data('sticky');
						$('.'+b+' > div, .'+b+' > ul, .'+b+' > nav' ).appendTo($(this));
					});
            		
            	}
			    
			},
            offset: -400
        });
        
    }
}
function destroyStickyHeader() {
	if($.fn.waypoint && windowWidth < 992){
		$('[data-fixed="fixed"]').waypoint("unsticky");
	}else{
		stickyMenu();
	}
     
}