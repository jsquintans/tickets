/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
//global variables
var responsiveflag = false;
$(window).scroll(function () {
    //top_column
    if($(window).width() >= 768){
      if ($(this).scrollTop() > 50) {
        if (!$('.header-container').hasClass('menuontop')){
            $('.header-container').addClass("menuontop");
        }
      }else {
        if ($('.header-container').hasClass('menuontop')){
        $('.header-container').removeClass('menuontop');
        }
      }

      if ($(this).scrollTop() > 98) {
        if (!$('#topcolumn').hasClass('ontop')){
            $('#topcolumn').addClass("ontop");
        }
      }else {
        if ($('#topcolumn').hasClass('ontop')){
        $('#topcolumn').removeClass('ontop');
        }
      }
    }else{
        if ($('.header-container').hasClass('menuontop')){
        $('.header-container').removeClass('menuontop');
        }
        if ($('#topcolumn').hasClass('ontop')){
        $('#topcolumn').removeClass('ontop');
        }
    }
});
$(window).resize(function(){
    //div_full_width();
    full_height();
    div_full_width_index();
    /* Leka: Menu mobile */
    if($(window).width() <= 767){
        var w2 = $(window).width()-30;
        $('.index1 #topmenu').width($('#columns').width());
        $('.index2 #topmenu').width(w2);
        $('.index3 #topmenu').width($('#columns').width());
        $('.index4 #topmenu').width($('#columns').width());
        $('.index5 #topmenu').width($('#columns').width());
        $('.index6 #topmenu').width($('#columns').width());    
        $('.index7 #topmenu').width($('#columns').width());
    }
});
$(document).ready(function(){
    //div_full_width();
    full_height();
    div_full_width_index();
	highdpiInit();
	responsiveResize();
	$(window).resize(responsiveResize);
    subMenuType();
    
/***** Leka JS ***** ***** ***** ***** ***** ***** *****/
    
    /* LEka: Header currency */
    $('#setCurrency').hover(function(){
        $(this).parent().find('.dropdown-menu-curr').show('slow');
    },function(){
        $('.dropdown-menu-curr').hide();
    });
    
    /* Leka: Block Cart, Search position */
    $('#blockcart_wrap').insertAfter('#nav-ownmenu #ownmenu');
    $('#search-top').insertAfter('#blockcart_wrap');
    
    /* Leka: Brandslider */
    $('.our-clients .client-slide').owlCarousel({
        loop:true,
        margin:30,
        nav:true,
        responsive:{
            0:{
                items:2
            },
            768:{
                items:3
            },
            1200:{
                items:4
            }
        }
    });
    
    /* Leka: Testimonial */
    $('.about-sebian .clients-about-slider').owlCarousel({
        loop:false,
        margin:30,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            992:{
                items:3
            }
        }
    });
    
    /* Leka: Collection */
    $('.section-collection .slide-collection').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:2
            }
        }
    });
    
     /* Leka: Serach index4 */ 
     $('.index4 #form-search').appendTo('.header-sidebar .cart-search');
     $('.leka-testimonial-3').each(function(){
              var owl = $(this).find('.client-carousel');
              owl.owlCarousel(
                {
                    margin:20,
                    autoplay:true,
                    dots:false,
                    loop:true,
                    items:1,
                    nav:false,
                    smartSpeed:1000
                }
              );
            });
     
    /* Leka: Product Tabs */
    $('ul.nav.nav-tabs li:first').addClass('active');
    $('.tab-content .tab-pane:first-child').addClass('active in');
    
    /* Leka: Menu page-template-sidebar */
    $(document).on('click','.page-template-sidebar #main-menu li.level-1.dropdown',function(){
        $(this).toggleClass('open-sub');
        $(this).find('.dropdown-menu').toggle();
    })
    /* Logo index7 */
    $('#index.index7 .logo-index').insertAfter('#index .main-header .sticky_content .logo-not-index');
    $('#index.index7 .main-header .sticky_content .logo-not-index').hide();
    /* Leka: Menu mobile */
    if($(window).width() <= 767){
        var w2 = $(window).width()-30;
        $('.index1 #topmenu').width($('#columns').width());
        $('.index2 #topmenu').width(w2);
        $('.index3 #topmenu').width($('#columns').width());
        $('.index4 #topmenu').width($('#columns').width());
        $('.index5 #topmenu').width($('#columns').width());
        $('.index6 #topmenu').width($('#columns').width());    
    }
    $(document).on('click','.mobile-navigation',function(){
        $('#topmenu').toggle();
        return false;
    })
    /* Parallax page */
    $('.paralax_page').html($('.get_breadcrumb').html());
    $('.get_breadcrumb').html('').hide();
    
	if (navigator.userAgent.match(/Android/i))
	{
		var viewport = document.querySelector('meta[name="viewport"]');
		viewport.setAttribute('content', 'initial-scale=1.0,maximum-scale=1.0,user-scalable=0,width=device-width,height=device-height');
		window.scrollTo(0, 1);
	}
	if (typeof quickView !== 'undefined' && quickView)
		quick_view();
	dropDown();

	if (typeof page_name != 'undefined' && !in_array(page_name, ['index', 'product']))
	{
		//bindGrid();
        bindGridOvic();

 		$(document).on('change', '.selectProductSort', function(e){
			if (typeof request != 'undefined' && request)
				var requestSortProducts = request;
 			var splitData = $(this).val().split(':');
 			var url = '';
			if (typeof requestSortProducts != 'undefined' && requestSortProducts)
			{
				url += requestSortProducts ;
				if (typeof splitData[0] !== 'undefined' && splitData[0])
				{
					url += ( requestSortProducts.indexOf('?') < 0 ? '?' : '&') + 'orderby=' + splitData[0] + (splitData[1] ? '&orderway=' + splitData[1] : '');
					if (typeof splitData[1] !== 'undefined' && splitData[1])
						url += '&orderway=' + splitData[1];
				}
				document.location.href = url;
			}
    	});

		$(document).on('change', 'select[name="n"]', function(){
			$(this.form).submit();
		});

		$(document).on('change', 'select[name="currency_payment"]', function(){
			setCurrency($(this).val());
		});
	}

	$(document).on('change', 'select[name="manufacturer_list"], select[name="supplier_list"]', function(){
		if (this.value != '')
			location.href = this.value;
	});

	$(document).on('click', '.back', function(e){
		e.preventDefault();
		history.back();
	});

	jQuery.curCSS = jQuery.css;
	if (!!$.prototype.cluetip)
		$('a.cluetip').cluetip({
			local:true,
			cursor: 'pointer',
			dropShadow: false,
			dropShadowSteps: 0,
			showTitle: false,
			tracking: true,
			sticky: false,
			mouseOutClose: true,
			fx: {
		    	open:       'fadeIn',
		    	openSpeed:  'fast'
			}
		}).css('opacity', 0.8);

	if (!!$.prototype.fancybox)
		$.extend($.fancybox.defaults.tpl, {
			closeBtn : '<a title="' + FancyboxI18nClose + '" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next     : '<a title="' + FancyboxI18nNext + '" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev     : '<a title="' + FancyboxI18nPrev + '" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
		});

	// Close Alert messages
	$(".alert.alert-danger").on('click', this, function(e){
		if (e.offsetX >= 16 && e.offsetX <= 39 && e.offsetY >= 16 && e.offsetY <= 34)
			$(this).fadeOut();
	});
    
    // Backtotop / dev28815
     $('#backtotop').click(function(){
      $("html, body").animate({ scrollTop: 0 }, 600);
       return false;
     });
     $(window).scroll(function(){
          if ($(this).scrollTop() > 500) {
           $('#backtotop').fadeIn();
          } else {
           $('#backtotop').fadeOut();
          }
     });
	$('#menu_sticky').sticky({topSpacing: 0});
	$('#menu_sticky').on('sticky-start', function () {
		if ((($(window).width() + scrollCompensate()) >= 768) && !($('#menu_sticky').hasClass('add_content'))) {
			if ($('#menu_sticky .sticky_content').length == 0){
				var sticky_content = $( "header > .main-header .sticky_content" ).clone();
				if (sticky_content.find('#form-search').length > 0)
					sticky_content.find('#form-search').remove();
				sticky_content.appendTo('#menu_sticky .sticky_container');
			}
			if ($('#menu_sticky #form-search').length == 0){
				$('#form-search').insertBefore('#menu_sticky .sticky_content');
			}
			$('#menu_sticky').addClass('add_content');
		}
	});
	
	$('#menu_sticky').on('sticky-end', function () {
		if ((($(window).width() + scrollCompensate()) >= 768) && ($('#menu_sticky').hasClass('add_content'))) {
			if ($('header > .main-header > .main-header-inner > #form-search').length == 0)
				$('#form-search').prependTo('header > .main-header .main-header-inner');
			
			$('#menu_sticky').removeClass('add_content');
		}
	});
});
function highdpiInit()
{
	if($('.replace-2x').css('font-size') == "1px")
	{
		var els = $("img.replace-2x").get();
		for(var i = 0; i < els.length; i++)
		{
			src = els[i].src;
			extension = src.substr( (src.lastIndexOf('.') +1) );
			src = src.replace("." + extension, "2x." + extension);

			var img = new Image();
			img.src = src;
			img.height != 0 ? els[i].src = src : els[i].src = els[i].src;
		}
	}
}


// Used to compensante Chrome/Safari bug (they don't care about scroll bar for width)
function scrollCompensate()
{
    var inner = document.createElement('p');
    inner.style.width = "100%";
    inner.style.height = "200px";

    var outer = document.createElement('div');
    outer.style.position = "absolute";
    outer.style.top = "0px";
    outer.style.left = "0px";
    outer.style.visibility = "hidden";
    outer.style.width = "200px";
    outer.style.height = "150px";
    outer.style.overflow = "hidden";
    outer.appendChild(inner);

    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2) w2 = outer.clientWidth;

    document.body.removeChild(outer);

    return (w1 - w2);
}

function responsiveResize()
{
	compensante = scrollCompensate();
	if (($(window).width()+scrollCompensate()) <= 767 && responsiveflag == false)
	{
		accordion('enable');
	    accordionFooter('enable');
		responsiveflag = true;
	}
	else if (($(window).width()+scrollCompensate()) >= 768)
	{
		accordion('disable');
		accordionFooter('disable');
	    responsiveflag = false;
	}
	//blockHover();
    blockHoverOvic();
}

function blockHover(status)
{
	var screenLg = $('body').find('.container').width() == 1170;

	if ($('.product_list').is('.grid'))
		if (screenLg)
			$('.product_list .button-container').hide();
		else
			$('.product_list .button-container').show();

	$(document).off('mouseenter').on('mouseenter', '.product_list.grid li.ajax_block_product .product-container', function(e){
		if (screenLg)
		{
			var pcHeight = $(this).parent().outerHeight();
			var pcPHeight = $(this).parent().find('.button-container').outerHeight() + $(this).parent().find('.comments_note').outerHeight() + $(this).parent().find('.functional-buttons').outerHeight();
			$(this).parent().addClass('hovered').css({'height':pcHeight + pcPHeight, 'margin-bottom':pcPHeight * (-1)});
			$(this).find('.button-container').show();
		}
	});

	$(document).off('mouseleave').on('mouseleave', '.product_list.grid li.ajax_block_product .product-container', function(e){
		if (screenLg)
		{
			$(this).parent().removeClass('hovered').css({'height':'auto', 'margin-bottom':'0'});
			$(this).find('.button-container').hide();
		}
	});
}
function blockHoverOvic(status)
{
	var screenLg = $('body').find('.container').width() == 1170;

	$(document).off('mouseenter').on('mouseenter', '.product_list.grid li.ajax_block_product .product-container', function(e){
		if (screenLg)
		{ 
			var pcHeight = $(this).parent().outerHeight();
			var pcPHeight = $(this).parent().find('.button-container').outerHeight() + $(this).parent().find('.comments_note').outerHeight() + $(this).parent().find('.functional-buttons').outerHeight();
			$(this).parent().addClass('hovered');
		}
	});

	$(document).off('mouseleave').on('mouseleave', '.product_list.grid li.ajax_block_product .product-container', function(e){
		if (screenLg)
		{
			$(this).parent().removeClass('hovered');
		}
	});
}

function quick_view()
{
	$(document).on('click', '.quick-view:visible, .quick-view-mobile:visible', function(e){
		e.preventDefault();
		//var url = this.rel;
        var url = $(this).attr('data-rel');
		var anchor = '';

		if (url.indexOf('#') != -1)
		{
			anchor = url.substring(url.indexOf('#'), url.length);
			url = url.substring(0, url.indexOf('#'));
		}

		if (url.indexOf('?') != -1)
			url += '&';
		else
			url += '?';

		if (!!$.prototype.fancybox)
			$.fancybox({
				'padding':  0,
				'width':    1087,
				'height':   610,
				'type':     'iframe',
				'href':     url + 'content_only=1' + anchor
			});
	});
}

function bindGrid()
{
	var view = $.totalStorage('display');

	if (!view && (typeof displayList != 'undefined') && displayList)
		view = 'list';

	if (view && view != 'grid')
		display(view);
	else
		$('.display').find('li#grid').addClass('selected');

	$(document).on('click', '#grid', function(e){
		e.preventDefault();
		display('grid');
	});

	$(document).on('click', '#list', function(e){
		e.preventDefault();
		display('list');
	});
}
function bindGridOvic()
{ 
	var view = $.totalStorage('display');

	if (!view && (typeof displayList != 'undefined') && displayList)
		view = 'list';

	if (view && view != 'grid'){ 
       display(view);
	}else{ 
       $('.display').find('#grid').addClass('selected');
	}

	$(document).on('click', '#grid', function(e){
		e.preventDefault();
		display('grid');
	});

	$(document).on('click', '#list', function(e){
		e.preventDefault();
		display('list');
	});
}

function display(view)
{
	if (view == 'list')
	{
        $('#grid').removeClass('selected');
        $('#list').addClass('selected');
		//$('ul.product_list').parent().addClass('list-style');
        $('ul.product_list').removeClass('grid').addClass('list products-list-view');
		$('.product_list > li').removeClass('col-xs-12 col-sm-6 col-md-4').addClass('col-xs-12');
		$('.product_list > li').each(function(index, element) {
			html = '';
			html = '<div class="row">';
				html += '<div class="col-sm-4"><div class="product-thumb">' + $(element).find('.product-thumb').html() + '</div></div>';
				html += '<div class="col-sm-8">';
                    html += '<div class="product-info">';
                        html += '<h3>' + $(element).find('.product-name').html() + '</h3>';
                        var price = $(element).find('.content_price').html();       // check : catalog mode is enabled
						if (price != null) {
							html += '<div class="content_price">'+ price + '</div>';
						}
                        var rating = $(element).find('.hook-reviews').html(); // check : rating
    					if (rating != null) {
    						html += '<div class="hook-reviews hidden">'+ rating + '</div>';
    					}
                        html += '<div class="product-desc">' + $(element).find('.product-desc').html() + '</div>';
    					html += '<div class="product-button">'; 
                            html += $(element).find('.button-container').html();
                            html += $(element).find('.product-button').html();
                        html += '</div>'
                        
                        html += '<div class="product-info-ss hidden">' + $(element).find('.product-info').html() + '</div>';
                    
						//html += $(element).find('.product-name').parent()[0].outerHTML;
                        //html += $('<div>').append($(element).find('.product-name').clone()).html();
    					/*
                        var colorList = $(element).find('.color-list-container').html();
    					if (colorList != null) {
    						html += '<div class="color-list-container">'+ colorList +'</div>';
    					}
    					*/
				    html += '</div>';
                html += '</div>';
			html += '</div>';
		$(element).html(html);
		});
		$('.display').find('li#list').addClass('selected');
		$('.display').find('li#grid').removeAttr('class');
		$.totalStorage('display', 'list');
	}
	else
	{
	    $('#grid').addClass('selected');
        $('#list').removeClass('selected');
	    //$('ul.product_list').parent().removeClass('products-list-view');
		$('ul.product_list').removeClass('list products-list-view').addClass('grid');
		$('.product_list > li').removeClass('col-xs-12').addClass('col-xs-12 col-sm-6 col-md-4');
		$('.product_list > li').each(function(index, element) {
		html = '';
		html += '<div class="product-container">';
			html += '<div class="product-thumb">'+ $(element).find('.product-thumb').html() + '</div>';
            html += '<div class="product-info">' + $(element).find('.product-info-ss').html() + '</div>';
		html += '</div>';
        
		$(element).html(html);
		});
		$('.display').find('li#grid').addClass('selected');
		$('.display').find('li#list').removeAttr('class');
		$.totalStorage('display', 'grid');
	}
}

function dropDown()
{
	elementClick = '#header .current';
	elementSlide =  'ul.toogle_content';
	activeClass = 'active';

	$(elementClick).on('click', function(e){
		e.stopPropagation();
		var subUl = $(this).next(elementSlide);
		if(subUl.is(':hidden'))
		{
			subUl.slideDown();
			$(this).addClass(activeClass);
		}
		else
		{
			subUl.slideUp();
			$(this).removeClass(activeClass);
		}
		$(elementClick).not(this).next(elementSlide).slideUp();
		$(elementClick).not(this).removeClass(activeClass);
		e.preventDefault();
	});

	$(elementSlide).on('click', function(e){
		e.stopPropagation();
	});

	$(document).on('click', function(e){
		e.stopPropagation();
		var elementHide = $(elementClick).next(elementSlide);
		$(elementHide).slideUp();
		$(elementClick).removeClass('active');
	});
}

function accordionFooter(status)
{
	if(status == 'enable')
	{
		$('#footer .footer-block h4').on('click', function(){
			$(this).toggleClass('active').parent().find('.toggle-footer').stop().slideToggle('medium');
		})
		$('#footer').addClass('accordion').find('.toggle-footer').slideUp('fast');
	}
	else
	{
		$('.footer-block h4').removeClass('active').off().parent().find('.toggle-footer').removeAttr('style').slideDown('fast');
		$('#footer').removeClass('accordion');
	}
}

function accordion(status)
{
	leftColumnBlocks = $('#left_column');
	if(status == 'enable')
	{
		var accordion_selector = '#right_column .block .title_block, #left_column .block .title_block, #left_column #newsletter_block_left h4,' +
								'#left_column .shopping_cart > a:first-child, #right_column .shopping_cart > a:first-child';

		$(accordion_selector).on('click', function(e){
			$(this).toggleClass('active').parent().find('.block_content').stop().slideToggle('medium');
		});
		$('#right_column, #left_column').addClass('accordion').find('.block .block_content').slideUp('fast');
		if (typeof(ajaxCart) !== 'undefined')
			ajaxCart.collapse();
	}
	else
	{
		$('#right_column .block .title_block, #left_column .block .title_block, #left_column #newsletter_block_left h4').removeClass('active').off().parent().find('.block_content').removeAttr('style').slideDown('fast');
		$('#left_column, #right_column').removeClass('accordion');
	}
}
function div_full_width(){
    var $ = jQuery;
    var contn_width = 1170;
    var window_width = $('#index').width();//$(window).width();
    
    // set container width
    if( window_width >= 1200 ) {
        contn_width = 1170;
    }else if( window_width >= 992 ){
        contn_width = 970;
    }else if( window_width >= 768 ){
        contn_width = 750;
    }else{
        contn_width = window_width;
    }
    
    $('.div_full_width').each(function(){
        $(this).css({
            //'margin-left': - ($(window).width() - $('#columns').width())/2,
            //'width': $(window).width()
            'margin-left': - (window_width - contn_width)/2,
            'width': window_width
        });
    });
}


function div_full_width_index(){
    var $ = jQuery;
    var contn_width = 1170;
    var window_width = $(window).width();
    var flag_mobile = false;
    //$('.div_full_width').each(function(){
//        var offset_left = $(this).offset().left;
//        alert(offset_left);
//        $(this).css({ 
//            'margin-left': - offset_left + 'px',//(window_width - contn_width)/2,
//            'width': window_width + 'px'
//        });
//    }); 
    
    // set container width
    if( window_width >= 1200 ) {
        contn_width = 1170;
        flag_mobile = false;
    }else if( window_width >= 992 ){
        contn_width = 940;
        flag_mobile = false;
    }else if( window_width >= 768 ){
        contn_width = 720;
        flag_mobile = false;
    }else{
        contn_width = $('#columns').width();
        flag_mobile = true;
    }
    if (flag_mobile == false) {
        $('.div_full_width').each(function(){
            var offset_left = $(this).offset().left;
            $(this).css({ 
                'margin-left': - (window_width - contn_width)/2,
                'width': window_width
            });
        });    
    }else {
        $('.div_full_width').each(function(){
            if ($(this).parent().width() < window_width) {
               $(this).css({ 
                    'margin-left': - (window_width - contn_width)/2
               }); 
            }else {
               $(this).css({ 
                    'margin-left': 0
               }); 
            }
            
            $(this).css({
                'width': window_width
            });
        }); 
    }
    
}

function full_height() {
    var $ = jQuery; 
    var window_height = $(window).height(); 
    $('.home-6-style .full-screen.fashion-full').each(function(){
        $(this).css('height', window_height+'px');
    });
}
function subMenuType () { 
    var header_style_flag = true;
    if ($('header').hasClass('header-style2') || $('header').hasClass('header-style3')) {
        header_style_flag = true;
    }else{
        header_style_flag = false;
    }
    $('#topmenu li.level-1 > ul.dropdown-menu').each(function(){
        if ($(this).hasClass('sub-menu-center')) { 
            $(this).parent().css('position','static'); //li.level-1
            $(this).parent().parent().css('position','static'); //ul.navbar-nav
            $(this).parent().parent().parent().css('position','static'); //#topmenu
            $(this).parent().parent().parent().parent().css('position','static'); //nav_topmenu 
            if (header_style_flag == true){
                $(this).parent().parent().parent().parent().parent().css('position','static');// .main-menu-wapper    
            }else {
                $(this).parent().parent().parent().parent().parent().css('position','relative');// .main-menu-wapper    
            }
        }
    })
    
    
}