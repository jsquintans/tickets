{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{assign var='option_tpl' value=OvicLayoutControl::getTemplateFile('footer.tpl')}
{if  $option_tpl!== null}
    {include file=$option_tpl}
{else}
    {if !isset($content_only) || !$content_only}
    					</div><!-- #center_column -->
    					{if isset($right_column_size) && !empty($right_column_size)}
    						<div id="right_column" class="side-bar col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
    					{/if}
    					</div><!-- .row -->
                        {if $page_name =='index'}
                            {if isset($HOME_BOTTOM_COLUMN) && $HOME_BOTTOM_COLUMN|trim}
            				      <div id="home_bottom_column" class="home_bottom_column">{$HOME_BOTTOM_COLUMN}</div>
                            {/if}
            			{/if}
    				</div><!-- #columns --> 
    			</div><!-- .columns-container -->
                
                {if isset($BOTTOM_COLUMN) && $BOTTOM_COLUMN|trim}
    			      <div id="bottom_column" class="bottom_column">{$BOTTOM_COLUMN}</div>
                {/if}
    			{if isset($HOOK_FOOTER)}
    				<!-- Footer --> 
                    <style type="text/css" media="screen">
                           .bloque_news{
                                position: fixed;
                                top: 30%;
                                left: 40%;
                                background: #fff;
                                padding: 30px;
                                border: 1px solid #000;
                                z-index: 999;
                                min-width: 380px;
                                -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
                                -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
                                box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
                           }

                           .bloque_news .newsletter-input-texto{
                                color: #000;
                           } 

                           .bloque_news button[type='submit']{
                                margin-top: 10px;
                                font-size: 12px;
                                padding-top: 5px;
                                padding-bottom: 5px;
                           }

                           .bloque_news img{
                                max-width: 340px;
                                display: block;
                                margin-left: auto;
                                margin-right: auto;
                                margin-bottom: 30px;
                           }
                    </style>
                    <div class='bloque_news' style='display:none;'>
                    <form action="//vl52555.dns-privadas.es/tickets/" method="post" _lpchecked="1">
                        <div class=''><img src='http://vl52555.dns-privadas.es/tickets/img/desarrollo-esterea-logo-1481106810.jpg' class='img-responsive' /></div>
                        <div class='form-group'>
                            <p class="newsletter-input-texto">Enter your email</p>
                            <input type='text' class='inputNew form-control newaletter-email2 newsletter-input2' id='newsletter-input2' />
                            <div class='response_check'></div>
                            <button type="submit" name="submitNewsletter2" class="newaletter-button2">Sign up</button>
                        </div>
                    </form>
                    </div>
                    {literal}
                       <script type="text/javascript" charset="utf-8">
                        
                        function validateEmail(email) {
                          var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                          return re.test(email);
                        }

                        jQuery(document).ready(function($) {



                            $(".btn_registrate_news").click(function(event) {
                                event.preventDefault();
                                $('.bloque_news').css({
                                    display: 'block'
                                });
                            });

                            $(".newaletter-button2").click(function(event) {
                                 $('.response_check').html("<p style='color:red;'></p>");
                                event.preventDefault();
                                email = $(".newaletter-email2").val();
                                console.log(email);
                                if(email != '')
                                 if (validateEmail(email)) {
                                    $(".newaletter-button2").prop( "disabled", true );
                                    $(".newaletter-email").val(email);
                                    $(".newaletter-button2").css('background-color','#c1cad0');
                                    $(".newaletter-button2").css('border-color','#c1cad0');
                                    $( ".newaletter-button" ).trigger( "click" );
                                 }else{

                                    //$(".newaletter-email2").css("color", "red");
                                    $('.response_check').html("<p style='color:red;'>The email entered is invalid</p>");

                                 }else{
                                    //$(".newaletter-email2").css("color", "red");
                                    $('.response_check').html("<p style='color:red;'>You have to enter an email</p>");

                                 }

                            });

                            $(document).mouseup(function (e)
                            {
                                var container = $(".bloque_news");

                                if (!container.is(e.target) // if the target of the click isn't the container...
                                    && container.has(e.target).length === 0) // ... nor a descendant of the container
                                {
                                    container.hide();
                                }
                            });
                        });
                    </script>
                    {/literal}
					<footer class="footer">
                        <div class="container"> {$HOOK_FOOTER}</div>
					</footer>
                    <a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">{l s='Scroll'}</a> 
                    <!-- #footer -->
    			{/if}
    		</div><!-- #page -->
    {/if}
    {include file="$tpl_dir./global.tpl"}
    	</body>
    </html>
{/if}