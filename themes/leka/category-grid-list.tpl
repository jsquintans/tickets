{if isset($orderby) AND isset($orderway)}
    <div class="display-product-option grid-list">
        <a id="grid" rel="nofollow" class="view-as-grid selected" href="javascript:void(0);" title="{l s='Grid'}"><i class="icon-th-large"></i></a>
        <a id="list" rel="nofollow" class="view-as-list" href="javascript:void(0);" title="{l s='List'}"><i class="icon-th-list"></i></a>
    </div>
{/if}